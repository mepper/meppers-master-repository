#######################################
#                                     #
#         FantasyNations.txt          #
#                                     #
#######################################
#
#######################################
#
# List of decisions :
#
# anglofrench_nation
# unite_turkish_republic
# gupta_nation
# jagiellon_emp
# occitania_nation
# celtic_nation
# almohad_nation
# north_sea_nation
# anglosaxon_nation
# restore_latine_empire
# form_ilkhanate
# illyria_nation
# arelate_nation
#
########################################

country_decisions = {
	
	unite_turkish_nation = {
		major = yes
		potential = {
			# has_global_flag = additional_tags_enabled
			OR = {
				primary_culture = turkish
				primary_culture = yorouk
			}
			NOT = { exists = TUR }
			forming_TUR_trigger = yes
			NOT = { tag = BYZ }
			is_colonial_nation = no
		}
		allow = {
			primary_culture = turkish
			is_free_or_tributary_trigger = yes
			OR = {
				check_variable = {   which = "Demesne_in_Anatolia"  value = 25 }
				check_variable = {   which = "Cores_on_Anatolia"  value = 15 }
			}
			capital = 1402
			is_at_war = no
		}
		effect = {
			
			north_anatolia_region = { limit = { owned_by = ROOT } remove_core = TUR add_core = TUR }
			north_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUR }
			
			south_anatolia_region = { limit = { owned_by = ROOT } remove_core = TUR add_core = TUR }
			south_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUR }
			
			greece_region = { limit = { owned_by = ROOT } remove_core = TUR add_core = TUR }
			greece_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUR }
			
			east_balkan_region = { limit = { owned_by = ROOT } remove_core = TUR add_core = TUR }
			east_balkan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUR }
			
			albania_area = { limit = { owned_by = ROOT } remove_core = TUR add_core = TUR }
			albania_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUR }
			
			add_prestige = 20
			if = {
				limit = {
					government = monarchy
					NOT = { has_country_modifier = title_6 }
				}
				change_title_6 = yes
			}
			
			if = {
				limit = {
					tag = OTT
					ai = yes
				}
				clr_country_flag = great_predator_plan_set
				
				every_province = {
					limit = {
						has_province_flag = conquest_expansion_target_OTT
					}
					clr_province_flag = conquest_expansion_target_OTT
				}
				
				set_country_flag = great_predator
				clr_country_flag = great_predator_plan_set
			}
			
			change_tag = TUR
			change_unit_type = turkishtech
			add_absolutism = 10
			
			if = {
				limit = {
					technology_group = muslim
				}
				change_technology_group = turkishtech
			}
			if = {
				limit = {
					has_country_modifier = turkish_beylik
				}
				remove_country_modifier = turkish_beylik
			}
			remove_country_modifier = obstacle_traditional_military
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = TUR_ideas }
				}
				swap_national_ideas_effect = yes
			}
			
			every_country = {
				limit = {
					OR = {
						primary_culture = turkish
						capital_scope = { region = north_anatolia_region }
						capital_scope = { region = south_anatolia_region }
						tag = MAM
					}
					NOT = { tag = ROOT }
				}
				add_historical_rival = TUR
				ROOT = { add_historical_rival = PREV }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	anglofrench_nation = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			NOT = { exists = UEF }
			NOT = { exists = GBR }
			NOT = { tag = NSE }
			NOT = { tag = ERG }
			NOT = { tag = PAP }
			NOT = { tag = ROM }
			OR = {
				AND = {
					tag = ENG
					NOT = { exists = FRA }
				}
				AND = {
					tag = FRA
					NOT = { exists = ENG }
				}
				AND = {
					OR = {
						culture_group = langue_d_oil
						culture_group = langue_d_oc
						primary_culture = english
					}
					NOT = { tag = FRA }
					NOT = { tag = ENG }
					NOT = { exists = FRA }
					NOT = { exists = ENG }
				}
			}
			check_variable = { which = "Demesne_in_England" value = 1 }
			check_variable = { which = "Demesne_in_France" value = 1 }
			is_colonial_nation = no
		}
		allow = {
			is_free_or_tributary_trigger = yes
			OR = {
				check_variable = { which = "Demesne_in_England" value = 16 }
				check_variable = { which = "Cores_on_England" value = 12 }
			}
			OR = {
				custom_trigger_tooltip = {
					check_variable = { which = "Demesne_in_France" value = 24 }
					tooltip = anglofrench_nation_2_claim
				}
				custom_trigger_tooltip = {
					check_variable = { which = "Cores_on_France" value = 18 }
					tooltip = anglofrench_nation_2_core
				}
			}
			owns = 236		# London
			owns = 183		# Paris
			is_at_war = no
		}
		effect = {
			north_england_region = { limit = { owned_by = ROOT } remove_core = UEF add_core = UEF }
			north_england_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = UEF }
			south_england_region = { limit = { owned_by = ROOT } remove_core = UEF add_core = UEF }
			south_england_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = UEF }
			east_france_region = { limit = { owned_by = ROOT } remove_core = UEF add_core = UEF }
			east_france_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = UEF }
			ouest_france_region = { limit = { owned_by = ROOT } remove_core = UEF add_core = UEF }
			ouest_france_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = UEF }
			aquitaine_region = { limit = { owned_by = ROOT } remove_core = UEF add_core = UEF }
			aquitaine_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = UEF }
			clr_global_flag = hundred_years_war
			add_prestige = 20
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			if = {
				limit = {
					OR = {
						ENG = { vassal_of = ROOT }
						ENG = { junior_union_with = ROOT }
					}
				}
				inherit = ENG
				set_global_flag = hundred_years_war_FRA_win
			}
			if = {
				limit = {
					OR = {
						FRA = { vassal_of = ROOT }
						FRA = { junior_union_with = ROOT }
					}
				}
				inherit = FRA
				set_global_flag = hundred_years_war_ENG_win
			}
			add_absolutism = 10
			change_tag = UEF
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = UEF_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	unite_turkish_republic = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			OR = {
				primary_culture = turkish
				primary_culture = yorouk
			}
			government = republic
			NOT = { exists = TUR }
			NOT = { exists = TUY }
			is_colonial_nation = no
		}
		allow = {
			primary_culture = turkish
			is_free_or_tributary_trigger = yes
			OR = {
				check_variable = {   which = "Demesne_in_Anatolia"  value = 25 }
				check_variable = {   which = "Cores_on_Anatolia"  value = 15 }
			}
			is_at_war = no
		}
		effect = {
			north_anatolia_region = { limit = { owned_by = ROOT } remove_core = TUY add_core = TUY }
			north_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUY }
			south_anatolia_region = { limit = { owned_by = ROOT } remove_core = TUY add_core = TUY }
			south_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = TUY }
			add_prestige = 20
			add_absolutism = 10
			change_tag = TUY
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = TUR_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	gupta_nation = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			NOT = { exists = GUP }
			religion_group = dharmic
			OR = {
				culture_group = eastern_aryan
				culture_group = hindusthani
				culture_group = central_indian
				culture_group = pahari_group
				culture_group = deccan_group
				culture_group = rajput
				culture_group = dravidian
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Indus" value = 25 }
				check_variable = { which = "Cores_on_Indus" value = 15 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Rajputana" value = 35 }
				check_variable = { which = "Cores_on_Rajputana" value = 25 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Doab" value = 20 }
				check_variable = { which = "Cores_on_Doab" value = 10 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Awadh" value = 15 }
				check_variable = { which = "Cores_on_Awadh" value = 10 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Bihar" value = 10 }
				check_variable = { which = "Cores_on_Bihar" value = 7 }
			}
			OR = {
				check_variable = { which = "Demesne_in_East_Bengal" value = 15 }
				check_variable = { which = "Cores_on_East_Bengal" value = 10 }
			}
			OR = {
				check_variable = { which = "Demesne_in_West_Bengal" value = 20 }
				check_variable = { which = "Cores_on_West_Bengal" value = 10 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Central_India" value = 35 }
				check_variable = { which = "Cores_on_Central_India" value = 25 }
			}
			OR = {
				check_variable = { which = "Demesne_in_Deccan" value = 35 }
				check_variable = { which = "Cores_on_Decan" value = 25 }
			}
			owns = 516   #Tonk (Ahmadabad)
			owns = 559   #Kukara (Patna)
		}
		effect = {
			north_india_superregion = { limit = { owned_by = ROOT } remove_core = GUP add_core = GUP }
			north_india_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GUP }
			west_india_superregion = { limit = { owned_by = ROOT } remove_core = GUP add_core = GUP }
			west_india_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GUP }
			rajputana_superregion = { limit = { owned_by = ROOT } remove_core = GUP add_core = GUP }
			rajputana_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GUP }
			central_india_superregion = { limit = { owned_by = ROOT } remove_core = GUP add_core = GUP }
			central_india_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GUP }
			east_india_superregion = { limit = { owned_by = ROOT } remove_core = GUP add_core = GUP }
			east_india_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GUP }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			change_tag = GUP
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = GUP_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	jagiellon_emp = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			NOT = { exists = JGL }
			OR = {
				AND = {
					tag = POL
					NOT = { exists = LIT }
					NOT = { exists = BOH }
					NOT = { exists = HUN }
					NOT = { exists = PLC }
				}
				AND = {
					tag = LIT
					NOT = { exists = POL }
					NOT = { exists = BOH }
					NOT = { exists = HUN }
					NOT = { exists = PLC }
				}
				AND = {
					tag = BOH
					NOT = { exists = LIT }
					NOT = { exists = POL }
					NOT = { exists = HUN }
					NOT = { exists = PLC }
				}
				AND = {
					tag = HUN
					NOT = { exists = LIT }
					NOT = { exists = BOH }
					NOT = { exists = POL }
					NOT = { exists = PLC }
				}
				AND = {
					tag = PLC
					NOT = { exists = LIT }
					NOT = { exists = BOH }
					NOT = { exists = HUN }
					NOT = { exists = POL }
				}
			}
		}
		allow = {
			is_at_war = no
			is_free_or_tributary_trigger = yes
			OR = {
				AND = {
					check_variable = { which = "Demesne_in_Poland" value = 16 }
					check_variable = { which = "Demesne_in_Lithuania" value = 20 }
					check_variable = { which = "Demesne_in_Bohemia" value = 5 }
					check_variable = { which = "Demesne_in_Hungary" value = 15 }
				}
				AND = {
					check_variable = { which = "Cores_on_Poland" value = 12 }
					check_variable = { which = "Cores_on_Lithuania" value = 15 }
					check_variable = { which = "Cores_on_Bohemia" value = 3 }
					check_variable = { which = "Cores_on_Hungary" value = 10 }
				}
			}
		}
		effect = {
			baltic_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			baltic_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			polonia_major_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			polonia_major_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			polonia_minor_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			polonia_minor_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			lithuania_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			lithuania_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			ruthenia_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			ruthenia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			magyar_plains_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			magyar_plains_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			north_carpathia_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			north_carpathia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			bohemia_region = { limit = { owned_by = ROOT } remove_core = JGL add_core = JGL }
			bohemia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JGL }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = JGL
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	occitania_nation = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			culture_group = langue_d_oc
			NOT = { primary_culture = provencal }
			NOT = { primary_culture = arpitan }
			NOT = { primary_culture = catalan }
			NOT = { exists = OCC }
			NOT = { tag = UEF }
			NOT = { tag = FRA }
			NOT = { tag = ENG }
			NOT = { tag = ROM }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Occitania" value = 20 }
				check_variable = { which = "Cores_on_Occitania" value = 15 }
			}
			owns = 196 # Tolzan
			is_core = 196
		}
		effect = {
			languedoc_region = { limit = { owned_by = ROOT } remove_core = OCC add_core = OCC }
			languedoc_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = OCC }
			provence_region = { limit = { owned_by = ROOT } remove_core = OCC add_core = OCC }
			provence_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = OCC }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			change_tag = OCC
			langue_d_oc_union_effect = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = OCC_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	celtic_nation = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			OR = {
				culture_group = gaelic
				culture_group = brythonic
			}
			NOT = { exists = CEL }
			NOT = { tag = GBR }
			NOT = { tag = FRA }
			NOT = { tag = ROM }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				AND = {
					check_variable = { which = "Demesne_in_Brittany" value = 4 }
					check_variable = { which = "Demesne_in_Wales" value = 2 }
					check_variable = { which = "Demesne_in_Scotland" value = 11 }
					check_variable = { which = "Demesne_in_Ireland" value = 10 }
				}
				AND = {
					check_variable = { which = "Cores_on_Brittany" value = 3 }
					check_variable = { which = "Cores_on_Wales" value = 2 }
					check_variable = { which = "Cores_on_Scotland" value = 8 }
					check_variable = { which = "Cores_on_Ireland" value = 8 }
				}
			}
		}
		effect = {
			brittany_area = { limit = { owned_by = ROOT } remove_core = CEL add_core = CEL }
			brittany_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CEL }
			scotland_region = { limit = { owned_by = ROOT } remove_core = CEL add_core = CEL }
			scotland_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CEL }
			ireland_region = { limit = { owned_by = ROOT } remove_core = CEL add_core = CEL }
			ireland_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = CEL }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			change_tag = CEL
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = CEL_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	north_sea_nation = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			NOT = { exists = NSE }
			NOT = { tag = GBR }
			NOT = { tag = UEF }
			NOT = { tag = KAL }
			NOT = { tag = ROM }
			OR = {
				primary_culture = danish
				primary_culture = norwegian
				primary_culture = english
			}
			religion_group = christian
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				AND = {
					check_variable = { which = "Demesne_in_Norway" value = 15 }
					check_variable = { which = "Demesne_in_Denmark" value = 12 }
					check_variable = { which = "Demesne_in_England" value = 15 }
				}
				AND = {
					check_variable = { which = "Cores_on_Norway" value = 10 }
					check_variable = { which = "Cores_on_Denmark" value = 8 }
					check_variable = { which = "Cores_on_England" value = 10 }
				}
			}
			owns = 12	# Copenhagen
			owns = 16	# Oslo
			owns = 236	# London
		}
		effect = {
			east_anglia_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			east_anglia_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			midlands_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			midlands_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			northumbria_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			northumbria_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			highlands_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			highlands_area = { limit = { NOT = { owned_by = ROOT } } remove_core = NSE add_permanent_claim = NSE }
			northern_norway_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			northern_norway_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			western_norway_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			western_norway_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			subarctic_islands_area = { limit = { owned_by = ROOT } remove_core = NSE add_core = NSE }
			subarctic_islands_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = NSE }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = NSE
			nord_germanic_union_effect = yes
			british_union_effect = yes
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 200
	}
	
	anglosaxon_nation = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			NOT = { exists = ASE }
			tag = SAX
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				AND = {
					check_variable = { which = "Demesne_on_the_Upper_Saxon_Circuit" value = 12 }
					check_variable = { which = "Demesne_on_the_Lower_Saxon_Circuit" value = 12 }
					check_variable = { which = "Demesne_in_England" value = 16 }
				}
				AND = {
					check_variable = { which = "Cores_on_the_Lower_Saxon_Circuit" value = 8 }
					check_variable = { which = "Cores_on_the_Lower_Saxon_Circuit" value = 8 }
					check_variable = { which = "Cores_on_England" value = 12 }
				}
			}
		}
		effect = {
			south_england_region = { limit = { owned_by = ROOT } remove_core = ASE add_core = ASE }
			south_england_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ASE }
			north_england_region = { limit = { owned_by = ROOT } remove_core = ASE add_core = ASE }
			north_england_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ASE }
			lower_saxon_circle_region = { limit = { owned_by = ROOT } remove_core = ASE add_core = ASE }
			lower_saxon_circle_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ASE }
			upper_saxon_circle_region = { limit = { owned_by = ROOT } remove_core = ASE add_core = ASE }
			upper_saxon_circle_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ASE }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = ASE
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ASE_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	restore_latine_empire = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			NOT = { exists = LAT }
			NOT = { tag = BYZ }
			NOT = { tag = ERG }
			NOT = { tag = PAP }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = GRE }
			NOT = { tag = ROM }
			owns = 1402	#Kostantiniyye
			religion = catholic
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			is_core = 1402
			OR = {
				check_variable = { which = "Demesne_in_Constantinople" value = 20 }
				check_variable = { which = "Cores_on_Constantinople" value = 15 }
			}
		}
		effect = {
			thrace_area = { limit = { owned_by = ROOT } remove_core = LAT add_core = LAT }
			thrace_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LAT }
			greece_region = { limit = { owned_by = ROOT } remove_core = LAT add_core = LAT }
			greece_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LAT }
			north_anatolia_region = { limit = { owned_by = ROOT } remove_core = LAT add_core = LAT }
			north_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LAT }
			#south_anatolia_region = { limit = { owned_by = ROOT } add_core = LAT }
			#south_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LAT }
			add_prestige = 15
			LAT = { Effect_set_capital = { target=1402 } }
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = LAT
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = LAT_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1.0
		}
		ai_importance = 400
	}
	
	form_ilkhanate = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			NOT = { exists = ILK }
			OR = {
				AND = {
					primary_culture = mongol
					tag = JAI
				}
				AND = {
					primary_culture = mongol
					tag = CHU
				}
				AND = {
					culture_group = altaic
					OR = {
						NOT = { exists = CHU }
						NOT = { exists = JAI }
					}
					tag = MUZ
				}
			}
		}
		allow = {
			OR = {
				AND = {
					tag = JAI
					NOT = { exists = CHU }
				}
				AND = {
					tag = CHU
					NOT = { exists = JAI }
				}
				AND = {
					tag = MUZ
					NOT = { exists = CHU }
					NOT = { exists = JAI }
				}
			}
			is_free_or_tributary_trigger = yes
			OR = {
				check_variable = { which = "Demesne_in_Ilkhanate_Region" value = 70 }
				check_variable = { which = "Cores_on_Ilkhanate_Region" value = 50 }
			}
			owns = 410 # Baghdad
			owns = 416 # Tabriz
			owns = 1315 # Soltaniyeh
			is_at_war = no
		}
		effect = {
			south_anatolia_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			south_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			north_anatolia_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			north_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			al_sham_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			al_sham_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			al_iraq_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			al_iraq_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			caucasia_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			caucasia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			khorasan_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			khorasan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			persia_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			persia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			afghanistan_region = { limit = { owned_by = ROOT } remove_core = ILK add_core = ILK }
			afghanistan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILK }
			add_prestige = 15
			ILK = { Effect_set_capital = { target=1315 } }
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = ILK
			persian_group_union_effect = yes
			armenian_union_effect = yes
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ILK_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	illyria_nation = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			NOT = { exists = ILL }
			NOT = { tag = LAT }
			NOT = { tag = PAP }
			NOT = { tag = ERG }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = ROM }
			culture_group = balkan_slavic
			capital_scope = {
				OR = {
					region = west_balkan_region
					region = central_balkan_region
				}
			}
		}
		allow = {
			OR = {
				check_variable = { which = "Demesne_in_Illyria" value = 20 }
				check_variable = { which = "Cores_on_Illyria" value = 15 }
			}
			owns = 131	# Croatia
			is_core = 131	# Croatia
			owns = 141	# Serbia
			is_core = 141	# Serbia
			is_at_war = no
		}
		effect = {
			west_balkan_region = { limit = { owned_by = ROOT } remove_core = ILL add_core = ILL }
			west_balkan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILL }
			central_balkan_region = { limit = { owned_by = ROOT } remove_core = ILL add_core = ILL }
			central_balkan_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ILL }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			add_absolutism = 10
			change_tag = ILL
			balkan_slavic_union_effect = yes
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ILL_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	arelate_nation = {
		major = yes
		potential = {
			has_global_flag = additional_tags_enabled
			OR = {
				primary_culture = provencal
				primary_culture = arpitan
			}
			NOT = { exists = ARL }
			NOT = { tag = UEF }
			NOT = { tag = FRA }
			NOT = { tag = ENG }
			NOT = { tag = ROM }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Kingdom_of_Arles" value = 26 }
				check_variable = { which = "Cores_on_Kingdom_of_Arles" value = 16 }
			}
			owns = 201 # Proensa
			is_core = 201
		}
		effect = {
			provence_region = { limit = { owned_by = ROOT } remove_core = ARL add_core = ARL }
			provence_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARL }
			high_countries_region = { limit = { owned_by = ROOT } remove_core = ARL add_core = ARL }
			high_countries_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ARL }
			
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
			ARL = { Effect_set_capital = { target=201 } }
			add_absolutism = 10
			change_tag = ARL
			
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = ARL_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	resurrect_roman_empire = {
		major = yes
		potential = {
			has_global_flag = extra_tags_enabled
			NOT = { exists = ROM }
			NOT = { tag = PAP }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = GRE }
			OR = {
				check_variable = { which = "Demesne_in_France" value = 15 }
				check_variable = { which = "Cores_on_France" value = 10 }
				check_variable = { which = "Demesne_in_Italy" value = 15 }
				check_variable = { which = "Cores_on_Italy" value = 10 }
				check_variable = { which = "Demesne_in_Two_Sicilies" value = 10 }
				check_variable = { which = "Cores_on_Two_Sicilies" value = 5 }
				check_variable = { which = "Demesne_in_Spain" value = 15 }
				check_variable = { which = "Cores_on_Spain" value = 10 }
			}
			OR = {
				religion = catholic
				religion = orthodox
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			num_of_cities = 250
			is_at_war = no
			is_core = 2530
			AND = {
				check_variable = { which = "Demesne_in_France" value = 35 }
				check_variable = { which = "Demesne_in_Italy" value = 35 }
				check_variable = { which = "Demesne_in_Two_Sicilies" value = 20 }
				check_variable = { which = "Demesne_in_Spain" value = 35 }
			}
		}
		effect = {
			france_superregion = { limit = { owned_by = ROOT } remove_core = ROM add_core = ROM }
			france_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROM }
			iberia_superregion = { limit = { owned_by = ROOT } remove_core = ROM add_core = ROM }
			iberia_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROM }
			italy_superregion = { limit = { owned_by = ROOT } remove_core = ROM add_core = ROM }
			italy_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = ROM }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { has_country_modifier = title_6 } }
				change_title_6 = yes
			}
			add_absolutism = 10
			change_tag = ROM
		}
		ai_will_do = {
			factor = 1.0
		}
		ai_importance = 400
	}
}
