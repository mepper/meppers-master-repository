########################################
#                                      #
#      IndianPolitical.txt      #
#                                      #
########################################
#
# 
#
########################################

country_decisions = {
	viceroyality_of_the_deccan = {
		potential = {
			NOT = { culture_group = deccan_group }
			NOT = { culture_group = dravidian }
			OR = {
				government = indian_monarchy
				government = rajput_monarchy
			}
			check_variable = { which = "Demesne_in_Deccan" value = 5 }
			NOT = { has_country_flag = viceroyality_of_the_deccan_created }
			NOT = { exists = HYD }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			check_variable = { which = "Demesne_in_Deccan" value = 10 }
			OR = {
				owns = 3164 # Daulatabad
				owns = 1587 # Bijapur
				owns = 545 # Hyderabad
			}
		}
		effect = {
			set_country_flag = viceroyality_of_the_deccan_created
			maharashtra_area = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT }
			east_deccan_region = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT }
			east_deccan_region = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD }
			west_deccan_region = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT }
			west_deccan_region = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD }
			andhra_area = { if = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT } }
			andhra_area = { if = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD } }
			north_cuttack_area = { if = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT } }
			north_cuttack_area = { if = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD } }
			south_cuttack_area = { if = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT } }
			south_cuttack_area = { if = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD } }
			north_orissa_area = { if = { limit = { owned_by = ROOT } add_core = HYD remove_core = ROOT remove_claim = ROOT } }
			north_orissa_area = { if = { limit = { NOT = { owned_by = ROOT } } add_claim = HYD } }
			create_vassal = HYD
			HYD = {
				change_government = indian_monarchy
				change_title_3 = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	nawab_of_the_carnatic = {
		potential = {
			NOT = { primary_culture = telegu }
			NOT = { tag = BIJ }
			NOT = { tag = GOC }
			NOT = { culture_group = dravidian }
			religion_group = muslim
			OR = {
				government = indian_monarchy
				government = rajput_monarchy
			}
			owns = 541 # Arcot
			NOT = { has_country_flag = nawab_of_the_carnatic_created }
			NOT = { exists = KRK }
		}
		allow = {
			is_at_war = no
		}
		effect = {
			set_country_flag = nawab_of_the_carnatic_created
			north_coromandel_area = { limit = { owned_by = ROOT } add_core = KRK remove_core = ROOT remove_claim = ROOT }
			north_coromandel_area = { limit = { NOT = { owned_by = ROOT } } add_core = KRK }
			south_coromandel_area = { limit = { owned_by = ROOT } add_core = KRK remove_core = ROOT remove_claim = ROOT }
			south_coromandel_area = { limit = { NOT = { owned_by = ROOT } } add_core = KRK }
			madura_area = { limit = { owned_by = ROOT } add_core = KRK remove_core = ROOT remove_claim = ROOT }
			madura_area = { limit = { NOT = { owned_by = ROOT } } add_core = KRK }
			south_carnatic_area = { limit = { owned_by = ROOT } add_core = KRK remove_core = ROOT remove_claim = ROOT }
			south_carnatic_area = { limit = { NOT = { owned_by = ROOT } } add_core = KRK }
			north_carnatic_area = { limit = { owned_by = ROOT } add_core = KRK remove_core = ROOT remove_claim = ROOT }
			north_carnatic_area = { limit = { NOT = { owned_by = ROOT } } add_core = KRK }
			create_vassal = KRK
			KRK = {
				Effect_set_capital = { target=541 }
				change_government = indian_monarchy
				change_religion = sunni
				change_title_2 = yes
				define_ruler = {
					dynasty = "Saadatullahid"
					DIP = 3
					ADM = 3
					MIL = 3
				}
				define_heir = {
					claim = 100
					dynasty = "Saadatullahid"
					adm = 3
					dip = 3
					mil = 3
					age = 16
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	nawab_of_kurpa = {
		potential = {
			NOT = { tag = BIJ }
			NOT = { tag = GOC }
			NOT = { tag = BAH }
			NOT = { tag = BAS }
			NOT = { tag = BRR }
			NOT = { culture_group = dravidian }
			religion_group = muslim
			OR = {
				government = indian_monarchy
				government = rajput_monarchy
			}
			owns = 547 # Tirupati
			NOT = { has_country_flag = nawab_of_kurpa_created }
			NOT = { exists = KRP }
		}
		allow = {
			is_at_war = no
		}
		effect = {
			set_country_flag = nawab_of_kurpa_created
			547 = { add_core = KRP remove_core = ROOT remove_claim = ROOT }
			543 = { add_core = KRP remove_core = ROOT remove_claim = ROOT }
			KRP = {
				change_primary_culture = telegu
			}
			create_vassal = KRP
			KRP = {
				Effect_set_capital = { target=547 }
				change_government = indian_monarchy
				change_religion = sunni
				change_primary_culture = capital
				change_title_2 = yes
				define_ruler = {
					dynasty = "Patan"
					DIP = 3
					ADM = 3
					MIL = 3
				}
				define_heir = {
					claim = 100
					dynasty = "Patan"
					adm = 3
					dip = 3
					mil = 3
					age = 16
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	nawab_of_savanur = {
		potential = {
			NOT = { tag = BIJ }
			NOT = { tag = GOC }
			NOT = { tag = BAH }
			NOT = { tag = BAS }
			NOT = { tag = BRR }
			NOT = { culture_group = dravidian }
			religion_group = muslim
			OR = {
				government = indian_monarchy
				government = rajput_monarchy
			}
			owns = 2677 # Savanaur
			NOT = { has_country_flag = nawab_of_savanur_created }
			NOT = { exists = SVN }
		}
		allow = {
			is_at_war = no
		}
		effect = {
			set_country_flag = nawab_of_savanur_created
			savanur_group = { limit = { owned_by = ROOT } add_core = SVN remove_core = ROOT remove_claim = ROOT }
			savanur_group = { limit = { NOT = { owned_by = ROOT } } add_core = SVN }
			SVN = {
				change_primary_culture = kannada
			}
			create_vassal = SVN
			SVN = {
				Effect_set_capital = { target=2677 }
				change_government = indian_monarchy
				change_religion = sunni
				change_primary_culture = capital
				change_title_2 = yes
				define_ruler = {
					dynasty = "Miyana"
					DIP = 3
					ADM = 3
					MIL = 3
				}
				define_heir = {
					claim = 100
					dynasty = "Miyana"
					adm = 3
					dip = 3
					mil = 3
					age = 16
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}