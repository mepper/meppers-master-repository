country_decisions = {
#	change_tax_rate_low = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = low_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = high_tax_rate
#				}
#				remove_country_modifier = high_tax_rate
#				subtract_stability_2 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = medium_tax_rate
#				}
#				remove_country_modifier = medium_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = low_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
#
#	change_tax_rate_medium = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = medium_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = high_tax_rate
#				}
#				remove_country_modifier = high_tax_rate
#				subtract_stability_1 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = low_tax_rate
#				}
#				remove_country_modifier = low_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = medium_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}
	
#	change_tax_rate_high = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = high_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = low_tax_rate
#				}
#				remove_country_modifier = low_tax_rate
#				subtract_stability_2 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = medium_tax_rate
#				}
#				remove_country_modifier = medium_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = high_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
#	send_tax_agents = {
#		major = yes
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			has_factions = yes
#
#		}
#		allow = {
#			NOT = { has_country_modifier = china_tax_change }
#			OR = {
#				faction_in_power = enuchs
#				faction_in_power = bureaucrats
#			}
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = high_tax_rate
#				}
#				remove_country_modifier = high_tax_rate
#				subtract_stability_1 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = low_tax_rate
#				}
#				remove_country_modifier = low_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = medium_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
	
#	change_tax_rate_low = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = low_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = high_tax_rate
#				}
#				remove_country_modifier = high_tax_rate
#				add_stability_2 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = medium_tax_rate
#				}
#				remove_country_modifier = medium_tax_rate
#				add_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = low_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 1
#		}
#	}
#
#	change_tax_rate_medium = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = medium_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = high_tax_rate
#				}
#				remove_country_modifier = high_tax_rate
#				add_stability_1 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = low_tax_rate
#				}
#				remove_country_modifier = low_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = medium_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
#
#	change_tax_rate_high = {
#		potential = {
#			chinese_imperial_gov_trigger = yes
#			NOT = { has_country_modifier = high_tax_rate }
#		}
#		allow = {
#			stability = 1
#			NOT = { has_country_modifier = china_tax_change }
#		}
#		effect = {
#			if = {
#				limit = {
#					has_country_modifier = low_tax_rate
#				}
#				remove_country_modifier = low_tax_rate
#				subtract_stability_2 = yes
#			}
#			if = {
#				limit = {
#					has_country_modifier = medium_tax_rate
#				}
#				remove_country_modifier = medium_tax_rate
#				subtract_stability_1 = yes
#			}
#			add_country_modifier = {
#				name = high_tax_rate
#				duration = -1
#			}
#			add_country_modifier = {
#				name = china_tax_change
#				duration = 3650
#			}
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
#
	
	reorganize_treasury_fleet = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			NOT = { has_country_modifier = treasury_fleet }
		}
		allow = {
			years_of_income = 5
			dip_tech = 15
			OR = {
				faction_in_power = enuchs
				faction_in_power = temples
				has_factions = no
			}
		}
		effect = {
			add_years_of_income = -5
			add_prestige = 30
			add_country_modifier = {
				name = treasury_fleet
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	abolish_treasury_fleet = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			has_country_modifier = treasury_fleet
		}
		allow = {
			stability = 1
			OR = {
				faction_in_power = bureaucrats
				faction_in_power = temples
				has_factions = no
			}
		}
		effect = {
			subtract_stability_1 = yes
			add_prestige = -10
			remove_country_modifier = treasury_fleet
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	#reform_nurgan_comission = {
	#	major = yes
	#	potential = {
	#		OR = {
	#			government = celestial_empire
	#			government = chinese_monarchy_1a
	#			government = chinese_monarchy_1b
	#		}
	#		has_factions = yes
	#		NOT = { has_country_modifier = reformed_nurgan_comission }
	#	}
	#	allow = {
	#		stability = 1
	#		dip_tech = 23
	#		faction_in_power = temples
	#		owns = 726
	#		owns = 731
	#	}
	#	effect = {
	#		add_prestige = 20
	#		add_legitimacy = 20
	#		add_country_modifier = {
	#			name = reformed_nurgan_comission
	#			duration = -1
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 0
	#	}
	#}
	
	choose_physiocratic_economy = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			NOT = { has_country_modifier = chinese_physiocratic_economy }
			has_country_modifier = chinese_mercantilistic_economy
		}
		allow = {
			stability = 1
			OR = {
				faction_in_power = bureaucrats
				faction_in_power = temples
				has_factions = no
			}
		}
		effect = {
			subtract_stability_1 = yes
			remove_country_modifier = chinese_mercantilistic_economy
			add_country_modifier = {
				name = chinese_physiocratic_economy
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	choose_mercantilistic_economy = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			NOT = { has_country_modifier = chinese_mercantilistic_economy }
			has_country_modifier = chinese_physiocratic_economy
		}
		allow = {
			stability = 1
			OR = {
				faction_in_power = enuchs
				faction_in_power = temples
			}
		}
		effect = {
			subtract_stability_1 = yes
			remove_country_modifier = chinese_physiocratic_economy
			add_country_modifier = {
				name = chinese_mercantilistic_economy
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	enact_wei_suo_system = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			NOT = { has_country_modifier = wei_suo_system }
			has_country_modifier = general_trained_militia
		}
		allow = {
			stability = 1
			OR = {
				faction_in_power = bureaucrats
				faction_in_power = temples
			}
		}
		effect = {
			subtract_stability_1 = yes
			remove_country_modifier = general_trained_militia
			add_country_modifier = {
				name = wei_suo_system
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	allow_general_trained_militia = {
		major = yes
		potential = {
			has_country_flag = mandate_of_heaven_claimed
			has_factions = yes
			NOT = { has_country_modifier = general_trained_militia }
			has_country_modifier = wei_suo_system
		}
		allow = {
			stability = 1
			mil_tech = 22
			OR = {
				faction_in_power = temples
			}
		}
		effect = {
			subtract_stability_1 = yes
			remove_country_modifier = wei_suo_system
			add_country_modifier = {
				name = general_trained_militia
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
			modifier = {
				factor = 0
				NOT = {
					is_year = 1550
				}
			}
		}
	}
	
}
