
# Ottoman vassal
vassal = {
	first = TUR
	second = IME
	start_date = 1540.1.1
	end_date = 1810.1.1
}
vassal = {
	first = CHU
	second = SYU
	start_date = 1356.1.1
	end_date = 1357.1.1
}
vassal = {
	first = JAI
	second = SYU
	start_date = 1360.1.1
	end_date = 1386.1.1
}
vassal = {
	first = CHU
	second = ARM
	start_date = 1356.1.1
	end_date = 1360.1.1
}

#Circassian vassal
vassal = {
	first = CIR
	second = ABZ
	start_date = 1356.1.1
	end_date = 1829.1.1
}
vassal = {
	first = CIR
	second = KBR
	start_date = 1450.1.1
	end_date = 1829.1.1
}
