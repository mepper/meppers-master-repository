#59 - Wittenberg

owner = SAX
controller = SAX
add_core = SAX

capital = "Wittenberg"
trade_goods = livestock
culture = high_saxon
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
marketplace = yes
local_fortification_1 = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#450.1.1 = {
#	add_permanent_province_modifier = {
#		name = "elbe_navigable_river"
#		duration = -1
#	}
#}
1500.1.1 = {
	road_network = yes
}
1502.1.1 = {
	small_university = yes
} # University of Wittenberg
1520.5.5 = {
	base_tax = 10
	base_production = 0
	base_manpower = 1
}
1530.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1589.1.1 = {
	fort_14th = yes
} # Current structure of Festung Königstein is built
 # First European porcelain is manufactured
1790.8.1 = {
	unrest = 5
} # Peasant revolt
1791.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} #Ceded to Prussia
