# 3350 - Wakasa

owner = HKW
controller = HKW
add_core = HKW

capital = "Obama"
trade_goods = fish
culture = kansai
religion = mahayana

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes # Wakasahime Shirine (Obama) and Kehi Shirine (Tsuruga).
harbour_infrastructure_1 = yes

discovered_by = chinese

1356.1.1 = {
	add_core = SBA
	add_core = ISK #home province
	add_core = TKD #home province
}
1361.1.1 = {
	controller = JAP
} # Owned briefly by Ishibashi Kazuyoshi, son of Ashikaga Yoshihiro
1363.1.1 = {
	owner = SBA
	controller = SBA
}
1366.1.1 = {
	owner = ISK
	controller = ISK
}
1440.1.1 = {
	owner = TKD
	controller = TKD
} # Owned by Wakasa-Takeda (separate faction?)
1501.1.1 = {
	base_tax = 3
	base_production = 2
}
1598.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
