# 76 - Salzburg

owner = SLZ
controller = SLZ
add_core = SLZ

capital = "Salzburg"
trade_goods = livestock
culture = austrian
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1500.1.1 = {
	road_network = yes
}
# The Salzburg Cathedral is rebuilt
1520.5.5 = {
	base_tax = 9
	base_production = 1
	base_manpower = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1628.1.1 = {
	temple = no
	great_temple = yes
}
1731.11.11 = {
	unrest = 5
} # Leopold von Firmian declared that all Protestants would be banished
1732.1.1 = {
	unrest = 0
}
# Given to Ferdinand III, former Grand Duke of Tuscany
1805.12.26 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ETR
} # Treaty of Pressburg, annexed by Habsburg
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1809.10.14 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = HAB
} # Treaty of Schönbrunn
1814.5.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BAV
} # Treaty of Paris
