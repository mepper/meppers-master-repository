# 3732 - Falkenstein

owner = FAL
controller = FAL
add_core = FAL

capital = "Münzenberg"
trade_goods = wool
culture = hessian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1470.1.1 = {
	fort_15th = yes
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
}
1535.1.1 = {
	owner = HES
	controller = HES
	add_core = HES
} # Inherited by Hesse
1548.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1567.3.31 = {
	owner = HDA
	controller = HDA
	add_core = HDA
	remove_core = HES
}
1806.7.12 = {
	hre = no
	owner = HES
	controller = HES
	add_core = HES
	remove_core = HDA
} # The Holy Roman Empire is dissolved
1813.10.14 = {
	controller = HES
}
1866.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HES
}
