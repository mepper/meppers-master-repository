# 3822 - Eichsfeld

owner = MAI
controller = MAI
add_core = MAI

capital = "Duderstadt"
trade_goods = wool
culture = thuringian
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1802.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = MAI
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.16 = {
	controller = FRA
} # Controlled by France
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
