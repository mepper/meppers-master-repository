# No previous file for Camerino

owner = CMR
controller = CMR
add_core = CMR

capital = "Camerino"
trade_goods = wool
culture = umbrian
religion = catholic

hre = no

base_tax = 4
base_production = 1
base_manpower = 0

is_city = yes
temple = yes
local_fortification_1 = yes
small_university = yes #University of Camerino
town_hall = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = pocket_province
}
1382.1.1 = {
	fort_14th = yes
}
1502.1.1 = {
	add_core = PAP
} # Cesare Borgia wipes the Da Varano
1520.5.5 = {
	base_tax = 5
	base_production = 1
	base_manpower = 0
	fort_14th = yes
}
1545.1.1 = {
	owner = PAP
	controller = PAP
} # Camerino falls under direct control of the Papal administration
1805.3.17 = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Treaty of Pressburg
1814.4.11 = {
	owner = PAP
	controller = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
