#1356 - Sundgau

owner = ALS
controller = ALS
add_core = ALS

capital = "Altkirch"
trade_goods = livestock #linen
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1324.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1379.1.1 = {
	controller = TIR
	owner = TIR
	add_core = TIR
}
1490.1.1 = {
	controller = HAB
	owner = HAB
	#add_core = HAB
	remove_core = TIR
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1530.1.5 = {
	controller = FRE
	owner = FRE
	add_core = FRE
	remove_core = HAB
}
1632.1.1 = {
	controller = SWE
}
1635.5.1 = {
	controller = FRA
}
1648.10.24 = {
	owner = FRA
	add_core = FRA
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
