# 1150 - Luangwa

owner = LDU
controller = LDU
add_core = LDU

capital = "Chipata"
trade_goods = gems
culture = nyasa
religion = animism

hre = no

base_tax = 4 #Theorical
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = central_african
discovered_by = east_african

450.1.1 = {
	set_province_flag = tribals_control_province
}
