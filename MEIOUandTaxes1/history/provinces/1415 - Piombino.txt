#1415 - Piombino

owner = PIS
controller = PIS
add_core = PIS

capital = "Piombino"
trade_goods = iron
culture = tuscan
religion = catholic

hre = yes #AdL: was part of HRE

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = FIR
	add_core = PIO
}
1399.1.1 = {
	owner = PIO
	controller = PIO
	remove_core = PIS
	remove_core = FIR
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.1.1 = {
	road_network = no
	paved_road_network = yes
	fort_14th = yes
}
1530.2.27 = {
	hre = no
}
1618.1.1 = {
	hre = no
}
1801.2.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21 = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11 = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
