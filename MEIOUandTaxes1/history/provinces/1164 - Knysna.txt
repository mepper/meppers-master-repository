# 1164 - Knysna

capital = "Knysna"
trade_goods = unknown # slaves
culture = xhosa
religion = animism

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 8
native_ferocity = 6
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "little_karoo_natural_harbour"
		duration = -1
	}
}
1488.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias

