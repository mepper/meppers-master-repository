# 1243 - Samoa

capital = "Samoa"
trade_goods = unknown # fish
culture = polynesian
religion = polynesian_religion

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 20
native_ferocity = 2
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
}
1722.1.1 = {
	discovered_by = NED
} # Jacob Roggeveen
