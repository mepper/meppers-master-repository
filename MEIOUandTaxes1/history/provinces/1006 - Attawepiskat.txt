# 1006 - Regensburg

owner = FRR
controller = FRR
add_core = FRR

capital = "Regensburg"
trade_goods = livestock
culture = bavarian
religion = catholic

hre = yes

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
marketplace = yes
town_hall = yes
temple = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 2
	base_production = 2
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
