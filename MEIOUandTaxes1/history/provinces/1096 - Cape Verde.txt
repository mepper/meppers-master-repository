# 1096 - Cape Verde

capital = "Cape Verde"
trade_goods = cotton

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 0
native_ferocity = 0
native_hostileness = 0

1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1457.1.1 = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	is_city = yes
	culture = portugese
	religion = catholic
	trade_goods = fish
	set_province_flag = trade_good_set
	naval_arsenal = yes
	marketplace = yes
	fort_14th = yes
}
