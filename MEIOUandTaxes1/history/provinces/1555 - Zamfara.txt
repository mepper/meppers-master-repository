# 1555 - Zamfara

owner = ZAF
controller = ZAF
add_core = ZAF

capital = "Gummi"
trade_goods = cotton
culture = haussa
religion = sunni

hre = no

base_tax = 14
base_production = 3
base_manpower = 2

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
workshop = yes
fort_14th = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 9 }
	set_variable = { which = settlement_score_progress_preset	value = 53 }
}