# 2233 - Gonghong
# FL - Korea Universalis
# LS - alpha 5

owner = KOR
controller = KOR
add_core = KOR

capital = "Gongju"
trade_goods = rice #chinaware
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392

hre = no

base_tax = 19
base_production = 0
base_manpower = 1

is_city = yes
road_network = yes

discovered_by = chinese
discovered_by = steppestech

1392.6.5 = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1444.1.1 = {
	base_tax = 43
}
1520.5.5 = {
	base_tax = 71
	base_manpower = 3
}
1592.4.28 = {
	unrest = 5
} # Japanese invasion
1593.5.18 = {
	controller = JOS
	unrest = 0
} # With Chinese help the Japanese are driven back
1600.1.1 = { } # Great devastation as a result of the Japanese invasion
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
