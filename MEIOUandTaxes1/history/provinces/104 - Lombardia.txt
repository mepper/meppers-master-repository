#104 - Lombardia

owner = MLO
controller = MLO
add_core = MLO

capital = "Mil�n"
trade_goods = wheat #steel
culture = lombard
religion = catholic

hre = yes

base_tax = 25
base_production = 13
base_manpower = 3

is_city = yes
fort_14th = yes
local_fortification_1 = yes
marketplace = yes
urban_infrastructure_2 = yes
corporation_guild = yes
temple = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
# The Duomo
1453.1.1 = {
	add_core = FRA
	fort_14th = yes
}
# University of Milano est. Before that: Pavia est. 1361
1499.8.10 = {
	controller = FRA
} # Louis XII of France invades...
1504.1.31 = {
	owner = FRA
} # ...and seizes Milan, Treaty of Lyon
1512.1.1 = {
	owner = MLO
	controller = MLO
} # Massimiliano Sforza restored by the Swiss
1515.1.1 = {
	controller = FRA
} # Francis I of France invades...
1516.8.13 = {
	owner = FRA
} # ...and seizes Milan, Treaty of Noyon
1520.5.5 = {
	base_tax = 37
	base_production = 13
	base_manpower = 3
	art_corporation = yes # Lombard School
}
1521.1.1 = {
	owner = MLO
	controller = MLO
} # The Spanish invade and restores the Sforzas
1526.5.22 = {
	controller = HAB
} # Milan joined the League of Cognac, and is invaded by the Emperor
1529.8.3 = {
	controller = MLO
} # Treaty of Cambrai restores the Sforzas and includes renounciation of French claims
1530.1.2 = {
	road_network = no
	paved_road_network = yes
	remove_core = FRA
	weapons = yes
}
1530.2.27 = {
	hre = no
}
1535.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
} # Annexed to Spain after the last Sforza died
1559.8.5 = {
	remove_core = FRA
}
1618.1.1 = {
	hre = no
}
1714.9.7 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
} # Treaty of Baden
1796.10.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
} # Transpadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1814.4.20 = {
	controller = REB
} # The Milan insurrection 1
1814.4.26 = {
	controller = HAB
} # The Milan insurrection ends
1848.3.18 = {
	controller = REB
} # The Milan insurrection 2
1848.3.23 = {
	controller = HAB
} # The Milan insurrection ends
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
} # ??
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
