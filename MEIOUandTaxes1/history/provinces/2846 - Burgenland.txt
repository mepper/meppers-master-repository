# 2846 - Burgenland

owner = HUN
controller = HUN
add_core = HUN

capital = "Eisenstadt"
trade_goods = wine
culture = austrian
religion = catholic

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
urban_infrastructure_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = HAB
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1440.1.1 = {
	owner = HAB
	controller = HAB
} # Control switches to the Habsburgs
1477.1.1 = {
	owner = HUN
	controller = HUN
} # Matthias Corvinus retakes control
1491.1.1 = {
	owner = HAB
	controller = HAB
} # Mortgaged by Vladislaus II of Bohemia-Hungary to Maximillian I von Habsburg
1500.1.1 = {
	road_network = yes
}
# 1647 : Returned to Hungary, until XXth century

1520.5.5 = {
	base_tax = 13
	base_production = 0
	base_manpower = 0
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}