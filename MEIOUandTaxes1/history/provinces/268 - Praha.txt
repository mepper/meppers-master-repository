#268 - Praha

owner = BOH
controller = BOH
add_core = BOH

capital = "Praha"
trade_goods = wheat #beer
culture = czech
religion = catholic

hre = yes

base_tax = 31
base_production = 6
base_manpower = 3

is_city = yes
marketplace = yes
workshop = yes
urban_infrastructure_1 = yes
road_network = yes
small_university = yes
temple = yes #St. Vitus Cathedral
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = silver
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_glassware
		duration = -1
	}
}
1457.1.1 = {
	unrest = 5
} # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1 = {
	unrest = 1
} # The Catholic nobility still undermines the crown.
1471.1.1 = {
	unrest = 0
}
1520.5.5 = {
	base_tax = 45
	base_production = 8
	base_manpower = 3
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1576.1.1 = {
	religion = reformed
}
1618.4.23 = {
	revolt = {
		type = protestant_rebels
		size = 2
	}
	controller = REB
} # Defenstration of Prague
1619.3.1 = {
	revolt = { }
	owner = PAL
	controller = PAL
	add_core = PAL
}
1620.11.8 = {
	owner = HAB
	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
# ... and let us start this session by executing the most inconvenient nobles....
1627.1.1 = {
	religion = catholic
} # Order from Ferdinand II to reconvert to Catholicism, many Protestant leave the country
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
