# 1241 - Kiritabi

capital = "Kiribati"
trade_goods = unknown # fish
culture = micronesian
religion = polynesian_religion

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 0.5
native_hostileness = 2

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = remote_island
		duration = -1
	}
}
1568.1.1 = {
	discovered_by = SPA
} # Alvaro de Mendana y Neyra
