# 345 - Ed-Dakhla

culture = hassaniya
religion = sunni
capital = "Ed Dakhla"
trade_goods = unknown # slaves

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 4.5
native_hostileness = 9

discovered_by = muslim
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 83 }
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1437.1.1 = {
	discovered_by = CAS
} # Cadamosto
1445.1.1 = {
	owner = CAS
	controller = CAS
	add_core = CAS
	citysize = 200
	trade_goods = fish
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
