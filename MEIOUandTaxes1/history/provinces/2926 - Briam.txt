# 2926 - Briam

owner = KBO
controller = KBO
add_core = KBO

capital = "Briam"
trade_goods = cotton
culture = haussa
religion = sunni

hre = no

base_tax = 12
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
workshop = yes
town_hall = yes
local_fortification_2 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 17 }
	set_variable = { which = settlement_score_progress_preset	value = 64 }
}