# 3720 - Vestjylland

owner = DEN
controller = DEN
add_core = DEN

capital = "Wibi�rgh"
trade_goods = livestock
culture = danish
religion = catholic

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1500.3.3 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1531.11.1 = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = { }
	controller = DAN
}
1536.1.1 = {
	religion = protestant
}
1644.1.25 = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13 = {
	controller = DAN
} #The Peace of Br�msebro
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
