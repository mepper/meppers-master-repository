# 67 - Vendsyssel
# MEIOU - Gigau

owner = DEN
controller = DEN
add_core = DEN

capital = "Aleburgh"
trade_goods = fish
culture = danish
religion = catholic

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
harbour_infrastructure_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "nordjylland_natural_harbour"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "herring_province_medium"
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1500.3.3 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1529.12.17 = {
	merchant_guild = yes
}
1531.11.1 = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = { }
	controller = DAN
}
1536.1.1 = {
	religion = protestant
}
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
