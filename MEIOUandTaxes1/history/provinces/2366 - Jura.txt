# 2366 - Baillage d'Aval
# Principal cities: Salins

owner = BUR
controller = BUR
add_core = BUR

capital = "Salins"
trade_goods = livestock
culture = bourguignon
religion = catholic

hre = yes

base_tax = 11
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1361.11.21 = {
	owner = ARS
	controller = ARS
	add_core = ARS
}
1369.6.19 = {
	owner = BUR
	controller = BUR
	remove_core = ARS
}
1444.1.1 = {
	remove_core = FRA
}
1477.1.6 = {
	add_core = FRA
}
1482.3.27 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BUR
} # Mary of burgondy dies, Lowlands to Austria
1520.5.5 = {
	base_tax = 12
	base_production = 1
	base_manpower = 0
}
1530.1.1 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = HAB
	#culture = bourguignon
	remove_core = FRA
	hre = no
}
1556.1.14 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1668.2.20 = {
	controller = FRA
} # The Prince de Cond� swiftly takes Franche-Comt� in the War of Devolution
1668.5.2 = {
	controller = SPA
} # Treaty of Aachen: Franche-Comt� returned to Spain
1670.1.1 = {
	add_core = FRA
} # Louis XIV lays claims through the Chambres de R�union
1674.9.1 = {
	controller = FRA
} # France captures Franche-Comt�
1678.9.19 = {
	owner = FRA
	remove_core = SPA
	hre = no
} # Peace of Nijmegen (France-Spain)

