# 2923 - Tadmekka

owner = ADR
controller = ADR
add_core = ADR

capital = "Tadmekka"
trade_goods = palm_date
culture = tuareg
religion = sunni

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 73 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}