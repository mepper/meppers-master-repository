#109 - Mantua

owner = MAN
controller = MAN
add_core = MAN

capital = "M�ntua"
trade_goods = wheat #linen
culture = emilian
religion = catholic

hre = yes

base_tax = 7
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
urban_infrastructure_1 = yes
workshop = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "mantua_lake"
		duration = -1
	}
}
1380.1.1 = {
	add_permanent_province_modifier = {
		name = "county_of_rolo"
		duration = -1
	}
}
1410.1.1 = {
	art_corporation = yes
} # Mantua School
1520.5.5 = {
	base_tax = 8
	base_production = 3
	base_manpower = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
	fort_14th = yes
}
1530.2.27 = {
	hre = no
}
1540.7.1 = {
	temple = yes # temple of San Pietro, 1540
}
1618.1.1 = {
	hre = no
}
1708.6.30 = {
	controller = HAB
	owner = HAB
	add_core = HAB
} # Annexed to Austria
1796.6.4 = {
	controller = FRA
} # Besieged by the French
1796.7.31 = {
	controller = HAB
} # The French are driven off by Austrian and Russian forces
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
} # Transpadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
# 1805.3.17 - Kingdom of Italy
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = HAB
} # ??
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
