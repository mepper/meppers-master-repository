owner = HKW
controller = HKW
add_core = HKW

capital = "Nakamura"
trade_goods = lumber
culture = shikoku
religion = mahayana #shinbutsu

hre = no

base_tax = 16
base_production = 0
base_manpower = 2

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1356.1.1 = {
	add_core = CSK
}
1420.1.1 = {
	owner = CSK
	controller = CSK
}
1501.1.1 = {
	base_tax = 28
	base_production = 1
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1599.7.11 = {
	owner = ODA
	controller = ODA
} # Ch?sokabe Motochika dies heirless
1600.1.1 = {
	fort_14th = yes
}
1600.9.15 = {
	owner = TGW
	controller = TGW
} # Battle of Sekigahara
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
