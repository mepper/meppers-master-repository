# 117 - Siena

owner = SIE
controller = SIE
add_core = SIE

capital = "Siena"
trade_goods = wine
culture = tuscan
religion = catholic

hre = yes #AdL: was part of the HRE

base_tax = 5
base_production = 5
base_manpower = 1

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
corporation_guild = yes
art_corporation = yes # Art Center of Siena in Trecento -> Sienese School
local_fortification_1 = yes
temple = yes
road_network = yes
fort_14th = yes
small_university = yes # Founded 1240

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1399.1.1 = {
	owner = MLO
	controller = MLO
} # Changes hands multiple times in the Italian wars
1403.1.1 = {
	owner = SIE
	controller = SIE
}
1502.2.2 = {
	owner = PAP
	controller = PAP
	fort_14th = yes
}
1503.3.29 = {
	owner = SIE
	controller = SIE
}
1520.5.5 = {
	base_tax = 10
	base_production = 2
	base_manpower = 1
}
1530.1.1 = {
	road_network = no
	paved_road_network = yes
}
1530.2.27 = {
	hre = no
}
1531.1.1 = {
	controller = SPA
	owner = SPA
	add_core = SPA
}
1552.1.1 = {
	controller = FRA
	owner = FRA
	add_core = FRA
}
1555.1.1 = {
	controller = SPA
	owner = SPA
	remove_core = FRA
}
1557.1.1 = {
	controller = FIR
	owner = FIR
	add_core = FIR
	remove_core = SPA
}
1569.1.1 = {
	owner = TUS
	controller = TUS
	add_core = TUS
	remove_core = FIR
} # Pope Pius V declared Duke Cosimo I de' Medici  Grand Duke of Tuscany
1618.1.1 = {
	hre = no
}
1801.2.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21 = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11 = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Treaty of Fontainebleu, Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
