#248 - Lothian

owner = SCO
controller = SCO
add_core = SCO

capital = "Edinburgh"
trade_goods = fish
culture = lowland_scottish
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_1 = yes
temple = yes #St Giles Cathedral, Edinburgh

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
	add_permanent_province_modifier = {
		name = "lothian_natural_harbour"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "herring_province_medium"
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = salt
}
1410.6.1 = {
	small_university = yes
} # University of St. Andrews
1482.8.1 = {
	controller = ENG
}
1483.3.1 = {
	controller = SCO
}
#Estimated
#College of Justice
1520.5.5 = {
	base_tax = 7
	base_production = 2
	base_manpower = 1
}
1547.10.1 = {
	controller = ENG
} # Rough Wooing
1550.1.1 = {
	controller = SCO
} # Scots Evict English with French Aid
1560.1.1 = {
	fort_14th = yes
}
1560.8.1 = {
	religion = reformed
	#reformation_center = reformed
}
1650.12.24 = {
	controller = ENG
} # Cromwell Captures Edinburgh Castle
1652.4.21 = {
	controller = SCO
} # Union of Scotland and the Commonwealth
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1 = { } # Regimental Camp, Tax Assessor Estimated
