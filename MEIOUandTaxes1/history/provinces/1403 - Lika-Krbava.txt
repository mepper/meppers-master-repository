# 1403 - Lika-Krbava

owner = CRO
controller = CRO
add_core = CRO

capital = "Senj"
trade_goods = lumber
culture = croatian
religion = catholic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1356.1.1 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1444.1.1 = {
	owner = HUN
	controller = HUN
	add_permanent_province_modifier = {
		name = croatian_kingdom
		duration = -1
	}
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1809.10.14 = {
	controller = FRA
	owner = FRA
	add_core = FRA
	add_core = HAB
	remove_core = VEN
}#treaty of schonnbrunn
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
