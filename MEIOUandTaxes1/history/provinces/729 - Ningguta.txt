# 729 - Haishenwai

owner = MWK
controller = MWK
add_core = MWK

capital = "Tetyukhe"
trade_goods = tin
culture = jurchen
religion = tengri_pagan_reformed

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1611.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = MWK
} # The Later Jin Khanate
1616.2.17 = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1858.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
} # Treaty of Aigun
