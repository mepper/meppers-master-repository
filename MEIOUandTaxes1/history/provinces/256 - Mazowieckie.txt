# 256 - Mazowiekiye (mazovia)

owner = MAZ
controller = MAZ
add_core = MAZ

capital = "Czersk"
trade_goods = carmine
culture = polish
religion = catholic

base_tax = 4
base_production = 1
base_manpower = 1

is_city = yes
marketplace = yes
town_hall = yes
road_network = yes
temple = yes
fort_14th = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

450.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "warsaw_confluence"
		duration = -1
	}
}
1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = POL
}
1520.5.5 = {
	base_tax = 7
	base_production = 1
	base_manpower = 1
}
1526.3.9 = {
	owner = POL
	controller = POL
	add_core = POL
	remove_core = MAZ
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1575.1.1 = {
	fort_16th = yes
}
1588.1.1 = {
	controller = REB
} # Civil war, Polish succession
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1596.5.25 = {
	rename_capital = "Warszawa"
	change_province_name = "Warszawa"
}
1656.7.28 = {
	controller = SWE
} # Battle of Warsaw, against Sweden & Brandenburg
1660.1.1 = {
	controller = PLC
} # End of Northern war
1702.5.1 = {
	controller = SWE
} # Occupied again
1706.9.24 = {
	controller = PLC
} # Karl XII defeated in the battle of Poltava
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
 # Kolegium Pijar�w founded
1768.2.28 = {
	unrest = 8
} # Uprisings against the Polish king & Russia
1772.8.18 = {
	unrest = 0
} # Uprisings suppressed by Russian troops
1794.3.24 = {
	unrest = 5
} # Kosciuszko uprising
1794.11.5 = {
	unrest = 0
} # Kosciuszko is captured
1795.10.24 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	add_core = POL
	remove_core = PLC
} # Third partition
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.11.3 = {
	controller = REB
} # Polish uprising instigated by Napoleon
1807.7.9 = {
	owner = POL
	controller = POL
	remove_core = RUS
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = RUS
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
