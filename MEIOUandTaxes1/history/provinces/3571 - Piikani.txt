# No previous file for Piikani

capital = "Piikani"
trade_goods = unknown
culture = nakota
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1760.1.1 = {
	owner = BLA
	controller = BLA
	add_core = BLA
	trade_goods = fur
	is_city = yes
} # Great Plain tribes spread over vast territories
