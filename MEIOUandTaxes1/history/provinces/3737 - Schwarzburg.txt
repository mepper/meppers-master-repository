# 3737 - Schwarzburg

owner = SCH
controller = SCH
add_core = SCH

capital = "Schwarzburg"
trade_goods = wheat
culture = thuringian
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1540.1.1 = {
	religion = protestant
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
