# 2913 - Bozo

owner = MAL
controller = MAL
add_core = MAL

capital = "Jenne"
trade_goods = rice
culture = bozo
religion = sunni

hre = no

base_tax = 6
base_production = 2
base_manpower = 1

is_city = yes
marketplace = yes
urban_infrastructure_1 = yes
temple = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 17 }
	set_variable = { which = settlement_score_progress_preset	value = 51 }
}
1356.1.1 = {
	add_core = JNN
}
1528.1.1 = {
	base_tax = 8
}