# 3725 - Ravensberg

owner = RAV
controller = RAV
add_core = RAV

capital = "Bielefeld"
trade_goods = wheat
culture = westphalian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1423.1.1 = {
	owner = JUL
	controller = JUL
	add_core = JUL
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
}
1521.3.15 = {
	owner = JBC
	controller = JBC
	add_core = JBC
	remove_core = JUL
}
1529.1.1 = {
	religion = protestant
}
1614.11.12 = {
	owner = BRA
	controller = BRA
	add_core = BRA
	remove_core = JUL
} # Treaty of Xanten, ending the war of the J�lich succession
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
