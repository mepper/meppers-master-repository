# 106 - Modena

owner = FER
controller = FER
add_core = FER

capital = "M�dna"
trade_goods = wax
culture = emilian
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
town_hall = yes
workshop = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1356.1.1 = {
	add_core = MOD
	add_permanent_province_modifier = {
		name = "county_of_mirandola"
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 9
	base_production = 2
	base_manpower = 0
}
1530.1.1 = {
	owner = MOD
	controller = MOD
	add_core = MOD
	remove_core = FER
	fort_14th = yes
}
1530.2.27 = {
	hre = no
}
1597.10.28 = {
	owner = MOD
	controller = MOD
	add_core = MOD
	remove_core = FER
}
1618.1.1 = {
	hre = no
}
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = MOD
} # Cispadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11 = {
	owner = MOD
	controller = MOD
	add_core = MOD
	remove_core = ITE
}
1859.8.20 = {
	controller = REB
}
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
