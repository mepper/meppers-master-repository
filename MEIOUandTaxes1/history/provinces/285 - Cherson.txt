# 285 - Pryazovia

owner = WHI
controller = WHI
add_core = WHI

capital = "Kyzyl-Yar"
trade_goods = wheat
culture = crimean
religion = sunni

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
	#set_variable = { which = ptm_capital_level		value = 1 } #Uncomment these for Crimea subtribe
	#set_variable = { which = ptm_substate_loyalty	value = 1 }
	#set_variable = { which = ptm_duchy_id			value = 285 }
	#set_variable = { which = ptm_duchy_type			value = 2 }
	#set_variable = { which = ptm_county_type		value = 2 }
}
1356.1.1 = {
	add_core = CRI
}
1382.1.1 = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
}
1441.1.1 = {
	owner = CRI
	controller = CRI
}
1444.1.1 = {
	remove_core = GOL
}
#1475.1.1 = {
#	add_core = TUR
#}
1501.1.1 = {
	base_tax = 3
	base_production = 1
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
