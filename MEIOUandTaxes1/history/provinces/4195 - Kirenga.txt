# No previous file for Kirenga

capital = "Kirenga"
trade_goods = unknown
culture = evenki
religion = tengri_pagan_reformed
base_tax = 1
base_production = 0
base_manpower = 0
native_size = 10
native_ferocity = 1
native_hostileness = 3

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	owner = SAK
	controller = SAK
	add_core = SAK
	is_city = yes
	trade_goods = fur
	culture = yakut
	set_province_flag = tribals_control_province
}
1426.1.1 = {
	citysize = 0
	native_size = 50
	native_ferocity = 4.5
	native_hostileness = 9
	owner = ---
	controller = ---
	remove_core = SAK
	culture = evenki
	trade_goods = unknown
	clr_province_flag = tribals_control_province
}
1632.1.1 = {
	discovered_by = RUS
}
1632.9.25 = {
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	trade_goods = fur
	capital = "Kirensk"
}
1657.1.1 = {
	add_core = RUS
	is_city = yes
}
