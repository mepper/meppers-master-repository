# 2853 - Trapani

owner = SIC
controller = SIC
add_core = SIC

capital = "Trapani"
trade_goods = fish
culture = sicilian
religion = catholic

hre = no

base_tax = 5
base_production = 2
base_manpower = 0

is_city = yes
road_network = yes
harbour_infrastructure_1 = yes
urban_infrastructure_1 = yes
workshop = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1356.1.1 = {
	add_core = ARA
	add_core = KNP
}
1409.1.1 = {
	owner = ARA
	controller = ARA
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.5.5 = {
	base_tax = 4
	base_production = 5
	base_manpower = 0
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1706.7.1 = {
	controller = SAV
}
1713.4.11 = {
	owner = SIC
	controller = SIC
	remove_core = SPA
}
1718.8.2 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = SIC
#	controller = SIC
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
