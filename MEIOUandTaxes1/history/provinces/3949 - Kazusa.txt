# 3949 - Kazusa

owner = KYO
controller = KYO

capital = "Owotaki"
trade_goods = rice
culture = kanto
religion = mahayana

hre = no

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = CHB #home province
} # Briefly held by Kyogoku
1362.1.1 = {
	owner = CHB
	controller = CHB
} # Assigned to chiba
1364.1.1 = {
	controller = JAP
} # Briefly conquered by Nitta for southern court ADD TAG
1364.6.6 = {
	add_core = USG
	owner = USG
	controller = USG
} # Taken back by the Uyesugi
1418.1.1 = {
	add_core = UTN
	owner = UTN
	controller = UTN
} # Held by Utsunomiya
1420.1.1 = {
	owner = USG
	controller = USG
}
1448.1.1 = {
	owner = CHB
	controller = CHB
} # Returned to Chiba, last Shugo on record
1490.1.1 = {
	owner = HJO
	controller = HJO
}
1501.1.1 = {
	base_tax = 23
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1603.1.1 = {
	capital = "Mito"
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
