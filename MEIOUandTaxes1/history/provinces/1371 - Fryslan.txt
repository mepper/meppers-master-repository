# 1371 - Frysl�n
# Ljouwert (Leeuwarden)

owner = FRI
controller = FRI
add_core = FRI

capital = "Ljouwert"
trade_goods = livestock
culture = frisian
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
town_hall = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1040.1.1 = {
	set_province_flag = freeholders_control_province
}
1356.1.1 = {
	add_core = HOL
}
1453.1.1 = {
	add_core = BUR
}
1473.2.23 = {
	owner = BUR
	controller = BUR
} # Charles the Bold annexes Friesland
1477.1.5 = {
	unrest = 10
} # death of Charles the Bold
1477.8.18 = {
	unrest = 0
} # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BUR
} # Mary of burgondy dies, Lowlands to Austria
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
}
1530.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = FRI
	remove_core = HAB
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1530.1.5 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = BUR
}
1536.1.1 = {
	religion = reformed
	#reformation_center = reformed
}
1549.11.4 = {
	add_core = NED
}
1556.1.14 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1559.5.12 = {
	unrest = 3
} # New bishoprics in the Lowlands create an outrage
1565.1.1 = {
	unrest = 5
} # Letters of Segovia, Philip I orders the harsh persecution of Calvinists
1566.4.5 = {
	unrest = 3
} # 'Eedverbond der Edelen', Margaretha of Parma promises leniency
1567.9.10 = {
	unrest = 4
} # Counts of Egmont & Hoorne arrested
1568.6.5 = {
	unrest = 6
} # Counts of Egmont & Hoorne beheaded
1569.1.1 = {
	unrest = 12
} # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1 = {
	unrest = 20
} # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1572.1.1 = {
	religion = reformed
}
1572.6.15 = {
	controller = REB
} # Rebels take control of large parts of Holland
1573.7.12 = {
	controller = SPA
} # Don Frederiqu� captures Haarlem and others again
1573.10.8 = {
	controller = REB
} # Rebels take Alkmaar, Alba replaced by Requ�sens
1579.1.23 = {
	owner = NED
	controller = NED
	remove_core = SPA
	unrest = 0
} # Union of Utrecht
1585.9.1 = { } # Amsterdam becomes the beacon of the Lowlands
# Professionalisation of the Army
# Vereenigte Oostindische Compagnie
1605.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1609.1.1 = { } # Amsterdam Bank
1631.6.8 = { } # Westerkerk finished
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1700.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1760.1.1 = {
	fort_16th = no
	fort_17th = yes
}
1786.1.1 = {
	unrest = 7
} # Unrest in the Netherlands
1786.5.1 = {
	controller = REB
	unrest = 3
} # 'The Patriots' rout the Dutch army and capture Amsterdam & Rotterdam
1786.10.13 = {
	controller = NED
	unrest = 0
} # With the help of 20,000 Prussians, the Dutch government regains control
1810.7.10 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1813.11.30 = {
	owner = NED
	controller = NED
	remove_core = FRA
} # William returns to the Netherlands
