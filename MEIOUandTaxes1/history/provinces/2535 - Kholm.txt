# 2535 - Kholm

owner = GVO
controller = GVO
add_core = GVO

capital = "Cholm"
trade_goods = livestock
culture = ruthenian
religion = orthodox

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = POL
	add_core = LIT
}
1366.9.1 = {
	owner = POL
	controller = POL
}
1387.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1393.1.1 = {
	remove_core = LIT
	remove_core = GVO
}
1520.5.5 = {
	base_tax = 5
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1660.1.1 = {
	controller = PLC
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1795.10.24 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = LIT
	add_core = PLC
} # Most of Lithuania became part of the Russian empire
1812.6.28 = {
	controller = FRA
} # Occupied by French troops
1812.12.10 = {
	controller = RUS
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
