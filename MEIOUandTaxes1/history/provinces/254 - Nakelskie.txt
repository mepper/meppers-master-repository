#254 - Krajna

owner = POL
controller = POL
add_core = POL

capital = "Naklo"
trade_goods = wheat
culture = polish
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
fort_14th = yes
# An important center of trade

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 4
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war, Polish succession
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1655.1.1 = {
	controller = SWE
} # Swedish invasion
1660.1.1 = {
	controller = PLC
} # End of Northern war
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
 # Several waves of Dutch & rural settlers
1772.2.17 = {
	add_core = PRU
} # First partition of Poland, only lost the northern part
1772.8.5 = {
	owner = PRU
	controller = PRU
	add_core = POL
	remove_core = PLC
} # First partition of Poland, the remaining part came under Prussian control
1794.3.24 = {
	controller = REB
	fort_14th = yes
} # Kosciuszko uprising, Prussia lost control briefly
1794.11.16 = {
	controller = PRU
} # The end of the uprising
1806.11.3 = {
	controller = REB
} # Polish uprising instigated by Napoleon
1807.7.9 = {
	owner = POL
	controller = POL
	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = PRU
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # Returned to Prussia after the Congress of Vienna
