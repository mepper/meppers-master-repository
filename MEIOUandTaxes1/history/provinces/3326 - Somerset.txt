# No previous file for Somerset

owner = ENG
controller = ENG
add_core = ENG

capital = "Bath"
trade_goods = cheese
culture = english
religion = catholic

hre = no

base_tax = 9
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
road_network = yes
local_fortification_1 = yes
harbour_infrastructure_1 = yes
temple = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.6.1 = {
	unrest = 2
} #Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} #Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} #Unpopularity of Warwick & War with Burgundy
1471.5.4 = {
	unrest = 2
} #Murder of Henry VI & Restoration of Edward IV
#Estimated
1483.6.26 = {
	unrest = 8
} #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} #Battle of Bosworth Field & the End of the War of the Roses
1490.1.1 = {
	temple = yes
	road_network = no
	paved_road_network = yes
}
1520.5.5 = {
	base_tax = 15
	base_production = 0
	base_manpower = 1
}
1541.1.1 = {
	religion = protestant
} #Gloucester Cathedral, from a Dissolved Benedictine Priory
#Estimated
1600.1.1 = {
	fort_14th = yes
} #Customs House Estimated
1642.8.22 = {
	controller = REB
} #Start of First English Civil War
1643.7.26 = {
	controller = ENG
}
1645.9.10 = {
	controller = REB
} #Surrender of Bristol
1646.5.5 = {
	controller = ENG
} #End of First English Civil War
#Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1775.1.1 = { } #Estimated
