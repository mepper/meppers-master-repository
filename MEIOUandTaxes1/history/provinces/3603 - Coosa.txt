# No previous file for Coosa

owner = CHE
controller = CHE
add_core = CHE

capital = "Cooasa"
trade_goods = fur
culture = cherokee
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

native_size = 15
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1785.11.2 = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
} # Treaty of Hopewell (with the Cherokee), define a new border
