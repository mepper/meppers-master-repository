# No previous file for Mhaigh Eo

owner = CNN
controller = CNN
add_core = CNN

capital = "Caisle�n an Bharraigh" # Castlebar
trade_goods = fish
culture = irish
religion = catholic

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = {
		name = clan_land
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 4
	base_production = 0
	base_manpower = 0
}
1543.7.1 = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # Burkes submit to England
1642.1.1 = {
	controller = REB
} # Estimated
1642.6.7 = {
	owner = IRE
	controller = IRE
} # Confederation of Kilkenny
1652.4.1 = {
	owner = ENG
	controller = ENG
} # End of the Irish Confederates
1655.1.1 = {
	fort_14th = yes
}
1689.3.12 = {
	controller = REB
} # James II Lands in Ireland
1691.9.23 = {
	controller = ENG
} # Capture of Limerick
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
# Estimated
1798.8.27 = {
	controller = REB
} # Republic of Connaught
1798.9.8 = {
	controller = GBR
}
