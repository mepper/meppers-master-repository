# 366 - Achinet

owner = CNA
controller = CNA
add_core = CNA

capital = "A�azo"
trade_goods = wheat
culture = guanche
religion = animism

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = sub_saharan
discovered_by = western

1402.1.1 = {
	add_core = CAS
} # Jean de Bethencourt vassal of castille conquier the islands
1448.1.1 = {
	controller = POR
	owner = POR
	add_core = POR
} # Marie de Bethencourt sold the islands to portugal
1459.1.1 = {
	controller = CNA
	owner = CNA
} # portuguese expelled by castillan forces
1479.9.4 = {
	remove_core = POR
} # Peace of Alca�obas between Spain and Portugal. Portugal fully recognises Castillian suzerainty over the islands.
1482.1.1 = {
	religion = catholic
	culture = castillian
	trade_goods = sugar
} # After a successful campaign, indigenous king Majarteme converts to catholicism and adopts the name Fernando
1498.1.1 = {
	owner = CAS
	controller = CAS
	capital = "Santa Cruz"
} # Conquest of Tenerife, the archipelago is fully incorporated into the crown, some indigenous redoubts still wage war
1500.3.3 = {
	base_tax = 3
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
