# 1067 - Khoongorai

owner = YEN
controller = YEN
add_core = YEN

capital = "Kem-Kemchik"
trade_goods = fur
culture = old_kirgiz
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	discovered_by = YUA
}
1604.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	rename_capital = "Krasnoyarsk"
	change_province_name = "Krasnoyarsk"
}
1629.1.1 = {
	add_core = RUS
}
