# 244 - Lancashire

owner = ENG
controller = ENG
add_core = ENG

capital = "Lancaster"
trade_goods = wool
culture = northern_e
religion = catholic

hre = no

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes
warehouse = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "lancashire_natural_harbour"
		duration = -1
	}
	set_province_flag = freeholders_control_province
}
1066.1.1 = {
	add_permanent_province_modifier = {
		name = "north_of_england"
		duration = -1
	}
}
1351.1.1 = {
	add_permanent_province_modifier = {
		name = "county_palatine"
		duration = -1
	}
}
1399.10.13 = {
	remove_province_modifier = "county_palatine"
	add_permanent_province_modifier = {
		name = "county_palatine_reformed"
		duration = -1
	}
} # United with the Crown on the accession of Henry IV, but without ever being assimilated into the Crown Estate
1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.6.1 = {
	unrest = 2
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.4.11 = {
	remove_province_modifier = "north_of_england"
	add_permanent_province_modifier = {
		name = "council_of_north"
		duration = -1
	}
} # Council established by Edward IV and headquartered at Sheriff Hutton Castle and Sandal Castle
1471.5.4 = {
	unrest = 2
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1520.5.5 = {
	base_tax = 20
	base_production = 1
	base_manpower = 1
}
1530.1.1 = {
	culture = english
}
1600.1.1 = {
	fort_14th = yes
} # Estimated
1641.11.22 = {
	remove_province_modifier = "council_of_north"
} # Council abolishedbecause of its support for Catholic Recusants
1644.1.1 = {
	controller = REB
} # Estimated
1645.1.1 = {
	controller = ENG
}
1700.1.1 = {
	capital = "Liverpool"
} # Includes Manchester; Tax Assessor Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
