#188 - Trois = Eveches
#Principal cities: Metz, Thionville

owner = MTV
controller = MTV
add_core = MTV

capital = "Metz"
trade_goods = iron
culture = lorrain
religion = catholic

hre = yes

base_tax = 13
base_production = 3
base_manpower = 1

is_city = yes
marketplace = yes
urban_infrastructure_1 = yes
corporation_guild = yes
warehouse = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1356.1.1 = {
#	#add_core = FRA
#}
1500.1.1 = {
	road_network = yes
}
1515.1.1 = {
	fort_14th = yes
} # Earlier than usual, Metz always was a very fortified city
# Saint Stephen's Cathedral finished
1520.1.1 = {
	temple = yes
}
1520.5.5 = {
	base_tax = 17
	base_production = 2
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1530.1.4 = {
	remove_core = FRA
}
1551.4.10 = {
	controller = FRA
} # Franco-Habsburg War (1551-1559): France takes Metz
1559.4.3 = {
	owner = FRA
	add_core = FRA
} # Peace of Cateau-Cambr�sis, France annexes Metz
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism

# Henri IV's quest to eliminate corruption and establish state control
# Le Parlement de Metz inaugurated
1636.7.10 = {
	controller = HAB
} # Habsburg forces ravage North Eastern France
1636.10.24 = {
	controller = FRA
} # Bernhard of Saxe-Weimar defeats the invaders and gradually pushes them back
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1650.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1674.1.1 = {
	fort_15th = no
	fort_16th = yes
} # Vauban's 'pointed' forts in Metz & Thionville
1730.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Cormontaigne's forts
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
