# 3359 - Hoki

owner = YMN
controller = YMN
add_core = YMN

capital = "Yonago"
trade_goods = tobacco
culture = chugoku
religion = mahayana

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 11
	base_manpower = 1
}
1542.1.1 = {
	discovered_by = POR
}
1552.1.1 = {
	add_core = ANG
	owner = ANG
	controller = ANG
}
1582.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
