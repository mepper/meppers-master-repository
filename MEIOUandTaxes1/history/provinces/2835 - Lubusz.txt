# 2835 - Lubusz

owner = BRA
controller = BRA
add_core = BRA

capital = "Lubusz"
trade_goods = livestock
culture = polabian
religion = catholic

hre = yes

base_tax = 7
base_production = 0
base_manpower = 1

is_city = yes
temple = yes
local_fortification_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

#450.1.1 = {
#	add_permanent_province_modifier = {
#		name = "oder_navigable_river"
#		duration = -1
#	}
#}
1402.1.1 = {
	owner = TEU
	controller = TEU
	add_core = TEU
}
1433.6.1 = {
	controller = POL
}
1433.12.1 = {
	controller = TEU
}
1444.1.1 = {
	add_core = BRA
}
1454.1.1 = {
	owner = BRA
	controller = BRA
	remove_core = TEU
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 11
}
1530.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
	culture = low_saxon
}
1535.1.1 = {
	revolt = {
		type = pretender_rebels
		size = 3
		leader = "Hans von K�strin"
	}
	controller = REB
}
1571.1.1 = {
	revolt = { }
}
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
	base_tax = 9
	base_production = 9
} # Friedrich III becomes king of Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
