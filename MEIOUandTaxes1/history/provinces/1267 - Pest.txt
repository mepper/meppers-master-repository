# 1267 - Pest

owner = HUN
controller = HUN
add_core = HUN

capital = "Pest"
trade_goods = wheat #linen
culture = hungarian
religion = catholic

hre = no

base_tax = 16
base_production = 2
base_manpower = 1

is_city = yes
workshop = yes
marketplace = yes
road_network = yes
temple = yes
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 17
	base_production = 3
	base_manpower = 2
}
1526.8.30 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
	fort_14th = yes
}
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1686.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
}
1703.8.1 = {
	controller = TRA
} # The town supports Francis II R�k�czi in his rebellion against the Habsburgs
1711.5.1 = {
	controller = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
1813.1.1 = {
	unrest = 3
} # Croatian national revival (Hrvatski narodni preporod)
