# 356 - Barqah (Cyrenaica)

owner = MAM
controller = MAM
add_core = MAM

capital = "Barneeq"
trade_goods = wool
culture = libyan
religion = sunni

base_tax = 9
base_production = 0
base_manpower = 0

is_city = yes
town_hall = yes
harbour_infrastructure_1 = yes
marketplace = yes
local_fortification_2 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = TRP
}
1400.1.1 = {
	temple = yes
}
1500.3.3 = {
	base_tax = 13
}
1516.1.1 = {
	remove_core = MAM
} # Mamluks fall to Ottomans, Ottomans do not advance up Nile
1530.1.1 = {
	owner = FZA
	controller = FZA
	add_core = FZA
	add_claim = TUR
	add_core = TRP
	remove_core = KNI
} # Under direct Ottoman control until 1629
1530.1.3 = {
	road_network = no
	paved_road_network = yes
	capital = "Marsa ibn Ghazi"
}
1609.1.1 = {
	revolt = {
		type = revolutionary_rebels
		size = 1
	}
	controller = REB
} # Janissary revolt
1610.1.1 = {
	revolt = { }
	controller = TUR
}
1711.1.1 = {
	owner = TRP
	controller = TRP
} # The Karamanli pashas gain some autonomy as the Ottoman central power weakens
