# 2157 - Sakhsara

capital = "Sakhsary"
trade_goods = unknown
culture = eveni
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 2
native_hostileness = 1

450.1.1 = {
	set_province_flag = tribals_control_province
}
1496.1.1 = {
	discovered_by = SAK
	owner = SAK
	controller = SAK
	add_core = SAK
	is_city = yes
	trade_goods = fur
	culture = yakut
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	capital = "Yakutsk"
#	religion = orthodox
#	culture = russian
}
