# 3817 - R�gen

owner = POM
controller = POM
add_core = POM

capital = "Bergen Auf R�gen"
trade_goods = fish
culture = pommeranian
religion = catholic

hre = yes

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	add_permanent_province_modifier = {
		name = "herring_province_small"
		duration = -1
	}
}
1295.1.1 = {
	owner = PWO
	controller = PWO
	add_core = PWO
	add_core = DEN
	remove_core = POM
}
1400.1.1 = {
	remove_core = DEN
}
1478.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PWO
} # Duchy reunited for a short period
#1529.1.1 = {
#	add_core = BRA
#}
1531.1.1 = {
	owner = PWO
	controller = PWO
	add_core = PWO
	remove_core = POM
} # Fifth Partition
1534.1.1 = {
	religion = protestant
	fort_14th = yes
}
1625.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PWO
} # Final reunification
1628.1.1 = {
	owner = SWE
	controller = SWE
} # First Treaty
1630.7.10 = {
	add_core = SWE
} # Treaty of Stettin
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.1.1 = {
	owner = FRA
	controller = FRA
} # French occupation
1813.10.13 = {
	owner = SWE
	controller = SWE
}
1814.1.14 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = SWE
} # Treaty of Kiel
1815.6.7 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = DEN
} # Congress of Vienna
