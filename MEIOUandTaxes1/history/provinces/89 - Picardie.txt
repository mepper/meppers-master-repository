#89 - Picardie

owner = FRA
controller = FRA
add_core = FRA

capital = "Amiens"
trade_goods = indigo
culture = picard
religion = catholic

hre = no

base_tax = 35
base_production = 4
base_manpower = 2

is_city = yes
local_fortification_1 = yes
temple = yes # Notre Dame d'Amiens
urban_infrastructure_1 = yes
marketplace = yes
workshop = yes
warehouse = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1356.1.1 = {
	add_core = FLA
}
1384.1.1 = {
	owner = BUR
	controller = BUR
	add_core = BUR
}
1444.1.1 = {
	remove_core = FRA
}
1477.1.5 = {
	add_core = FRA
}
1482.3.27 = {
	owner = FRA
	controller = FRA
	remove_core = BUR
	#add_core = HAB
} # Charles the Bold dies and transfers Bourgogne to France
1513.8.16 = {
	controller = ENG
} # Henry VIII defeats La Palice at Guinnegate and sacks Therouanne
1514.5.5 = {
	controller = FRA
} # Henry VIII concludes a seperate peace with France
1520.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 42
	base_production = 4
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1544.7.5 = {
	controller = ENG
} # English forces take hold of parts of Picardie in the Anglo-French War (1542-1546)
1546.8.1 = {
	controller = FRA
} # Peace is concluded, back to status quo
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1628.1.1 = {
	unrest = 3
}
1630.1.1 = {
	unrest = 0
}
1638.1.1 = {
	unrest = 3
}
1640.1.1 = {
	unrest = 0
}
1642.1.1 = {
	unrest = 3
}
1644.1.1 = {
	unrest = 0
}
1650.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1740.1.1 = {
	fort_15th = no
	fort_16th = yes
}

