# 4001 - Olesnica

owner = GLO
controller = GLO

capital = "Olesnica"
trade_goods = wheat #cloth
culture = silesian
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1313.1.1 = {
	add_core = OLE
	owner = OLE
	controller = OLE
}
1329.1.1 = {
	add_core = BOH
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = GLO
	add_core = BOH
}
1419.8.16 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1437.12.9 = {
#	owner = BOH
#	controller = BOH
	remove_core = HUN
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 6
}
1523.1.1 = {
	religion = reformed
}
1526.8.30 = {
	add_permanent_claim = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1530.1.4 = {
#	owner = HAB
#	controller = HAB
#	add_core = HAB
}
1618.1.1 = {
	unrest = 5
}
1619.3.1 = {
	revolt = { }
#	owner = PAL
#	controller = PAL
	add_core = PAL
}
1620.11.8 = {
#	owner = HAB
#	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1647.5.31 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1648.1.1 = {
	unrest = 0
}
1694.1.1 = {
	unrest = 4
}
1702.1.1 = {
	unrest = 0
}
1742.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # Peace of Breslau after the first Silesian War
1763.1.1 = {
	remove_core = HAB
} # End of the third Silesian War
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
