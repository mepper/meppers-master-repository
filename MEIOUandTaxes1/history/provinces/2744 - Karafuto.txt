# 2744 - Karafuto

capital = "Kushunkotan"
trade_goods = unknown
culture = aynu
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 4

discovered_by = chinese

450.1.1 = {
	set_province_flag = tribals_control_province
}
1640.1.1 = {
	discovered_by = RUS
} # Ivan Moskvitin
1643.1.1 = {
	discovered_by = NED
}
1679.1.1 = {
	owner = KKZ
	controller = KKZ
	citysize = 320
	trade_goods = fur
	discovered_by = chinese
	rename_capital = "�tomari"
	change_province_name = "Karafuto"
	set_province_flag = trade_good_set
} # Russian & Japanese attempts at colonization
1700.1.1 = {
	citysize = 500
}
1704.1.1 = {
	add_core = KKZ
	add_core = JAP
}
1767.1.1 = {
	add_core = RUS
}
1787.1.1 = {
	discovered_by = FRA
} # Jean-Francois de La P�rouse
1800.1.1 = {
	citysize = 980
}
