# No previous file for Lei Mauras

owner = PRO
controller = PRO
add_core = PRO

capital = "Tolon"
trade_goods = olive
culture = provencal
religion = catholic

hre = yes

base_tax = 13
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
town_hall = yes
temple = yes
road_network = yes
#university = yes # L'Universit� d'Aix-en-Provence (est. 1405)

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "toulon_large_natural_harbor"
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_claim = ANJ
	add_permanent_claim = KNP
	add_permanent_claim = ARA
}
1384.1.1 = {
	owner = ANJ
	controller = ANJ
	add_core = FRA
}
#1423.1.1 = {
#	controller = ARA
#}
1481.1.1 = {
	owner = FRA
	controller = FRA
	remove_core = ARA
	hre = no
	fort_14th = yes
} # Provence added to France after the death of Duke Charles
# Jacques Coeur's factories in Marseille (est. 1450)
# Le Parlement d'Aix
# La Cath�drale Saint-Sauveur finished
1520.5.5 = {
	base_tax = 13
	base_production = 2
	base_manpower = 1
	military_harbour_2 = yes
}
1526.2.1 = {
	controller = HAB
} # Austria invades the Provence during the Franco-Habsburg war (1521-1529)
1529.8.3 = {
	controller = FRA
} # Treaty of Cambrai (Ladies' Peace): Back to status quo
1530.1.1 = {
	fort_14th = yes
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1536.7.1 = {
	controller = HAB
} # Charles V again captures the Provence in another Franco-Habsburg war
1536.9.15 = {
	controller = FRA
} # Charles V retreats from the Provence
1545.1.1 = {
	unrest = 5
} # Unrest among the Waldensians in the Luberon mountains
1546.1.1 = {
	unrest = 0
} # Peace returns
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
# Henri IV's quest to eliminate corruption and establish state control
1590.6.1 = {
	controller = SAV
} # Duke Emmanuel Phillibert enters Aix-en-Provence
1591.4.5 = {
	controller = FRA
} # The Savoyard army is routed at Esparron
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1630.1.1 = {
	unrest = 5
}
1633.1.1 = {
	unrest = 0
}
1649.1.1 = {
	unrest = 5
}
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1659.1.1 = {
	unrest = 5
}
1662.1.1 = {
	unrest = 0
}
1680.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1690.1.1 = {
	fort_15th = no
	fort_16th = yes
}
# Marseille becomes a true city of trade
1750.1.1 = {
	fort_16th = no
	fort_17th = yes
}
