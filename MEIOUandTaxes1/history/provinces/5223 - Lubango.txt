# 5223 - Bengo

capital = "Bengo"
trade_goods = unknown # salt
culture = mbundu
religion = animism

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

native_size = 40
native_ferocity = 4.5
native_hostileness = 9

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
}
1481.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias
1520.1.1 = {
	base_tax = 8
}
1617.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 144
	trade_goods = salt
	set_province_flag = trade_good_set
	base_tax = 8
}
1641.1.1 = {
	owner = NED
	controller = NED
} # Captured by the Dutch
1648.1.1 = {
	owner = POR
	controller = POR
} # A Brazilian-Portuguese expedition expelled the Dutch
