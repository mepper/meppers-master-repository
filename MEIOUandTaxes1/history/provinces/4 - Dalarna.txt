# 4 - Dalarna
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Falun"
trade_goods = lumber
culture = swedish
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = freeholders_control_province
	set_province_flag = mined_goods
	set_province_flag = copper
	add_permanent_province_modifier = {
		name = stora_kopparberget_modifier
		duration = -1
	}
}
1500.3.3 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1527.6.1 = {
	religion = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
1544.1.1 = {
	fort_14th = yes
} #Västerås Castle
#controller of trade route from the Swedish Mine Region
#a lot of weapon factories in this region
#Västmanlandsregemente
1625.1.1 = {
	fort_14th = no
	fort_15th = yes
}
#minor court belonging to Svea Hovrätt
1670.1.1 = { } #Due to the "Staplepolicy act"
#Due to the support of manufactories
1755.1.1 = {
	fort_15th = no
	fort_16th = yes
}
