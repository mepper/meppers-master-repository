owner = POL
controller = POL
add_core = POL

capital = "Sandomierz"
trade_goods = wheat
culture = polish
religion = catholic

hre = no

base_tax = 14
base_production = 0
base_manpower = 2

is_city = yes
temple = yes
local_fortification_1 = yes
road_network = yes
marketplace = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 20
	base_production = 1
	base_manpower = 2
}
1530.1.4 = {
	farm_estate = yes
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1656.1.1 = {
	controller = SWE
}
1660.1.1 = {
	controller = PLC
}
1795.10.24 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
} # Third partition
1809.10.14 = {
	owner = POL
	controller = POL
	remove_core = PRU
} # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = {
	controller = RUS
}
1814.4.11 = {
	controller = POL
}
1815.6.9 = {
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
