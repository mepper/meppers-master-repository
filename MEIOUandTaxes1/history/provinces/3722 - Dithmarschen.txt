# 3722 - Dithmarschen

owner = DIT
controller = DIT
add_core = DIT

capital = "Heide"
trade_goods = wheat
culture = old_saxon
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = {
	set_province_flag = freeholders_control_province
}
1356.1.1 = {
	add_core = SHL
	add_core = LAU
}
1500.1.1 = {
	road_network = yes
}
1533.1.1 = {
	religion = protestant
} # Council of the Forty-Eight turns the Ditmarsian Catholic Church into a Lutheran state church.
1559.1.1 = {
	owner = SHL
	controller = SHL
	add_core = DAN
}
1720.7.3 = {
	remove_core = DAN
} # Treaty of Frederiksborg
1773.1.1 = {
	add_core = DAN
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
