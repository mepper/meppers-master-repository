# 2637 - County of Mark

owner = MRK
controller = MRK
add_core = MRK

capital = "Hamm"
trade_goods = hemp
culture = westphalian
religion = catholic

hre = yes

base_tax = 10
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
town_hall = yes
marketplace = yes
warehouse = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 11
	base_production = 1
}
1521.3.15 = {
	owner = JBC
	controller = JBC
	add_core = JBC
	remove_core = MRK
}
1609.1.1 = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # The local line became extinct in the male line in 1609, when Kleve passed to the son-in-law, the elector of Brandenburg.
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king of Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Tilsit
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Congress of Vienna
