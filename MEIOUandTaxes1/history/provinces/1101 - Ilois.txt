# 1101 - Ilois

capital = "Diego Garcia"
trade_goods = unknown #fish

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 0
native_ferocity = 0
native_hostileness = 0

1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1510.1.1 = {
	discovered_by = POR
} # Diego Gracia
1780.1.1 = {
	owner = FRA
	controller = FRA
	citysize = 250
	trade_goods = slaves
	culture = francien
	religion = catholic
	set_province_flag = trade_good_set
}
1800.1.1 = {
	citysize = 425
}
1805.1.1 = {
	add_core = FRA
}
