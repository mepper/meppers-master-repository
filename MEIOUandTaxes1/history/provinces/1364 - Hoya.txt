# 1364 - Hoya

owner = HOY
controller = HOY
add_core = HOY

capital = "Hoya"
trade_goods = iron
culture = old_saxon
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1530.1.1 = {
	religion = protestant
}
1546.1.1 = {
	fort_14th = yes
}
1582.2.26 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = HOY
}
#1692.1.1 = {
#	owner = HAN
#	controller = HAN
#	add_core = HAN
#	remove_core = LUN
#}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
1866.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
}
