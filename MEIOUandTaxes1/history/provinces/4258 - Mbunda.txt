# No previous file for Mbunda

capital = "Mbunda"
trade_goods = livestock
culture = lunda
religion = animism

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 1
native_hostileness = 7

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
}