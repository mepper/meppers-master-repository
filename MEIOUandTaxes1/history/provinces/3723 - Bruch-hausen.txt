# 3723 - Bruch-hausen

owner = FRW
controller = FRW
add_core = FRW

capital = "Worms"
trade_goods = wine
culture = hessian
religion = catholic

hre = yes

base_tax = 2
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
workshop = yes
marketplace = yes
urban_infrastructure_1 = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1500.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 3
	base_production = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
