# 2279 - Settsu
# GG/LS - Japanese Civil War

owner = AKM
controller = AKM
add_core = AKM

capital = "Owosaka"
trade_goods = tea #chinaware
culture = kansai
religion = mahayana #shinbutsu
# Sakai

hre = no

base_tax = 18
base_production = 4
base_manpower = 2

is_city = yes
local_fortification_1 = yes
paved_road_network = yes
merchant_guild = yes
urban_infrastructure_1 = yes
workshop = yes
harbour_infrastructure_2 = yes
temple = yes

discovered_by = chinese

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = awesome_natural_place
	add_permanent_province_modifier = {
		name = "settsu_large_natural_harbor"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}
1501.1.1 = {
	base_tax = 32
	base_production = 7
	base_manpower = 4
}
1535.1.1 = {
	owner = MIY
	controller = MIY
	add_core = MIY
}
1542.1.1 = {
	discovered_by = POR
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	fort_14th = yes
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
