# No previous file for Myaamia

owner = MMI
controller = MMI
add_core = MMI

capital = "Myaamia"
trade_goods = unknown
culture = miami
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

native_size = 15
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1680.1.1 = {
	owner = IRO
	controller = IRO
	citysize = 100
	culture = iroquois
} # Taken by Iroquois in Beaver Wars.
1684.1.1 = { } # Nicolas Perrot
1701.8.14 = {
	owner = MMI
	controller = MMI
	is_city = yes
	culture = illini
} # Return of the Miami after the end of the Beaver Wars
1809.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	trade_goods = fur
	religion = protestant
	culture = american
} # Fort Lisa (actually on other side of river, but here for gameplay reasons)
