# 2936 - Touggourt

owner = TOG
controller = TOG
add_core = TOG

capital = "Touggourt"
trade_goods = pepper
culture = berber
religion = sunni

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 82 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1530.1.1 = {
	add_permanent_claim = ALG
}
1671.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
} # Virtually independent of the Ottoman empire
