# 3365 - Awa

owner = HKW
controller = HKW
add_core = HKW

capital = "Tokushima"
trade_goods = indigo #the only producer of indigo in Japan
culture = shikoku
religion = mahayana

hre = no

base_tax = 18
base_production = 0
base_manpower = 2

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = MIY
}
1450.1.1 = {
	owner = MIY
	controller = MIY
}
1501.1.1 = {
	base_tax = 31
	base_production = 1
	base_manpower = 3
}
1542.1.1 = {
	discovered_by = POR
}
1550.1.1 = {
	owner = CSK
	controller = CSK
	add_core = CSK
}
1585.1.1 = {
	owner = ODA
	controller = ODA
} # Hashiba (later Toyotomi) Hideyoshi invaded Shikoku with a force of 100,000 men, Chosokabe Motochika keeps only Tosa
1600.9.15 = {
	owner = TGW
	controller = TGW
} # Battle of Sekigahara
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
