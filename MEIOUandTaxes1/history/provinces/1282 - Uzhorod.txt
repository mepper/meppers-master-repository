# 1282 - Uzska

owner = HUN
controller = HUN
add_core = HUN

capital = "Uzhorod"
trade_goods = lumber
culture = slovak
religion = catholic

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1501.1.1 = {
	base_tax = 8
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = TRA
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1703.8.1 = {
	controller = TRA
} # The town supports Francis II R�k�czi in his rebellion against the Habsburgs
1711.5.1 = {
	controller = HAB
} # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
