# 13 - S�deschlesvige

owner = SHL
controller = SHL
add_core = SHL

capital = "Fl�nsburgh"
trade_goods = livestock
culture = danish
religion = catholic

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "slesvig_natural_harbour"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = danevirke
		duration = -1
	}
}
1356.1.1 = {
	add_core = DEN
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 5
	base_production = 1
	base_manpower = 0
}
1523.6.21 = {
	remove_core = DEN
	#add_core = DAN
}
1530.1.1 = {
	religion = protestant
	culture = old_saxon
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1644.1.12 = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13 = {
	controller = SHL
} #The Peace of Br�msebro
1657.10.23 = {
	controller = SWE
} #Karl X Gustavs First Danish War-Captured by Wrangel
1658.2.26 = {
	controller = SHL
} #The Peace of Roskilde - Duchy fully independent
1713.3.13 = {
	owner = DAN
	controller = DAN
} # With Siege of T�nning, Denmark takes back control over entire Slesvig
1720.7.3 = {
	remove_core = SHL
} #
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
