# 1844 - Marienburg

owner = TEU
controller = TEU
add_core = TEU

capital = "Marienburg" # and Elbing
trade_goods = wheat
culture = prussian
religion = catholic

hre = no

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
road_network = yes
harbour_infrastructure_1 = yes
marketplace = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

450.1.1 = {
	add_permanent_province_modifier = {
		name = ordensburg_marienburg
		duration = -1
	}
}
1444.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1454.3.6 = {
	add_core = POL
} # Beginning of the "Thirteen years war"
1466.10.19 = {
	owner = POL
	controller = POL
	remove_core = TEU
	culture = prussian
	capital = "Elbing"
} # Peace treaty, "Peace of Torun"
1467.1.1 = {
	unrest = 7
	controller = REB
} # War of the Priests
1479.1.1 = {
	controller = POL
} # First treaty of Piotrk�w
1512.1.1 = {
	unrest = 0
} # Second treaty of Piotrk�w
1520.5.5 = {
	base_tax = 3
	base_production = 1
	base_manpower = 0
}
1524.1.25 = {
	unrest = 6
} # Debt crisis
1525.1.1 = {
	unrest = 0
	religion = protestant
}
1525.4.10 = {
	#add_core = PRU
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1572.1.1 = {
	unrest = 6
} # Sigismund II dies
1576.1.1 = {
	unrest = 8
} # Danzig rebellion
1577.6.13 = {
	controller = PLC
} # Danzig War, under siege by Poland
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
	unrest = 0
} # Coronation of Sigismund III
1600.1.1 = {
	fort_14th = yes
}
1772.8.5 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # First partition of Poland
