# 2392 - Zahumje

owner = BOS
controller = BOS
add_core = BOS

capital = "Mostar"
trade_goods = wool
culture = croatian
religion = gnostic

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1441.1.1 = {
	owner = HUM
	controller = HUM
	add_core = HUM
	remove_core = BOS
}
1444.1.1 = {
	add_core = TUR
}
1482.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HUM
	add_core = BOS
	culture = bosnian
} # The Ottoman province of Hezergovina
1520.5.5 = {
	base_tax = 2
	base_production = 0
	base_manpower = 0
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1650.1.1 = {
	culture = bosnian
	religion = sunni
}
1700.1.1 = {
	unrest = 7
}
