# No previous file for Tuk�ti

capital = "Tuk�ti"
trade_goods = unknown
culture = ge
religion = pantheism

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

native_size = 45
native_ferocity = 1
native_hostileness = 3

450.1.1 = {
	set_province_flag = tribals_control_province
}
1500.1.1 = {
	base_tax = 1
	native_size = 15
}
1728.9.18 = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	citysize = 280
	culture = portugese
	religion = catholic
	trade_goods = gems
	change_province_name = "Diamantino"
	rename_capital = "Diamantino"
}
1750.1.1 = {
	add_core = BRZ
	culture = brazilian
	citysize = 1280
}
1822.9.7 = {
	owner = BRZ
	controller = BRZ
}
1825.1.1 = {
	remove_core = POR
}
