# 3369 - Osumi

owner = SMZ
controller = SMZ
add_core = SMZ

capital = "Kirishima"
trade_goods = tea #green tea
culture = kyushu
religion = mahayana

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese

1376.1.1 = {
	add_core = IGW
	controller = IGW
	owner = IGW
}
1391.1.1 = {
	owner = SMZ
	controller = SMZ
}
1501.1.1 = {
	base_tax = 12
	base_manpower = 1
}
1542.1.1 = {
	discovered_by = POR
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
