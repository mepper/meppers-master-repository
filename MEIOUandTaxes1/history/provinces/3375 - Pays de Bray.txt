# No previous file for Pays de Bray

owner = FRA
controller = FRA

capital = "Rouen"
trade_goods = wheat
culture = normand
religion = catholic

hre = no

base_tax = 16
base_production = 3
base_manpower = 1

is_city = yes
temple = yes
urban_infrastructure_1 = yes
marketplace = yes
corporation_guild = yes
road_network = yes
fort_14th = yes

# La Notre Dame de Rouen

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1066.1.1 = {
	owner = ENG
	controller = ENG
}
1204.6.24 = {
	owner = FRA
	controller = FRA
}
1343.1.1 = {
	add_core = NRM
	add_core = FRA
	add_core = NAV
	add_core = ENG
}
1378.1.1 = {
	remove_core = NAV
}
1419.1.19 = {
	owner = ENG
	controller = ENG
}
1429.7.17 = {
	owner = ENG
	controller = ENG
}
1449.1.1 = {
	owner = FRA
	controller = FRA
}
1467.6.15 = {
	add_core = BUR
} # Charles the Bold ascends to the throne and lays claims
1471.1.1 = {
	controller = BUR
} # Charles the Bold invades France and ravages the country as far as Rouen
1471.8.1 = {
	controller = FRA
} # Charles the Bold has to retire
1475.8.29 = {
	remove_core = ENG
} # Treaty of Picquigny, ending the Hundred Year War
1477.1.5 = {
	remove_core = BUR
} # Charles the Bold dies at Nancy
1520.5.5 = {
	base_tax = 17
	base_production = 6
	base_manpower = 1
}
1525.1.1 = {
	fort_14th = yes
}
1571.1.1 = {
	unrest = 5
} # Unrest spreads in catholic territory: massacres of protestants in Rouen, Orange & Paris
1574.4.1 = {
	unrest = 0
} # Charles IX dies, situation cools a bit
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
# Henri IV's quest to eliminate corruption and establish state control
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1625.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1631.1.1 = {
	unrest = 5
} # Region is rebellious until about 1639
1641.1.1 = {
	unrest = 0
}
1660.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1680.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Vauban's 'pointed' fort in Dieppe

