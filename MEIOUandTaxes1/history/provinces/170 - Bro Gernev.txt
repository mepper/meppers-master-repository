# 170 Kernev- Principal cities: Brest, saint pol, quimper,
# MEIOU-GG - Hundred Year War

owner = BRI
controller = BRI
add_core = BRI

capital = "Brest"
trade_goods = fish # naval_supplies
culture = breton
religion = catholic

hre = no

base_tax = 20
base_production = 1
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes
temple = yes
town_hall = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "finistere_natural_harbour"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}
1341.4.30 = {
	owner = MNF
	controller = MNF
	add_core = MNF
	add_core = BLO
	remove_core = BRI
} # Jean III de Bretagne dies in Caen
1365.4.12 = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1 = {
	add_core = FRA
} # Charles V invades Brittany without resistance
1520.5.5 = {
	base_tax = 24
	base_production = 1
	base_manpower = 1
	military_harbour_2 = yes
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1530.8.4 = {
	owner = FRA
	controller = FRA
} # Union Treaty
1550.1.1 = {
	fort_14th = yes
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism

# Henri IV's quest to eliminate corruption and establish state control
# Brest begins to become a major naval base under the influence of Richelieu
1640.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1670.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1680.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Vauban's 'pointed' fort in Brest
# Le Saint-Louis
1793.3.7 = { } # Guerres de l'Ouest
1796.12.23 = { } # The last rebels are defeated at the battle of Savenay
1799.10.15 = { } # Guerres de l'Ouest
