# No previous file for Nytra (Nytra + Skalica + Kremnica)

owner = HUN
controller = HUN
add_core = HUN

capital = "Kremnica"
trade_goods = iron
culture = slovak
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
urban_infrastructure_1 = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = iron
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1790.1.1 = {
	fort_15th = no
} # The town, along with the castle, burns down
