# 2624 - Valtulina

owner = MLO
controller = MLO
add_core = MLO

capital = "Sundri"
trade_goods = livestock
culture = lombard
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1453.1.1 = {
	add_core = FRA
}
# University of Milano est. Before that: Pavia est. 1361
1499.1.1 = {
	controller = FRA
} # Louis XII of France invades...
1499.6.1 = {
	owner = FRA
} # ...and seizes Milan
1500.1.1 = {
	road_network = yes
}
1512.1.1 = {
	owner = GBN
	controller = GBN
	add_core = GBN
	remove_core = FRA
	remove_core = MLO
}
1520.5.5 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1535.1.1 = {
	add_core = SPA
	remove_core = MLO
	unrest = 3
}
1618.1.1 = {
	hre = no
}
1714.9.7 = {
	add_core = HAB
	remove_core = SPA
}
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = HAB
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
