# 1031 - Tajima
# GG/LS - Japanese Civil War

owner = YMN
controller = YMN
add_core = YMN

capital = "Izushi"
trade_goods = livestock
culture = chugoku
religion = mahayana #shinbutsu

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 10
}
1542.1.1 = {
	discovered_by = POR
}
1582.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
