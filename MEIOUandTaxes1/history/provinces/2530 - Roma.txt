# 2530 - Roma

owner = PAP
controller = PAP
add_core = PAP

capital = "Roma"
trade_goods = olive #services
culture = umbrian
religion = catholic

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

is_city = yes
marketplace = yes
urban_infrastructure_1 = yes
art_corporation = yes # Roman School
local_fortification_1 = yes
great_temple = yes
road_network = yes
small_university = yes #La Sapienza

extra_cost = 10

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1100.1.1 = {
	set_province_flag = pocket_province
} # City is ruined at this time...
1309.1.1 = {
	add_core = PA2
	owner = PA2
	controller = PA2
}
1356.1.1 = {
	unrest = 5
}
1378.1.1 = {
	remove_core = PA2
	owner = PAP
	controller = PAP
	save_global_event_target_as = the_vatican
}
1378.3.27 = {
	unrest = 0
}
1420.1.1 = {
	fine_arts_academy = yes
	art_corporation = no
	#library = yes # Vatican Library
}
1520.5.5 = {
	base_tax = 2
	base_production = 6
	base_manpower = 1
}
1530.1.1 = {
	road_network = no
	paved_road_network = yes
}
1750.1.1 = {
	add_core = ITA
}
1809.4.10 = {
	controller = FRA
} # Occupied by French troops
1809.10.14 = {
	owner = FRA
	add_core = FRA
} # Treaty of Schönbrunn
1814.4.11 = {
	owner = PAP
	controller = PAP
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1870.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
