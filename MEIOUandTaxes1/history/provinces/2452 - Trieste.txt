# 2452 - Trieste

owner = TTE
controller = TTE
add_core = TTE

capital = "Triest"
trade_goods = wine
culture = friulian
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1356.1.1 = {
#	add_claim = VEN
#	add_claim = HAB
#	owner = AQU
#	controller = AQU
#	add_core = AQU
#}
1368.1.1 = {
	controller = VEN
}
1372.1.1 = {
	controller = TTE
}
1382.11.1 = {
	owner = STY
	controller = STY
	add_core = STY
	#remove_core = AQU
	hre = yes
}
1490.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = STY
}
1508.3.1 = {
	controller = VEN
} #Venice seizes Trieste after Austrian invasion
1508.4.1 = {
	owner = VEN
} #Treaty between Austria and Venice cedes Trieste and Fiume
1508.12.10 = {
	controller = HAB
} #Austria retakes Trieste after outbreak of War of Leage of Cambrai
1516.12.1 = {
	owner = HAB
} #Treaty of Brussels
1520.5.5 = {
	base_tax = 4
	base_production = 0
	base_manpower = 0
}
1550.1.1 = {
	fort_14th = yes
} # The fort is rebuilt
# 1719 - declared a free port
1797.1.1 = {
	controller = FRA
}
1797.10.17 = {
	controller = HAB
} # Treaty of Campo Formio
1800.1.1 = {
	culture = venetian
}
1805.1.1 = {
	controller = FRA
}
1805.12.26 = {
	controller = HAB
} # Treaty of Pressburg
1809.8.1 = {
	controller = FRA
}
1809.10.14 = {
	owner = FRA
	add_core = FRA
}
1813.9.20 = {
	controller = HAB
} # Occupied by Austrian forces
1814.4.6 = {
	owner = HAB
	remove_core = FRA
} # Napoleon abdicates
