# 2295 - Wakamatsu
# GG/LS - Japanese Civil War

owner = KTK
controller = KTK
add_core = KTK

capital = "Kurokawa"
trade_goods = rice
culture = tohoku
religion = mahayana #shinbutsu

hre = no

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 21
	base_production = 2
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
