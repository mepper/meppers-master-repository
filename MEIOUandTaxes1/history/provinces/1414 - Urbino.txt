# 1414 - Urbino

owner = URB
controller = URB
add_core = URB

capital = "Urb�n"
trade_goods = wool
culture = romagnol
religion = catholic

hre = no

base_tax = 1
base_production = 2
base_manpower = 0

is_city = yes
temple = yes
road_network = yes
urban_infrastructure_1 = yes
workshop = yes
local_fortification_1 = yes
marketplace = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = "republic_of_san_marino"
		duration = -1
	}
}
1369.1.1 = {
	controller = PAP
	owner = PAP
	add_core = PAP
}
1375.1.1 = {
	controller = URB
	owner = URB
}
1502.12.8 = {
	owner = PAP
	controller = PAP
	fort_14th = yes
}
1503.8.28 = {
	owner = URB
	controller = URB
}
1520.5.5 = {
	base_tax = 3
	base_production = 1
	base_manpower = 0
}
1523.1.1 = {
	rename_capital = "P�s're"
	road_network = no
	paved_road_network = yes
}
1626.1.1 = {
	controller = PAP
	owner = PAP
	remove_core = URB
} # Annexed to the Holy See
1805.3.17 = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Treaty of Pressburg
1814.4.11 = {
	owner = PAP
	controller = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
