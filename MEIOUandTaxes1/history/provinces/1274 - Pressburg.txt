# 1274 - Pressburg

owner = HUN
controller = HUN
add_core = HUN

capital = "Pressburg"
trade_goods = wine
culture = slovak
religion = catholic

hre = no

base_tax = 19
base_production = 1
base_manpower = 1

is_city = yes
 # Saint Martin's Cathedral
local_fortification_1 = yes
town_hall = yes
road_network = yes
marketplace = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1452.1.1 = {
	temple = yes
}
1465.1.1 = {
	small_university = yes
}
1520.5.5 = {
	base_tax = 18
	base_production = 2
	base_manpower = 1
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	fort_14th = no
	fort_15th = yes
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
