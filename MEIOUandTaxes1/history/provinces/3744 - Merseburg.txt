# 3744 - Merseburg

owner = MBG
controller = MBG
add_core = MBG

capital = "Merseburg"
trade_goods = wheat
culture = high_saxon
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1100.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1500.1.1 = {
	road_network = yes
}
1544.1.1 = {
	religion = protestant
}
1565.1.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
}
