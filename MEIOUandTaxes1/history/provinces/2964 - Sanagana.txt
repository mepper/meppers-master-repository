# 2964 - Sanagana

owner = MAL
controller = MAL
add_core = MAL

capital = "Awlil"
trade_goods = palm_date
culture = hassaniya
religion = sunni

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 81 }
	set_province_flag = mined_goods
	set_province_flag = salt
}
1437.1.1 = {
	discovered_by = POR
} # Cadamosto