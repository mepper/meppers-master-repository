# 2972 Arrif

owner = FEZ
controller = FEZ
add_core = FEZ

capital = "Taounat"
trade_goods = lumber
culture = fassi
religion = sunni

hre = no

base_tax = 6
base_production = 1
base_manpower = 0

is_city = yes
temple = yes
town_hall = yes

discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1500.3.3 = {
	base_tax = 5
	base_production = 2
}
1530.1.1 = {
	add_core = MOR
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1664.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
1672.1.1 = {
	unrest = 4
} # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = {
	unrest = 0
}
