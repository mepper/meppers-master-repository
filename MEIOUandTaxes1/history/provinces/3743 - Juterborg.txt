# 3743 - J�terbog

owner = MAG
controller = MAG
add_core = MAG

capital = "Juterborg"
trade_goods = wheat
culture = low_saxon
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1524.1.1 = {
	religion = protestant
}
1648.10.24 = {
	owner = SAX
	controller = SAX
	add_core = SAX
} # Peace of Westphalia
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} # Congress of Vienna
