# 2849 - Pommern-Colberg

owner = POM
controller = POM
add_core = POM

capital = "Colberg"
trade_goods = wheat
culture = kashubian # pommeranian
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1295.1.1 = {
	owner = PKA
	controller = PKA
	add_core = PKA
	remove_core = POM
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 5
}
1529.1.1 = {
	fort_14th = yes
}
1545.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PKA
	religion = protestant
	culture = pommeranian
}
1630.1.1 = {
	unrest = 7
}#Devastating population losses in Thirty Years War
1648.10.24 = {
	owner = BRA
	controller = BRA
	add_core = BRA
	remove_core = PKA
}
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
}
1750.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1775.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
