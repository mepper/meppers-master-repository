# 253 - Ross

owner = SCO
controller = SCO
add_core = SCO

capital = "Inbhir Nis" # Inverness
trade_goods = lumber
culture = highland_scottish
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	add_permanent_province_modifier = {
		name = "highlands_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = TLI
}
1412.1.1 = {
	owner = TLI
	controller = TLI
}
1424.1.1 = {
	owner = SCO
	controller = SCO
}
1437.1.1 = {
	owner = TLI
	controller = TLI
}
1476.1.1 = {
	owner = SCO
	controller = SCO
}
1493.1.1 = {
	remove_core = TLI
}
1520.5.5 = {
	base_tax = 2
	base_production = 0
	base_manpower = 0
}
1560.8.1 = {
	religion = reformed
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
