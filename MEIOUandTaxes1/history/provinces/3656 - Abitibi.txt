# No previous file for Abitibi

capital = "Abitibi"
trade_goods = unknown
culture = cree
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	owner = WCR
	controller = WCR
	add_core = WCR
	trade_goods = fur
	is_city = yes
} # Before the Beaver Wars
