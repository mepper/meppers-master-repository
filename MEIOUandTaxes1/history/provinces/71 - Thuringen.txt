# 71 - Thuringen

owner = THU
controller = THU
add_core = THU

capital = "Gotha"
trade_goods = indigo #Woad
culture = thuringian
religion = catholic

hre = yes

base_tax = 5
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
local_fortification_1 = yes
marketplace = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1440.1.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
} # Duchy of Thuringia is inherited by Saxony
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1524.1.1 = {
	religion = protestant
}
1546.1.1 = {
	fort_14th = yes
}
1547.5.19 = {
	owner = THU
	controller = THU
	add_core = THU
	remove_core = SAX
}
1572.11.6 = {
	owner = SWR
	controller = SWR
	add_core = SWR
	remove_core = THU
}
1640.1.1 = {
	owner = SGO
	controller = SGO
	add_core = SGO
	remove_core = SWR
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
