# 660 - Gyalthang

owner = DLI
controller = DLI
add_core = DLI

capital = "Tong'an"
trade_goods = wool
culture = lisu
religion = animism

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1253.1.1 = {
	owner = YUA
	controller = YUA
}
1274.1.1 = {
	add_core = YUA
} #creation of yunnan_area province
1350.1.1 = {
	owner = DLI #actually never under Mong Mao kingdom
	controller = DLI
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1369.3.17 = {
	marketplace = yes
	courthouse = yes
}
1383.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1520.1.1 = {
	base_tax = 8
	base_production = 1
}
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
