# UAG - Ouagadougou

government = tribal_monarchy_elective government_rank = 1
mercantilism = 0.0
primary_culture = mossi
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 1495 # Wagadugu
fixed_capital = 1495 # Wagadugu
historical_rival = MAL

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 0
	set_country_flag = no_cavalry
}

1337.1.1 = {
	monarch = {
		name = "Koundoumie"
		dynasty = "Ouagadougou"
		dip = 3
		adm = 3
		mil = 3
	}
}