# Country: IDN - Yavapeh
# MEIOU-FB Indonesia
# Unification nation
# 2010-jan-16 - FB - HT3 changes

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
technology_group = austranesian
primary_culture = javan
religion = sunni
capital = 2108   # Kalapa (Jakarta)

1000.1.1 = {
	add_country_modifier = { name = title_6 duration = -1 }
	set_country_flag = title_6
	#set_variable = { which = "centralization_decentralization" value = -1 }
	add_absolutism = -100
	add_absolutism = 40
}
