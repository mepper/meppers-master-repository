# EST - Estland

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = estonian
religion = catholic
capital = 36

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}
1542.1.1 = {
	religion = protestant
}
