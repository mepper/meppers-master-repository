#WES - Westfalia
#MEIOU-GG Governemnt changes
#10.01.27 FB-HT3 - make HT3 changes

government = enlightened_despotism government_rank = 1
mercantilism = 0.0
primary_culture = old_saxon
religion = protestant
technology_group = western
capital = 81	# Kassel

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1807.7.9 = {
	monarch = {
		name = "J�r�me"
		dynasty = "Bonaparte"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}