# NRM - Alen�on

government = medieval_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = francien
add_accepted_culture = normand
religion = catholic
capital = 1386	# Alen�on

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 20
}

1346.1.1   = {
	monarch = {
		name = "Charles III"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	set_country_flag = french_apanage
	set_country_flag = appanage_subj
#	add_country_modifier = {
#		name = "feudal_structure"
#		duration = -1
#	}
}

1367.1.1   = {
	monarch = {
		name = "Pierre II"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1404.1.1   = {
	monarch = {
		name = "Jean I"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1414.1.1   = {
	government = feudal_monarchy remove_country_modifier = title_1 clr_country_flag = title_1 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}

1415.10.25 = {
	monarch = {
		name = "Jean II"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

#1439.1.1 = {
#	remove_country_modifier = "feudal_structure"
#	add_country_modifier = {
#		name = "feudal_army_organisation"
#		duration = -1
#	}
#} # The Birth of a Permanent Taxation System

#1445.1.1 = {
#	remove_country_modifier = "feudal_army_organisation"
#} # The Creation of the First Permanent Army

1476.1.1   = {
	monarch = {
		name = "Ren�"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1492.1.1   = {
	monarch = {
		name = "Charles IV"
		dynasty = "de Valois"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1515.1.1   = {
	government = despotic_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}

1525.1.1   = {
	clr_country_flag = french_apanage
	clr_country_flag = appanage_subj
}

1589.8.3   = {
	government = administrative_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}

1661.3.9 = {
	government = absolute_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}
