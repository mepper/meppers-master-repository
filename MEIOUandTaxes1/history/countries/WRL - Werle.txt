# WRL - Werle

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = pommeranian
capital = 57

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1234.1.1 = {
	monarch = {
		name = "Nikolaus I"
		dynasty = "von Mecklenburg"
		birth_date = 1210.1.1
		ADM = 5
		DIP = 3
		MIL = 2
	}
}

1245.1.1 = {
	heir = {
		name = "Heinrich"					# Eldest son of Nikolaus II
		monarch_name = "Heinrich I"
		dynasty = "von Mecklenburg"
		birth_date = 1245.1.1
		death_date = 1291.10.8
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1277.5.15 = {
	monarch = {
		name = "Heinrich I"					# After death of Nikolaus, his three sons ruled jointly until 1281 when they decided to divide the land between themselves
		dynasty = "von Mecklenburg"
		birth_date = 1245.1.1
		ADM = 3
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Nikolaus"					# Eldest son of Heinrich I
		monarch_name = "Nikolaus II"
		dynasty = "von Mecklenburg"
		birth_date = 1270.1.1				# estimated
		death_date = 1299.1.1
		claim = 95
		ADM = 3
		DIP = 1
		MIL = 5
	}
}

# 1281 partition of Werle into Werle-G�strow, Werle-Parchim (Johann I) and Werle-Prisannewitz (Bernhard I)

# 1286 unmarried Bernhard I dies without heir, Prisannewitz falls back to Werle-G�strow

# 1291 Nikolaus assassinated his father together with his brother Heinrich as they feared for their heritage rights after their father remarried, were dethroned and hunted by their cousin Nikolaus II of Werle-Parchim, who subsequently unites the lands of Werle under his rule

1291.1.1 = {
	monarch = {
		name = "Nikolaus II"
		dynasty = "von Mecklenburg"
		birth_date = 1270.1.1
		ADM = 3
		DIP = 1
		MIL = 5
	}
	heir = {
		name = "Heinrich"					# second son of Heinrich I, younger brother of Nikolaus II
		monarch_name = "Heinrich II"
		dynasty = "von Mecklenburg"
		birth_date = 1275.1.1				# estimated
		death_date = 1309.1.1
		claim = 95
		ADM = 3
		DIP = 1
		MIL = 5
	}
}

1292.1.1 = {
	monarch = {
		name = "Nikolaus II"				# nephew of Heinrich I, before was Herr of Werle-Parchim since 1283, unites lands of Werle under his rule
		dynasty = "von Mecklenburg"
		birth_date = 1274.1.1
		ADM = 4
		DIP = 5
		MIL = 5
	}
}
1299.1.1 = {
	heir = {
		name = "Johann"						# Eldest son of Nikolaus II
		monarch_name = "Johann III"
		dynasty = "von Mecklenburg"
		birth_date = 1299.1.1
		death_date = 1352.8.28
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

# 1316 after death of Nikolaus II the lands of Werle were partitioned once again between his son Johann III (Werle-Goldberg) and his brother Johann II (Werle-G�strow)

1316.2.19 = {
	monarch = {
		name = "Johann III"
		dynasty = "von Mecklenburg"
		birth_date = 1299.1.1
		ADM = 3
		DIP = 4
		MIL = 2
	}
}
1320.1.1 = {
	heir = {
		name = "Johann"					# first son of Johann III
		monarch_name = "Johann IV"
		dynasty = "von Mecklenburg"
		birth_date = 1320.1.1				# estimated
		death_date = 1341.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1341.1.2 = {
	heir = {
		name = "Nikolaus"					# second son of Johann III
		monarch_name = "Nikolaus IV"
		dynasty = "von Mecklenburg"
		birth_date = 1330.1.1
		death_date = 1354.11.13
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

# 1347 after the death of Johann II of Werle-G�strow, his two sons Nikolaus III and Bernhard II further divide the land into G�strow and Waren

1350.1.1 = {
	monarch = {
		name = "Nikolaus IV"				# replaced his father who stepped down because of bad health
		dynasty = "von Mecklenburg"
		birth_date = 1330.1.1
		ADM = 3
		DIP = 4
		MIL = 3
	}
	heir = {
		name = "Johann"						# only son of Johann Nikolaus IV
		monarch_name = "Johann IV"
		dynasty = "von Mecklenburg"
		birth_date = 1349.1.1
		death_date = 1374.12.14
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1354.11.14 = {
	monarch = {
		name = "Nikolaus III"				# of Werle-G�strow, custodian for his nephew Johann
		dynasty = "von Mecklenburg"
		birth_date = 1322.1.1
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Johann"
		monarch_name = "Johann IV"
		dynasty = "von Mecklenburg"
		birth_date = 1349.1.1
		death_date = 1374.12.14
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

#1354.11.14 = {
#	monarch = {
#		name = "Bernhard II"				# of Werle-Waren, younger brother of Nikolaus III (Werle-G�strow) and uncle to Johann IV (Werle-Goldberg), gains Goldberg after his nephew died childless > Waren-Goldberg
#		dynasty = "von Mecklenburg"
#		birth_date = 1320.1.1
#		ADM = 3
#		DIP = 5
#		MIL = 3
#	}
#	heir = {
#		name = "Johann"
#		monarch_name = "Johann VI"			# son of Bernhard II
#		dynasty = "von Mecklenburg"
#		birth_date = 1342.1.1
#		death_date = 1385.10.16
#		claim = 95
#		ADM = 4
#		DIP = 4
#		MIL = 2
#	}
#}

1361.1.2 = {
	monarch = {
		name = "Johann IV"
		dynasty = "von Mecklenburg"
		birth_date = 1349.1.1
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1382.4.14 = {
	monarch = {
		name = "Johann VI"
		dynasty = "von Mecklenburg"
		birth_date = 1342.1.1
		ADM = 4
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Nikolaus"					# eldest son of Johann VI
		monarch_name = "Nikolaus VI"
		dynasty = "von Mecklenburg"
		birth_date = 1362.1.1
		death_date = 1408.1.22
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1385.10.17 = {
	monarch = {
		name = "Nikolaus VI"
		dynasty = "von Mecklenburg"
		birth_date = 1362.1.1
		ADM = 3
		DIP = 4
		MIL = 3
	}
	heir = {
		name = "Christoph"					# second son of Johann VI, younger brother of Nikolaus VI
		monarch_name = "Christoph V"
		dynasty = "von Mecklenburg"
		birth_date = 1373.1.1
		death_date = 1425.8.25
		claim = 95
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1408.1.23 = {
	monarch = {
		name = "Christoph V"
		dynasty = "von Mecklenburg"
		birth_date = 1373.1.1
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1425.8.26 = {
	monarch = {
		name = "Wilhelm"					# of Werle-G�strow, inherited Waren-Goldberg after Christoph V died without heir, thus all lands of house Werle are united again
		dynasty = "von Mecklenburg"
		birth_date = 1393.1.1
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

# 1436.9.8 Wilhelm, last member of house Werle, dies without heir, thus the lands of Werle fall back to Mecklenburg, G�strow to Schwerin/ Waren to Stargard

