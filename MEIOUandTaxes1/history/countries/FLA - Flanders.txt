# FLA - Duchy of Flanders

government = feudal_monarchy government_rank = 1
mercantilism = 10
technology_group = western
religion = catholic
primary_culture = flemish
add_accepted_culture = lorrain
capital = 90
historical_friend = BUR

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1322.1.1 = {
	monarch = {
		name = "Louis I"
		dynasty = "de Dampierre"
		DIP = 3
		ADM = 5
		MIL = 3
	}
}

1330.10.25 = {
	heir = {
		name = "Louis"
		monarch_name = "Louis II"
		dynasty = "de Dampierre"
		birth_date = 1330.10.25
		death_date = 1384.1.30
		claim = 95
		DIP = 3
		ADM = 5
		MIL = 3
	}
}

1346.1.1   = {
	monarch = {
		name = "Louis II"
		dynasty = "de Dampierre"
		DIP = 3
		ADM = 5
		MIL = 3
	}
}

1350.4.13  = {
	heir = {
		name = "Margarethe"
		monarch_name = "Margarethe III"
		dynasty = "de Dampierre"
		birth_date = 1350.4.13
		death_date = 1405.3.21
		claim = 95
		DIP = 3
		ADM = 5
		MIL = 3
		female = yes
	}
}

1384.1.30 = {
	monarch = {
		name = "Margarethe III"
		dynasty = "de Dampierre"
		DIP = 3
		ADM = 5
		MIL = 3
		female = yes
	}
	remove_accepted_culture = lorrain
}

# Incorporated to the duchy of Burgundy through her marriage with Philippe the Bold
