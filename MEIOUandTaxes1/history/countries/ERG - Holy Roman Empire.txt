# ERG - Holy Roman Empire

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = frankish
religion = catholic
capital = 81

1000.1.1 = {
	add_country_modifier = { name = title_6 duration = -1 }
	set_country_flag = title_6
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
1750.1.1 = { government = constitutional_republic }
