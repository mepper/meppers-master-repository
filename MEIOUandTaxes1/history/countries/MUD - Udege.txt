# MUD - Udege

government = tribal_nomads_hereditary government_rank = 1
mercantilism = 0.0
technology_group = steppestech
primary_culture = jurchen
religion = tengri_pagan_reformed
capital = 3247 # Ussuri

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1 = { #fictional
	monarch = {
		name = "Abuga"
		dynasty = "Udege"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
