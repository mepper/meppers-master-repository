# County of Freiburg
# Tag : FRE

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = rhine_alemanisch
religion = catholic
capital = 69	# Breisgau

historical_rival = HAB

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1316.1.1 = {
	monarch = {
		name = "Konrad II"
		dynasty = "von Urach"
		birth_date = 1300.1.1
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

1350.1.1 = {
	monarch = {
		name = "Friedrich"
		dynasty = "von Urach"
		birth_date = 1319.1.1
		ADM = 3
		DIP = 2
		MIL = 4
	}
	queen = {
		country_of_origin = FRE
		name = "Clara"
		dynasty = "von Hachberg"
		birth_date = 1321.5.8
		death_date = 1386.9.29
		female = yes
		ADM = 3
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Egino"
		monarch_name = "Egino III"
		dynasty = "von Urach"
		birth_date = 1342.1.1
		death_date = 1385.9.16
		claim = 95
		ADM = 6
		DIP = 3
		MIL = 3
	}
}

1356.1.1 = {
	monarch = {
		name = "Clara von Hachberg"
		dynasty = "von Hachberg"
		birth_date = 1321.5.8
		female = yes
		regent = yes
		ADM = 3
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Egino"
		monarch_name = "Egino III"
		dynasty = "von Urach"
		birth_date = 1342.1.1
		death_date = 1385.9.16
		claim = 95
		ADM = 6
		DIP = 3
		MIL = 3
	}
}

1358.1.1 = {
	monarch = {
		name = "Egino III"
		dynasty = "von Urach"
		birth_date = 1319.4.26
		ADM = 6
		DIP = 3
		MIL = 3
	}
}

# 1368 - People buy freedom and seek protection from the Habsburgs

1530.1.1 = {
	remove_historical_rival = HAB
	primary_culture = high_alemanisch
}
