# LAT - Latin Empire

government = despotic_monarchy
government_rank = 6
mercantilism = 0.1
technology_group = western
primary_culture = frankish
religion = catholic
capital = 1402

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}
