# BAW - Bayern-Ingolstadt
# 2009-dec-31 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = bavarian
religion = catholic
capital = 1509

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

#1392 Bavaria-Landshut split into Bavaria-Landshut Bavaria-Ingolstadt Bavaria-Munich
1392.1.1 = {
	monarch = {
		name = "Stephan III der Pr�chtige"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	set_country_flag = wittelsbach_succession
}

1413.9.26 = {
	monarch = {
		name = "Ludwig VII der B�rtige"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1443.5.1 = {
	monarch = {
		name = "Ludwig VIII der H�ckrige"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

#1445.4.7 => Annexation by the duchy of Bayern-Landshut
