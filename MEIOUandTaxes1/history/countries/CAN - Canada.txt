# CAN - Canada

government = constitutional_republic government_rank = 1
mercantilism = 0.0
technology_group = western
religion = protestant
primary_culture = american
capital = 991	# Ottawa

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -1 }
	add_absolutism = -100
	add_absolutism = 60
}
