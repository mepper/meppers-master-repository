# AFA - Sultanate of Aussa

government = feudal_monarchy government_rank = 1
mercantilism = 10
primary_culture = afar
religion = sunni
technology_group = east_african unit_type = soudantech
capital = 1315 # Dawe

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
}
1356.1.1 = {
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
1530.1.1   = {
	monarch = {
		name = "'Uthman"
		dynasty = "Walashma"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1734.1.1   = {
	monarch = {
		name = "Kaxxafo I"
		dynasty = "Mudaito"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1749.1.1   = {
	monarch = {
		name = "Kaxxafo II"
		dynasty = "Mudaito"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1779.1.1   = {
	monarch = {
		name = "Aydacis I"
		dynasty = "Mudaito"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1801.1.1   = {
	monarch = {
		name = "Aydacis II"
		dynasty = "Mudaito"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1832.1.1   = {
	monarch = {
		name = "Canfaxe I"
		dynasty = "Mudaito"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}
