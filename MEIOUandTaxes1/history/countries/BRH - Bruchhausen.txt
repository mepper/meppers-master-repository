# BRH - Bruchhausen

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = old_saxon
religion = catholic
capital = 3723

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1324.1.1 = {
	monarch = {
		name = "Gerhard"
		dynasty = "von Bruchhausen"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
