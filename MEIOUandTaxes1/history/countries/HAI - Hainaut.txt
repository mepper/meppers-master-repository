# HAI - Hainaut

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = wallonian
capital = 91	# Mons

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 30
}

1280.2.10 = {
	monarch = {
		name = "John II"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 3
		MIL = 1
	}
	heir = {
		name = "Guillaume"
		monarch_name = "Guillaume"
		dynasty = "von Wittelsbach"
		birth_date = 1260.1.1
		death_date = 1337.6.7
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

#1299.11.10 John unites Holland and Hainaut in personal union. Holland and Hainaut until 1433.1.1 when absorbed by Burgundy

1304.8.22 = {
	monarch = {
		name = "Guillaume"
		dynasty = "von Wittelsbach"
		ADM = 5
		DIP = 4
		MIL = 3
	}
	heir = {
		name = "Guillaume"
		monarch_name = "Guillaume II"
		dynasty = "von Wittelsbach"
		birth_date = 1300.1.1
		death_date = 1345.9.26
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 4
	}
} #Guillaume I of Hainaut & William III of Holland

1337.6.7 = {
	monarch = {
		name = "Guillaume II"
		dynasty = "von Wittelsbach"
		ADM = 2
		DIP = 2
		MIL = 4
	}
} #Guillaume II of Hainaut & William IV of Holland

1345.9.26 = {
	monarch = {
		name = "Margaret II"
		dynasty = "von Wittelsbach"
		ADM = 5
		DIP = 5
		MIL = 2
		female = yes
	}
} #Margaret II of Hainaut & Margaret I of Holland
#Margaret was married to HRE William IV the Bavarian
#she was regent from William IV'd death on 1347.10.11

1349.5.9 = {
	monarch = {
		name = "Guillaume III"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 2
		MIL = 2
	}
} #Guillaume III of Hainaut & William V of Holland

#1354.1.1 Guillaume unites Hainaut-Holland in a PU with Bayern-Straubing

1389.1.1 = {
	monarch = {
		name = "Albert"
		dynasty = "von Wittelsbach"
		ADM = 5
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "Guillaume"
		monarch_name = "Guillaume VI"
		dynasty = "von Wittelsbach"
		birth_date = 1365.4.5
		death_date = 1417.5.31
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1404.1.1 = {
	monarch = {
		name = "Guillaume IV"
		dynasty = "von Wittelsbach"
		ADM = 2
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Jacquette"
		monarch_name = "Jacquette"
		dynasty = "von Wittelsbach"
		birth_date = 1401.8.16
		death_date = 1436.10.8
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 2
		female = yes
	}
}

1417.5.31  = {
	monarch = {
		name = "Johann"
		dynasty = "von Wittelsbach"
		ADM = 5
		DIP = 4
		MIL = 4
	}
}

#1426.1.1 end of PU with Bayern-Straubing

1426.1.1 = {
	monarch = {
		name = "Jacquette"
		dynasty = "von Wittelsbach"
		ADM = 3
		DIP = 2
		MIL = 2
		female = yes
	}
}

#1433.1.1 absorbed by BUR
