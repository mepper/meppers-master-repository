# ARS - Comt� d'Artois

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = picard
religion = catholic
technology_group = western
capital = 88	# Artois

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1237.1.1   = {
	monarch = {
		name = "Robert I"
		dynasty = "Capet"
		birth_date = 1216.2.8
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1275.1.1   = {
	monarch = {
		name = "Robert II"
		dynasty = "Capet"
		birth_date = 1250.9.8
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1302.7.11 = {
	monarch = {
		name = "Robert III"
		dynasty = "Capet"
		birth_date = 1268.11.27
		ADM = 3
		DIP = 6
		MIL = 5
	}
}

1309.1.1 = {
	monarch = {
		name = "Mahaut"
		dynasty = "Capet"
		ADM = 4
		DIP = 6
		MIL = 3
		female = yes
	}
}

1319.10.27   = {
	monarch = {
		name = "Jeanne II"
		dynasty = "Capet"
		birth_date = 1291.3.16
		ADM = 4
		DIP = 3
		MIL = 3
		female = yes
	}
}

1330.1.21 = {
	monarch = {
		name = "Jeanne III"
		dynasty = "Capet"
		birth_date = 1308.5.2
		ADM = 4
		DIP = 3
		MIL = 3
		female = yes
	}
	queen = {
		name = "Eudes IV"
		dynasty = "de Bourgogne"
		birth_date = 1295.2.15
		death_date = 1349.4.3
		ADM = 2
		DIP = 5
		MIL = 3
	}
}

1349.4.3   = {
	monarch = {
		name = "Jeanne de Boulogne"
		dynasty = "d'Auvergne"
		birth_date = 1326.5.8
		female = yes
		regent = yes
		ADM = 3
		DIP = 3
		MIL = 1
	}
	heir = {
		name = "Philippe"
		monarch_name = "Philippe de Rouvres"
		dynasty = "de Bourgogne"
		birth_date = 1346.3.26
		death_date = 1361.11.21
		claim = 100
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

1361.11.21 = {
	monarch = {
		name = "Margarethe III"
		dynasty = "de Dampierre"
		DIP = 3
		ADM = 5
		MIL = 3
		female = yes
	}
}

# 1369.6.19 : incorporated back into Burgundy through marriage with Philippe the Bold
