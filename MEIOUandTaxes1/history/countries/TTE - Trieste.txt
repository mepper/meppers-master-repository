# TTE - Trieste

government = merchant_republic government_rank = 1
mercantilism = 10
primary_culture = friulian
religion = catholic
technology_group = western
capital = 2452
historical_friend = HAB
historical_rival = VEN

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	add_absolutism = -100
	add_absolutism = 20
}

1355.1.1 = {
	monarch = {
		name = "Republican Council"
		ADM = 3
		DIP = 3
		MIL = 1
	}
}

# To Austria in 1382