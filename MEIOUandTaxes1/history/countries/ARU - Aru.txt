# Country: ARU - Aru

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = batak
religion = sunni		#became muslim c1450, but set from start for playbalance
technology_group = austranesian
capital = 618	# Batak

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1267.1.1 = {
	monarch = {
		name = "Gocah"
		dynasty = "Tuanku"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
