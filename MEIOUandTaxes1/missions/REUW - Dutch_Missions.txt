# Dutch Missions

defend_the_lowlands = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		holland_area
		gelderland_area
		frisia_area
	}
	
	target_provinces = {
		owned_by = SPA
	}
	allow = {
		tag = NED
		NOT = { is_year = 1649 }
		NOT = { has_country_flag = defended_lowlands }
		is_neighbor_of = SPA
		owns = 96	# Zeeland
		owns = 97	# Holland
		owns = 98	# Utrecht
		owns = 99	# Gelre
		owns = 1371	# Friesland
		owns = 1372	# Oversticht
		owns = 2370 # Gravenhage
		low_countries_region = { owned_by = SPA }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = SPA }
		NOT = { low_countries_region = { owned_by = SPA } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 5
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_war_exhaustion = -5
		add_army_tradition = 25
		set_country_flag = defended_lowlands
		add_country_modifier = {
			name = "defended_the_lowlands"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


reconquer_breda = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = NED
		exists = SPA
		is_free_or_tributary_trigger = yes
		SPA = { owns = 95 }
		NOT = { has_country_flag = reconquer_breda }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			AND = {
				SPA = { NOT = { owns = 95 } }
				NOT = { owns = 95 }
			}
		}
	}
	success = {
		owns = 95 # Breda
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 5
		}
	}
	immediate = {
		add_claim = 95 # Breda
	}
	abort_effect = {
		remove_claim = 95 # Breda
	}
	effect = {
		set_country_flag = reconquer_breda
		96 = {
			add_territorial_core_effect = yes
		}
	}
}

the_netherlands_discover_the_carribean = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { carribeans_region = { has_discovered = ROOT } }
		carribeans_region = { range = ROOT }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		carribeans_region = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_colony_in_the_carribean = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		num_of_ports = 1
		num_of_colonists = 1
		carribeans_region = {
			has_discovered = ROOT
			range = ROOT
			is_empty = yes
		}
		NOT = {
			carribeans_region = {
				country_or_vassal_holds = ROOT
			}
		}
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
				NOT = { carribeans_region = { is_empty = yes } }
			}
		}
	}
	success = {
		carribeans_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


the_netherlands_discover_north_america = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { east_america_superregion = { has_discovered = ROOT } }
		east_america_superregion = { range = ROOT }
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		east_america_superregion = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_colony_in_north_america = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		num_of_ports = 1
		num_of_colonists = 1
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
		east_america_superregion = {
			has_discovered = ROOT
			range = ROOT
			is_empty = yes
		}
		NOT = {
			east_america_superregion = {
				country_or_vassal_holds = ROOT
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { east_america_superregion = { country_or_vassal_holds = ROOT } }
				NOT = { east_america_superregion = { is_empty = yes } }
			}
		}
	}
	success = {
		east_america_superregion = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_colony_in_south_africa = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		num_of_colonists = 1
		num_of_ports = 1
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
		OR = {
			1163 = { has_discovered = ROOT }
			1164 = { has_discovered = ROOT }
			1581 = { has_discovered = ROOT }
			1582 = { has_discovered = ROOT }
		}
		NOT = { owns_or_non_sovereign_subject_of = 1163 }
		NOT = { owns_or_non_sovereign_subject_of = 1164 }
		NOT = { owns_or_non_sovereign_subject_of = 1581 }
		NOT = { owns_or_non_sovereign_subject_of = 1582 }
		OR = {
			1163 = { range = ROOT }
			1164 = { range = ROOT }
			1581 = { range = ROOT }
			1582 = { range = ROOT }
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { owns_or_non_sovereign_subject_of = 1163 }
				NOT = { owns_or_non_sovereign_subject_of = 1164 }
				NOT = { owns_or_non_sovereign_subject_of = 1581 }
				NOT = { owns_or_non_sovereign_subject_of = 1582 }
				1163 = { is_empty = no }
				1164 = { is_empty = no }
				1581 = { is_empty = no }
				1582 = { is_empty = no }
			}
		}
	}
	success = {
		OR = {
			owns_or_non_sovereign_subject_of = 1163
			owns_or_non_sovereign_subject_of = 1164
			owns_or_non_sovereign_subject_of = 1581
			owns_or_non_sovereign_subject_of = 1582
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_colony_in_indonesia = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		num_of_colonists = 1
		num_of_ports = 1
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
		southeast_asia_superregion = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = {
			southeast_asia_superregion = {
				country_or_vassal_holds = ROOT
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { southeast_asia_superregion = { country_or_vassal_holds = ROOT } }
				NOT = { southeast_asia_superregion = { is_empty = yes } }
			}
		}
	}
	success = {
		southeast_asia_superregion = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_colony_in_brazil = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		num_of_colonists = 1
		num_of_ports = 1
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
		NOT = { has_country_flag = dutch_brasil_mission }
		south_america = {
			has_discovered = ROOT
		}
		NOT = { owns_or_non_sovereign_subject_of = 743 }
		NOT = { owns_or_non_sovereign_subject_of = 744 }
		NOT = { owns_or_non_sovereign_subject_of = 745 }
		NOT = { owns_or_non_sovereign_subject_of = 1482 }
		NOT = { owns_or_non_sovereign_subject_of = 1490 }
		OR = {
			743 = { range = ROOT }
			744 = { range = ROOT }
			745 = { range = ROOT }
			1482 = { range = ROOT }
			1490 = { range = ROOT }
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { owns_or_non_sovereign_subject_of = 743 }
				NOT = { owns_or_non_sovereign_subject_of = 744 }
				NOT = { owns_or_non_sovereign_subject_of = 745 }
				NOT = { owns_or_non_sovereign_subject_of = 1482 }
				NOT = { owns_or_non_sovereign_subject_of = 1490 }
				743 = { is_empty = no }
				744 = { is_empty = no }
				745 = { is_empty = no }
				1482 = { is_empty = no }
				1490 = { is_empty = no }
			}
		}
	}
	success = {
		OR = {
			owns_or_non_sovereign_subject_of = 743
			owns_or_non_sovereign_subject_of = 744
			owns_or_non_sovereign_subject_of = 745
			owns_or_non_sovereign_subject_of = 1482
			owns_or_non_sovereign_subject_of = 1490
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = dutch_brasil_mission
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


the_netherlands_discover_australia = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = {
			has_country_modifier = "colonial_enthusiasm"
		}
		east_america_superregion = {
			has_discovered = ROOT
			country_or_vassal_holds = ROOT
		}
		southeast_asia_superregion = {
			has_discovered = ROOT
			country_or_vassal_holds = ROOT
		}
		NOT = { southern_australia_area = { has_discovered = ROOT } }
		southern_australia_area = { range = ROOT }
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		southern_australia_area = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


dutch_control_of_taiwan = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = NED
		is_free_or_tributary_trigger = yes
		southeast_asia_superregion = { country_or_vassal_holds = ROOT }
		NOT = { 2304 = { has_discovered = this } }
		NOT = { owns_or_non_sovereign_subject_of = 2304 }
		2304 = { range = ROOT }
		NOT = { has_country_modifier = east_india_trade_rush }
		NOT = { has_country_flag = took_taiwan }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns_or_non_sovereign_subject_of = 2304
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_year = 1650
		}
		modifier = {
			factor = 2
			is_year = 1700
		}
	}
	immediate = {
		add_claim = 2304
	}
	abort_effect = {
		remove_claim = 2304
	}
	effect = {
		add_prestige = 10
		set_country_flag = took_taiwan
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		2304 = {
			add_territorial_core_effect = yes
		}
	}
}


# Removed pending a suitable replacement for the bonus.
#embrace_the_reformation = {
#	
#	type = country
#
#	category = ADM
#	
#	allow = {
#		tag = NED
#		religion = catholic
#		low_countries_region = { religion = reformed }
#		num_of_missionaries = 1
#		owns = 97 # Amsterdam
#		NOT = { has_country_modifier = innovative_modifier }
#	}
#	abort = {
#	}
#	success = {
#		religion = reformed
#	}
#	chance = {
#		factor = 1000
#		modifier = {
#			factor = 2
#			is_year = 1650
#		}
#		modifier = {
#			factor = 2
#			has_idea = humanist_tolerance
#		}
#	}
#	effect = {
#		add_country_modifier = {
#			name = innovative_modifier
#			duration = 3650
#		}
#	}
#}


take_malacca_from_portugal = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = NED
		exists = POR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { is_year = 1654 }
		southeast_asia_superregion = { has_discovered = ROOT }
		POR = { owns_or_non_sovereign_subject_of = 596 }
		NOT = { has_country_modifier = east_india_trade_rush }
		NOT = { has_country_flag = took_malacca_from_por }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = POR }
		owns_or_non_sovereign_subject_of = 596
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			southeast_asia_superregion = { country_or_vassal_holds = ROOT }
		}
	}
	immediate = {
		add_claim = 596
	}
	abort_effect = {
		remove_claim = 596
	}
	effect = {
		add_war_exhaustion = -4
		add_army_tradition = 25
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		set_country_flag = took_malacca_from_por
		596 = {
			add_territorial_core_effect = yes
		}
	}
}


take_ceylon_from_portugal = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = NED
		exists = POR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { is_year = 1654 }
		NOT = { has_country_flag = took_ceylon_from_por }
		NOT = { has_country_modifier = east_india_trade_rush }
		indian_coast_group = { has_discovered = ROOT }
		POR = {
			owns_or_non_sovereign_subject_of = 573
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = POR }
		owns_or_non_sovereign_subject_of = 573
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			indian_coast_group = { country_or_vassal_holds = ROOT }
		}
	}
	immediate = {
		add_claim = 573
	}
	abort_effect = {
		remove_claim = 573
	}
	effect = {
		add_war_exhaustion = -4
		add_army_tradition = 25
		set_country_flag = took_ceylon_from_por
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		573 = {
			add_territorial_core_effect = yes
		}
	}
}


establish_trade_in_indonesian_cot = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = NED
		southeast_asia_superregion = {
			has_discovered = ROOT
			range = ROOT
		}
		NOT = {
			596 = {		# Malacca
				is_strongest_trade_power = NED
			}
		}
		NOT = { has_country_flag = establish_trade_in_indonesian_cot }
		NOT = { has_country_modifier = east_india_trade_rush }
	}
	abort = {
	}
	success = {
		596 = {		# Malacca
			is_strongest_trade_power = NED
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = shrewd_commerce_practise
		}
	}
	effect = {
		add_dip_power = 50
		set_country_flag = establish_trade_in_indonesian_cot
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}
