# Economical Missions

get_minor_cash_reserve = {
	
	type = country
	
	category = ADM
	
	allow = {
		NOT = { years_of_income = 0.5 }
		NOT = { num_of_loans = 1 }
		NOT = { has_country_flag = got_minor_cash_reserve }
		monthly_income = 30
	}
	abort = {
		num_of_loans = 2
	}
	immediate = {
		add_country_modifier = {
			name = "improved_economy_mission"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = improved_economy_mission
		add_country_modifier = {
			name = "improved_economy_mission_malus"
			duration = 1825
		}
	}
	success = {
		years_of_income = 1.0
	}
	chance = {
		factor = 800
	}
	effect = {
		add_years_of_income = 0.25
		remove_country_modifier = improved_economy_mission
		add_adm_power = 50
		set_country_flag = got_minor_cash_reserve
		define_advisor = { type = treasurer skill = 1 discount = yes }
	}
}


amass_wealth = {
	
	type = country
	
	category = ADM
	
	allow = {
		NOT = { treasury = 1000 }
		monthly_income = 100
		treasury = 300
		is_at_war = no
		NOT = { num_of_loans = 1 }
		NOT = { has_country_flag = amassed_wealth }
	}
	abort = {
		OR = {
			NOT = { treasury = 100 }
			num_of_loans = 2
		}
	}
	success = {
		treasury = 2000
	}
	immediate = {
		add_country_modifier = {
			name = "improved_economy_mission"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = improved_economy_mission
		add_country_modifier = {
			name = "improved_economy_mission_malus"
			duration = 1825
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.02
			inflation = 1
		}
		modifier = {
			factor = 1.05
			full_idea_group = economic_ideas
		}
		modifier = {
			factor = 1.02
			full_idea_group = administrative_ideas
		}
	}
	effect = {
		add_treasury = 200
		remove_country_modifier = improved_economy_mission
		add_inflation = -1
		add_adm_power = 100
		set_country_flag = amassed_wealth
		define_advisor = { type = treasurer skill = 2 discount = yes }
	}
}


recover_negative_stability = {
	
	type = country
	
	category = ADM
	
	allow = {
		is_at_war = no
		NOT = { stability = 0 }
		NOT = { has_country_flag = recovered_negative_stability }
	}
	abort = {
	}
	immediate = {
		add_country_modifier = {
			name = "improved_stability_mission"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = improved_stability_mission
		add_country_modifier = {
			name = "improved_stability_mission_malus"
			duration = 1825
		}
	}
	success = {
		stability = 0
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.25
			adm = 3
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.05
			NOT = { stability = -1 }
		}
	}
	effect = {
		add_prestige = 5
		add_adm_power = 20
		remove_country_modifier = improved_stability_mission
		set_country_flag = recovered_negative_stability
	}
}


improve_economical_mismanagement = {
	
	type = country
	
	category = ADM
	
	allow = {
		is_at_war = no
		num_of_loans = 10
		OR = {
			NOT = { has_country_flag = improved_economic_mismanagement }
			had_country_flag = { flag = improved_economic_mismanagement days = 7300 }
		}
		NOT = { has_country_modifier = impressed_financial_sector }
	}
	abort = {
		is_bankrupt = yes
	}
	immediate = {
		add_country_modifier = {
			name = "impressed_financial_sector"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = impressed_financial_sector
	}
	success = {
		NOT = { num_of_loans = 1 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.05
			adm = 2
		}
	}
	effect = {
		add_inflation = -1
		set_country_flag = improved_economic_mismanagement
		add_prestige = 5
	}
}

restore_currency = {
	
	type = country
	
	category = ADM
	
	allow = {
		inflation = 10
		NOT = {
			inflation = 30
		}
		has_idea_group = economic_ideas
		OR = {
			NOT = { has_country_flag = restored_currency }
			had_country_flag = { flag = restored_currency days = 7300 }
		}
	}
	abort = {
		inflation = 35
		NOT = { has_idea_group = economic_ideas }
	}
	immediate = {
		add_country_modifier = {
			name = "improved_inflation_mission"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = improved_inflation_mission
		add_country_modifier = {
			name = "improved_inflation_mission_malus"
			duration = 1825
		}
	}
	success = {
		NOT = {
			inflation = 5
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			full_idea_group = economic_ideas
		}
		modifier = {
			factor = 1.1
			is_bankrupt = yes
		}
	}
	effect = {
		remove_country_modifier = improved_inflation_mission
		set_country_flag = restored_currency
		add_inflation = -5
		add_adm_power = 10
	}
}


recover_from_warexhaustion = {
	
	type = country
	
	category = ADM
	ai_mission = yes
	
	allow = {
		war_exhaustion = 5
		NOT = { war_exhaustion = 3 }
		is_at_war = no
		OR = {
			NOT = { has_country_flag = recovered_from_warexhaustion }
			had_country_flag = { flag = recovered_from_warexhaustion days = 7300 }
		}
		NOT = { has_country_modifier = national_recovery }
	}
	abort = {
		is_at_war = yes
	}
	immediate = {
		add_country_modifier = {
			name = "improved_WE_mission"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = improved_WE_mission
		add_country_modifier = {
			name = "improved_WE_mission_malus"
			duration = 730
		}
	}
	success = {
		NOT = { war_exhaustion = 1 }
		is_at_war = no
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.11
			war_exhaustion = 8
		}
		modifier = {
			factor = 1.17
			war_exhaustion = 12
		}
	}
	effect = {
		add_prestige = 10
		remove_country_modifier = improved_WE_mission
		set_country_flag = recovered_from_warexhaustion
		add_country_modifier = {
			name = "national_recovery"
			duration = 1825
		}
	}
}


buildup_manpower_reserves = {
	
	type = country
	
	category = ADM
	
	allow = {
		max_manpower = 5
		NOT = { manpower_percentage = 0.50 }
		is_at_war = no
		OR = {
			NOT = { has_country_flag = built_up_mp_reserves }
			had_country_flag = { flag = built_up_mp_reserves days = 7300 }
		}
		NOT = { has_country_modifier = national_recovery }
	}
	abort = {
		# AND = {
		is_at_war = yes
		war_exhaustion = 8
		# }
	}
	immediate = {
		add_country_modifier = {
			name = "national_recovery"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = national_recovery
		add_country_modifier = {
			name = "national_recovery_malus"
			duration = 1095
		}
	}
	success = {
		manpower_percentage = 0.90
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.06
			NOT = { manpower_percentage = 0.40 }
		}
		modifier = {
			factor = 1.08
			NOT = { manpower_percentage = 0.30 }
		}
		modifier = {
			factor = 1.05
			NOT = { manpower_percentage = 0.20 }
		}
	}
	effect = {
		set_country_flag = built_up_mp_reserves
		remove_country_modifier = national_recovery
		add_army_tradition = 10
		add_prestige = 5
	}
}
