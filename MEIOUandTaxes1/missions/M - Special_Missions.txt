# Special Missions

reclaim_jerusalem = {
	
	type = country
	
	category = MIL
	
	target_provinces_list = {
		379
	}
	allow = {
		NOT = { is_year = 1600 }
		mil = 4
		#Judea must be owned by non-christians.
		379 = {
			owner = {
				NOT = { religion_group = christian }
			}
		}
		religion_group = christian
		NOT = { has_country_flag = reclaimed_jerusalem }
		is_at_war = no
		monthly_income = 80
		is_free_or_tributary_trigger = yes
		capital_scope = {
			continent = europe
		}
	}
	abort = {
		OR = {
			NOT = { religion_group = christian }
			379 = {
				owner = {
					religion_group = christian
				}
			}
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		379 = { owned_by = ROOT }
	}
	chance = {
		factor = 10000
		modifier = {
			factor = 0.05
			is_year = 1500
		}
	}
	immediate = {
		add_claim = 379
	}
	abort_effect = {
		remove_claim = 379
	}
	effect = {
		add_prestige = 40
		add_treasury = 1000
		set_country_flag = reclaimed_jerusalem
		add_ruler_modifier = {
			name = liberator_of_jerusalem
		}
		379 = {
			add_territorial_core_effect = yes
		}
	}
}


reclaim_mecca = {
	
	type = country
	
	category = MIL
	
	target_provinces_list = {
		385
	}
	allow = {
		#Mecca must be owned by non-muslims.
		385 = {
			owner = {
				NOT = { religion_group = muslim }
			}
		}
		religion_group = muslim
		is_at_war = no
		is_free_or_tributary_trigger = yes
		NOT = { has_country_flag = reclaimed_mecca }
		monthly_income = 80
	}
	abort = {
		OR = {
			NOT = { religion_group = muslim }
			385 = {
				owner = {
					religion_group = muslim
				}
			}
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		385 = { owned_by = ROOT }
	}
	chance = {
		factor = 10000
	}
	immediate = {
		add_claim = 385
	}
	abort_effect = {
		remove_claim = 385
	}
	effect = {
		add_prestige = 40
		add_treasury = 1000
		set_country_flag = reclaimed_mecca
		add_ruler_modifier = {
			name = liberator_of_mecca
		}
		385 = {
			add_territorial_core_effect = yes
		}
	}
}
