force_convert_mission = {
	
	type = neighbor_countries
	
	category = MIL
	
	allow = {
		ROOT = {
			religion = catholic
			has_country_modifier = counter_reformation
			num_of_cities = 15
			NOT = { has_country_modifier = force_converted_country_mission }
		}
		OR = {
			religion = reformed
			religion = protestant
		}
		NOT = { num_of_cities = ROOT }
		piety = -0.60
		army_size = ROOT
		NOT = {
			alliance_with = ROOT
			has_opinion = { who = ROOT value = 50 }
			marriage_with = ROOT
		}
	}
	immediate = {
		ROOT = {
			add_country_modifier = {
				name = "force_converted_country_mission"
				duration = -1
			}
		}
	}
	abort = {
		OR = {
			ROOT = { NOT = { religion = catholic } }
			AND = {
				NOT = { religion = reformed }
				NOT = { religion = protestant }
			}
		}
	}
	abort_effect = {
		ROOT = {
			remove_country_modifier = force_converted_country_mission
			add_country_modifier = {
				name = "force_converted_country_mission_malus"
				duration = 1825
			}
		}
	}
	success = {
		religion = ROOT
	}
	chance = {
		factor = 700
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.5
		}
		modifier = {
			piety = 0.20
			factor = 1.4
		}
		modifier = {
			piety = 0.60
			factor = 1.4
		}
		modifier = {
			factor = 1.25
			is_defender_of_faith = yes
		}
	}
	effect = {
		ROOT = {
			add_prestige = 100
			add_papal_influence = 20
			remove_country_modifier = force_converted_country_mission
		}
	}
}


convert_province_mission = {
	
	type = our_provinces
	
	category = ADM
	
	allow = {
		FROM = {
			is_at_war = no
			num_of_missionaries = 1
			NOT = { has_country_flag = converted_province_mission }
			piety = -0.60
		}
		NOT = { religion = FROM }
		NOT = { has_province_modifier = religious_migration }
	}
	abort = {
		OR = {
			has_province_modifier = religious_migration
			NOT = { owned_by = FROM }
		}
	}
	immediate = {
		add_province_modifier = {
			name = "force_converted_province_mission"
			duration = -1
		}
	}
	abort_effect = {
		remove_province_modifier = force_converted_province_mission
		add_province_modifier = {
			name = "force_converted_province_mission_malus"
			duration = 1825
		}
	}
	success = {
		religion = FROM
	}
	chance = {
		factor = 700
		modifier = {
			factor = 0.5
			NOT = { FROM = { piety = -0.20 } }
		}
		modifier = {
			factor = 1.4
			FROM = { piety = 0.20 }
		}
		modifier = {
			factor = 1.4
			FROM = { piety = 0.60 }
		}
		modifier = {
			factor = 1.4
			FROM = {  num_of_missionaries = 2 }
		}
		modifier = {
			factor = 1.4
			FROM = {  is_defender_of_faith = yes }
		}
	}
	effect = {
		owner = {
			set_country_flag = converted_province_mission
			add_prestige = 5
			add_adm_power = 15
		}
		remove_province_modifier = force_converted_province_mission
	}
}


restore_holy_see = {
	
	type = country
	
	category = ADM
	
	allow = {
		religion = catholic
		NOT = { has_country_modifier = restored_holy_see_mission }
		monthly_income = 30
		NOT = { owns = 2530 }
		NOT = { 2530 = { owned_by = PAP } }
		NOT = { 2530 = { owner = { vassal_of = PAP } } } #Dei Gratia
		is_excommunicated = no
		capital_scope = {
			continent = europe
		}
		piety = -0.60
	}
	immediate = {
		add_claim = 2530
	}
	abort = {
		OR = {
			2530 = { owned_by = PAP }
			NOT = { religion = catholic }
		}
	}
	abort_effect = {
		remove_claim = 2530
	}
	success = {
		owns = 2530 # Roma
	}
	chance = {
		factor = 700
		modifier = {
			factor = 1.4
			is_defender_of_faith = yes
		}
		modifier = {
			piety = 0.60
			factor = 1.4
		}
		modifier = {
			piety = 0.20
			factor = 1.4
		}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.5
		}
		modifier = {
			factor = 1.10
			mil = 4
		}
		modifier = {
			factor = 1.25
			monthly_income = 100
		}
		modifier = {
			factor = 1.10
			tag = SPA
		}
		modifier = {
			factor = 2
			2530 = {	owner = { NOT = { religion = catholic } } }
		}
		modifier = {
			factor = 1.25
			2530 = { any_neighbor_province = { owned_by = ROOT } }
		}
	}
	effect = {
		2530 = { cede_province = PAP }
		add_country_modifier = {
			name = "restored_holy_see_mission"
			duration = 3650
		}
		add_adm_power = 50
		add_mil_power = 50
		add_dip_power = 50
		add_prestige = 5
		reverse_add_opinion = { who = PAP modifier = opinion_restored_holy_see }
		hidden_effect = {
			remove_claim = 2530
		}
	}
}


solidify_our_papal_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		religion = catholic
		NOT = { has_country_modifier = solidified_papal_relations_modifier }
		NOT = { tag = PAP }
		exists = PAP
		NOT = { war_with = PAP }
		NOT = { government = papal_government } #Dei Gratia
		is_excommunicated = no
		PAP = {
			has_opinion = { who = ROOT value = 0 }
			NOT = { has_opinion = { who = ROOT value = 50 } }
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	immediate = {
		add_country_modifier = {
			name = "improved_relation_mission_bonus"
			duration = -1
		}
	}
	abort = {
		OR = {
			NOT = { religion = catholic }
			tag = PAP
			NOT = { exists = PAP }
			war_with = PAP
			is_excommunicated = yes
		}
	}
	abort_effect = {
		remove_country_modifier = improved_relation_mission_bonus
		add_country_modifier = {
			name = "improved_relation_mission_bonus_malus"
			duration = 1825
		}
	}
	success = {
		PAP = { has_opinion = { who = ROOT value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.1
			is_defender_of_faith = yes
		}
		modifier = {
			factor = 1.05
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		remove_country_modifier = improved_relation_mission_bonus
		add_country_modifier = {
			name = "solidified_papal_relations_modifier"
			duration = 3650
		}
	}
}

control_the_pope = {
	
	type = country
	
	category = DIP
	
	allow = {
		papacy_active = yes
		religion = catholic
		is_excommunicated = no
		is_papal_controller = no
		papal_influence = 0
		num_of_cardinals = 1
		NOT = { has_country_modifier = solidified_papal_relations_modifier }
		NOT = { has_country_modifier = controlled_the_pope_mission }
		NOT = { tag = PAP }
		OR = {
			government = theocracy
			num_of_cities = 5
		}
	}
	immediate = {
		add_country_modifier = {
			name = "solidified_papal_relations_modifier"
			duration = -1
		}
	}
	abort = {
		OR = {
			papacy_active = no
			NOT = { religion = catholic }
			is_excommunicated = yes
		}
	}
	abort_effect = {
		remove_country_modifier = solidified_papal_relations_modifier
		add_country_modifier = {
			name = "solidified_papal_relations_modifier_malus"
			duration = 1825
		}
	}
	success = {
		is_papal_controller = yes
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.1
			is_defender_of_faith = yes
		}
		modifier = {
			factor = 1.1
			papal_influence = 10
		}
		modifier = {
			factor = 1.1
			papal_influence = 25
		}
		modifier = {
			factor = 1.1
			papal_influence = 50
		}
	}
	effect = {
		add_prestige = 5
		remove_country_modifier = solidified_papal_relations_modifier
		add_country_modifier = {
			name = "controlled_the_pope_mission"
			duration = 3650
		}
	}
}

achive_religious_unity = {
	
	type = country
	
	category = ADM
	
	allow = {
		has_factions = no
		religion = catholic #Since the modifier mentions the Counter-Reformation
		is_religion_enabled = protestant #See last note
		OR = {
			AND = {
				NOT = { religious_unity = 0.9 }
				check_variable = { which = inquisition value = 2 }
				religion = catholic
				NOT = { has_country_flag = excommunicated }
			}
			AND = {
				NOT = { religious_unity = 0.95 }
				check_variable = { which = inquisition value = 1 }
				NOT = { check_variable = { which = inquisition value = 2 } }
				religion = catholic
				NOT = { has_country_flag = excommunicated }
			}
			AND = {
				NOT = { religious_unity = 1 }
				NOT = { check_variable = { which = inquisition value = 1 } }
			}
		}
		NOT = { has_country_modifier = religious_tolerance }
		piety = -0.60
	}
	immediate = {
		add_country_modifier = {
			name = "force_converted_country_mission"
			duration = -1
		}
	}
	abort = {
		has_country_modifier = religious_tolerance
	}
	abort_effect = {
		remove_country_modifier = force_converted_country_mission
		add_country_modifier = {
			name = "force_converted_country_mission_malus"
			duration = 1825
		}
	}
	success = {
		religious_unity = 1
	}
	chance = {
		factor = 700
		modifier = {
			piety = 0.60
			factor = 1.4
		}
		modifier = {
			piety = 0.20
			factor = 1.4
		}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 0.5
		}
		modifier = {
			NOT = { religious_unity = 0.90 }
			factor = 0.7
		}
		modifier = {
			NOT = { religious_unity = 0.80 }
			factor = 0.7
		}
		modifier = {
			NOT = { religious_unity = 0.70 }
			factor = 0.7
		}
		modifier = {
			NOT = { religious_unity = 0.60 }
			factor = 0.7
		}
		modifier = {
			factor = 1.4
			is_defender_of_faith = yes
		}
		modifier = {
			factor = 1.4
			num_of_missionaries = 2
		}
	}
	effect = {
		remove_country_modifier = force_converted_country_mission
		add_prestige = 10
		add_adm_power = 100
		add_dip_power = 100
	}
}
