# Timurids / Mughal Missions

conquer_delhi = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = DLH
	}
	allow = {
		tag = TIM
		exists = DLH
		is_free_or_tributary_trigger = yes
		mil = 4
		DLH = {
			is_neighbor_of = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = DLH }
		}
	}
	success = {
		NOT = { exists = DLH }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_year = 1526
		}
		modifier = {
			factor = 2
			TIM = { num_of_infantry = ROOT num_of_cavalry = ROOT }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 25
		add_adm_power = 400
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


fund_the_taj_mahal = {
	
	type = country
	
	category = ADM
	
	allow = {
		tag = MUG
		NOT = { is_year = 1648 }
		treasury = 50
		NOT = { has_country_flag = taj_mahal_exists }
	}
	abort = {
	}
	success = {
		advisor = artist
		treasury = 200
		NOT = { num_of_loans = 1 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			adm = 6
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = taj_mahal_exists
	}
}


vassalize_golcanda = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = MUG
		exists = GOC
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_golcanda_done }
		NOT = { has_country_modifier = indian_ambition }
		GOC = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = GOC }
			GOC = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		GOC = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GOC value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GOC
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GOC
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = indian_ambition
			duration = 3650
		}
		set_country_flag = vassalize_golcanda_done
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = GOC
			}
		}
	}
}


vassalize_gondwana = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = MUG
		exists = GDW
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_gondwana_done }
		NOT = { has_country_modifier = military_vassalization }
		GDW = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = GDW }
			GDW = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		GDW = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GDW value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GDW
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GDW
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		set_country_flag = vassalize_gondwana_done
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = GDW
			}
		}
	}
}


defend_the_mughal_borders = {
	
	type = country
	
	category = MIL
	
	target_provinces_list = {
		575			# Balochistan
		576			# Lasbela
		577			# Quetta
		448			# Qalat
		578			# Hazarajat
		508			# Adinapur
	}
	
	target_provinces = {
		owned_by = PER
	}
	allow = {
		tag = MUG
		exists = PER
		is_free_or_tributary_trigger = yes
		is_neighbor_of = PER
		NOT = { has_country_modifier = afghanistan_secured_mug }
		NOT = { has_country_flag = defended_mughal_borders }
		NOT = { war_with = PER }
		MUG = {
			OR = {
				owns = 448 # Qalat
				owns = 508 # Adinapur
				owns = 575 # Balochistan
				owns = 576 # Lasbela
				owns = 577 # Quetta
				owns = 578 # Hazarajat
			}
		}
		PER = {
			OR = {
				owns = 448 # Qalat
				owns = 508 # Adinapur
				owns = 575 # Balochistan
				owns = 576 # Lasbela
				owns = 577 # Quetta
				owns = 578 # Hazarajat
			}
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PER }
		}
	}
	success = {
		NOT = { war_with = PER }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_adm_power = 50
		add_country_modifier = {
			name = "afghanistan_secured_mug"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

vassalize_ahmadnagar = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = MUG
		exists = AHM
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_ahmadnagar_done }
		NOT = { has_country_modifier = indian_ambition }
		AHM = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = AHM }
			AHM = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		AHM = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = AHM value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = AHM
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AHM
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = vassalize_ahmadnagar_done
		add_country_modifier = {
			name = "indian_ambition"
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = AHM
			}
		}
	}
}


conquer_khandesh = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = KHD
	}
	allow = {
		tag = MUG
		exists = KHD
		is_free_or_tributary_trigger = yes
		mil = 4
		is_neighbor_of = KHD
		NOT = { has_country_modifier = indian_ambition }
		NOT = { has_country_flag = conquered_khandesh }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_stability_1 = yes
		set_country_flag = conquered_khandesh
		add_country_modifier = {
			name = "indian_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


annex_berar = {
	
	type = country
	
	category = DIP
	ai_mission = yes
	
	target_provinces = {
		owned_by = BRB
	}
	allow = {
		tag = MUG
		exists = BRR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = conquered_berar }
		BRR = {
			vassal_of = MUG
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = BRR }
			BRR = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = BRR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = BRR value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = BRR value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_berar
		add_country_modifier = {
			name = "indian_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


vassalize_gujarat = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = MUG
		exists = GUJ
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_gujarat_done }
		NOT = { has_country_modifier = indian_ambition }
		GUJ = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = GUJ }
			GUJ = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		GUJ = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GUJ value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GUJ
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GUJ
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = vassalize_gujarat_done
		add_country_modifier = {
			name = "indian_ambition"
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = GUJ
			}
		}
	}
}


vassalize_orissa = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = MUG
		exists = ORI
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_orissa_done }
		NOT = { has_country_modifier = military_vassalization }
		ORI = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = ORI }
			ORI = { is_lesser_in_union = yes }
		}
	}
	success = {
		ORI = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = ORI value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = ORI
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = ORI
		}
	}
	effect = {
		set_country_flag = vassalize_orissa_done
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		add_prestige = 5
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = ORI
			}
		}
	}
}


defeat_england = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	
	target_trade_company_region_list = {
		trade_company_delhi
		trade_company_bengal
		trade_company_kutch
		trade_company_konkan
		trade_company_orissa
		trade_company_ceylon
	}
	target_provinces = {
		owned_by = ENG
	}
	allow = {
		tag = MUG
		exists = ENG
		is_free_or_tributary_trigger = yes
		mil = 4
		ENG = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
		}
		OR = {
			north_india_superregion = { owned_by = ENG }
			central_india_superregion = { owned_by = ENG }
			south_india_superregion = { owned_by = ENG }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = ENG }
		}
	}
	success = {
		NOT = { war_with = ENG }
		NOT = { north_india_superregion = { owned_by = ENG } }
		NOT = { central_india_superregion = { owned_by = ENG } }
		NOT = { south_india_superregion = { owned_by = ENG } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { dip = 1 }
		}
		modifier = {
			factor = 2
			NOT = { dip = 0 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 50
		add_stability_2 = yes
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


defend_timurid_lands = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = TIM
		exists = SHY
		is_free_or_tributary_trigger = yes
		is_neighbor_of = SHY
		NOT = { alliance_with = PER }
		persia_region = { owned_by = ROOT }
		persia_region = { owned_by = SHY }
		NOT = { has_country_modifier = defended_against_shybanids }
		NOT = { has_country_flag = defend_timurid_lands_mission }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SHY }
		}
	}
	success = {
		NOT = { war_with = SHY }
		NOT = { persia_region = { owned_by = SHY } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			PER = { NOT = { mil = 2 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_army_tradition = 50
		set_country_flag = defend_timurid_lands_mission
		add_country_modifier = {
			name = "defended_against_shybanids"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


defend_timurid_lands_against_persia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		merv_area
		sistan_area
		khorasan_area
		tabarestan_area
		kerman_area
		hormuz_area
		south_kurdistan_area
		luristan_area
		farsistan_area
		iraq_e_ajam_area
		azerbaijan_area
		isfahan_area
		balkh_area
		kabulistan_area
		kandahar_area
		baluchi_area
		quetta_area
	}
	
	target_provinces = {
		owned_by = PER
	}
	allow = {
		tag = TIM
		exists = PER
		is_free_or_tributary_trigger = yes
		is_neighbor_of = PER
		NOT = { alliance_with = PER }
		persia_region = { owned_by = ROOT }
		persia_region = { owned_by = PER }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PER }
		}
	}
	success = {
		NOT = { war_with = PER }
		NOT = { persia_region = { owned_by = PER } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			PER = { NOT = { mil = 2 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_army_tradition = 50
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


timurid_break_the_persian_border = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = TIM
		exists = PER
		is_free_or_tributary_trigger = yes
		mil = 4
		PER = { is_neighbor_of = ROOT }
		persia_region = { owned_by = PER }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PER }
		}
	}
	success = {
		NOT = { war_with = PER }
		NOT = { persia_region = { owned_by = PER } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		persia_region = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		persia_region = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 50
		add_war_exhaustion = -5
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


annex_the_jalayrids = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = JAI
	}
	allow = {
		tag = TIM
		exists = JAI
		is_free_or_tributary_trigger = yes
		dip = 4
		JAI = {
			vassal_of = TIM
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = JAI }
			JAI = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = JAI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = JAI value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = JAI value = 200 }
		}
	}
	effect = {
		add_prestige = 10
	}
}


vassalize_the_akkoyunlu = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = TIM
		exists = AKK
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_the_akkoyunlu_done }
		NOT = { has_country_modifier = military_vassalization }
		AKK = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = AKK }
			AKK = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		AKK = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = AKK value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = AKK
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AKK
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		set_country_flag = vassalize_the_akkoyunlu_done
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = AKK
			}
		}
	}
}


vassalize_the_qara_qoyunlu = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = TIM
		exists = QAR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_the_qara_qoyunlu_done }
		NOT = { has_country_modifier = military_vassalization }
		QAR = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = QAR }
			QAR = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		QAR = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = QAR value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = QAR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = QAR
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		set_country_flag = vassalize_the_qara_qoyunlu_done
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = QAR
			}
		}
	}
}


vassalize_georgia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = TIM
		exists = GEO
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_flag = vassalize_georgia_done }
		NOT = { has_country_modifier = military_vassalization }
		GEO = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = GEO }
			GEO = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		GEO = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GEO value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GEO
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GEO
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		set_country_flag = vassalize_georgia_done
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = GEO
			}
		}
	}
}
