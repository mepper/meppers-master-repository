# Aragonese Missions

vassalize_navarra = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = ARA
		NOT = { has_country_modifier = military_vassalization }
		exists = NAV
		is_free_or_tributary_trigger = yes
		NAV = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = NAV }
			NAV = {
				is_subject_other_than_tributary_trigger = yes
			}
		}
	}
	success = {
		NAV = { vassal_of = ARA }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = NAV value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = NAV
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = NAV
		}
	}
	effect = {
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = NAV
			}
		}
	}
}

conquer_sardinia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		127
		2241
		2242
		2852
	}
	
	target_provinces = {
		NOT = { owned_by = ARA }
	}
	allow = {
		OR = {
			tag = ARA
			tag = BLE
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = {
			all_target_province = {
				owned_by = ROOT
			}
		}
		OR = {
			is_core = 127
			is_core = 2241
			is_core = 2242
			is_core = 2852
		}
		OR = {
			127 = { owner = { NOT = { alliance_with = ROOT } } }
			2241 = { owner = { NOT = { alliance_with = ROOT } } }
			2242 = { owner = { NOT = { alliance_with = ROOT } } }
			2852 = { owner = { NOT = { alliance_with = ROOT } } }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_sicily = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		west_sicily_area
		east_sicily_area
	}
	
	target_provinces = {
		owned_by = SIC
	}
	allow = {
		tag = ARA
		exists = SIC
		NOT = { has_country_modifier = mediterranean_ambition }
		is_free_or_tributary_trigger = yes
		mil = 4
		SIC = {
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			SIC = { is_subject_of = ROOT }
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

diplo_annex_sicily = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = SIC
	}
	allow = {
		tag = ARA
		exists = SIC
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = mediterranean_ambition }
		dip = 4
		SIC = {
			vassal_of = ARA
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SIC }
			SIC = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = SIC }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			SIC = { has_opinion = { who = ARA value = 100 } }
		}
		modifier = {
			factor = 2
			SIC = { has_opinion = { who = ARA value = 200 } }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
	}
}

diplo_annex_naples = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = KNP
	}
	allow = {
		tag = ARA
		exists = KNP
		NOT = { has_country_modifier = mediterranean_ambition }
		is_free_or_tributary_trigger = yes
		dip = 4
		KNP = {
			vassal_of = ARA
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = KNP }
			KNP = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = KNP }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			KNP = { has_opinion = { who = ARA value = 100 } }
		}
		modifier = {
			factor = 2
			KNP = { has_opinion = { who = ARA value = 200 } }
		}
	}
	effect = {
		add_adm_power = 50
		add_prestige = 5
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
	}
}

conquer_naples = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = KNP
	}
	allow = {
		tag = ARA
		exists = KNP
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = mediterranean_ambition }
		mil = 4
		KNP = {
			NOT = { is_subject_of = ARA }
		}
		NOT = { exists = SIC }
		owns = 125		# Palermo
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = KNP }
			KNP = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		senior_union_with = KNP
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 300
			target = KNP
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = KNP
		}
	}
	effect = {
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = KNP
			}
		}
	}
}


become_king_of_gonder = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = ARA
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = military_victory }
		has_discovered = 1570 # Tigray
		NOT = { owns = 1570 } # Tigray
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1570 # Tigray
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		add_claim = 1570 # Tigray
	}
	abort_effect = {
		remove_claim = 1570 # Tigray
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		1570 = {
			add_territorial_core_effect = yes
		}
	}
}


defeat_saruhan = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = AYD
	}
	allow = {
		tag = ARA
		exists = AYD
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_victory }
		mil = 4
		AYD = { owns = 1438 } # Aydin
		owns = 121 # Campania
		owns = 125 # Palermu
		owns = 127 # Sassari
		NOT = { owns = 1570 } # Tigray
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = {
				exists = AYD
			}
		}
	}
	success = {
		NOT = { exists = AYD }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}
