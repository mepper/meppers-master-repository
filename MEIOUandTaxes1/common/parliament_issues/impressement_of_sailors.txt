impressment_of_sailors = {
	
	category = 2
	
	allow = {
		num_of_ports = 2
		has_idea_group = naval_ideas
	}
	
	effect = {
	}
	
	modifier = {
		recover_navy_morale_speed = 0.05
		sailors_recovery_speed = 0.10
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			navy_size_percentage = 0.7
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 5 }
		}
	}
}