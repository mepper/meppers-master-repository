offices_to_vassals = {
	
	category = 3
	
	allow = {
		OR = {
			vassal = 2
			personal_union = 2
			AND = {
				vassal = 1
				personal_union = 1
			}
		}
		government = monarchy
	}
	
	effect = {
		random_subject_country = {
			add_opinion = {
				who = ROOT # was FROM but made no sense
				modifier = parliament_closer_ties
			}
		}
	}
	
	modifier = {
		diplomatic_annexation_cost = -0.15
		vassal_income = 0.10
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			vassal = 4
		}
		modifier = {
			factor = 2
			personal_union = 2
		}
	}
}