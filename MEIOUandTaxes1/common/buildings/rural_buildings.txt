###################
# Rural buildings #
###################

# Mines
# Mines 1
# Mines 2
# Mines 3
# Mines 4
# Mines 5

# Rural Infrastructures
# Rural Infrastructure 1
# Rural Infrastructure 2
# Rural Infrastructure 3
# Rural Infrastructure 4
# Rural Infrastructure 5
# Rural Infrastructure 6
# Rural Infrastructure 7
# Rural Infrastructure 8
# Rural Infrastructure 9
# Rural Infrastructure 10

################################################
# Mines
################################################

mines_1 = {
	cost = 100
	time =  12
	
	trigger = {
		OR = {
			can_build_mines_1 = yes
			AND = {
				can_keep_mines_1 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = mines_1 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		
	}
	
	ai_will_do = {
		factor = 1000
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.2 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			OR = {
				has_province_flag = gold
				has_province_flag = gems
				has_province_flag = silver
			}
		}
		
		modifier = {
			factor = 0.8
			local_autonomy = 20
		}
		modifier = {
			factor = 0.7
			local_autonomy = 40
		}
		modifier = {
			factor = 0.6
			local_autonomy = 60
		}
		modifier = {
			factor = 0.5
			local_autonomy = 80
		}
	}
}

mines_2 = {
	#cost = 250
	cost = 150
	time =  12
	
	#make_obsolete = mines_1
	
	trigger = {
		OR = {
			can_build_mines_2 = yes
			AND = {
				can_keep_mines_2 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = mines_2 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		
	}
	
	ai_will_do = {
		factor = 1000
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.2 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			OR = {
				has_province_flag = gold
				has_province_flag = gems
				has_province_flag = silver
			}
		}
		
		modifier = {
			factor = 0.8
			local_autonomy = 20
		}
		modifier = {
			factor = 0.7
			local_autonomy = 40
		}
		modifier = {
			factor = 0.6
			local_autonomy = 60
		}
		modifier = {
			factor = 0.5
			local_autonomy = 80
		}
	}
}

mines_3 = {
	#cost = 450
	cost = 200
	time =  12
	
	#make_obsolete = mines_2
	
	trigger = {
		OR = {
			can_build_mines_3 = yes
			AND = {
				can_keep_mines_3 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = mines_3 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		
	}
	
	ai_will_do = {
		factor = 1000
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.2 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			OR = {
				has_province_flag = gold
				has_province_flag = gems
				has_province_flag = silver
			}
		}
		
		modifier = {
			factor = 0.8
			local_autonomy = 20
		}
		modifier = {
			factor = 0.7
			local_autonomy = 40
		}
		modifier = {
			factor = 0.6
			local_autonomy = 60
		}
		modifier = {
			factor = 0.5
			local_autonomy = 80
		}
	}
}

mines_4 = {
	#cost = 750
	cost = 300
	time =  12
	
	#make_obsolete = mines_3
	
	trigger = {
		OR = {
			can_build_mines_4 = yes
			AND = {
				can_keep_mines_4 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = mines_4 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		
	}
	
	ai_will_do = {
		factor = 1000
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.2 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			OR = {
				has_province_flag = gold
				has_province_flag = gems
				has_province_flag = silver
			}
		}
		
		modifier = {
			factor = 0.8
			local_autonomy = 20
		}
		modifier = {
			factor = 0.7
			local_autonomy = 40
		}
		modifier = {
			factor = 0.6
			local_autonomy = 60
		}
		modifier = {
			factor = 0.5
			local_autonomy = 80
		}
	}
}

mines_5 = {
	#cost = 1150
	cost = 400
	time =  12
	
	#make_obsolete = mines_4
	
	trigger = {
		OR = {
			can_build_mines_5 = yes
			AND = {
				can_keep_mines_5 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = mines_5 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		
	}
	
	ai_will_do = {
		factor = 1000
		
		modifier = {
			factor = 0
			OR = {
				owner = { is_at_war = yes }
				owner = { is_bankrupt = yes }
				OR = {
					AND = {
						is_capital = yes
						owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.05 } } }
					}
					owner = { NOT = { check_variable = { which = "wellbeing_composite" value = 1.2 } } }
				}
			}
		}
		
		modifier = {
			factor = 5
			OR = {
				has_province_flag = gold
				has_province_flag = gems
				has_province_flag = silver
			}
		}
		
		modifier = {
			factor = 0.8
			local_autonomy = 20
		}
		modifier = {
			factor = 0.7
			local_autonomy = 40
		}
		modifier = {
			factor = 0.6
			local_autonomy = 60
		}
		modifier = {
			factor = 0.5
			local_autonomy = 80
		}
	}
}

################################################
# Rural Infrastructures
################################################

rural_infrastructure_1 = {
	#cost = 10000
	cost = 10000
	time =  12
	
	trigger = {
		can_keep_rural_infrastructure_1 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_2 = {
	#cost = 20000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_1
	
	trigger = {
		can_keep_rural_infrastructure_2 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_3 = {
	#cost = 30000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_2
	
	trigger = {
		can_keep_rural_infrastructure_3 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_4 = {
	#cost = 40000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_3
	
	trigger = {
		can_keep_rural_infrastructure_4 = yes
	}
	
	modifier = {
		
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_5 = {
	#cost = 50000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_4
	
	trigger = {
		can_keep_rural_infrastructure_5 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_6 = {
	#cost = 60000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_5
	
	trigger = {
		can_keep_rural_infrastructure_6 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}

rural_infrastructure_7 = {
	#cost = 70000
	cost = 10000
	time =  12
	
	#make_obsolete = rural_infrastructure_6
	
	trigger = {
		can_keep_rural_infrastructure_7 = yes
	}
	
	modifier = {
		
	}
	ai_will_do = {
		factor = 0
	}
}