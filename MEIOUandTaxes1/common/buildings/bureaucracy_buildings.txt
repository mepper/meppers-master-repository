#########################
# Bureaucracy buildings  #
#########################

# Bureaucracy
# Bureaucracy 1
# Bureaucracy 2
# Bureaucracy 3
# Bureaucracy 4
# Bureaucracy 5

###############
# Bureaucracy #
###############

bureaucracy_1 = {
	cost = 99999
	time =  30
	
	trigger = {
		OR = {
			can_build_bureaucracy_1 = yes
			AND = {
				can_keep_bureaucracy_1 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = bureaucracy_1 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		max_states = 1
		local_autonomy = -0.10
		#global_tax_income = -30
		local_build_cost = 0.25
		province_trade_power_modifier = -0.10
	}
	
	ai_will_do = {
		factor = 0
	}
}

bureaucracy_2 = {
	#cost = 750
	cost = 99999
	time =  30
	
	trigger = {
		OR = {
			can_build_bureaucracy_2 = yes
			AND = {
				can_keep_bureaucracy_2 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = bureaucracy_2 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		max_states = 2
		local_autonomy = -0.125
		#global_tax_income = -60
		local_build_cost = 0.5
		province_trade_power_modifier = -0.20
	}
	
	ai_will_do = {
		factor = 0
	}
}

bureaucracy_3 = {
	#cost = 1350
	cost = 99999
	time =  30
	
	trigger = {
		OR = {
			can_build_bureaucracy_3 = yes
			AND = {
				can_keep_bureaucracy_3 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = bureaucracy_3 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		max_states = 3
		local_autonomy = -0.15
		#global_tax_income = -120
		local_build_cost = 1
		province_trade_power_modifier = -0.25
	}
	
	
	ai_will_do = {
		factor = 0
	}
}

bureaucracy_4 = {
	#cost = 2250
	cost = 99999
	time =  30
	
	trigger = {
		OR = {
			can_build_bureaucracy_4 = yes
			AND = {
				can_keep_bureaucracy_4 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = bureaucracy_4 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		max_states = 4
		local_autonomy = -0.2
		#global_tax_income = -240
		local_build_cost = 1.5
		province_trade_power_modifier = -0.3
	}
	
	ai_will_do = {
		factor = 0
	}
}

bureaucracy_5 = {
	#cost = 3450
	cost = 99999
	time =  30
	
	trigger = {
		OR = {
			can_build_bureaucracy_5 = yes
			AND = {
				can_keep_bureaucracy_5 = yes # Redundant can_keep so that buildings still get removed if they are disqualifed
				has_building = bureaucracy_5 # has_building = itself so that building never removes itself unless it fails the can_keep
			}
		}
	}
	
	modifier = {
		max_states = 5
		#global_tax_income = -500
		local_autonomy = -0.2
		local_build_cost = 2.5
		province_trade_power_modifier = -0.35
	}
	
	ai_will_do = {
		factor = 0
	}
}

