unify_china = {
	type = superiority
	
	badboy_factor = 0.05
	prestige_factor = 0.5
	peace_cost_factor = 0.1
	
	allowed_provinces = {
		chinese_region_trigger = yes
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		# # po_annex
		po_demand_provinces
	}
	war_name = CHINESE_CIVIL_WAR
}

unify_china_gp = {
	type = superiority
	
	badboy_factor = 0.05
	prestige_factor = 0.5
	peace_cost_factor = 0.1
	
	allowed_provinces = {
		chinese_region_trigger = yes
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		## po_annex = yes
		po_demand_provinces
	}
	
	war_name = CHINESE_CIVIL_WAR
}

unify_jurchens = {
	type = take_province
	
	badboy_factor = 0.1
	prestige_factor = 0.5
	peace_cost_factor = 0.1
	
	allowed_provinces = {
		jurchen_region_trigger = yes
	}
	
	allowed_provinces_are_eligible = yes
	
	peace_options = {
		po_become_vassal
		# po_annex
		po_demand_provinces
	}
	
	war_name = JURCHEN_CIVIL_WAR
}
