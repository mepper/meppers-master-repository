# 00_prices.txt

#		wheat
#		millet
#		rice
#		maize
#		wine
#		wax
#		wool
#		wool
#		silk
#		hemp
#		gold
#		gems
#		iron
#		copper
#		lead
#		naval_supplies
#		fish
#		fur
#		salt
#		slaves
#		ivory
#		tea
#		chinaware
#		cinnamon
#		clove
#		pepper
#		coffee
#		cotton
#		sugar
#		tobacco
#		opium
#		glassware
#		tin
#		coal
#		sulphur
#		sandal
#		cloth
#		jewelery
#		cacao
#		cheese
#		rum
#		carmine
#		steel
#		arms
#		hardware
#		ammunitions
#		lumber
#		furniture
#		livestock
#		marble
#		beer
#		olive
#		linen
#		brazil
#		carpet
#		palm
#		silver
#		nutmeg
#		ebony
#		leather
#		indigo
#		services





wheat = {
	base_price = 0.45
}

millet = {
	base_price = 0.35
}

rice = {
	base_price = 0.5
}

maize = {
	base_price = 0.45
}

wine = {
	base_price = 0.85
}

wax = {
	base_price = 0.75
}

wool = {
	base_price = 0.75
}

subsistence = {
	base_price = 0
}

pearls = {
	base_price = 2.5
}

hemp = {
	base_price = 0.85
}

gold = {
	base_price = 0
	goldtype = yes
}

gems = {
	base_price = 0
	goldtype = yes
}

iron = {
	base_price = 5
}

copper = {
	base_price = 4
}

lead = {
	base_price = 3.5
}

alum = {
	base_price = 3.5
}

fish = {
	base_price = 0.5
}

fur = {
	base_price = 1.25
}

salt = {
	base_price = 1
}

slaves = {
	base_price = 1.5
}

ivory = {
	base_price = 1.5
}

tea = {
	base_price = 0.85
}

obsidian = {
	base_price = 1.25
}

cinnamon = {
	base_price = 1.5
}

clove = {
	base_price = 1.25
}

pepper = {
	base_price = 1.25
}

coffee = {
	base_price = 1
}

cotton = {
	base_price = 0.85
}

sugar = {
	base_price = 1
}

tobacco = {
	base_price = 1
}

opium = {
	base_price = 1
}

glassware = {
	base_price = 4
}

tin = {
	base_price = 1.5
}

coal = {
	base_price = 5
}

sulphur = {
	base_price = 3
}

sandal = {
	base_price = 4
}

unknown = {
	base_price = 0
}

cloth = {
	base_price = 4
}

jewelery = {
	base_price = 5
}

cacao = {
	base_price = 1
}

cheese = {
	base_price = 0.65
}

rum = {
	base_price = 3.5
}

carmine = {
	base_price = 1.25
}

steel = {
	base_price = 4
}

arms = {
	base_price = 5
}

hardware = {
	base_price = 5
}

ammunitions = {
	base_price = 5
}

lumber = {
	base_price = 0.65
}

furniture = {
	base_price = 4
}

livestock = {
	base_price = 0.65
}

marble = {
	base_price = 4
}

beer = {
	base_price = 3
}

olive = {
	base_price = 0.65
}

linen = {
	base_price = 3
}

brazil = {
	base_price = 1.25
}

penal = {
	base_price = 0
	goldtype = yes
}

carpet = {
	base_price = 4
}

palm = {
	base_price = 0.65
}

silver = {
	base_price = 0
	goldtype = yes
}

nutmeg = {
	base_price = 2
}

ebony = {
	base_price = 1.25
}

leather = {
	base_price = 4
}

indigo = {
	base_price = 1.5
}

services = {
	base_price = 0
	goldtype = yes
}

potatoes = {
	base_price = 1
}

palm_date = {
	base_price = 0.75
}

incense = {
	base_price = 2.5
}
