# Mongol Reformed Cavalry
#Modified for MEIOU by FB 10.02.07
#tech level 20 c1500

unit_type = mongol_tech
type = cavalry

maneuver = 3
#offensive_morale = 20
#defensive_morale = 20
#offensive_fire = 12
#defensive_fire = 12
#offensive_shock = 15
#defensive_shock = 15
#FB
#these values are extra high to give the chinese a hard time
#offensive_morale = 7
#defensive_morale = 5
#offensive_fire = 12
#defensive_fire = 10
#offensive_shock = 12
#defensive_shock = 10

#myzael
offensive_morale = 2
defensive_morale = 2
offensive_fire =   3
defensive_fire =   3
offensive_shock =  3
defensive_shock =  3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}