# Mansabdar Cavalry (24)

unit_type = austranesian
type = cavalry
maneuver = 2

offensive_morale = 6
defensive_morale = 4
offensive_fire = 1
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}