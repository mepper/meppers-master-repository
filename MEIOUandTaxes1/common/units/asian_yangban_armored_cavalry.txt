#Yangban Cavalry (8)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 2
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 3

trigger = {
	any_owned_province = { region = korea_region }
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}

