#34 - Dragoons

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 4
defensive_morale = 6
offensive_fire = 5
defensive_fire = 1
offensive_shock = 1
defensive_shock = 6

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
