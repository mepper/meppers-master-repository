#Light Hussars (45)

type = cavalry
unit_type = indian
maneuver = 2

offensive_morale = 9
defensive_morale = 4
offensive_fire = 6
defensive_fire = 3
offensive_shock = 4
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}