# south_american Cuirassier Cavalry (25)

type = cavalry
unit_type = south_american
maneuver = 2

offensive_morale = 5
defensive_morale = 3
offensive_fire = 1
defensive_fire = 2
offensive_shock = 3
defensive_shock = 4

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}