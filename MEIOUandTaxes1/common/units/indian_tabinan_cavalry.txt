# Tabinan Cavalry (20)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}