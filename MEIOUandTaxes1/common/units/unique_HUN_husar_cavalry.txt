#18 - Medium Hussars - Hussars

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 5 #BONUS
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 4

trigger = {
	primary_culture = hungarian
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}



