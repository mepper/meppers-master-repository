# Silhedar Cavalry (24)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 4
defensive_morale = 3
offensive_fire = 1
defensive_fire = 0
offensive_shock = 6
defensive_shock = 4

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}