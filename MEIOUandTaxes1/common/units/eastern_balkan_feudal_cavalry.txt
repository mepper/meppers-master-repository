#6 - Feudal Cavalry

unit_type = eastern
type = cavalry

maneuver = 1
offensive_morale = 2
defensive_morale = 1
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 4

trigger = {
	NOT = { primary_culture = serbian }
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}

