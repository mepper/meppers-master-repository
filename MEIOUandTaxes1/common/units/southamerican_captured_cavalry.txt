# South American Captured Cavalry

type = cavalry
unit_type = south_american
maneuver = 2

offensive_morale = 4
defensive_morale = 1
offensive_fire = 0
defensive_fire = 2
offensive_shock = 3
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}