#16 - Heavy Cavalry

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 4
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
