# Kshatriya Cavalry (20)

unit_type = austranesian
type = cavalry
maneuver = 2

offensive_morale = 4
defensive_morale = 4
offensive_fire = 0
defensive_fire = 0
offensive_shock = 5
defensive_shock = 3


trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}