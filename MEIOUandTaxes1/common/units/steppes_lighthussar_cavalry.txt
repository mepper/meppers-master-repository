#Light Hussars (45)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 6
defensive_morale = 6
offensive_fire = 5
defensive_fire = 3
offensive_shock = 5
defensive_shock = 3
trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}