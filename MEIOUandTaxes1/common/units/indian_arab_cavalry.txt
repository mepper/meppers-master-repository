# Arab Cavalry (0)

unit_type = indian
type = cavalry
maneuver = 2

#Tech 0
offensive_morale = 1
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 1

trigger = {
	OR = {
		religion_group = muslim
		religion = sikhism
	}
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
