#Shock Cavalry (28)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 1
defensive_fire = 1
offensive_shock = 6
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}