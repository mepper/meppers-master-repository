#4 - Farima Braves

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 3
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 0

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
