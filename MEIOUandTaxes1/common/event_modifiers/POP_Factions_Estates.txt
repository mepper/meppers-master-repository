aristocrats_influence_10 = {
	mr_aristocrats_influence = -0.1
}

aristocrats_influence_20 = {
	mr_aristocrats_influence = -0.2
}

aristocrats_influence_30 = {
	mr_aristocrats_influence = -0.3
}

aristocrats_influence_40 = {
	mr_aristocrats_influence = -0.4
}

aristocrats_influence_50 = {
	mr_aristocrats_influence = -0.5
}

aristocrats_influence_60 = {
	mr_aristocrats_influence = -0.6
}

aristocrats_influence_70 = {
	mr_aristocrats_influence = -0.7
}

aristocrats_influence_80 = {
	mr_aristocrats_influence = -0.8
}

aristocrats_influence_90 = {
	mr_aristocrats_influence = -0.9
}

aristocrats_influence_100 = {
	mr_aristocrats_influence = -1
}


artisans_influence_10 = {
	mr_guilds_influence = -0.1
}

artisans_influence_20 = {
	mr_guilds_influence = -0.2
}

artisans_influence_30 = {
	mr_guilds_influence = -0.3
}

artisans_influence_40 = {
	mr_guilds_influence = -0.4
}

artisans_influence_50 = {
	mr_guilds_influence = -0.5
}

artisans_influence_60 = {
	mr_guilds_influence = -0.6
}

artisans_influence_70 = {
	mr_guilds_influence = -0.7
}

artisans_influence_80 = {
	mr_guilds_influence = -0.8
}

artisans_influence_90 = {
	mr_guilds_influence = -0.9
}

artisans_influence_100 = {
	mr_guilds_influence = -1
}


traders_influence_10 = {
	mr_traders_influence = -0.1
}

traders_influence_20 = {
	mr_traders_influence = -0.2
}

traders_influence_30 = {
	mr_traders_influence = -0.3
}

traders_influence_40 = {
	mr_traders_influence = -0.4
}

traders_influence_50 = {
	mr_traders_influence = -0.5
}

traders_influence_60 = {
	mr_traders_influence = -0.6
}

traders_influence_70 = {
	mr_traders_influence = -0.7
}

traders_influence_80 = {
	mr_traders_influence = -0.8
}

traders_influence_90 = {
	mr_traders_influence = -0.9
}

traders_influence_100 = {
	mr_traders_influence = -1
}


aristocrats_feeble = {
	
	global_manpower_modifier = 0.025
	land_forcelimit_modifier = 0.025
}

aristocrats_weak = {
	mr_aristocrats_influence = 0.20
	
	max_states = 1
	global_manpower_modifier = 0.025
	land_forcelimit_modifier = 0.025
	global_regiment_cost = -0.025
	global_autonomy = 0.025
	fort_maintenance_modifier = -0.1
	republican_tradition = -0.5
	global_foreign_trade_power = -0.025
}

aristocrats_capable = {
	mr_aristocrats_influence = 0.4
	
	max_states = 2
	global_manpower_modifier = 0.05
	land_forcelimit_modifier = 0.05
	global_regiment_cost = -0.025
	global_autonomy = 0.025
	fort_maintenance_modifier = -0.1
	republican_tradition = -1
	global_foreign_trade_power = -0.05
	vassal_income = 0.025
}

aristocrats_strong = {
	mr_aristocrats_influence = 0.6
	
	max_states = 3
	global_manpower_modifier = 0.05
	land_forcelimit_modifier = 0.05
	global_regiment_cost = -0.05
	global_autonomy = 0.05
	fort_maintenance_modifier = -0.15
	republican_tradition = -1.5
	global_foreign_trade_power = -0.075
	vassal_income = 0.025
}

aristocrats_powerful = {
	mr_aristocrats_influence = 0.8
	
	max_states = 4
	global_manpower_modifier = 0.05
	land_forcelimit_modifier = 0.05
	global_regiment_cost = -0.05
	global_autonomy = 0.05
	fort_maintenance_modifier = -0.2
	republican_tradition = -2
	global_foreign_trade_power = -0.075
	vassal_income = 0.05
}

aristocrats_dominant = {
	mr_aristocrats_influence = 0.9
	
	max_states = 5
	global_manpower_modifier = 0.05
	land_forcelimit_modifier = 0.05
	global_regiment_cost = -0.05
	global_autonomy = 0.05
	fort_maintenance_modifier = -0.2
	republican_tradition = -2.5
	global_foreign_trade_power = -0.1
	vassal_income = 0.075
}

artisans_feeble = {
	
	production_efficiency = 0.05
}

artisans_weak = {
	mr_guilds_influence = 0.2
	
	possible_mercenaries = 0.2
	production_efficiency = 0.1
	republican_tradition = 0.25
}

artisans_capable = {
	mr_guilds_influence = 0.4
	
	possible_mercenaries = 0.4
	production_efficiency = 0.15
	republican_tradition = 0.50
}

artisans_strong = {
	mr_guilds_influence = 0.6
	
	possible_mercenaries = 0.6
	production_efficiency = 0.2
	republican_tradition = 0.75
}

artisans_powerful = {
	mr_guilds_influence = 0.8
	
	possible_mercenaries = 0.8
	production_efficiency = 0.25
	republican_tradition = 1
}

artisans_dominant = {
	mr_guilds_influence = 0.9
	
	possible_mercenaries = 1.0
	production_efficiency = 0.3
	republican_tradition = 1.25
}

traders_feeble = {
	
	global_trade_power = 0.05
	global_tax_modifier = -0.05
}

traders_weak = {
	mr_traders_influence = 0.2
	
	possible_mercenaries = 0.20
	global_prov_trade_power_modifier = 0.05
	global_trade_power = 0.025
	naval_maintenance_modifier = -0.025
	naval_forcelimit_modifier = 0.025
	global_tax_modifier = -0.05
	republican_tradition = 0.25
}

traders_capable = {
	mr_traders_influence = 0.40
	
	merchants = 1
	possible_mercenaries = 0.4
	global_prov_trade_power_modifier = 0.1
	global_trade_power = 0.05
	naval_maintenance_modifier = -0.05
	naval_forcelimit_modifier = 0.05
	global_tax_modifier = -0.075
	republican_tradition = 0.5
}

traders_strong = {
	mr_traders_influence = 0.6
	
	merchants = 1
	possible_mercenaries = 0.6
	global_prov_trade_power_modifier = 0.15
	global_trade_power = 0.075
	naval_maintenance_modifier = -0.075
	naval_forcelimit_modifier = 0.075
	global_tax_modifier = -0.10
	republican_tradition = 0.75
}

traders_powerful = {
	mr_traders_influence = 0.8
	
	merchants = 1
	possible_mercenaries = 0.8
	global_prov_trade_power_modifier = 0.2
	global_trade_power = 0.10
	naval_maintenance_modifier = -0.1
	naval_forcelimit_modifier = 0.01
	global_tax_modifier = -0.15
	republican_tradition = 1
}

traders_dominant = {
	mr_traders_influence = 0.9
	
	merchants = 2
	possible_mercenaries = 1.0
	global_prov_trade_power_modifier = 0.25
	global_trade_power = 0.15
	naval_maintenance_modifier = -0.15
	naval_forcelimit_modifier = 0.015
	global_tax_modifier = -0.20
	republican_tradition = 1.25
}

backup_merchants = {
	merchants = 3
}

backup_diplomats = {
	diplomats = 1
}

backup_diplomats_2 = {
	diplomats = 2
}