#Country Name: Futa Jallon
#Tag: FLO

graphical_culture = africangfx

color = { 175  82  16 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	unique_MAN_sumba_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Mule #0" = 20
	"Mamadu #1" = 40
	"Al-Hajj #0" = 60
	"'Ali Zalil #0" = 20
	"Muhammad #0" = 60
	"Da'ud #0" = 20
	"Bakari #0" = 20
	"'Abd al-Rahman #0" = 20
	"Bakr #0" = 20
	"Al-Mukhtar #0" = 20
	"Mahmud #1" = 20
	"Majan #0" = 1
	"Birahim #0" = 1
	"'Ali #0" = 1
	"Amada Sire #0" = 1
	"Nguia #0" = 1
	"Hammadi #0" = 1
	"Bubu #0" = 1
	"Ilo #0" = 1
	"Kanta #0" = 1
	"Al-Mansur #0" = 1
	"'Ammar #0" = 1
	"Sa'ud 'Arjud #0" = 1
	"Haddu #0" = 1
	"Sa'id #0" = 1
	"Dhu al-Nun #0" = 1
	"'Abd Allah #0" = 1
	"Nasr #0" = 1
	"Yahya #0" = 1
	"Sanibar #0" = 1
	"'Abbas #0" = 1
	"Benkano #0" = 1
	"Zenka #0" = 1
	"Mami #0" = 1
	"Ba-Bakribin #0" = 1
	"'Abd al-Qadir #0" = 1
	"Yusuf #0" = 1
	"Santa'a #0" = 1
	"Al-Fa' Ibrahim #0" = 1
	"Sandaki #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Konare
	Deo
	Keita
	Traor�
	Toure
	Diarra
	Moussa
	Modibo
	Amadou
	Toumani
}

ship_names = {
	Kiringa Timbuktu "Koumbi Saleh" Gao
	Jenne "Sundiata Keita" "Kankan Musa"
	"Kouroukan Fouga" "Wali Keita" "Quati Keita"
	"Khalifa Keita" Sakura
}
