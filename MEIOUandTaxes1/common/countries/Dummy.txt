#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 93  160  163 }

historical_idea_groups = {
	trade_ideas
	innovativeness_ideas
	naval_ideas
	administrative_ideas
	economic_ideas
	leadership_ideas
	diplomatic_ideas
	quality_ideas
}

historical_units = { #French group
	western_halberd_infantry
	western_knight_cavalry
	unique_FRA_chevalier_cavalry
	western_manatarms_infantry
	western_runner_cavalry
	western_lance_infantry
	western_gendarme_cavalry
	western_pikeandshot_infantry
	western_earlytercio_infantry
	western_caracole_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	unique_FRA_musketeer_of_the_guard_infantry
	western_line_infantry
	western_carabinier_cavalry
	western_drill_infantry
	western_hunter_cavalry
	unique_FRA_grognard_infantry
	western_lancer_cavalry
	western_jominian_infantry
}

monarch_names = {
	"Charles #2" = 40
	"Louis #1" = 40
	"Ren� #1" = 20
	"Nicolas #0" = 10
	"Jean #0" = 5
	"Raymond Berengar #3" = 1
	"Alphonse #2" = 1
	"Robert #1" = 1
	"Adh�mar #0" = 1
	"Aymeric #0" = 1
	"Bartomiu #0" = 1
	"Benedit #0" = 1
	"Bernat #0" = 1
	"Bertran #0" = 1
	"Dami�n #0" = 1
	"Emeric #0" = 1
	"Enric #0" = 1
	"Est�ve #0" = 1
	"Ferr�n #0" = 1
	"Folquet #0" = 1
	"Frances #0" = 1
	"Gast� #0" = 1
	"Guilhem #0" = 1
	"Huc #0" = 1
	"Jacme #0" = 1
	"Jaufre #0" = 1
	"Jordan #0" = 1
	"Loys #0" = 1
	"Lucas #0" = 1
	"Mart� #0" = 1
	"Matheu #0" = 1
	"Miqueu #0" = 1
	"Per #0" = 1
	"Pol #0" = 1
	"Pons #0" = 1
	"Sanc #0" = 1
	"Thibaud #0" = 1
	"Vicentz #0" = 1
	"Victor #0" = 1
	"Xavier #0" = 1
	
	"Blanche #0" = -1
	"Ali�nor #0" = -1
	"Berthe #0" = -1
	"Constance #0" = -1
	"Mathilde #0" = -1
	"Isabeau #0" = -1
	"Jeanne #0" = -1
	"Marie #0" = -1
	"Charlotte #0" = -1
	"Anne #0" = -1
	"Marguerite #0" = -1
	"Ad�la�de #0" = -1
	"Claude #0" = -1
}

leader_names = {
	"d'Abeille" "d'Adh�mar" "d'Agoult" "d'Aiglun" "d'Aigui�res" "d'Allemagne" "d'Amalric" "d'Audiffredi"
	"de Bagnols" "de Barberin" "de Barcillon" "de Baschi" "des Baux" "de Bermond" "de Bionneau" "de Brunet"
	"de Cadenet" "de Cascaris-Castellet" "de Castaing" "de Castellane" "de Castillon" Coquerel "de Cordes" "de Cormis" "de Crillon"
	"d'Entrecasteaux" "d'Espagnet" Esparron "d'Espinouze"
	"de Fargis" "Fauque de Jonqui�res" "Ferr� de la Grange" 
	"de Gassendi" "de G�vaudan" "de Gombert" "de Gordes" "de Grignan" Grimaldi "de Grimaud" "de Guast" "des Isnards" 
	"de Lauris" "de Libertat" "de Lom�nie"
	"de Monteil" "de Montpezat" 
	"de Paule" "de Pena" "Pene de la Borde" "de la P�russe" "de Pontev�s"
	"de Raimondis" "de Revest" "de Riquetti"
	"de Sabran" "de Sade" "de Saint Victoret" "de S�guiran" "de Simiane"
	"de Valbelle"
}

ship_names = {
	Achille Agr�able "Aigle Volant" Alcyon Alouette
	Altier Aquilon Ardente Assur� Avenant
	Aventurier Baleine B�casse Bienvenu Bizarre
	Bor�e "Branche d'Olivier" Brillant Bucentaure Cach�
	Caille Capable Centaure C�sar "Cheval Marin"
	Coche "Commerce de Marseille" "Commerce de Montpellier" "Comte de Provence" Conqu�rant
	Constant Content Croissant Dauphine Diamant
	Dieppoise Dur Dromadaire Eclatant Eole
	Esp�rance Fantasque Ferme Fid�le Fier
	Florissant Formidable Fort Foudroyant Gaillard
	Heureux H�ros Jason Lion Mignon
	Minerve Oc�an Orph�e Parfait Ph�nix
	Poli Pompeux Portefaix Protecteur Proven�al
	Provence Prudent Puissant R�ale Redoutable
	Requin "Royal Louis" Sage Sagittaire "Royal Th�r�se"
	"Saint Esprit" "Saint Joseph" "Saint Philippe" Sardine Sceptre
	S�duisant Seine S�rieux Solide Souverain
	Suffisant T�m�raire Terrible Tigre Tonnant
	Toulon Toulouse Trident Triomphant Triton
	Union Vaillant Vigilant Volontaire Z�l�
}

army_names = {
	"Arm�e de Marseille" "Arm�e des Baux"		  "Arm�e de Provence-Forcalquier" 
	"Arm�e d'Orange"	  "Arm�e du Pays d'Arles" "Arm�e des Alpes"
	"Arm�e du Rh�ne"	  "Arm�e de la Durance"	"Arm�e de Haute Provence"
	"Arm�e de Basse Provence" "Arm�e du Vivarais"
	"Arm�e de $PROVINCE$" 
}