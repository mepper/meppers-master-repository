#Country Name: Igbo
#Tag: NRI

graphical_culture = africangfx

color = { 161  139  40 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	unique_DAH_amazon_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Eze Nri Nkungu #0" = 20
	"Eze Nri Afonso #0" = 20
	"Eze Nri Ntangu #0" = 20
	"Eze Nri Makose #0" = 20
	"Eze Nri Mwene Mbatu #0" = 20
	"Eze Nri Nobo Sinda #0" = 20
	"Eze Nri Mwe Pwati #0" = 20
	"Eze Nri Nguli Nkama #0" = 20
	"Eze Nri Ilunga #0" = 1
	"Eze Nri Kasongo #0" = 1
	"Eze Nri Sanza #0" = 1
	"Eze Nri Kumwimba #0" = 1
	"Eze Nri Kekenya #0" = 1
	"Eze Nri Kaumbo #0" = 1
	"Eze Nri Miketo #0" = 1
	"Eze Nri Nday #0" = 1
	"Eze Nri Mwine #0" = 1
	"Eze Nri Maloba #0" = 1
	"Eze Nri Kitamba #0" = 1
	"Eze Nri Cibindi #0" = 1
	"Eze Nri Nawej #0" = 1
	"Eze Nri Munying #0" = 1
	"Eze Nri Kateng #0" = 1
	"Eze Nri Cakasekene #0" = 1
	"Eze Nri Cikombe #0" = 1
	"Eze Nri Kalong #0" = 1
	"Eze Nri Mutand #0" = 1
	"Eze Nri Mbala #0" = 1
	"Eze Nri Mukaz #0" = 1
	"Eze Nri Lukwesa #0" = 1
	"Eze Nri Kanyembo #0" = 1
	"Eze Nri Kapumba #0" = 1
	"Eze Nri Mwango #0" = 1
	"Eze Nri Ngoupou #0" = 1
	"Eze Nri Fifen #0" = 1
	"Eze Nri Kutu #0" = 1
	"Eze Nri Nsangu #0" = 1
	"Eze Nri Bele #0" = 1
	"Eze Nri Lobe #0" = 1
	"Eze Nri Nunga #0" = 1
	"Eze Nri Zangabiru #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Nguesso
	Mani
	Youla
	Zanga
	Youlou
	Mboma
	Song
	Abena
	Lissouba
	Ndo
}

ship_names = {
	Fang Ntufia Malebo Ibonga Nyamu
	Tchintchesse Muvili Mubana Bilama Binzembele
	Doala "Nfumu iampuena" Nkangala
	"Ntangua odimini" Nvumu Nzalu Nzoke Mavanga
}
