#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 219  206  94 }

historical_idea_groups = {
	popular_religion_ideas
	trade_ideas
	quantity_ideas
	leadership_ideas
	merchant_marine_ideas
	economic_ideas
	logistic_ideas
	administrative_ideas
}

#Tibetan group
historical_units = {
	asian_warrior_monk_infantry
	asian_horse_archer_cavalry
	#complete guesses follow
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_repeating_crossbow_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_banner_infantry
	asian_forbidden_soldier_cavalry
	asian_new_guard_infantry
	asian_platoon_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_lighthussar_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Batula" = 40
	"Toghan" = 40
	"Galdan" = 40
	"Amursana" = 20
	"Erdeni" = 20
	"Choghtu" = 20
	"Dawa" = 20
	"Esen" = 20
	"G�shi" = 20
	"Lha-bzang" = 20
	"Khara Khula" = 20
	"Kho" = 20
	"Sengge" = 20
	"Tseten" = 20
	"Tsewang" = 20
	"Kundelung" = 15
	"Lobzang" = 15
	"Sutai" = 15
	"Tsobasa" = 15
	"Ubasi" = 15
	"Quduqa" = 10
	"Toghto" = 10
	"Altai" = 1
	"B�ki" = 1
	"Bayan" = 1
	"Dayan" = 1
	"Hami" = 1
	"Molon" = 1
	"Oori" = 1
	"Orl�k" = 1
	"Sechen" = 1
	"Radi" = 1
	"Talman" = 1
	"Todo" = 1
	"Tumen" = 1
	"Tumu" = 1
	"Ubashi" = 1
	"Ulan" = 1
	"Wehe" = 1
	"Yelu" = 1
	"Zaya" = 1
	
	"Damo Nyetuma" = -1
	"Kunga Pemo" = -1
	"Kunga Lhanzi" = -1
	"Dondrub Dolma" = -1
	"Namkha Kyi" = -1
	"Kamala" = -1
	"Indira" = -1
	"Hoelun" = -1
	"Tam�l�n" = -1
	"Sorghaghtani" = -1
}

leader_names = {
	Borjigin
	Chuluun
	Dairtan
	Dotno
	Duutan
	Okhid
	Khuushan
	Onkhod
	Khaliuchin
	Myangad
	Khavkhchin
	Tangud
	Oimuud
	Besud
	Togoruutan
	ar-togoruutan
	Ikh-khotgoid
	Baga-khotgoid
	Uvur-togoruutan
	Tsuvdag
	Kherdeg
	Orchid
	Burged
	Unduriinkhen
	Doloon
	Khorkhoinkhon
	Barga
	Khariad
	avkhainkhan
	Tsookhor
	Toos
	Sharnuud
	Zelmen
	Tumt
	ayagachin
	Darkhad
	Bicheech
	Khurniad
	Uushgiinkhan
	Uushin
	Choros
	Ogniud
	Khukhnuud
	Khirgis
	Bashgid
	Mudar
	Doloon-suvai
	Khukh-nokhoinkhon
	Todno
	Khemchigiinkhen
	Uuld
	Tugchin
	Ukher-tsagaanikhan
	Tsognuud
	Khukh-nuur
	Ukher-onkhod
	Khonin-khotgoid
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Amdo "Altan Khan" Khoshot Ol�ts G�shi
}

army_names = {
	"$PROVINCE$ Tumen"
}