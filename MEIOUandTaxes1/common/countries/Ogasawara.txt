#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 255  0  38 }

historical_idea_groups = {
	logistic_ideas
	trade_ideas
	popular_religion_ideas
	naval_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	merchant_marine_ideas
}

#Japanese group
historical_units = {
	asian_light_foot_infantry
	asian_horse_archer_cavalry
	asian_bushi_cavalry
	asian_shashu_no_ashigaru_infantry
	asian_samurai_cavalry
	asian_samurai_infantry
	asian_yarigumi_infantry
	asian_late_samurai_cavalry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_horse_guard_cavalry
	asian_new_guard_infantry
	asian_volley_infantry
	asian_armeblanche_cavalry
	asian_bayonet_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Masunaga" = 75
	"Nagamoto" = 75
	"Chosusu" = 75
	"Nagahide" = 75
	"Mochinaga" = 75
	"Kiyomune" = 75
	"Nagatomo" = 75
	"Sadatomo" = 75
	"Nagamune" = 75
	"Nagatoki" = 75
	"Sadayoshi" = 75
	
	"Ako" = -1
	"Asahi" = -1
	"Aya" = -1
	"Harukiri" = -1
	"Inuwaka" = -1
	"Itoito" = -1
	"Itsuitsu" = -1
	"Koneneme" = -1
	"Mitsu" = -1
	"Narime" = -1
	"Sakami" = -1
	"Shiro" = -1
	"Tatsuko" = -1
	"Tomiko" = -1
	"Toyome" = -1
	"Yamabukime" = -1
}

leader_names = {
	Asai Abe Adachi Akamatsu Akechi Akita Akiyama Akizuki Amago
	Ando Anayama Asakura Ashikaga Asahina
	Chosokabe
	Date
	Hara Hatakeyama Hatano Hayashi Honda Hojo Hosokawa
	Idaten Ii Ikeda Imagawa Inoue Ishida Ishikawa Ishimaki Ito
	Kikkawa Kiso Kitabatake
	Maeda Matsuda Matsudaira Miura Mikumo Miyoshi Mogami M�ri
	Nanbu Nitta Niwa
	Oda �tomo Ouchi
	Rokkaku
	Sakai Sakuma Shimazu Shiba Sanada Sogo Suwa
	Takeda Takigawa Toda Toki Tokugawa Toyotomi Tsutsui
	Uesugi Ukita
	Yagyu Yamana
	# Ogasawara Flavor
	Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara Ogasawara
	# Vassals of Ogasawara
	Mimura Asahara Omi Atobe Uchikoshi Oi Okura Osafune Ohigata
}

ship_names = {
	"Asai Maru" "Abe Maru" "Adachi Maru" "Akamatsu Maru" "Akechi Maru"
	"Akita Maru" "Akiyama Maru" "Akizuki Maru" "Amago Maru" "Ando Maru"
	"Anayama Maru" "Asakura Maru" "Ashikaga Maru" "Asano Maru" "Ashina Maru"
	"Atagi Maru" "Azai Maru"
	"Bito Maru" "Byakko Maru"
	"Chiba Maru" "Chousokabe Maru"
	"Date Maru" "Doi Maru"
	"Fujiwara Maru" "Fuji-san Maru"
	"Genbu maru"
	"Haga Maru" "Hatakeyama Maru" "Hatano Maru" "Honda Maru" "Hojo Maru"
	"Hosokawa Maru" "Hachisuka Maru" "Hayashi Maru" "Hiki Maru"
	"Idaten Maru" "Ikeda Maru" "Imagawa Maru" "Ishida Maru" "Ishikawa Maru"
	"Ishimaki Maru" "Ii Maru" "Inoue Maru" "Ito Maru"
	"Kikkawa Maru" "Kiso Maru" "Kisona Maru" "Kitabatake Maru" "Kyogoku Maru"
	"Maeda Maru" "Matsuda Maru" "Matsudaira Maru" "Miura Maru" "Mikumo Maru"
	"Miyoshi Maru" "Mogami Maru" "Mori Maru"
	"Nitta Maru" "Niwa Maru" "Nihon Maru" "Nanbu Maru"
	"Oda Maru" "Otomo Maru" "Ouchi Maru"
	"Rokkaku Maru"
	"Sakai Maru" "Sakuma Maru" "Satake Maru" "Shimazu Maru" "Shiba Maru"
	"Sanada Maru" "Sogo Maru" "Suwa Maru" "Seiryu Maru" "Suzaku Maru"
	"Takeda Maru" "Tokugawa Maru" "Taira Maru" "Toyotomi Maru" "Tada Maru"
	"Toki Maru" "Tsugaru Maru" "Tsutsui Maru" "Tenno Maru"
	"Uesugi Maru" "Ukita Maru" "Uchia Maru"
	"Yamana Maru" "Yagyu Maru"
}