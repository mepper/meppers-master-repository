#Country Name: Gurma
#Tag: GUR
#MEIOU-FB - African/Sudan tech group split Nov 08
#dharper's African cavalry added

graphical_culture = africangfx

color = { 217  228  105 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Pastoralists and Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Beriguwiemda #0" = 40
	"Zolgo #0" = 20
	"Zongman #0" = 20
	"Nengmitoni #0" = 20
	"Dimani #0" = 20
	"Yanza #0" = 20
	"Dariziogo #0" = 20
	"Luro #0" = 20
	"Tutugri #0" = 20
	"Zagale #0" = 20
	"Zokuli #0" = 20
	"Gungoble #0" = 20
	"Boadu Akofu Berempon #0" = 20
	"Boa Amponsem #0" = 20
	"Ntim Gyakari #0" = 20
	"Ose Tutu #0" = 20
	"Opoku Fofie #0" = 20
	"Opoku Ware #0" = 20
	"Kusi Obodum #0" = 20
	"Osei Kojo #0" = 20
	"Osei Kwame #0" = 20
	"Osei Fofie #0" = 20
	"Osei Bonsu #0" = 20
	"Osei Yaw #0" = 20
	"Kwaku Dua #0" = 20
	"Salifu Saatankugri #0" = 1
	"Mahama Kuluguba #0" = 1
	"Nyagse #0" = 1
	"Zulande #0" = 1
	"Zangina #0" = 1
	"Andan Sigili #0" = 1
	"Jimli #0" = 1
	"Zibirim #0" = 1
	"Gariba #0" = 1
	"Yakuba #0" = 1
	"Ayekeraa #0" = 1
	"Kokobo #0" = 1
	"Ahaha #0" = 1
	"Warembe Ampen #0" = 1
	"Agyinamoa #0" = 1
	"Twum #0" = 1
	"Kobla Amana #0" = 1
	"Akenten #0" = 1
	"Obiri Yeboa #0" = 1
	"Opoku #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Addy
	Afrifa
	Sadami
	Sabah
	Zakari
	Offei
	Donkor
	Akoto
	Sribor
	Moyoyo
}

ship_names = {
	Nyame "Asase Yaa" Bia Tano
	Dwoada Benada Wukuada Yawoada
	Fiada Memeneda Kwasiada
}
