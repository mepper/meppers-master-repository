#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 219  179  202 }

historical_idea_groups = {
	administrative_ideas
	popular_religion_ideas
	trade_ideas
	quantity_ideas
	leadership_ideas
	merchant_marine_ideas
	economic_ideas
	logistic_ideas
}

historical_units = { #Manchu group
	asian_mixed_crossbow_infantry
	asian_fire_lance_cavalry
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_handgunner_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_wagon_infantry
	asian_dragoon_cavalry
	asian_charge_infantry
	asian_regular_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_hunter_cavalry
	asian_impulse_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Agbarjin" = 20
	"Adai" = 20
	"Altan" = 20
	"Arigaba" = 10
	"Arugtai" = 10
	"Ayurparibhadra" = 10
	"Bodi" = 20
	"Bek" = 20
	"Bahamu" = 20
	"Bars" = 20
	"Buyan" = 20
	"Biligt�" = 10
	"Dayan" = 20
	"Darayisung" = 20
	"Delbeg" = 20
	"Ejei" = 10
	"Elbeg" = 10
	"Engke" = 10
	"G�y�k" = 10
	"G�n" = 20
	"Gulichi" = 20
	"Irinchibal" = 10
	"Jorightu" = 10
	"Kublai" = 10
	"K�ke" = 10
	"Ligdan" = 20
	"Mark�rgis" = 10
	"M�ngke" = 10
	"Mulan" = 20
	"Manduulun" = 20
	"�ljei" = 20
	"�r�g" = 15
	"Oyiradai" = 20
	"?g?ei" = 10
	"Qayshan" = 10
	"Qoshila" = 10
	"Sayn" = 20
	"Suddhipala" = 10
	"Tayisung" = 20
	"Temujin" = 10
	"Tem�r" = 10
	"Toghun" = 10
	"T�men" = 10
	"Tolui" = 10
	"Toq" = 10
	"Uskhal" = 10
	"Yes�n" = 10
	"Ariq" = 1
	"Batu" = 1
	"Eljigidei" = 1
	"Joichi" = 1
	
	"Hejing" = -5
	"Hejia" = -5
	"Hexiao" = -5
	"Zhuangjing" = -5
	"Tem�len" = -1
	"B�rte" = -1
	"Mandukhai" = -1
	"Samar" = -1
	"Zaufishan" = -1
}

leader_names = {
	Borjigin
	Besud
	Jalair
	Hatagin
	Olkhonud
	Harnut
	Eljigin
	Bayaud
	Gorlos
	Darhad
	Sharnud
	Uriankhan
	Chinos
	Onkhod
	Mangud
	Ujeed
	Onniud
	Myangan
	Yunsheebuu
	Tumed
	Sartuul
	Tangud
	Merged
	Barga
	Daguur
	Tsakhar
	Kharchin
	Uuld
	Torguut
	Jongoor
	Khotogoid
	Avga
	Tugchin
	Guchid
	Khorchin
	Khitad
	Ikh
	Asud
	Baatud
	Barnud
	Khuuchid
	Khalbagad
	Khangad
	Jarangiinkhan
	Togoruutan
	Khar'd
	Khorkhoi-nudten
	Buurluud
	Khavchig
	Bulagachid
	Gozuul
	Uzuun
	Echeed
	Khavkhchin
	Orovgod
	Tavnag
	Khaakhar
	Khunguud
	Burged
	Khuitserleg
	Chutsuut
	Ulanguud
	Ezen
	Undgaa
	Gahan
	Aksagal
	Zuun-shuvuuchin
	Zaaruud
	Dalandaganad
	Tsagaan-zel
	Khar-zel
	Khariad
	Daar'tan
	Barchuul
	Gerchuud
	Baachuud
	Buuchuud
	Khuluud
	Basigid
	Tsookhor
	Khiitluud
	Gal
	Motoi
	Tele
	Sood
	Mankhilag
	Khamnigan
	Taijiud
	Naiman
	Soloon
	Khoid
	Khereed
	Tsoros
	Talas
	Baarin
	Alag-aduun
	Saljud
	Uvashi
	Uneged
	Asan
	Uuhan
	Uush
	Tsoor
	Jalaid
	Zaisanguud
	Zurchid
	Sunid
	Tatar
	Tuuhai
	Usun
	Khalkha
	Khandgai
	Khangin
	Khiad
	Khoton
	Khurlad
	Tsagaan
	Nirun
	Durved
	Baatar
	Zelme
	Tuved
	Tavuud
	Khoshuud
	Khasag
	Urad
	Yamaat
}

ship_names = {
	Jurgen
	Heseri
	Niohuru
	Fuca
	Hun
	Jecen
	Donggo
	Neyen
	Jusheri
	Wanggiya
	Ilan
	Huye
}

army_names = {
	"$PROVINCE$ Jalan" 
}
