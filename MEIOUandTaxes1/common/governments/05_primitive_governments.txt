##############################   Primitive   ##############################
##############################  Governments  ##############################

### Demian made significant changes to Tribal governments due to the new "tribal tribute" mechanic.  Tribal Tribute mechanic makes many of these bonuses redundant and excessive.

tribal_monarchy = {
	monarchy = yes
	tribal = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	color = { 220 210 95 }
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
		2 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
		3 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
		4 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
		5 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
		6 = {
			stability_cost_modifier = 0.05
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.20
		}
	}
}
tribal_monarchy_elective = {
	tribal = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	color = { 210 200 80 }
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		2 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		3 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		4 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		5 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		6 = {
			legitimacy = 0.10
			unjustified_demands = -0.05
			global_manpower_modifier = 0.1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
	}
}

tribal_theocracy = {
	tribal = yes
	religion = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	royal_marriage = no
	has_devotion = yes
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	
	color = { 245 240 180 }
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.25
		}
		2 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.25
		}
		3 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
		}
		4 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.25
		}
		5 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.25
		}
		6 = {
			stability_cost_modifier = -0.10
			devotion = 0.05
			global_unrest = -1
			
			tolerance_own = 1
			tolerance_heretic = -1
			tolerance_heathen = -1
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -1
			
			global_institution_spread = -0.3
			global_autonomy = 0.2
			max_absolutism = -20
			technology_cost = 0.25
		}
	}
}

tribal_confederation = {
	republic = yes
	tribal = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	color = { 175 200 25 }
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
		2 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
		3 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
		4 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
		5 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
		6 = {
			stability_cost_modifier = -0.05
			merchants = 1
			global_prov_trade_power_modifier = 0.15
			trade_range_modifier = 0.10
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -3
			
			global_institution_spread = -0.20
			global_autonomy = 0.2
			max_absolutism = -30
			technology_cost = 0.15
		}
	}
}

tribal_federation = {
	tribal = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	color = { 200 210 45 }
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		2 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		3 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		4 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		5 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
		6 = {
			merchants = 1
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.05
			
			land_attrition = -0.10
			hostile_attrition = 1
			
			#land_forcelimit = -2
			
			global_institution_spread = -0.25
			global_autonomy = 0.2
			max_absolutism = -25
			technology_cost = 0.20
		}
	}
}


tribal_amalgamation = { #For non-represented areas
	tribal = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = no
	nation_designer_cost = 0
	
	republican_name = yes
	royal_marriage = no
	
	color = { 170 165 100 }
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 1
	
	rank = {
		1 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
		2 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
		3 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
		4 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
		5 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
		6 = {
			land_morale = 0.1
			infantry_power = 0.1
			defensiveness = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 2
			
			land_forcelimit = -10
			global_unrest = -100
			
			global_autonomy = 1
			max_absolutism = -100
			technology_cost = 0.30
		}
	}
}

tribal_nomads = {
	tribal = yes
	nomad = yes
	
	color = { 215 185 65 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -5
	
	rank = {
		1 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		2 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		3 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		4 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		5 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		6 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			
			stability_cost_modifier = 0.15
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
	}
}

tribal_nomads_hereditary = {
	monarchy = yes
	tribal = yes
	nomad = yes
	
	color = { 215 185 65 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -5
	
	rank = {
		1 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		2 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		3 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		4 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		5 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		6 = {
			global_prov_trade_power_modifier = 0.10
			trade_range_modifier = 0.25
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 1
			
			stability_cost_modifier = 0.20
			global_institution_spread = -0.25
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
	}
}

tribal_nomads_altaic = {
	monarchy = yes
	tribal = yes
	nomad = yes
	
	color = { 220 165 95 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -4
	
	rank = {
		1 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		2 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		3 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		4 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		5 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		6 = {
			global_regiment_cost = -0.10
			land_maintenance_modifier = -0.10
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.2
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 3
			vassal_income = 0.125
			
			stability_cost_modifier = 0.30
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
	}
}

tribal_nomads_steppe = {
	monarchy = yes
	tribal = yes
	nomad = yes
	
	color = { 210 195 35 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -5
	
	rank = {
		1 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		2 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		3 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		4 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		5 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
		6 = {
			reinforce_cost_modifier = -0.25
			trade_range_modifier = 0.20
			
			land_attrition = -0.25
			hostile_attrition = 1
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.25
			#land_forcelimit = -3
			land_forcelimit = 2
			vassal_income = 0.125
			
			stability_cost_modifier = 0.25
			global_institution_spread = -0.3
			
			global_autonomy = 0.25
			max_absolutism = -25
			technology_cost = 0.25
		}
	}
}

cossack_republic = { #Bey
	republic = yes
	tribal = yes
	nomad = yes
	
	color = { 215 200 45 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -5
	
	rank = {
		1 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
		2 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
		3 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
		4 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
		5 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
		6 = {
			unjustified_demands = -0.05
			trade_range_modifier = 0.20
			
			land_attrition = -0.10
			hostile_attrition = 0.5
			
			leader_land_manuever = 1
			cav_to_inf_ratio = 0.15
			land_forcelimit = -2
			
			stability_cost_modifier = 0.10
			global_institution_spread = -0.20
			
			global_autonomy = 0.15
			max_absolutism = -15
		}
	}
}

native_council = {
	native_mechanic = yes
	allow_migration = no
	# allow_westernize = no
	
	color = { 220 210 125 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	rank = {
		1 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
		2 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
		3 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
		4 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
		5 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
		6 = {
			trade_efficiency = 0.10
			global_institution_spread = -0.3
			
			global_autonomy = 0
			max_absolutism = -10
			technology_cost = 0.25
		}
	}
}

native_council_settled = {
	native_mechanic = yes
	# allow_westernize = no
	
	color = { 220 210 160 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	rank = {
		1 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
		2 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
		3 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
		4 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
		5 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
		6 = {
			trade_efficiency = 0.15
			global_institution_spread = -0.3
			
			global_autonomy = 0.05
			max_absolutism = -15
			technology_cost = 0.25
		}
	}
}


siberian_native_council = {
	native_mechanic = no
	allow_migration = no
	# allow_westernize = no
	
	color = { 220 210 195 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	rank = {
		1 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
		2 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
		3 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
		4 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
		5 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
		6 = {
			technology_cost = 0.05
			trade_efficiency = 0.10
			global_trade_goods_size_modifier = 0.1
			global_institution_spread = -0.20
			
			global_autonomy = 0.20
			max_absolutism = -20
			technology_cost = 0.25
		}
	}
}