##############################     Other     ##############################
##############################  Governments  ##############################

revolutionary_republic = {
	republic = yes
	
	color = { 50 85 210 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	duration = 4
	
	republican_name = yes
	royal_marriage = no
	
	max_states = 2
	
	ai_will_do = {
		factor = 0
		modifier = {
			factor = 0
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 0
			government = monarchy
		}
		modifier = {
			factor = 0
			government = merchant_republic
		}
		modifier = {
			factor = 5
			government = bureaucratic_despotism
		}
		modifier = {
			factor = 5
			has_idea = privateers
			has_idea = grand_army
			#has_idea = vetting
			#has_idea = efficient_spies
			#has_idea = shady_recruitment
		}
	}
	
#	faction = rr_jacobins
#	faction = rr_royalists
#	faction = rr_girondists
	
	rank = {
		1 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
		2 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
		3 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
		4 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
		5 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
		6 = {
			stability_cost_modifier = 0.20
			tolerance_heretic = 1
			tolerance_heathen = 1
			land_morale = 0.1
			
			global_autonomy = -0.05
			max_absolutism = 5
		}
	}
}

revolutionary_empire = {
	monarchy = yes
	
	color = { 179 175 175 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	
	max_states = 1
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = despotic_monarchy
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = enlightened_despotism
		}
		modifier = {
			factor = 5
			has_idea = patron_of_art
			has_idea = pragmatism
			has_idea = scientific_revolution
			has_idea = dynamic_court
			has_idea = tolerance_idea
			has_idea = optimism
			has_idea = formalized_officer_corps
		}
	}
	
	rank = {
		1 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
		2 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
		3 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
		4 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
		5 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
		6 = {
			stability_cost_modifier = -0.20
			global_prov_trade_power_modifier = -0.10
			global_manpower_modifier = 0.1
			land_morale = 0.15
			land_forcelimit_modifier = 0.15
			
			global_autonomy = 0.05
			max_absolutism = 5
		}
	}
}

imperial_city = {
	republic = yes
	free_city = yes
	
	color = { 220 205 15 }
	
	valid_for_new_country = no
	allow_convert = no
	duration = 4
	
	republican_name = yes
	royal_marriage = no
	
	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds
	
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	nation_designer_trigger = {
		has_dlc = "Common Sense"
		capital_scope = { is_part_of_hre = yes }
		normal_or_historical_nations = yes
	}
	
	max_states = -10
	
	rank = {
		1 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
		2 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
		3 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
		4 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
		5 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
		6 = {
			stability_cost_modifier = -0.10
			defensiveness = 0.40
			global_trade_goods_size_modifier = 0.1
			prestige = 0.1
			land_forcelimit_modifier = -0.2
			diplomatic_reputation = 1
			
			global_autonomy = 0.10
			max_absolutism = -40
		}
	}
}

# Special City States
trading_city = {
	republic = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = no
	
	color = { 30 110 240 }
	
	duration = 4
	
	republican_name = yes
	royal_marriage = no
	
	
	can_use_trade_post = yes
	can_form_trade_league = no
	is_trading_city = yes
	
	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -5
	
	rank = {
		1 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
		2 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
		3 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
		4 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
		5 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
		6 = {
			trade_efficiency = 0.15
			technology_cost = 0.10
		}
	}
}

merchant_imperial_city = {
	republic = yes
	free_city = yes
	
	color = { 220 205 15 }
	
	valid_for_new_country = no
	allow_convert = no
	duration = 4
	
	republican_name = yes
	royal_marriage = no
	
	#boost_income = yes
	can_use_trade_post = yes
	can_form_trade_league = yes
	
	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds
	
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	nation_designer_trigger = {
		has_dlc = "Common Sense"
		capital_scope = { is_part_of_hre = yes }
		normal_or_historical_nations = yes
	}
	
	max_states = -10
	
	rank = {
		1 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
		2 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
		3 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
		4 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
		5 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
		6 = {
			merchants = 1
			trade_efficiency = 0.20
			stability_cost_modifier = 0.10
			
			defensiveness = 0.10
			
			global_autonomy = 0.1
			max_absolutism = -10
		}
	}
}

#SpecialforMilan
ambrosian_republic = {
	republic = yes
	
	color = { 210 140 150 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	
	duration = 3
	
	republican_name = yes
	royal_marriage = no
	
	max_states = 0
	
	rank = {
		1 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
		2 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
		3 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
		4 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
		5 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
		6 = {
			global_tax_modifier = 0.05
			production_efficiency = 0.05
			land_morale = 0.05
			tolerance_own = -1
			mercenary_cost = -0.10
			
			global_autonomy = 0.05
			max_absolutism = -10
		}
	}
}

# Arab Countries, Earlygame
iqta = {
	monarchy = yes
	queen = no
	
	color = { 65 160 60 }
	
	unique_government = yes
	
	government_abilities = {
		iqta_mechanic
	}
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	
	allow_convert = no
	
	max_states = 3
	
	rank = {
		1 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
		2 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
		3 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
		4 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
		5 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
		6 = {
			vassal_income = 0.33
			heir_chance = 1.0
			
			
			global_autonomy = 0.10
		}
	}
}

# Special for the Dutch
dutch_republic = {
	republic = yes
	duration = 4
	
	color = { 220 120 20 }
	
	valid_for_new_country = no
	allow_convert = no
	
	republican_name = no
	royal_marriage = yes
	
	dutch_mechanics = yes
	has_parliament = yes
	
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	nation_designer_trigger = {
		has_dlc = "Res Publica"
	}
	
	max_states = 2
	
	rank = {
		1 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
		2 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
		3 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
		4 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
		5 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
		6 = {
			trade_efficiency = 0.10
			
			tolerance_heretic = 1	#FB-HT3
			tolerance_heathen = 1	#FB-HT3
			
			global_autonomy = 0.1
			max_absolutism = -5
		}
	}
}

# Polish-Lithuanian Republic
elective_monarchy = { #King
	monarchy = yes
	
	color = { 170 95 25 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	nation_designer_trigger = {
		has_dlc = "Res Publica"
	}
	allow_convert = no
	
	#is_elective = yes
	
	max_states = 2
	
	rank = {
		1 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		2 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		3 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		4 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		5 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		6 = {
			vassal_income = -0.125
			prestige_decay = -0.01
			legitimacy = 0.5
			global_manpower = -0.1
			
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
	}
}


#Special for Ireland
irish_monarchy = {
	monarchy = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 75 75 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = despotic_monarchy }
			NOT = { government = absolute_monarchy }
			NOT = { government = constitutional_monarchy }
			NOT = { government = enlightened_despotism }
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = despotic_monarchy
		}
		modifier = {
			factor = 5
			has_idea = grand_army
			has_idea = glorious_arms
			has_idea = battlefield_commisions
			has_idea = improved_foraging
			#has_idea = improved_manuever
			#has_idea = napoleonic_warfare
		}
	}
	ai_importance = 2
	
	max_states = -2
	
	#bonus
	rank = {
		1 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		2 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		3 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		4 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		5 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
		6 = {
			#major diplomatic penalties, unable to form alliances
			defensiveness = 0.15
			vassal_income = 0.125
			global_manpower_modifier = 0.20
			hostile_attrition = 2
			
			global_autonomy = 0.0
			max_absolutism = -20
		}
	}
}

#Special for Portugal
portuguese_monarchy = {
	monarchy = yes
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = -4
	
	rank = {
		1 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
		2 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
		3 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
		4 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
		5 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
		6 = {
			trade_efficiency = 0.20
			global_trade_power = 0.10
			merchants = 1
			global_foreign_trade_power = 0.20
			
			land_maintenance_modifier = 0.10
			
			naval_forcelimit_modifier = 0.20
			naval_maintenance_modifier = -0.15
			
			global_autonomy = 0.2
			max_absolutism = 0
		}
	}
}

# Colonial Nations
colonial_government = {
	republic = yes
	
	color = { 80 120 180 }
	
	duration = 8
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		1 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		2 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		3 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		4 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		5 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		6 = {
			land_forcelimit_modifier = -0.15
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
	}
}

feudal_colonial_government = {
	republic = yes
	
	color = { 180 120 80 }
	
	duration = 0
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		1 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		2 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		3 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		4 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		5 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
		6 = {
			land_forcelimit_modifier = -0.05
			
			global_trade_goods_size_modifier = 0.15
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.15
			global_trade_power = 0.10
			
			global_autonomy = 0.10
			max_absolutism = -20
		}
	}
}

trader_colonial_government = {
	republic = yes
	
	color = { 80 180 120 }
	
	duration = 8
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 0
	
	rank = {
		1 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
		2 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
		3 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
		4 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
		5 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
		6 = {
			land_forcelimit_modifier = -0.25
			
			global_trade_goods_size_modifier = 0.20
			
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.20
			global_trade_power = 0.35
			
			global_autonomy = 0.20
			max_absolutism = -20
		}
	}
}

population_colonial_government = {
	republic = yes
	
	color = { 120 180 80 }
	
	duration = 8
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		1 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
		2 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
		3 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
		4 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
		5 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
		6 = {
			land_forcelimit_modifier = -0.20
			global_trade_goods_size_modifier = 0.10
			production_efficiency = 0.10
			tolerance_heretic = 1
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			#development_cost = -0.25
			
			global_autonomy = 0.05
			max_absolutism = -20
		}
	}
}

trade_company_gov = {
	republic = yes
	
	color = { 120 180 180 }
	
	duration = 8
	
	valid_for_new_country = no
	valid_for_nation_designer = no
	nation_designer_cost = 0
	allow_convert = no
	
	republican_name = no
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 0
	
	
	rank = {
		1 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
		2 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
		3 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
		4 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
		5 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
		6 = {
			stability_cost_modifier = -0.2
			num_accepted_cultures = 3 # accepted_culture_threshold = -0.25
			liberty_desire = -20
			state_maintenance_modifier = -0.1
			core_creation = -0.25
			
			global_autonomy = 0.15
			max_absolutism = -20
		}
	}
}

# Ottoman Empire
ottoman_government = {
	monarchy = yes
	
	color = { 126  203  120 }
	
	maintain_dynasty = yes
	unique_government = yes
	has_harem = yes
	valid_for_new_country = no
	allow_convert = no
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	nation_designer_trigger = {
		religion_group = muslim
	}
	
	max_states = 4
	
	rank = {
		1 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
		2 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
		3 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
		4 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
		5 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
		6 = {
			unjustified_demands = -0.1
			defensiveness = -0.1
			legitimacy = 0.10
			state_maintenance_modifier = 0.1
			
			max_absolutism = 10
		}
	}
}

#stupid thing
prussian_monarchy = {
	monarchy = yes
	
	color = { 179 100 100 }
	
	unique_government = yes
	valid_for_new_country = no
	allow_convert = no
	
	valid_for_nation_designer = yes
	nation_designer_cost = 50
	
	militarised_society = no
	
	#bonus
	max_states = 3
	
	
	rank = {
		1 = {
			
		}
		2 = {
			
		}
		3 = {
			
		}
		4 = {
			
		}
		5 = {
			
		}
		6 = {
			
		}
	}
}

#Special for Mamluks
mamluk_government = {
	monarchy = yes
	
	color = { 250 125 10 }
	
	unique_government = yes
	
#	government_abilities = {
#		mamluk_mechanic
#	}
	
	valid_for_new_country = no
	allow_convert = no
	heir = no
	queen = no
	foreign_slave_rulers = yes
	
	valid_for_nation_designer = yes
	nation_designer_cost = 15
	
	max_states = 1
	
	rank = {
		1 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
		2 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
		3 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
		4 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
		5 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
		6 = {
			global_unrest = 1
			global_autonomy = 0.10
			defensiveness = -0.25
			land_maintenance_modifier = -0.10
			yearly_corruption = 0.5
			
			num_accepted_cultures = 2
			same_culture_advisor_cost = -0.25
			promote_culture_cost = -0.5
			monarch_admin_power = 1
		}
	}
}

#Special for Persia, Ardabil, Oman and Rassids
feudal_theocracy = { #Imamates, Sheikdoms
	monarchy = yes
	
	color = { 75 170 75 }
	
	unique_government = yes
	
	government_abilities = {
		feudal_theocracy_mechanic
	}
	
	valid_for_new_country = no
	allow_convert = no
	
	valid_for_nation_designer = yes
	nation_designer_cost = 15
	
	max_states = 1
	
	rank = {
		1 = {
			
		}
		2 = {
			
		}
		3 = {
			
		}
		4 = {
			
		}
		5 = {
			
		}
		6 = {
			
		}
	}
}
# Unique Russian Governments

veche_republic = {
	republic = yes
	
	color = { 60 120 180 }
	
	unique_government = yes
	
	duration = 4
	
	valid_for_new_country = no
	allow_convert = no
	
	republican_name = yes
	royal_marriage = no
	
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	
	nation_designer_trigger = {
		culture_group = east_slavic
	}
	
	max_states = 3
	
	rank = {
		1 = {
			merchants = 1
		}
		2 = {
			merchants = 1
		}
		3 = {
			merchants = 1
		}
		4 = {
			merchants = 1
		}
		5 = {
			merchants = 1
		}
		6 = {
			merchants = 1
		}
	}
	fixed_rank = 1
	
	government_abilities = {
		russian_mechanic
	}
	
	boost_income = yes
	can_use_trade_post = yes
	can_form_trade_league = yes
	
	faction = mr_aristocrats
	faction = mr_traders
	faction = mr_guilds
	
	ai_will_do = {
		factor = 0
	}
}

principality = {
	monarchy = yes
	
	color = { 140 50 50 }
	
	unique_government = yes
	
	valid_for_new_country = no
	allow_convert = no
	
	
	valid_for_nation_designer = yes
	nation_designer_cost = 5
	nation_designer_trigger = {
		culture_group = east_slavic
	}
	
	max_states = 3
	
	rank = {
		1 = {
		}
		2 = {
		}
		3 = {
		}
		4 = {
		}
		5 = {
		}
		6 = {
		}
	}
	fixed_rank = 1
	
	government_abilities = {
		russian_mechanic
	}
}

tsardom = {
	monarchy = yes
	
	color = { 140 50 50 }
	
	unique_government = yes
	
	valid_for_new_country = no
	allow_convert = no
	
	
	valid_for_nation_designer = yes
	nation_designer_cost = 40
	nation_designer_trigger = {
		culture_group = east_slavic
	}
	
	max_states = 10
	
	rank = {
		1 = {
			yearly_absolutism = 0.1
		}
		2 = {
			yearly_absolutism = 0.1
		}
		3 = {
			yearly_absolutism = 0.1
		}
		4 = {
			yearly_absolutism = 0.1
		}
		5 = {
			yearly_absolutism = 0.1
		}
		6 = {
			yearly_absolutism = 0.1
		}
	}
	fixed_rank = 3
	
	government_abilities = {
		russian_mechanic
	}
	claim_states = yes
}
