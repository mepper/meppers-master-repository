reduce_local_autonomy_times5_effect = {
	if = {
		limit = {
			local_autonomy = 5
			NOT = { local_autonomy = 10 }
		}
		add_local_autonomy = -1
	}
	if = {
		limit = {
			local_autonomy = 10
			NOT = { local_autonomy = 15 }
		}
		add_local_autonomy = -2
	}
	if = {
		limit = {
			local_autonomy = 15
			NOT = { local_autonomy = 20 }
		}
		add_local_autonomy = -3
	}
	if = {
		limit = {
			local_autonomy = 20
			NOT = { local_autonomy = 25 }
		}
		add_local_autonomy = -4
	}
	if = {
		limit = {
			local_autonomy = 25
			NOT = { local_autonomy = 30 }
		}
		add_local_autonomy = -5
	}
	if = {
		limit = {
			local_autonomy = 30
			NOT = { local_autonomy = 35 }
		}
		add_local_autonomy = -6
	}
	if = {
		limit = {
			local_autonomy = 35
			NOT = { local_autonomy = 40 }
		}
		add_local_autonomy = -7
	}
	if = {
		limit = {
			local_autonomy = 40
			NOT = { local_autonomy = 45 }
		}
		add_local_autonomy = -8
	}
	if = {
		limit = {
			local_autonomy = 45
			NOT = { local_autonomy = 50 }
		}
		add_local_autonomy = -9
	}
	if = {
		limit = {
			local_autonomy = 50
			NOT = { local_autonomy = 55 }
		}
		add_local_autonomy = -10
	}
	if = {
		limit = {
			local_autonomy = 55
			NOT = { local_autonomy = 60 }
		}
		add_local_autonomy = -11
	}
	if = {
		limit = {
			local_autonomy = 60
			NOT = { local_autonomy = 65 }
		}
		add_local_autonomy = -12
	}
	if = {
		limit = {
			local_autonomy = 65
			NOT = { local_autonomy = 70 }
		}
		add_local_autonomy = -13
	}
	if = {
		limit = {
			local_autonomy = 70
			NOT = { local_autonomy = 75 }
		}
		add_local_autonomy = -14
	}
	if = {
		limit = {
			local_autonomy = 75
			NOT = { local_autonomy = 80 }
		}
		add_local_autonomy = -15
	}
	if = {
		limit = {
			local_autonomy = 80
			NOT = { local_autonomy = 85 }
		}
		add_local_autonomy = -16
	}
	if = {
		limit = {
			local_autonomy = 85
			NOT = { local_autonomy = 90 }
		}
		add_local_autonomy = -17
	}
	if = {
		limit = {
			local_autonomy = 90
			NOT = { local_autonomy = 95 }
		}
		add_local_autonomy = -18
	}
	if = {
		limit = {
			local_autonomy = 95
			NOT = { local_autonomy = 100 }
		}
		add_local_autonomy = -19
	}
	if = {
		limit = { local_autonomy = 100 }
		add_local_autonomy = -20
	}
}