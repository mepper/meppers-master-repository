# This file contains the core of the system.

clear_budget_tooltip_variables = {
	hidden_effect = {
		set_variable = { which = court_state_contribute_monthly		value = 0 }
		set_variable = { which = education_state_contribute_monthly	value = 0 }
		set_variable = { which = capital_maintenance_monthly		value = 0 }
		set_variable = { which = tribal_tribute_favor_monthly		value = 0 }
		set_variable = { which = EstateIncome_Trade_monthly			value = 0 }
		set_variable = { which = AI_expenses_monthly				value = 0 }
		set_variable = { which = road_expenses_monthly				value = 0 }
		set_variable = { which = art_and_uni_expenses_monthly		value = 0 }
	}
}

remove_misc_expense_modifiers = {
	clear_income_modifier = { modname=misc_expenses value=5242.88 type=province }
	clear_income_modifier = { modname=misc_expenses value=2621.44 type=province }
	clear_income_modifier = { modname=misc_expenses value=1310.72 type=province }
	clear_income_modifier = { modname=misc_expenses value=655.36 type=province }
	clear_income_modifier = { modname=misc_expenses value=327.68 type=province }
	clear_income_modifier = { modname=misc_expenses value=163.84 type=province }
	clear_income_modifier = { modname=misc_expenses value=81.92 type=province }
	clear_income_modifier = { modname=misc_expenses value=40.96 type=province }
	clear_income_modifier = { modname=misc_expenses value=20.48 type=province }
	clear_income_modifier = { modname=misc_expenses value=10.24 type=province }
	clear_income_modifier = { modname=misc_expenses value=5.12 type=province }
	clear_income_modifier = { modname=misc_expenses value=2.56 type=province }
	clear_income_modifier = { modname=misc_expenses value=1.28 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.64 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.32 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.16 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.08 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.04 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.02 type=province }
	clear_income_modifier = { modname=misc_expenses value=0.01 type=province }
}

add_misc_expenses_modifiers = {
	capital_scope = {
		set_variable = { which = misc_expenses_calc			value = 0 }
		change_variable = { which = misc_expenses_calc		which = PREV }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=5242.88 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=2621.44 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=1310.72 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=655.36 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=327.68 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=163.84 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=81.92 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=40.96 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=20.48 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=10.24 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=5.12 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=2.56 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=1.28 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.64 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.32 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.16 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.08 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.04 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.02 type=province }
		reset_income_modifier = { varname=misc_expenses_calc modname=misc_expenses value=0.01 type=province }
		set_variable = { which = misc_expenses_calc			value = 0 }
	}
	set_variable = { which = misc_expenses_calc			value = 0 }
}

# Calls the event that itself hosts the main effect.
set_misc_expenses = {
	country_event = { id = miscexpenses.002 }
}

# The main effect of the misc expenses system. It does the preliminary work, and then calls add_misc_expenses_modifiers to add the maintenance modifiers.
calc_misc_expenses = {
	set_variable = { which = misc_expenses_calc 	value = 0 }
	if = {
		limit = {
			check_variable = { which = court_state_contribute		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = court_state_contribute
		}
	}
	if = {
		limit = {
			check_variable = { which = education_state_contribute		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = education_state_contribute
		}
	}
	if = {
		limit = {
			check_variable = { which = AI_expenses		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = AI_expenses
		}
	}
	if = {
		limit = {
			check_variable = { which = capital_maintenance		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = capital_maintenance
		}
	}
	if = {
		limit = {
			check_variable = { which = EstateIncome_Trade		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = EstateIncome_Trade
		}
	}
	if = {
		limit = {
			check_variable = { which = tribal_tribute_favor		value = 0.001 }
			check_variable = { which = estate_tribals_weight_share		value = 0.01 }
			has_country_flag = tribals_recieving_tribute
		}
		change_variable = {
			which = misc_expenses_calc
			which = tribal_tribute_favor
		}
	}
	if = {
		limit = {
			check_variable = { which = road_expenses		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = road_expenses
		}
	}
	if = {
		limit = {
			check_variable = { which = art_and_uni_expenses		value = 0.001 }
		}
		change_variable = {
			which = misc_expenses_calc
			which = art_and_uni_expenses
		}
	}
	set_variable = { which = misc_expenses_cache 	value = 0 }
	change_variable = {
		which = misc_expenses_cache
		which = misc_expenses_calc
	}
	multiply_variable = { which = misc_expenses_calc 	value = 10 }
	set_variable = { which = dev_var 	value = 0 }
	capital_scope = {
		set_variable = { which = dev_var 	value = 0 }
		export_to_variable = {
			which = dev_var
			value = development
		}
		PREV = {
			change_variable = {
				which = dev_var
				which = PREV
			}
		}
		set_variable = { which = misc_expenses_cache 	value = 0 }
		change_variable = {
			which = misc_expenses_cache
			which = PREV
		}
		set_variable = { which = dev_var 	value = 0 }
	}
	
	if = {
		limit = {
			check_variable = { which = dev_var		value = 0.001 }
			check_variable = { which = misc_expenses_calc		value = 0.001 }
		}
		divide_variable = {
			which = misc_expenses_calc
			which = dev_var
		}
	}
	if = {
		limit = {
			check_variable = { which = misc_expenses_calc		value = 0.001 }
		}
		divide_variable = {
			which = misc_expenses_calc
			value = 12
		}
	}
	
	add_misc_expenses_modifiers = yes
	if = {
		limit = {
			NOT = { has_global_flag = show_for_developers }
		}
		set_variable = { which = dev_var 	value = 0 }
	}
}

# Called whenever a province increases its population.
# If the province is the capital, the state maintenance modifiers have to be updated to match the new base cost.
update_misc_expenses_if_capital = {
	if = {
		limit = {
			is_capital = yes
		}
		owner = {
			set_misc_expenses = yes
		}
	}
}