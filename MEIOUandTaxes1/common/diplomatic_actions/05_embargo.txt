# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	disallow				Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

# embargo action
embargoaction = {
	condition = {
		tooltip = MEDIAVALEMBARGO
		potential = {
		}
		allow = {
			OR = {
				OR = {
					is_subject = no
					is_subject_of_type = tributary_state
				}
				overlord = { NOT = { government = medieval_monarchy } }
			}
		}
	}
	condition = {
		tooltip = AMALGAMATION
		potential = {
			OR = {
				government = tribal_amalgamation
				FROM = { government = tribal_amalgamation }
			}
		}
		allow = {
			always = no
		}
	}
	condition = {
		tooltip = PIZARROBROTHERS
		potential = {
			OR = {
				FROM = {
					tag = PIZ
				}
				tag = PIZ
			}
		}
		allow = {
			always = no
		}
	}
}
