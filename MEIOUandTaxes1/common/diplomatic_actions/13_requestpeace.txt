# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	disallow				Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

requestpeace = {
	condition = {
		tooltip = AMALGAMATION
		potential = {
			has_country_flag = great_predator
			has_country_flag = in_good_shape
			
			ai = yes
		}
		allow = {
			FROM = {
				OR = {
					NOT = { has_country_modifier = gp_3 }
					
					ROOT = {
						war_score_against = {
							who = FROM
							
							value = 90
						}
					}
				}
				
				OR = {
					NOT = {
						has_country_modifier = gp_10
					}
					
					ROOT = {
						AND = {
							war_score_against = {
								who = FROM
								
								value = 90
							}
							
							NOT = { check_variable = { which = num_of_wd value = 4 } }
						}
					}
					
					war_score_against = {
						who = ROOT
						
						value = 30
					}
					
					any_ally = {
						defensive_war_with = ROOT
						
						war_score_against = {
							who = ROOT
							
							value = 30
						}
					}
				}
			}
		}
	}
	condition = {
		tooltip = gp_war_cont
		potential = {
			FROM = {
				has_country_flag = great_predator
				has_country_flag = in_good_shape
				
				ai = yes
			}
		}
		allow = {
			OR = {
				NOT = { has_country_modifier = gp_3 }
				
				FROM = {
					war_score_against = {
						who = ROOT
						
						value = 90
					}
				}
			}
			
			OR = {
				NOT = {
					has_country_modifier = gp_10
				}
				
				FROM = {
					AND = {
						war_score_against = {
							who = ROOT
							
							value = 90
						}
						
						NOT = { check_variable = { which = num_of_wd value = 4 } }
					}
				}
				
				war_score_against = {
					who = FROM
					
					value = 30
				}
				
				any_ally = {
					defensive_war_with = FROM
					
					war_score_against = {
						who = FROM
						
						value = 30
					}
				}
			}
		}
	}
}
