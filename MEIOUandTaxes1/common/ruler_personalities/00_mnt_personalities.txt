#######################################
#########                   ###########
####       MEIOU and Taxes        #####
#########                   ###########
#######################################


###############################################
# Good
###############################################

#"Statesman" Traits
fanatical_priest_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	chance = {
		modifier = {
			factor = -10
			NOT = { religion = yes }
		}
	}
	tolerance_heretic = -1.5
	tolerance_heathen = -1.5
	tolerance_own = 1
	global_missionary_strength = 0.02
	
	different_religion_war_multiplier = 1.5
	heretic_ally_acceptance = -1000
	heathen_ally_acceptance = -1000
	wants_heathen_land = yes
	wants_heretic_land = yes
	
	nation_designer_cost = 5
}

midas_touched_personality = {
	ruler_allow = {
		allow = { adm = 5 }
	}
	heir_allow = {
		allow = { adm = 5 }
	}
	consort_allow = {
		allow = { adm = 5 }
	}
	
	trade_efficiency = 0.05
	production_efficiency = 0.05
	global_tax_modifier = 0.05
	
	nation_designer_cost = 5
}

master_theologian_personality = {
	ruler_allow = {
		allow = { adm = 5 }
	}
	heir_allow = {
		allow = { adm = 5 }
	}
	consort_allow = {
		allow = { adm = 5 }
	}
	
	chance = {
		modifier = {
			factor = -0.5
			NOT = { religion = yes }
		}
		modifier = {
			factor = 1.5
			religion = yes
		}
	}
	
	adm_tech_cost_modifier = -0.05
	tolerance_heretic = 0.5
	tolerance_heathen = 0.5
	global_missionary_strength = 0.01
	
	nation_designer_cost = 5
}

scholarly_theologian_personality = {
	ruler_allow = {
		allow = {
			adm = 4
			NOT = { ruler_has_personality = master_theologian_personality }
		}
	}
	heir_allow = {
		allow = {
			adm = 4
			NOT = { heir_has_personality = master_theologian_personality }
		}
	}
	consort_allow = {
		allow = {
			adm = 4
			NOT = { consort_has_personality = master_theologian_personality }
		}
	}
	
	chance = {
		modifier = {
			factor = -0.5
			NOT = { religion = yes }
		}
		modifier = {
			factor = 1.5
			religion = yes
		}
	}
	
	adm_tech_cost_modifier = -0.025
	tolerance_heretic = 0.5
	tolerance_heathen = 0.5
	
	nation_designer_cost = 5
}

forgiving_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	improve_relation_modifier = 0.25
	prestige = -0.25
	global_unrest = -1
	
	nation_designer_cost = 5
}


#Economic/Administrative Traits
crafty_merchant_personality = {
	ruler_allow = {
		allow = { dip = 5 }
	}
	heir_allow = {
		allow = { dip = 5 }
	}
	consort_allow = {
		allow = { dip = 5 }
	}
	
	chance = {
		modifier = {
			factor = 1
			government = republic
		}
	}
	
	global_own_trade_power = 0.05
	global_foreign_trade_power = 0.05
	
	nation_designer_cost = 5
}

generous_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = 0.1
	global_unrest = -2
	global_tax_modifier = -0.05
	
	nation_designer_cost = 5
}

sceptical_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	tolerance_heretic = 0.5
	tolerance_heathen = 0.5
	tolerance_own = -0.5
	prestige = -0.10
	
	global_missionary_strength = -0.01
	nation_designer_cost = 5
}

energetic_personality = {
	ruler_allow = {
		allow = { mil = 4 }
	}
	heir_allow = {
		allow = { mil = 4 }
	}
	consort_allow = {
		allow = { mil = 4 }
	}
	
	prestige = 0.1
	global_tax_modifier = 0.025
	trade_efficiency = 0.05
	advisor_cost = -0.025
	production_efficiency = 0.05
	land_morale = 0.025
	
	nation_designer_cost = 5
}

wise_personality = {
	ruler_allow = {
		allow = { adm = 5 }
	}
	heir_allow = {
		allow = { adm = 5 }
	}
	consort_allow = {
		allow = { adm = 5 }
	}
	
	technology_cost = -0.025
	diplomatic_reputation = 0.25
	prestige = 0.25
	advisor_pool = 1
	
	nation_designer_cost = 5
}

#Diplomacy Traits
elusive_shadow_personality = {
	ruler_allow = {
		allow = { dip = 4 }
	}
	heir_allow = {
		allow = { dip = 4 }
	}
	consort_allow = {
		allow = { dip = 4 }
	}
	
	prestige = -0.1
	diplomatic_reputation = 0.5
	spy_offence = 0.10
	
	nation_designer_cost = 5
}

grey_eminence_personality = {
	ruler_allow = {
		allow = { dip = 5 }
	}
	heir_allow = {
		allow = { dip = 5 }
	}
	consort_allow = {
		allow = { dip = 5 }
	}
	
	prestige = 0.1
	diplomatic_upkeep = 1
	improve_relation_modifier = 0.25
	diplomatic_reputation = 0.75
	
	nation_designer_cost = 5
}

gruff_diplomat_personality = {
	ruler_allow = {
		allow = { NOT = { dip = 3 } }
	}
	heir_allow = {
		allow = { NOT = { dip = 3 } }
	}
	consort_allow = {
		allow = { NOT = { dip = 3 } }
	}
	
	diplomatic_reputation = -0.5
	improve_relation_modifier = -0.25
	diplomatic_upkeep = 1
	
	nation_designer_cost = 5
}

flamboyant_schemer_personality = {
	ruler_allow = {
		allow = { dip = 5 }
	}
	heir_allow = {
		allow = { dip = 5 }
	}
	consort_allow = {
		allow = { dip = 5 }
	}
	
	prestige = 0.1
	diplomatic_upkeep = 1
	spy_offence = 0.20
	global_spy_defence = 0.10
	
	nation_designer_cost = 5
}

honest_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = 0.1
	diplomatic_reputation = 0.5
	spy_offence = -0.05
	global_spy_defence = -0.05
	
	nation_designer_cost = 5
}

#Military Traits
brilliant_strategist_personality = {
	ruler_allow = {
		allow = { mil = 5 }
	}
	heir_allow = {
		allow = { mil = 5 }
	}
	consort_allow = {
		allow = { mil = 5 }
	}
	
	land_forcelimit_modifier = 0.05
	land_morale = 0.025
	discipline = 0.01
	prestige_from_land = 0.25
	army_tradition_from_battle = 0.25
	
	nation_designer_cost = 5
}

knowledged_tactician_personality = {
	ruler_allow = {
		allow = { mil = 4 }
	}
	heir_allow = {
		allow = { mil = 4 }
	}
	consort_allow = {
		allow = { mil = 4 }
	}
	
	prestige_from_land = 0.25
	land_attrition = -0.05
	mil_tech_cost_modifier = -0.025
	
	nation_designer_cost = 5
}

martial_cleric_personality = {
	ruler_allow = {
		allow = { mil = 4 }
	}
	heir_allow = {
		allow = { mil = 4 }
	}
	consort_allow = {
		allow = { mil = 4 }
	}
	
	chance = {
		modifier = {
			factor = -10
			NOT = { religion = yes }
		}
	}
	
	tolerance_heathen = -1
	land_morale = 0.10
	
	nation_designer_cost = 5
}

tough_soldier_personality = {
	ruler_allow = {
		allow = { mil = 4 }
	}
	heir_allow = {
		allow = { mil = 4 }
	}
	consort_allow = {
		allow = { mil = 4 }
	}
	
	discipline = 0.01
	shock_damage = 0.1
	land_morale = 0.025
	
	nation_designer_cost = 5
}

valorous_personality = {
	ruler_allow = {
		allow = { mil = 4 }
	}
	heir_allow = {
		allow = { mil = 4 }
	}
	consort_allow = {
		allow = { mil = 4 }
	}
	
	land_morale = 0.1
	prestige = 0.25
	diplomatic_reputation = 0.25
	
	nation_designer_cost = 5
}


###############################################
# Neutral/Others
###############################################
temperate_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = 0.10
	global_unrest = -0.5
	
	nation_designer_cost = 5
}

###############################################
# Bad
###############################################

#"Statesman" Traits
coward_personality = {
	ruler_allow = {
		allow = { NOT = { mil = 3 } }
	}
	heir_allow = {
		allow = { NOT = { mil = 3 } }
	}
	consort_allow = {
		allow = { NOT = { mil = 3 } }
	}
	
	enemy_strength_multiplier = 1.25
	
	land_morale = -0.10
	diplomatic_reputation = -0.5
	prestige = -0.25
	
	nation_designer_cost = 5
}

lazy_personality = {
	ruler_allow = {
		allow = { NOT = { mil = 3 } }
	}
	heir_allow = {
		allow = { NOT = { mil = 3 } }
	}
	consort_allow = {
		allow = { NOT = { mil = 3 } }
	}
	
	prestige = -0.25
	global_tax_modifier = -0.05
	trade_efficiency = -0.05
	production_efficiency = -0.05
	
	nation_designer_cost = 5
}

selfish_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = -0.25
	stability_cost_modifier = 0.10
	diplomatic_reputation = -0.75
	
	nation_designer_cost = 5
}

#Economic/Administrative Traits
hopeless_spender_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	infantry_cost = 0.05
	cavalry_cost = 0.05
	artillery_cost = 0.05
	heavy_ship_cost = 0.05
	light_ship_cost = 0.05
	galley_cost = 0.05
	transport_cost = 0.05
	build_cost = 0.05
	
	global_tax_modifier = -0.05
	nation_designer_cost = 5
}

suspicious_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	diplomatic_upkeep = -1
	global_spy_defence = 0.20
	prestige = -0.10
	
	nation_designer_cost = 5
}

indulgent_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = 0.25
	land_morale = -0.05
	global_tax_modifier = -0.1
	
	nation_designer_cost = 5
}

#Diplomacy Traits
trusting_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	yearly_corruption = 0.025
	advisor_cost = -0.025
	spy_offence = -0.05
	global_spy_defence = -0.10
	diplomatic_reputation = -0.5
	
	nation_designer_cost = 5
}

amateurish_pettifogger_personality = {
	ruler_allow = {
		allow = { NOT = { dip = 2 } }
	}
	heir_allow = {
		allow = { NOT = { dip = 2 } }
	}
	consort_allow = {
		allow = { NOT = { dip = 2 } }
	}
	
	prestige = -0.1
	diplomatic_reputation = -0.75
	
	nation_designer_cost = 5
}

naive_puppet_master_personality = {
	ruler_allow = {
		allow = { NOT = { dip = 3 } }
	}
	heir_allow = {
		allow = { NOT = { dip = 3 } }
	}
	consort_allow = {
		allow = { NOT = { dip = 3 } }
	}
	
	prestige = -0.1
	spy_offence = -0.05
	global_spy_defence = -0.10
	
	nation_designer_cost = 5
}

deceitful_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige = -0.1
	ae_impact = 0.05
	stability_cost_modifier = 0.05
	
	improve_relation_modifier = -0.15
	nation_designer_cost = 5
}

arbitrary_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	improve_relation_modifier = -0.10
	prestige = -0.10
	global_unrest = 1
	
	nation_designer_cost = 5
}

modest_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	global_tax_modifier = 0.1
	stability_cost_modifier = -0.1
	
	prestige = -0.1
	nation_designer_cost = 5
}

proud_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	legitimacy = 0.10
	prestige = 0.10
	
	diplomatic_reputation = -0.5
	nation_designer_cost = 5
}

vengeful_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	land_morale = 0.025
	prestige_from_land = 0.25
	diplomatic_reputation = -0.5
	prestige = -0.10
	
	nation_designer_cost = 5
}

aggressive_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	prestige_from_land = 0.25
	diplomatic_reputation = -1
	improve_relation_modifier = -0.15
	fabricate_claims_cost = -0.10
	ae_impact = 0.05
	
	nation_designer_cost = 5
}

#Military Traits
misguided_warrior_personality = {
	ruler_allow = {
		allow = { NOT = { mil = 3 } }
	}
	heir_allow = {
		allow = { NOT = { mil = 3 } }
	}
	consort_allow = {
		allow = { NOT = { mil = 3 } }
	}
	
	discipline = -0.025
	land_morale = -0.05
	army_tradition_decay = 0.005
	prestige = -0.10
	
	nation_designer_cost = 5
}

reckless_personality = {
	ruler_allow = { }
	heir_allow = { }
	consort_allow = { }
	
	land_morale = 0.05
	shock_damage = 0.1
	siege_ability = -0.1
	prestige = -0.1
	defensiveness = -0.05
	
	nation_designer_cost = 5
}





# Already used in 00_core.txt

# charismatic_negotiator_personality = {
# diplomatic_reputation = 1
# diplomatic_upkeep = 1
# improve_relation_modifier = 0.1
# }

# intricate_webweaver_personality = {
# diplomatic_upkeep = 1
# spy_offence = 0.1
# global_spy_defence = 0.10
# }

# silver_tongue_personality = {
# trade_efficiency = 0.05
# diplomatic_upkeep = 1
# global_trade_power = 0.1
# }

# cruel_personality = {
# tolerance_heretic = -1
# tolerance_heathen = -1
# global_tax_modifier = 0.1
# global_unrest = 2
# }

# just_personality = {
# prestige = 0.2
# global_unrest = -4
# improve_relation_modifier = 0.1
# }

# zealous_personality = {
# global_missionary_strength = 0.01
# tolerance_heretic = -1
# tolerance_heathen = -1
# }
