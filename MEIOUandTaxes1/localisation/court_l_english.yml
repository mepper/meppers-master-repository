﻿l_english:
 court.001.t:1 "$MONARCH$'s Court"
 court.001.d:1 "As the ruler of $COUNTRY$, $MONARCH$ was expected to live in a style fit for a $MONARCHTITLE$, and maintain a court to match $RULER_ADJ$ power - and $RULER_ADJ$ ambitions.\n\nMaintaining a proper court was usually the second-largest expense for a nation after paying for the army.  Even a humble ruler had multiple palaces to maintain, servants to pay, feasts to hold, courtiers to entertain, art to commission and ceremonies to perform.  Although the expense could be ruinous, a luxurious court was indispensable to a ruler.  Ostentatious displays of wealth were not just for the $MONARCHTITLE$'s benefit - they were a public display of $RULER_ADJ$ power!  This impressed foreign dignitaries and kept locals from considering rebellion.  A lavish court also provided entertainment and pensioned positions for aristocrats, keeping them occupied jockeying for position at court rather than raising their banners against their liege."
 court.001.e_maintain:1 "Maintain the same type of court"
 court.001.e_best:1 "Spare no expense!"
 court.001.e_better:1 "Spend lavishly"
 court.001.e_good:1 "Maintain a luxurious court"
 court.001.e_bad:1 "Maintain a small but sensible court"
 court.001.e_worst:1 "Pare it down to the bare minimum"
 court_level_1: "No Court (lvl. 1)"
 desc_court_level_1: "Our ruler either can't afford a suitable court or prefers to keep to army camps and office desks, spending as little time with courtiers as possible.  While this may show an admirable work ethic, it is also a failure to grasp the influence to be gained through displays of luxury.  It may win the friendship of your minister of treasury, but it won't win the friendship of your aristocrats."
 court_level_2: "Shabby Court (lvl. 2)"
 desc_court_level_2: "\n§WArt Power in Capital:§! §G+10%§!\n§WArt Power in Regional Capitals:§! §G+5%§!\n\nOur ruler's court is rather simple.  While there is something to be said for not wasting our money on frivolities, we will not impress anyone with the wealth or culture of our nation, and many of our aristocrats are finding other things to occupy their time."
 court_level_3: "Respectable Court (lvl. 3)"
 desc_court_level_3: "\n§WArt Power in Capital:§! §G+25%§!\n§WArt Power in Regional Capitals:§! §G+12.5%§!\n\nIt is clear to anyone who attends our court that our efforts serious, but it won't leave many lasting impressions nor be any envious destination for foreign dignitaries or diplomats."
 court_level_4: "Impressive Court (lvl. 4)"
 desc_court_level_4: "\n§WArt Power in Capital:§! §G+45%§!\n§WArt Power in Regional Capitals:§! §G+22.5%§!\n\nOur ruler's court is lavish.  With artists, entertainers and feasts to fill palaces, it would take months to become tired of its splendour."
 court_level_5: "Luxurious Court (lvl. 5)"
 desc_court_level_5: "\n§WArt Power in Capital:§! §G+60%§!\n§WArt Power in Regional Capitals:§! §G+30%§!\n\nOur court is amongst the most splendid, and attracts talent from around the continent!  Nobles at home and abroad dream of the next occasion they can visit our gilded halls!"
 court_level_6: "Magnificent Court (lvl. 6)"
 desc_court_level_6: "\n§WArt Power in Capital:§! §G+75%§!\n§WArt Power in Regional Capitals:§! §G+37.5%§!\n\nOur ruler's court is the envy of emperors!  Filled with brilliant artists, exciting entertainments and glittering galas, it is sure to impress visitors.  It is the centre of attention for our aristocracy and employs thousands!  One could exhaust lifetimes plunging its pleasures!"
 art_level_1: "Cultured (lvl. 1)"
 desc_art_level_1: ""
 art_level_2: "Admirable Culture (lvl. 2)"
 desc_art_level_2: ""
 art_level_3: "Impressive Culture (lvl. 3)"
 desc_art_level_3: ""
 art_level_4: "Highly Cultured (lvl. 4)"
 desc_art_level_4: ""
 art_level_5: "World Renown Culture (lvl. 5)"
 desc_art_level_5: ""
 
 court_best:1 "Magnificent Court"
 desc_court_best:1 "Our ruler's court is the envy of emperors!  Filled with brilliant artists, exciting entertainments and glittering galas, it is sure to impress visitors.  It is the centre of attention for our aristocracy and employs thousands!"
 court_better:1 "Regal Court"
 desc_court_better:1 "Our ruler's court is lavish.  With artists, entertainers and feasts to fill palaces, it would take months to become tired of its splendour."
 court_bad:1 "Simple Court"
 desc_court_bad:1 "Our ruler's court is rather simple.  While there is something to be said for not wasting our money on frivolities, we will not impress anyone with the wealth or culture of our nation, and many of our aristocrats are finding other things to occupy their time."
 court_worst:1 "No Court"
 desc_court_worst:1 "Our ruler prefers to keep to army camps and office desks, spending as little time with courtiers as possible.  This shows an admirable work ethic, but also a failure to grasp the influence to be gained through displays of luxury.  It may not be ideal to have idle aristocrats or unemployed artists."
 court.002.t:1 "Court Expenses Spiral out of Control"
 court.002.d:1 "The $MONARCHTITLE$'s court in $CAPITAL_CITY$ employs thousands of cooks, servants, performers and guards, not to mention the expense of maintaining palaces and holding festivities.  It all adds up!  And up, and up, and up.  In fact, the expense is beginning to cripple the treasury!  Our treasurer is adamant that we must immediately curtail our expenses."
 court.002.a:1 "Let our gold flow as freely as the wine"
 court.002.b:1 "Put an end to our reckless spending"
 court.002.c:1 "Take charge of the finances personally"
 court.003.t:1 "Bored Aristocrats"
 court.003.d:1 "Familiarity breeds contempt, and it appears our aristocracy has become all too familiar with our court.  Despite the glittering palaces, sumptuous feasts and impressive entertainment, it seems that increasingly our guests leave...bored.  This is humiliating to us!  Our master of ceremonies begs us to let him hire new entertainment and import fabulous new luxuries with which to impress."
 court.003.a:1 "Reinvigorate our court"
 court.003.b:1 "It should be good enough for them"
 court.003.c:1 "Take charge of the entertainment personally"
 court.004.t:1 "The $MONARCHTITLE$'s favourite"
 court.004.d:1 "It is an open secret that the $MONARCHTITLE$ has a court favourite.  The pair is almost inseparable, clearly in love.  Yet now the object of $MONARCH$'s affections is making demands - a grand ball, new outfits, more entertainment!  Perhaps it is time to consider letting go of our restraint and hosting a court that anyone could be proud of!  Or then again, perhaps it is time to pick a new favourite."
 court.004.a:1 "I would do anything for love"
 court.004.b:1 "I won't do that"
 raise_court_level_title:1 "Improve our court"
 raise_court_level_desc:1 "Our court is not as impressive as it could be.  A proper [Root.Monarch.GetTitle] deserves better than this!"
 
 court.100.title: "§GThe Royal Court§!"
 court.100.desc: "The grandeur and prestige of a country is often times inseparable from the splendour of its royal court.  Aside from naked displays of wealth, elements of a nation's court include artists, intellectuals, entertainers, opulent feasts, towering palaces, grand galas, verdant gardens, and much, much more.  A spartan court - while it may not truly reflect the true wealth of your country - can leave both foreign dignitaries and domestic aristocrats uninterested or unwilling to remain engaged with their sovereign.  A magnificent court which dazzles its visitors, however, will reap incalculable diplomatic benefits.  So important are courts as an instrument of governance that court costs were often only rivalled by the cost of the military.\n\nAside from improving your court via yearly funding, your capital buildings will give roughly half their maintenance cost toward improving your court.  A higher court level will also slightly improve the loyalty and stability gains made from the semi-random events which improve loyalty.\n\nCourt Level: [Root.court_level.GetValue]%\nChange Last Year: [Root.court_yearly_gain.GetValue]%\nCourt Wealth: [Root.court_money_endowed.GetValue]¤\nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]¤\nYearly Bonus from Capital Buildings: [Root.court_capital_bonus.GetValue]¤"
 court.100.a: "§GIncrease§! Annual Funding"
 court.100.b: "§RDecrease§! Annual Funding"
 court.100.c: "Grant One Time §OStipend§!"
 court.100.e: "Exit"
 court.100.f: "Current Court Level and Information"
 court.100.g: "Don't show me Court Level Change Popups anymore."
 court.100.h: "Show me Court Level Change Popups again."
 
 court.101.title: "§YIncrease Funding§!"
 court.101.desca: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: 0"
 court.101.descb: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]"
 court.101.a: "Increase Annual Funding by §G1§!¤"
 court.101.b: "Increase Annual Funding by §G5§!¤"
 court.101.c: "Increase Annual Funding by §G10§!¤"
 court.101.d: "Increase Annual Funding by §G25§!¤"
 court.101.e: "Increase Annual Funding by §G50§!¤"
 court.101.f: "Increase Annual Funding by §G100§!¤"
 court.101.g: "Increase Annual Funding by §G500§!¤"
 court.101.y: "§GBack.§!"
 court.101.z: "§RExit.§!"
 
 court.1011.title: "§YIncrease Funding§!"
 court.1011.desca: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: 0"
 court.1011.descb: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]"
 court.1011.a: "Increase Annual Funding by §G1§!¤"
 court.1011.b: "Increase Annual Funding by §G5§!¤"
 court.1011.c: "Increase Annual Funding by §G10§!¤"
 court.1011.d: "Increase Annual Funding by §G25§!¤"
 court.1011.e: "Increase Annual Funding by §G50§!¤"
 court.1011.f: "Increase Annual Funding by §G100§!¤"
 court.1011.g: "Increase Annual Funding by §G500§!¤"
 court.1011.y: "§GBack.§!"
 court.1011.z: "§RExit.§!"
 
 court.102.title: "§YDecrease Funding§!"
 court.102.desca: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: 0"
 court.102.descb: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]"
 court.102.a: "Decrease Annual Funding by §R1§!¤"
 court.102.b: "Decrease Annual Funding by §R5§!¤"
 court.102.c: "Decrease Annual Funding by §R10§!¤"
 court.102.d: "Decrease Annual Funding by §R25§!¤"
 court.102.e: "Decrease Annual Funding by §R50§!¤"
 court.102.f: "Decrease Annual Funding by §R100§!¤"
 court.102.g: "Decrease Annual Funding by §R500§!¤"
 court.102.y: "§GBack.§!"
 court.102.z: "§RExit.§!"
 
 court.1021.title: "§YDecrease Funding§!"
 court.1021.desca: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: 0"
 court.1021.descb: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]"
 court.1021.a: "Decrease Annual Funding by §R1§!¤"
 court.1021.b: "Decrease Annual Funding by §R5§!¤"
 court.1021.c: "Decrease Annual Funding by §R10§!¤"
 court.1021.d: "Decrease Annual Funding by §R25§!¤"
 court.1021.e: "Decrease Annual Funding by §R50§!¤"
 court.1021.f: "Decrease Annual Funding by §R100§!¤"
 court.1021.g: "Decrease Annual Funding by §R500§!¤"
 court.1021.y: "§GBack.§!"
 court.1021.z: "§RExit.§!"
 
 court.103.title: "§YLump Sum Funding§!"
 court.103.desca: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: 0"
 court.103.descb: "You can adjust court costs here, and your court will sustain itself from the court wealth pool.  One must keep in mind though that both the court wealth pool and court level will decline fairly rapidly, so one must continually inject funds into the court to maintain its greatness.\n\nCourt Wealth: [Root.court_money_endowed.GetValue]¤ \nCurrent Yearly Funding: [Root.court_state_contribute.GetValue]"
 court.103.a: "Grant our court §G10§!¤"
 court.103.b: "Grant our court §G50§!¤"
 court.103.c: "Grant our court §G100§!¤"
 court.103.d: "Grant our court §G500§!¤"
 court.103.e: "Grant our court §G1000§!¤"
 court.103.f: "Grant our court §G5000§!¤"
 court.103.g: "Grant our court §G10000§!¤"
 court.103.y: "§GBack.§!"
 court.103.z: "§RExit.§!"
 
 court.104.title: "§YMore Information§!"
 court.104.desc: "Down below you will see the court level thresholds as well as the bonuses that your court gives to capital and regional capital art power.\n\nCourt Thresholds:\nNo Court: 0-15\nShabby Court: 15-30\nRespectable Court: 30-45\nImpressive Court: 45-65\nLuxurious Court: 65-90\nMagnificent Court: 90-100.\n\nArt Power Bonuses:\nShabby Court: 10% in Capital, 5% in Regional Capitals\nRespectable Court: 20% in Capital, 15% in Regional Capitals\nImpressive Court: 30% in Capital, 15% in Regional Capitals\nLuxurious Court: 40% in Capital, 20% in Regional Capitals\nMagnificent Court: 50% in Capital, 25% in Regional Capitals."
 court.104.y: "§GBack.§!"
 court.104.z: "Exit!"
 
 loyalty_improved_by_court: "These gains to loyalty and stability are multiplied by §G[Root.court_level_modifier.GetValue]§!% due to our current court level."

 court_level_2_art_bonus: "Art Power Modifier in Capital: §G+10%§!\nArt Power Modifier in Regional Capitals: §G+5%§!"
 court_level_3_art_bonus: "Art Power Modifier in Capital: §G+20%§!\nArt Power Modifier in Regional Capitals: §G+10%§!"
 court_level_4_art_bonus: "Art Power Modifier in Capital: §G+30%§!\nArt Power Modifier in Regional Capitals: §G+15%§!"
 court_level_5_art_bonus: "Art Power Modifier in Capital: §G+40%§!\nArt Power Modifier in Regional Capitals: §G+20%§!"
 court_level_6_art_bonus: "Art Power Modifier in Capital: §G+50%§!\nArt Power Modifier in Regional Capitals: §G+25%§!"