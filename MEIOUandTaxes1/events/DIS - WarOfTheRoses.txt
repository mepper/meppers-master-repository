# Start Event
country_event = {
	id = war_of_the_roses.1
	title = war_of_the_roses.1.t
	desc = war_of_the_roses.1.d
	
	picture = WAR_OF_THE_ROSES_eventPicture
	major = yes
	
	is_triggered_only = yes
	
	option = {		# Support the House of Lancaster
		name = war_of_the_roses.1.a
		ai_chance = { factor = 95 }
		set_country_flag = war_of_the_roses
		set_country_flag = supports_house_of_lancaster
		define_ruler = {
			dynasty = Lancaster
		}
		add_yearly_manpower = -0.25
		if = {
			limit = {
				owns = 245		# Yorkshire
				245 = {
					units_in_province = 1
				}
			}
			random_owned_province = {
				limit = {
					OR = { region = south_england_region region = north_england_region region = scotland_region region = ireland_region }
					NOT = { units_in_province = 1 }
				}
				add_province_modifier = {
					name = wotr_local_support_for_york
					duration = 3650
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 3
					leader = "Richard York"
					win = yes
				}
			}
		}
		if = {
			limit = {
				owns = 245		# Yorkshire
				245 = {
					NOT = { units_in_province = 1 }
				}
			}
			245 = {
				add_province_modifier = {
					name = wotr_local_support_for_york
					duration = 3650
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 3
					leader = "Richard York"
					win = yes
				}
			}
		}
	}
	
	option = {		# Support the House of York
		name = war_of_the_roses.1.b
		ai_chance = { factor = 5 }
		set_country_flag = supports_house_of_york
		define_ruler = {
			dynasty = York
		}
		add_yearly_manpower = -0.25
		if = {
			limit = {
				owns = 244		# Lancashire
				244 = {
					units_in_province = 1
				}
			}
			random_owned_province = {
				limit = {
					OR = { region = south_england_region region = north_england_region region = scotland_region region = ireland_region }
					NOT = { units_in_province = 1 }
				}
				add_province_modifier = {
					name = wotr_local_support_for_lancaster
					duration = 3650
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 3
					leader = "Edward Lancaster"
				}
			}
		}
		if = {
			limit = {
				owns = 244		# Lancashire
				244 = {
					NOT = { units_in_province = 1 }
				}
			}
			244 = {
				add_province_modifier = {
					name = wotr_local_support_for_lancaster
					duration = 3650
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 3
					leader = "Edward Lancaster"
				}
			}
		}
	}
}

########################################
# Provinces Declare their Support
########################################

# Support in Province
country_event = {
	id = war_of_the_roses.2
	title = war_of_the_roses.2.t
	desc = war_of_the_roses.2.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	trigger = {
		any_owned_province = {
			is_overseas = no
			NOT = { has_province_modifier = wotr_local_support }
			NOT = { has_province_modifier = wotr_local_support_for_lancaster }
			NOT = { has_province_modifier = wotr_local_support_for_york }
		}
	}
	
	is_triggered_only = yes
	
	option = {		# This is most welcomed
		name = war_of_the_roses.2.a
		random_owned_province = {
			limit = {
				is_overseas = no
				NOT = { has_province_modifier = wotr_local_support }
				NOT = { has_province_modifier = wotr_local_support_for_lancaster }
				NOT = { has_province_modifier = wotr_local_support_for_york }
			}
			add_province_modifier = {
				name = wotr_local_support
				duration = 3650
			}
			every_neighbor_province = {
				limit = {
					owned_by = ROOT
					NOT = { has_province_modifier = wotr_local_support }
					NOT = { has_province_modifier = wotr_local_support_for_lancaster }
					NOT = { has_province_modifier = wotr_local_support_for_york }
				}
				add_province_modifier = {
					name = wotr_local_support
					duration = 3650
				}
			}
		}
	}
}

# Province Declares fo Lancaster!
country_event = {
	id = war_of_the_roses.3
	title = war_of_the_roses.3.t
	desc = war_of_the_roses.3.t
	picture = WAR_OF_THE_ROSES_eventPicture
	
	trigger = {
		has_country_flag = supports_house_of_york
		any_owned_province = {
			is_overseas = no
			is_capital = no
			NOT = { has_province_modifier = wotr_local_support }
			NOT = { has_province_modifier = wotr_local_support_for_lancaster }
			NOT = { has_province_modifier = wotr_local_support_for_york }
		}
	}
	
	is_triggered_only = yes
	
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {		# Dire news
		name = war_of_the_roses.3.a
		
		random_owned_province = {
			limit = {
				is_overseas = no
				NOT = { has_province_modifier = wotr_local_support }
				NOT = { has_province_modifier = wotr_local_support_for_lancaster }
				NOT = { has_province_modifier = wotr_local_support_for_york }
			}
			add_province_modifier = {
				name = wotr_local_support_for_lancaster
				duration = 3650
			}
			every_neighbor_province = {
				limit = {
					owned_by = ROOT
					NOT = { has_province_modifier = wotr_local_support }
					NOT = { has_province_modifier = wotr_local_support_for_lancaster }
					NOT = { has_province_modifier = wotr_local_support_for_york }
				}
				add_province_modifier = {
					name = wotr_local_support_for_lancaster
					duration = 3650
				}
			}
		}
	}
}

# Province Declares fo York!
country_event = {
	id = war_of_the_roses.4
	title = war_of_the_roses.4.t
	desc = war_of_the_roses.4.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	trigger = {
		has_country_flag = supports_house_of_lancaster
		any_owned_province = {
			is_overseas = no
			is_capital = no
			NOT = { has_province_modifier = wotr_local_support }
			NOT = { has_province_modifier = wotr_local_support_for_lancaster }
			NOT = { has_province_modifier = wotr_local_support_for_york }
		}
	}
	
	is_triggered_only = yes
	
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {		# Dire news
		name = war_of_the_roses.4.a
		
		random_owned_province = {
			limit = {
				is_overseas = no
				NOT = { has_province_modifier = wotr_local_support }
				NOT = { has_province_modifier = wotr_local_support_for_lancaster }
				NOT = { has_province_modifier = wotr_local_support_for_york }
			}
			add_province_modifier = {
				name = wotr_local_support_for_york
				duration = 3650
			}
			every_neighbor_province = {
				limit = {
					owned_by = ROOT
					NOT = { has_province_modifier = wotr_local_support }
					NOT = { has_province_modifier = wotr_local_support_for_lancaster }
				}
				add_province_modifier = {
					name = wotr_local_support_for_york
					duration = 3650
				}
			}
		}
	}
}

########################################
# Internal Problems
########################################

# Rebel Leaders Captured
province_event = {
	id = war_of_the_roses.6
	title = war_of_the_roses.6.t
	desc = war_of_the_roses.6.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		FROM = { tag = REB }
		owner = {
			has_disaster = war_of_the_roses
			tag = ENG
		}
	}
	
	option = {		# Pardon them
		name = war_of_the_roses.6.a
		ai_chance = { factor = 50 }
		owner = {
			add_legitimacy = -10
			every_owned_province = {
				hidden_effect = {
					set_variable = { which = added_unrest value = -3 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_3
			}
		}
	}
	option = {		# Execute them
		name = war_of_the_roses.6.b
		ai_chance = { factor = 50 }
		owner = {
			add_legitimacy = 10
			every_owned_province = {
				hidden_effect = {
					set_variable = { which = added_unrest value = 6 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_6
			}
		}
	}
}

# Legitimacy Questioned
country_event = {
	id = war_of_the_roses.8
	title = war_of_the_roses.8.t
	desc = war_of_the_roses.8.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		NOT = { legitimacy = 50 }
	}
	
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {		# Buy their continued support
		name = war_of_the_roses.8.a
		ai_chance = { factor = 50 }
		add_years_of_income = -0.25
	}
	option = {		# How dare they question me?
		name = war_of_the_roses.8.b
		ai_chance = { factor = 50 }
		add_legitimacy = -20
	}
}


########################################
# Rebel Stronghold & Invasion
########################################

# Rumors of Large Revolt in Province
country_event = {
	id = war_of_the_roses.9
	title = war_of_the_roses.9.t
	desc = war_of_the_roses.9.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	trigger = {
		any_owned_province = {
			NOT = { OR = { region = south_england_region region = north_england_region region = scotland_region region = ireland_region } }
			unrest = 3
			OR = {
				has_province_modifier = wotr_local_support_for_lancaster
				has_province_modifier = wotr_local_support_for_york
			}
			NOT = { controlled_by = REB }
		}
	}
	
	is_triggered_only = yes
	fire_only_once = yes
	
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {		# Send money to help the local authorities
		name = war_of_the_roses.9.a
		ai_chance = { factor = 25 }
		add_years_of_income = -0.75
		add_dip_power = -50
	}
	option = {		# The money is better needed closer to home
		name = war_of_the_roses.9.b
		ai_chance = { factor = 75 }
		random_owned_province = {
			limit = {
				NOT = { region = south_england_region }
				NOT = { region = north_england_region }
				NOT = { region = scotland_region }
				NOT = { region = ireland_region }
				unrest = 3
				OR = {
					has_province_modifier = wotr_local_support_for_lancaster
					has_province_modifier = wotr_local_support_for_york
				}
				NOT = { controlled_by = REB }
			}
			spawn_rebels = {
				type = pretender_rebels
				size = 2
			}
			add_province_modifier = {
				name = rebel_refugees
				duration = 3650
			}
		}
	}
}

# Lancaster Stronghold
province_event = {
	id = war_of_the_roses.10
	title = war_of_the_roses.10.t
	desc = war_of_the_roses.10.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		FROM = { tag = REB }
		has_province_modifier = rebel_refugees
		has_province_modifier = wotr_local_support_for_lancaster
	}
	
	
	option = {		# This is a serious threat
		name = war_of_the_roses.10.a
		add_province_modifier = {
			name = rebel_stronghold
			duration = 1825
		}
	}
}

# York Stronghold
province_event = {
	id = war_of_the_roses.11
	title = war_of_the_roses.11.t
	desc = war_of_the_roses.11.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		FROM = { tag = REB }
		has_province_modifier = rebel_refugees
		has_province_modifier = wotr_local_support_for_york
	}
	
	
	option = {		# This is a serious threat
		name = war_of_the_roses.11.a
		add_province_modifier = {
			name = rebel_stronghold
			duration = 1825
		}
	}
}


########################################
# Foreign Support
########################################

# Support House of Lancaster
country_event = {
	id = war_of_the_roses.12
	title = war_of_the_roses.12.t
	desc = war_of_the_roses.12.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	trigger = {
		is_neighbor_of = ENG
		
		OR = {
			capital_scope = {
				OR = { region = south_england_region region = north_england_region region = scotland_region region = ireland_region }
			}
			tag = FRA
		}
		ENG = {
			has_disaster = war_of_the_roses
			has_country_flag = supports_house_of_york
		}
		NOT = { has_country_flag = support_for_war_of_the_roses }
		
		ENG = {
			any_owned_province = {
				OR = {
					any_neighbor_province = {
						owned_by = ROOT
					}
					has_port = yes
				}
				unit_in_siege = no
			}
		}
	}
	
	mean_time_to_happen = { years = 5 }
	
	immediate = {
		set_country_flag = support_for_war_of_the_roses
	}
	
	option = {		# Send them a token force
		name = war_of_the_roses.12.a
		ai_chance = { factor = 50 }
		add_manpower = -3
		hidden_effect = {
			ENG = {
				country_event = { id = war_of_the_roses.14 days = 1 }
			}
		}
	}
	option = {		# Send them some money
		name = war_of_the_roses.12.b
		ai_chance = { factor = 25 }
		add_years_of_income = -0.5
		hidden_effect = {
			ENG = {
				country_event = { id = war_of_the_roses.14 days = 1 }
			}
		}
	}
	option = {		# We will not support rebels
		name = war_of_the_roses.12.c
		ai_chance = { factor = 25 }
		add_prestige = 10
	}
}

# Support House of York
country_event = {
	id = war_of_the_roses.13
	title = war_of_the_roses.13.t
	desc = war_of_the_roses.13.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	
	trigger = {
		is_neighbor_of = ENG
		
		OR = {
			capital_scope = {
				OR = { region = south_england_region region = north_england_region region = scotland_region region = ireland_region }
			}
			tag = FRA
		}
		ENG = {
			has_disaster = war_of_the_roses
			has_country_flag = supports_house_of_lancaster
		}
		NOT = { has_country_flag = support_for_war_of_the_roses }
		
		ENG = {
			any_owned_province = {
				OR = {
					any_neighbor_province = {
						owned_by = ROOT
					}
					has_port = yes
				}
				unit_in_siege = no
			}
		}
	}
	
	
	mean_time_to_happen = { years = 5 }
	
	immediate = {
		set_country_flag = support_for_war_of_the_roses
	}
	
	option = {		# Send them a token force
		name = war_of_the_roses.13.a
		ai_chance = { factor = 50 }
		add_manpower = -3
		hidden_effect = {
			ENG = {
				country_event = { id = war_of_the_roses.14 days = 1 }
			}
		}
	}
	option = {		# Send them some money
		name = war_of_the_roses.13.b
		ai_chance = { factor = 25 }
		add_years_of_income = -0.5
		hidden_effect = {
			ENG = {
				country_event = { id = war_of_the_roses.14 days = 1 }
			}
		}
	}
	option = {		# We will not support rebels
		name = war_of_the_roses.13.c
		ai_chance = { factor = 25 }
		add_prestige = 10
	}
}

# Country Intervenes!
country_event = {
	id = war_of_the_roses.14
	title = war_of_the_roses.14.t
	desc = war_of_the_roses.14.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	option = {		# How dare they!
		name = war_of_the_roses.14.a
		if = {
			limit = {
				any_owned_province = {
					any_neighbor_province = {
						owned_by = FROM
					}
					unit_in_siege = no
				}
			}
			random_owned_province = {
				limit = {
					any_neighbor_province = {
						owned_by = FROM
					}
					unit_in_siege = no
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 1
					friend = FROM
				}
			}
		}
		if = {
			limit = {
				NOT = {
					any_owned_province = {
						any_neighbor_province = {
							owned_by = FROM
						}
						unit_in_siege = no
					}
				}
				any_owned_province = {
					has_port = yes
					unit_in_siege = no
				}
			}
			random_owned_province = {
				limit = {
					has_port = yes
					unit_in_siege = no
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 1
					friend = FROM
				}
			}
		}
	}
}

########################################
# Tudors take over
########################################

country_event = {
	id = war_of_the_roses.15
	title = war_of_the_roses.15.title
	desc = war_of_the_roses.15.desc
	picture = WAR_OF_THE_ROSES_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		had_country_flag = { flag = war_of_the_roses days = 1500 }
		dynasty = Lancaster
	}
	
	option = {
		name = war_of_the_roses.15.opta
		define_ruler = {
			dynasty = Tudor
		}
		add_stability_1 = yes
	}
}

########################################
# The End of the War
########################################

# The End of the War of the Roses
country_event = {
	id = war_of_the_roses.100
	title = war_of_the_roses.100.t
	desc = war_of_the_roses.100.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			# Clear Country Modifiers
			remove_country_modifier = low_fertility
			# Clear Province Modifiers
			remove_province_modifier = wotr_local_support
			remove_province_modifier = wotr_local_support_for_lancaster
			remove_province_modifier = wotr_local_support_for_york
			remove_province_modifier = rebel_refugees
			remove_province_modifier = rebel_stronghold
			# Set Flag
			set_country_flag = had_war_of_the_roses
		}
	}
	
	option = {		# Order has finally been restored
		name = war_of_the_roses.100.a
		add_stability_1 = yes
		add_prestige = 50
		hidden_effect = {
			if = {
				limit = {
					has_country_flag = supports_house_of_york
				}
				every_country = {
					limit = {
						has_country_flag = support_for_war_of_the_roses
					}
					country_event = { id = war_of_the_roses.104 days = 1 }
				}
				clr_country_flag = supports_house_of_york
			}
			if = {
				limit = {
					has_country_flag = supports_house_of_lancaster
				}
				every_country = {
					limit = {
						has_country_flag = support_for_war_of_the_roses
					}
					country_event = { id = war_of_the_roses.105 days = 1 }
				}
				clr_country_flag = supports_house_of_lancaster
			}
		}
	}
}

# House of Lancaster wins the War of the Roses
country_event = {
	id = war_of_the_roses.101
	title = war_of_the_roses.101.t
	desc = war_of_the_roses.101.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		tag = ENG
		has_disaster = war_of_the_roses
		has_country_flag = supports_house_of_york
		OR = {
			dynasty = Lancaster
			dynasty = Tudor
		}
	}
	
	immediate = {
		hidden_effect = {
			
			# Clear Flags
			clr_country_flag = supports_house_of_lancaster
			clr_country_flag = supports_house_of_york
			
			# Clear Country Modifiers
			remove_country_modifier = low_fertility
			
			# Clear Province Modifiers
			remove_province_modifier = wotr_local_support
			remove_province_modifier = wotr_local_support_for_lancaster
			remove_province_modifier = wotr_local_support_for_york
			remove_province_modifier = rebel_refugees
			remove_province_modifier = rebel_stronghold
			
			
			# Set Flag
			set_country_flag = had_war_of_the_roses
		}
		end_disaster = war_of_the_roses
	}
	
	option = {		# Long live the House of Lancaster!
		name = war_of_the_roses.101.a
		add_stability_1 = yes
		add_prestige = -50
		hidden_effect = {
			country_event = { id = flavor_eng.9237 days = 15 }
			every_country = {
				limit = {
					has_country_flag = support_for_war_of_the_roses
				}
				country_event = { id = war_of_the_roses.103 days = 1 }
			}
		}
	}
}

# House of York wins the War of the Roses
country_event = {
	id = war_of_the_roses.102
	title = war_of_the_roses.102.t
	desc = war_of_the_roses.102.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		tag = ENG
		has_disaster = war_of_the_roses
		has_country_flag = supports_house_of_lancaster
		dynasty = York
	}
	
	immediate = {
		hidden_effect = {
			# Clear Flags
			clr_country_flag = supports_house_of_lancaster
			clr_country_flag = supports_house_of_york
			
			# Clear Country Modifiers
			remove_country_modifier = low_fertility
			
			# Clear Province Modifiers
			remove_province_modifier = wotr_local_support
			remove_province_modifier = wotr_local_support_for_lancaster
			remove_province_modifier = wotr_local_support_for_york
			remove_province_modifier = rebel_refugees
			remove_province_modifier = rebel_stronghold
		}
		end_disaster = war_of_the_roses
	}
	
	option = {		# Long live the House of York!
		name = war_of_the_roses.102.a
		add_stability_1 = yes
		add_prestige = -50
		hidden_effect = {
			country_event = { id = flavor_eng.9237 days = 15 }
			every_country = {
				limit = {
					has_country_flag = support_for_war_of_the_roses
				}
				country_event = { id = war_of_the_roses.103 days = 1 }
			}
		}
	}
}

# Improved Relations with England
country_event = {
	id = war_of_the_roses.103
	title = war_of_the_roses.103.t
	desc = war_of_the_roses.103.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = support_for_war_of_the_roses
	}
	
	option = {		# Good news
		name = war_of_the_roses.103.a
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = eng_support_during_war_of_the_roses
			}
		}
	}
}

# Relations Deteriorate with England
country_event = {
	id = war_of_the_roses.104
	title = war_of_the_roses.104.t
	desc = war_of_the_roses.104.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = support_for_war_of_the_roses
	}
	
	option = {		# Oh...
		name = war_of_the_roses.104.a
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = eng_supported_rebels_during_war_of_the_roses
			}
		}
	}
}

# Relations Deteriorate with England
country_event = {
	id = war_of_the_roses.105
	title = war_of_the_roses.105.t
	desc = war_of_the_roses.105.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = support_for_war_of_the_roses
	}
	
	option = {		# Oh...
		name = war_of_the_roses.105.a
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = eng_supported_rebels_during_war_of_the_roses
			}
		}
	}
}

