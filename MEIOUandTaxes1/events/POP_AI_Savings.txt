namespace = POP_AI_Savings

### By Demian
### Rewritten by KJH

# Calculate AI spending
country_event = {
	id = POP_AI_Savings.001
	title = no_localisation
	desc = no_localisation
	picture = COMET_SIGHTED_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	immediate = {
		every_country = {
			limit = {
				exists = yes
			}
			if = {
				limit = {
					ai = no
				}
				set_misc_expenses = yes
			}
			else = {   ### AI use of education and courts
				# trade estate total is the total amount of trade income lost to estates
				# while mechanically it is considered as an expenditure,
				# in design perspective it should be considered as an income loss
				export_to_variable = {
					which = monthly_country_income
					value = monthly_income
				}
				set_variable = { which = trade_estate_subtract which = EstateIncome_Trade }
				divide_variable = { which = trade_estate_subtract value = 12 }
				
				subtract_variable = { which = monthly_country_income which = trade_estate_subtract }
				
				# Effect country expenditure based on country's treasury
				# In theory, AIs should invest all of its surplus
				# When it has ((24 / (1 + e^(-0.05*income))) - 12) * 100 amount of ducats in treasury
				set_variable = { which = trade_estate_subtract value = 0 }
				
				export_to_variable = {
					which = treasury_factor
					value = treasury
				}
				
				if = {
					limit = {
						num_of_loans = 1
					}
					Export_num_of_loans = { output=total_loan }
					
					multiply_variable = { which = total_loan which = country_total_population }
					divide_variable = { which = total_loan value = 0.95 }
					multiply_variable = { which = total_loan value = 0.4 }
					
					subtract_variable = { which = treasury_factor which = total_loan }
					
					set_variable = { which = total_loan value = 0 }
				}
				
				if = {
					limit = {
						check_variable = { which = monthly_country_income value = 120 }
					}
					set_variable = { which = divisor value = 1200 }
				}
				else = {
					logistic_funct = {
						type1=which type2=value type3=value type4=value
						inp=monthly_country_income midpnt=0 steepns=0.05 maxval=24
					}
					subtract_variable = { which = logistic_val value = 12 }
					multiply_variable = { which = logistic_val value = 100 }
					
					set_variable = { which = divisor which = logistic_val }
					
					set_variable = { which = logistic_val value = 0 }
					if = {
						limit = {
							is_variable_equal = { which = divisor value = 0 }
						}
						set_variable = { which = divisor value = 0.001 }
					}
				}
				
				divide_variable = { which = treasury_factor which = divisor }
				
				set_variable = { which = divisor value = 0 }
				
				if = {
					limit = {
						check_variable = { which = treasury_factor value = 1 }
					}
					set_variable = { which = treasury_factor value = 1 }
				}
				else_if = {
					limit = {
						NOT = { check_variable = { which = treasury_factor value = 0 } }
					}
					set_variable = { which = treasury_factor value = 0 }
				}
				
				# Do not spend so much if low on troops or ships
				export_to_variable = {
					which = land_forcelimit_var
					value = land_forcelimit
				}
				
				export_to_variable = {
					which = army_size_var
					value = army_size
				}
				
				divide_variable = { which = army_size_var which = land_forcelimit_var }
				multiply_variable = { which = army_size_var value = 2 }
				
				if = {
					limit = {
						check_variable = { which = army_size_var value = 1 }
					}
					set_variable = { which = army_size_var value = 1 }
				}
				
				multiply_variable = { which = treasury_factor which = army_size_var }
				
				exp_funct = {
					type1=which type2=value
					inp=treasury_factor exp=1.5
				}
				
				set_variable = { which = treasury_factor which = exp_val }
				
				set_variable = { which = exp_val value = 0 }
				set_variable = { which = army_size_var value = 0 }
				set_variable = { which = land_forcelimit_var value = 0 }
				
				# Country's actual income without taking additional expenses into consideration
				set_variable = { which = country_actual_income_modified value = 0 }
				
				change_variable = { which = country_actual_income_modified which = court_state_contribute }
				change_variable = { which = country_actual_income_modified which = education_state_contribute }
				change_variable = { which = country_actual_income_modified which = AI_expenses }
				
				divide_variable = { which = country_actual_income_modified value = 12 }
				change_variable = { which = country_actual_income_modified which = country_actual_income }
				
				if = {
					limit = {
						NOT = { check_variable = { which = country_actual_income_modified value = 0 } }
					}
					set_variable = { which = country_actual_income_modified value = 0 }
				}
				else = {
					multiply_variable = { which = treasury_factor value = 0.2 } # scales the income removed to savings/court/education/construction by amount in current treasury. Always leaves out 50% of actual surplus income
					change_variable = { which = treasury_factor value = 0.3 }
				}
				
				set_variable = { which = AI_expenses value = 0 }
				set_variable = { which = court_spent_AI value = 0 }
				set_variable = { which = education_spent_AI value = 0 }
				set_variable = { which = percent_remaining_for_savings value = 1 } #used to set percentage that remains for savings/construction at the end, thus is reduced by court and education percentages dynamically
				
				if = {
					limit = {
						check_variable = { which = treasury_factor value = 0.001 }
						
						NOT = { num_of_loans = 3 }
					}
					set_variable = { 	  which = court_spent_AI 			which = country_actual_income_modified }
					multiply_variable = { which = court_spent_AI			value = 12 }
					set_variable = { 	  which = court_PRFS 				value = 0 }
					trigger_switch = {
						on_trigger = government_rank
						6 = {
							multiply_variable = { which = court_spent_AI			value = 0.15 } #large powers are MUCH more likely to have capital buildings to sustain their courts
							change_variable = { which = court_PRFS	value = 0.15 }
						}
						5 = {
							multiply_variable = { which = court_spent_AI			value = 0.18 }
							change_variable = { which = court_PRFS	value = 0.18 }
						}
						4 = {
							multiply_variable = { which = court_spent_AI			value = 0.21 }
							change_variable = { which = court_PRFS	value = 0.21 }
						}
						3 = {
							multiply_variable = { which = court_spent_AI			value = 0.24 }
							change_variable = { which = court_PRFS	value = 0.24 }
						}
						2 = {
							multiply_variable = { which = court_spent_AI			value = 0.27 }
							change_variable = { which = court_PRFS	value = 0.27 }
						}
						1 = {
							multiply_variable = { which = court_spent_AI			value = 0.30 } #lower ranks need that court for alliances to survive, large powers generally don't need alliances as much
							change_variable = { which = court_PRFS	value = 0.30 }
						}
					}
					if = { #reduce court spending by half if at war
						limit = {
							is_at_war = yes
						}
						multiply_variable = { which = court_spent_AI	value = 0.5 }
						multiply_variable = { which = court_PRFS	value = 0.5 }
					}
					
					subtract_variable = { which = percent_remaining_for_savings		which = Court_PRFS }
					set_variable = { 	  which = court_PRFS 				value = 0 }
					
					multiply_variable = { which = court_spent_AI			which = treasury_factor }

					
					set_variable = {   which = education_spent_AI	which = country_actual_income_modified } ### Secular source
					multiply_variable = { which = education_spent_AI	value = 12 }
					set_variable = { 	  which = education_PRFS 				value = 0 }
					#compensate for lack of church funding
					if = {
						limit = {
							patriarch_authority = 0.9
						}
						multiply_variable = { which = education_spent_AI	value = 0.12 }
						change_variable = { which = education_PRFS	value = 0.12 }
					}
					else_if = {
						limit = {
							patriarch_authority = 0.7
						}
						multiply_variable = { which = education_spent_AI	value = 0.15 }
						change_variable = { which = education_PRFS	value = 0.15 }
					}
					else_if = {
						limit = {
							patriarch_authority = 0.5
						}
						multiply_variable = { which = education_spent_AI	value = 0.18 }
						change_variable = { which = education_PRFS	value = 0.18 }
					}
					else_if = {
						limit = {
							patriarch_authority = 0.3
						}
						multiply_variable = { which = education_spent_AI	value = 0.21 }
						change_variable = { which = education_PRFS	value = 0.21 }
					}
					else_if = {
						limit = {
							patriarch_authority = 0.1
						}
						multiply_variable = { which = education_spent_AI	value = 0.24 }
						change_variable = { which = education_PRFS	value = 0.24 }
					}
					else = {
						multiply_variable = { which = education_spent_AI	value = 0.27 }
						change_variable = { which = education_PRFS	value = 0.27 }
					}
					
					if = {
						limit = {
							has_country_flag = great_predator # For their Army Tradition
						}
						multiply_variable = { which = education_spent_AI	value = 1.10 } #Increase by 10% to keep up army tradition.
						multiply_variable = { which = education_PRFS	value = 1.10 } #Increase by 10% to keep up army tradition.
					}
					
					subtract_variable = { which = percent_remaining_for_savings		which = education_PRFS }
					set_variable = { 	  which = education_PRFS 				value = 0 }
					
					multiply_variable = { which = education_spent_AI	which = treasury_factor }
					
					
					set_variable = {    which = court_state_contribute			which = court_spent_AI }
					set_variable = { 	which = education_state_contribute		which = education_spent_AI }
					
					# Calculate savings
					if = {
						limit = {
							NOT = { num_of_loans = 1 }
						}
						if = {
							limit = {
								is_at_war = no
							}
							# Total savings
							set_variable = { which = AI_savings_total which = country_actual_income_modified }
							multiply_variable = { which = AI_savings_total value = 12 }
							multiply_variable = { which = AI_savings_total which = percent_remaining_for_savings } #percent_remaining_for_savings should be the resulting percentage after court and education
							set_variable = { which = percent_remaining_for_savings value = 0 }
							
							set_variable = { which = AI_savings_multiplier which = treasury_factor }
							
							# If doing good, save more, if not, save less
							if = {
								limit = {
									check_variable = { which = wellbeing_composite 	value = 1.1 }
								}
								if = {
									limit = {
										check_variable = { which = wellbeing_composite 	value = 1.25 }
									}
									multiply_variable = { which = AI_savings_multiplier value = 1.4 }
								}
								else = {
									multiply_variable = { which = AI_savings_multiplier value = 1.20 }
								}
							}
							else_if = {
								limit = {
									NOT = { check_variable = { which = wellbeing_composite value = 0.9 } }
								}
								if = {
									limit = {
										NOT = { check_variable = { which = wellbeing_composite 	value = 0.8 } }
									}
									divide_variable = { which = AI_savings_multiplier value = 1.5 }
								}
								else = {
									divide_variable = { which = AI_savings_multiplier value = 1.25 }
								}
							}
							
							# It's not supposed to get higher than 0.95
							if = {
								limit = {
									check_variable = { which = AI_savings_multiplier value = 0.95 }
								}
								set_variable = { which = AI_savings_multiplier value = 0.95 }
							}
							
							multiply_variable = { which = AI_savings_total which = AI_savings_multiplier }
							change_variable = { which = AI_expenses which = AI_savings_total }
							
							set_variable = { which = AI_savings_multiplier value = 0 }
							
							# Divide total savings between war chest, savings and construction fund
							set_variable = { which = country_war_chest_added value = 0 }
							set_variable = { which = country_savings_plan_added value = 0 }
							set_variable = { which = country_construction_plan_added value = 0 }
							
							if = {
								limit = {
									check_variable = { which = total_upper_class_country value = 5 }
								}
								set_variable = { which = savings_share_savings which = country_war_chest }
								
								set_variable = { which = divisor which = monthly_country_income }
								multiply_variable = { which = divisor value = 48 }
								
								divide_variable = { which = savings_share_savings which = divisor }
								
								if = {
									limit = {
										check_variable = { which = savings_share_savings value = 1 }
									}
									set_variable = { which = savings_share_savings value = 1 }
								}
								
								multiply_variable = { which = savings_share_savings which = savings_share_savings }
								
								set_variable = { which = savings_share_warchest value = 1 }
								subtract_variable = { which = savings_share_warchest which = savings_share_savings }
								
								set_variable = { which = savings_share_cp which = country_savings_plan }
								
								set_variable = { which = divisor which = monthly_country_income }
								multiply_variable = { which = divisor value = 24 }
								
								divide_variable = { which = savings_share_cp which = divisor }
								
								if = {
									limit = {
										check_variable = { which = savings_share_cp value = 1 }
									}
									set_variable = { which = savings_share_cp value = 1 }
								}
								
								multiply_variable = { which = savings_share_cp which = savings_share_cp }
								multiply_variable = { which = savings_share_cp which = savings_share_savings }
								
								subtract_variable = { which = savings_share_savings which = savings_share_cp }
								
								set_variable = { which = divisor value = 0 }
								
								set_variable = { which = country_war_chest_added which = AI_savings_total }
								multiply_variable = { which = country_war_chest_added which = savings_share_warchest }
								
								set_variable = { which = country_savings_plan_added which = AI_savings_total }
								multiply_variable = { which = country_savings_plan_added which = savings_share_savings }
								
								set_variable = { which = country_construction_plan_added which = AI_savings_total }
								multiply_variable = { which = country_construction_plan_added which = savings_share_cp }
								
								
								set_variable = { which = savings_share_warchest value = 0 }
								set_variable = { which = savings_share_savings value = 0 }
								set_variable = { which = savings_share_cp value = 0 }
							}
							else = {
								set_variable = { which = country_war_chest_added which = AI_savings_total }
							}
							
							change_variable = { which = country_war_chest which = country_war_chest_added }
							change_variable = { which = country_savings_plan which = country_savings_plan_added }
							change_variable = { which = country_construction_plan which = country_construction_plan_added }
							
							
							set_variable = { which = country_war_chest_added value = 0 }
							set_variable = { which = country_savings_plan_added value = 0 }
							set_variable = { which = country_construction_plan_added value = 0 }
						}
					}
					
					set_variable = { which = country_actual_income_modified value = 0 }
					set_variable = { which = monthly_country_income value = 0 }
				}
				
				set_misc_expenses = yes
				
				# Pay off debt with savings
				if = {
					limit = {
						num_of_loans = 1
					}
					set_variable = { which = savings_total which = country_war_chest }
					change_variable = { which = savings_total which = country_savings_plan }
					change_variable = { which = savings_total which = country_construction_plan }
					
					if = {
						limit = {
							check_variable = { which = savings_total value = 1 }
						}
						Export_num_of_loans = { output=total_loan }
						
						multiply_variable = { which = total_loan which = country_total_population }
						divide_variable = { which = total_loan value = 0.95 }
						multiply_variable = { which = total_loan value = 0.4 }
						
						if = {
							limit = {
								check_variable = { which = savings_total which = total_loan }
							}
							set_variable = { which = savings_total which = total_loan }
							
							if = {
								limit = {
									check_variable = { which = country_savings_plan which = total_loan }
								}
								subtract_variable = { which = country_savings_plan which = total_loan }
							}
							else = {
								subtract_variable = { which = total_loan which = country_savings_plan }
								
								set_variable = { which = country_savings_plan value = 0 }
								
								if = {
									limit = {
										check_variable = { which = country_construction_plan which = total_loan }
									}
									subtract_variable = { which = country_construction_plan which = total_loan }
								}
								else = {
									subtract_variable = { which = total_loan which = country_construction_plan }
									
									set_variable = { which = country_construction_plan value = 0 }
									
									subtract_variable = { which = country_war_chest which = total_loan }
								}
							}
						}
						else = {
							set_variable = { which = country_war_chest value = 0 }
							set_variable = { which = country_savings_plan value = 0 }
							set_variable = { which = country_construction_plan value = 0 }
						}
						
						set_variable = { which = ducat_gain which = savings_total }
						
						scaled_ducat_gained_country = yes
						
						
						set_variable = { which = total_loan value = 0 }
					}
					
					set_variable = { which = savings_total value = 0 }
				}
				
				if = { ### Transfer manpower to infrastruture fund if manpower is high
					limit = {
						manpower = 5
						
						OR = {
							manpower_percentage = 0.8
							
							AND = {
								manpower_percentage = 0.65
								
								check_variable = { which = total_upper_class_country value = 10 }
							}
						}
					}
					export_to_variable = {
						which = ai_manpower_invested_in_infrastructure
						value = manpower
					}
					
					multiply_variable = { which = ai_manpower_invested_in_infrastructure value = 0.20 } # 20 percent
					set_variable = { which = manpower_cost which = ai_manpower_invested_in_infrastructure }
					
					# Mitigating city state manpower buffs
					export_to_variable = {
						which = total_manpower
						value = max_manpower
					}
					export_to_variable = {
						which = global_manpower_mod
						value = modifier:global_manpower_modifier
					}
					export_to_variable = {
						which = oe_manpower
						value = overextension_percentage
					}
					
					change_variable = { which = global_manpower_mod value = 1 }
					
					divide_variable = { which = total_manpower which = global_manpower_mod }
					
					multiply_variable = { which = oe_manpower value = 50 }
					
					change_variable = { which = total_manpower which = oe_manpower }
					
					set_variable = { which = subtract value = 3 } # Base manpower gain for all countries = 3000, 1 = 1000
					divide_variable = { which = subtract which = total_manpower }
					
					set_variable = { which = multiplier value = 1 }
					subtract_variable = { which = multiplier which = subtract }
					
					multiply_variable = { which = ai_manpower_invested_in_infrastructure which = multiplier }
					
					
					# Finish the calc
					multiply_variable = { which = ai_manpower_invested_in_infrastructure	value = 20 } # Conversion rate is 50 men per ducat
					change_variable = {   which = country_construction_plan					which = ai_manpower_invested_in_infrastructure }
					
					scaled_manpower_cost_country = yes
					
					
					# Clean
					set_variable = { which = ai_manpower_invested_in_infrastructure value = 0 }
					set_variable = { which = multiplier value = 0 }
					set_variable = { which = subtract value = 0 }
					set_variable = { which = total_manpower value = 0 }
					set_variable = { which = oe_manpower value = 0 }
					set_variable = { which = global_manpower_mod value = 0 }
				}
			}
		}
	}
	
	option = {
		name = no_localisation
	}
}

country_event = {  ### This event is triggered at the end of a war to re-instate the war chest
	id = POP_AI_Savings.003
	title = POP_AI_Savings.003.t
	desc = POP_AI_Savings.003.d
	picture = REFORM_eventPicture
	is_triggered_only = yes
	hidden = no
	
	trigger = {
		has_country_flag = war_chest_put_away
		is_at_war = no
		ai = yes
	}
	
	immediate = { ### This event is triggered at the end of a war to re-instate the war chest
		clr_country_flag = war_chest_put_away
		change_variable = { which = country_war_chest 				which = country_war_chest_put_away }
		set_variable = { 	which = country_war_chest_put_away		value = 0 }
	}
	
	option = {
		name = "POP_AI_Savings.003.a"
		
	}
}

country_event = {  ### Gives AI money during war from their battle chest which they saved up in peacetime
	id = POP_AI_Savings.999
	title = POP_AI_Savings.999.t
	desc = POP_AI_Savings.999.d
	picture = REFORM_eventPicture
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		#	has_country_flag = center_of_universe
		is_year = 1358
		#	is_at_war = yes
		#	NOT = { has_country_flag = war_chest_put_away }
		#	is_year = 1358
		#	ai = yes
	}
	
	immediate = { ### This event triggers at the start of a war and then proceeds to put away half the war chest so that it can't all be exhausted in one war.  It is added back after the war is over
		every_country = {
			limit = {
				is_at_war = yes
			}
			if = {
				limit = {
					NOT = { has_country_flag = war_chest_put_away }
					ai = yes
				}
				set_country_flag = war_chest_put_away
				set_variable = { 	which = country_war_chest_put_away 	value = 0 }
				set_variable = {	which = country_war_chest_put_away	which = country_war_chest }
				divide_variable = { which = country_war_chest_put_away	value = 2 }
				divide_variable = { which = country_war_chest 			value = 2 }
				
				set_variable = { which = war_money_given which = country_war_chest }
				divide_variable = { which = war_money_given value = 2 }
			}
			if = {
				limit = {
					has_country_flag = war_chest_put_away
					num_of_loans = 1
					check_variable = { which = country_war_chest 	value = 1 }
				}
				if = {
					limit = {
						NOT = { check_variable = { which = AI_number_of_months_war_chest_tapped	value = 1 } }
					}
					set_variable = { which = AI_number_of_months_war_chest_tapped	value = 0 }
				}
				change_variable = { 	which = AI_number_of_months_war_chest_tapped  value = 1 }
				
				subtract_variable = { which = country_war_chest 	which = war_money_given }
				
				set_variable = { 	  which = ducat_gain which = war_money_given }
				multiply_variable = { which = ducat_gain value = 1.25 }
				
				scaled_ducat_gained_country = yes
			}
			if = {
				limit = {
					has_country_flag = war_chest_put_away
					is_at_war = no
				}
				clr_country_flag = war_chest_put_away
				change_variable = { which = country_war_chest 				which = country_war_chest_put_away }
				set_variable = { 	which = country_war_chest_put_away		value = 0 }
				
				set_variable = { which = war_money_given	value = 0 }
			}
		}
	}
	
	option = {
		name = "POP_AI_Savings.999.a"
		
	}
}