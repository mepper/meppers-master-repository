########################################
# Civil War in Castile
# Mix of Castilian Civil Wars involving
# nobles and outside intervention
########################################

namespace = castilian_civil_war

# Civil War in Castile
country_event = {
	id = castilian_civil_war.1
	title = "castilian_civil_war.1.name"
	desc = "castilian_civil_war.1.desc"
	picture = CIVIL_WAR_eventPicture
	
	is_triggered_only = yes
	
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	immediate = {
		subtract_stability_2 = yes
		hidden_effect = {
			random_owned_province = {
				limit = {
					is_capital = no
					has_building = fort_15th
				}
				set_province_flag = cas_civil_war_start
			}
			random_owned_province = {
				limit = {
					is_capital = no
					NOT = { has_province_flag = cas_civil_war_start }
				}
				set_province_flag = cas_civil_war_2
			}
			random_owned_province = {
				limit = {
					is_capital = no
					NOT = { has_province_flag = cas_civil_war_start }
					NOT = { has_province_flag = cas_civil_war_2 }
				}
				set_province_flag = cas_civil_war_3
			}
			random_owned_province = { #While primarily a pretender rebellion there are parts of the nobility that will makes use of this to further their own positions in general.
				limit = {
					is_capital = no
					NOT = { has_province_flag = cas_civil_war_start }
					NOT = { has_province_flag = cas_civil_war_2 }
					NOT = { has_province_flag = cas_civil_war_3 }
					OR = {
						has_building = fort_15th
						has_building = fort_16th
						AND = {
							owner = {
								NOT = {
									any_owned_province = {
										has_building = fort_15th
									}
								}
							}
							development = 10
						}
					}
				}
				spawn_rebels = {
					type = noble_rebels
					size = 1
					win = yes
				}
			}
		}
	}
	
	option = {		# Support Portuguese Candidate (heir)
		name = "castilian_civil_war.1.opta"
		set_country_flag = civil_war_in_castile
		set_country_flag = cas_portugal_candidate
		custom_tooltip = "castilian_civil_war.1.a.tt"
		add_legitimacy = -20
		random_owned_province = {
			limit = {
				has_province_flag = cas_civil_war_start
			}
			spawn_rebels = {
				type = pretender_rebels
				friend = ENR
				size = 1
				win = yes
			}
			clr_province_flag = cas_civil_war_start
		}
		every_owned_province = {
			limit = {
				OR = {
					has_province_flag = cas_civil_war_2
					has_province_flag = cas_civil_war_3
				}
			}
			spawn_rebels = {
				friend = ENR
				type = pretender_rebels
				size = 1
				win = yes
			}
			clr_province_flag = cas_civil_war_2
			clr_province_flag = cas_civil_war_3
		}
		POR = {
			country_event = { id = castilian_civil_war.3 days = 10 }
		}
		ARA = {
			country_event = { id = castilian_civil_war.5 days = 20 }
		}
		ENR = {
			set_country_flag = cas_aragon_candidate
			set_country_flag = civil_war_in_castile
		}
	}
}


# The Civil War Spreads to PROVINCE
country_event = {
	id = castilian_civil_war.2
	title = "castilian_civil_war.2.name"
	desc = "castilian_civil_war.2.desc"
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		any_owned_province = {
			continent = europe
			is_capital = no
			controlled_by = owner
			any_neighbor_province = { controlled_by = ENR }
		}
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					continent = europe
					is_capital = no
					controlled_by = owner
					any_neighbor_province = { controlled_by = ENR }
				}
				random_list = {
					80 = { set_province_flag = revolt_1 }
					15 = { set_province_flag = revolt_2 }
					5 = { set_province_flag = revolt_3 }
				}
			}
		}
	}
	
	option = {		# Where is the loyalty...
		name = "flavor_spa.EVTOPTA3560"
		random_owned_province = {
			limit = { has_province_flag = revolt_3 }
			cede_province = ENR
			clr_province_flag = revolt_3
		}
		random_owned_province = {
			limit = { has_province_flag = revolt_2 }
			spawn_rebels = {
				friend = ENR
				type = pretender_rebels
				size = 2
			}
			clr_province_flag = revolt_2
		}
		random_owned_province = {
			limit = { has_province_flag = revolt_1 }
			spawn_rebels = {
				friend = ENR
				type = pretender_rebels
				size = 1
			}
			clr_province_flag = revolt_1
		}
	}
}

# Civil War in Castile - Portugal
country_event = {
	id = castilian_civil_war.3
	title = "castilian_civil_war.3.name"
	desc = "castilian_civil_war.3.desc"
	picture = ACCUSATION_eventPicture
	
	is_triggered_only = yes
	
	option = { #Support this claimant
		name = "castilian_civil_war.3.opta"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				alliance_with = ARA
				NOT = { is_rival = CAS }
			}
			modifier = {
				factor = 0.5
				alliance_with = CAS
			}
		}
		CAS = {
			country_event = { id = castilian_civil_war.4 days = 15 }
			if = {
				limit = { has_country_flag = cas_aragon_candidate }
				add_opinion = {
					who = POR
					modifier = cas_civil_war_interferance
				}
			}
			if = {
				limit = { has_country_flag = cas_portugal_candidate }
				add_opinion = {
					who = POR
					modifier = cas_civil_war_support
				}
			}
		}
		if = {
			limit = {
				CAS = { has_country_flag = cas_portugal_candidate }
			}
			add_opinion = {
				who = CAS
				modifier = cas_civil_war_support
			}
		}
		add_manpower = -2
		add_opinion = {
			who = ENR
			modifier = cas_civil_war_interferance
		}
	}
	option = { #Let the Castilians handle their own conflicts.
		name = "castilian_civil_war.3.optb"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				is_rival = ARA
			}
			modifier = {
				factor = 0
				is_rival = CAS
			}
		}
		ENR = {
			country_event = { id = castilian_civil_war.4 days = 15 }
			if = {
				limit = { has_country_flag = cas_aragon_candidate }
				add_opinion = {
					who = POR
					modifier = cas_civil_war_support
				}
			}
			if = {
				limit = { has_country_flag = cas_portugal_candidate }
				add_opinion = {
					who = POR
					modifier = cas_civil_war_interferance
				}
			}
		}
		if = {
			limit = {
				ENR = { has_country_flag = cas_portugal_candidate }
			}
			add_opinion = {
				who = ENR
				modifier = cas_civil_war_support
			}
		}
		add_manpower = -2
		add_opinion = {
			who = CAS
			modifier = cas_non_interferance
		}
	}
}

# Portuguese Support
country_event = {
	id = castilian_civil_war.4
	title = "castilian_civil_war.4.name"
	desc = "castilian_civil_war.4.desc"
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_disaster = castilian_civil_war
	}
	
	option = { #Great!
		name = "castilian_civil_war.4.opta"
		#trigger = { has_country_flag = cas_portugal_candidate }
		add_manpower = 3
		add_mil_power = 50
	}
	#option = { #Despeakable!
	#	name = "castilian_civil_war.4.optb"
	#	trigger = { has_country_flag = cas_aragon_candidate }
	#	random_owned_province = {
	#		limit = {
	#			is_capital = no
	#		}
	#		spawn_rebels = {
	#			type = pretender_rebels
	#			size = 1
	#			friend = POR
	#		}
	#	}
	#	add_mil_power = 50
	#}
}

# Castilian Civil War - Aragon
country_event = {
	id = castilian_civil_war.5
	title = "castilian_civil_war.5.name"
	desc = "castilian_civil_war.5.desc"
	picture = ACCUSATION_eventPicture
	
	is_triggered_only = yes
	
	option = { #Support this claimant
		name = "castilian_civil_war.5.opta"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				alliance_with = POR
				NOT = { is_rival = ENR }
			}
			modifier = {
				factor = 0.5
				alliance_with = ENR
			}
		}
		ENR = {
			country_event = {
				id = castilian_civil_war.6
				days = 15
			}
			if = {
				limit = { has_country_flag = cas_portugal_candidate }
				add_opinion = {
					who = ARA
					modifier = cas_civil_war_interferance
				}
			}
			if = {
				limit = { has_country_flag = cas_aragon_candidate }
				add_opinion = {
					who = ARA
					modifier = cas_civil_war_support
				}
			}
		}
		if = {
			limit = {
				ENR = { has_country_flag = cas_aragon_candidate }
			}
			add_opinion = {
				who = ENR
				modifier = cas_civil_war_support
			}
		}
		add_manpower = -2
		add_opinion = {
			who = CAS
			modifier = cas_civil_war_interferance
		}
		hidden_effect = {
			if = {
				limit = {
					CAS = { has_country_flag = cas_aragon_candidate }
					any_neighbor_country = {
						tag = FRA
						NOT = { alliance_with = ARA }
					}
					is_year = 1365
				}
				FRA = { country_event = { id = castilian_civil_war.7 days = 20 } }
			}
		}
	}
	option = { #Let the Castilians handle their own conflicts.
		name = "castilian_civil_war.5.optb"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				is_rival = CAS
			}
			modifier = {
				factor = 0
				is_rival = ENR
			}
		}
		CAS = {
			country_event = {
				id = castilian_civil_war.6
				days = 15
			}
			if = {
				limit = { has_country_flag = cas_portugal_candidate }
				add_opinion = {
					who = ARA
					modifier = cas_civil_war_support
				}
			}
			if = {
				limit = { has_country_flag = cas_aragon_candidate }
				add_opinion = {
					who = ARA
					modifier = cas_civil_war_interferance
				}
			}
		}
		if = {
			limit = {
				CAS = { has_country_flag = cas_aragon_candidate }
			}
			add_opinion = {
				who = CAS
				modifier = cas_civil_war_support
			}
		}
		add_manpower = -2
		add_opinion = {
			who = ENR
			modifier = cas_non_interferance
		}
	}
}

# Aragonese Support
country_event = {
	id = castilian_civil_war.6
	title = "castilian_civil_war.6.name"
	desc = "castilian_civil_war.6.desc"
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_disaster = castilian_civil_war
	}
	
	option = { #Great!
		name = "castilian_civil_war.6.opta"
		#trigger = { has_country_flag = cas_aragon_candidate }
		add_manpower = 2
		add_mil_power = 50
	}
	#	option = { #Despicable!
	#		name = "castilian_civil_war.6.optb"
	#		trigger = { has_country_flag = cas_portugal_candidate }
	#		random_owned_province = {
	#			limit = {
	#				is_capital = no
	#			}
	#			spawn_rebels = {
	#				type = pretender_rebels
	#				size = 1
	#				friend = ARA
	#			}
	#		}
	#	}
}

# Castilian Civil War - France
country_event = {
	id = castilian_civil_war.7
	title = "castilian_civil_war.7.name"
	desc = "castilian_civil_war.7.desc"
	picture = ACCUSATION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		CAS = {
			has_disaster = castilian_civil_war
		}
	}
	
	option = { #Support this claimant
		name = "castilian_civil_war.7.opta"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				alliance_with = CAS
			}
		}
		add_manpower = -2
		CAS = {
			country_event = {
				id = castilian_civil_war.8
				days = 15
			}
			add_opinion = {
				who = FRA
				modifier = cas_civil_war_interferance
			}
		}
	}
	option = { #Let the Castilians handle their own conflicts.
		name = "castilian_civil_war.7.optb"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				is_rival = CAS
			}
		}
		add_opinion = {
			who = CAS
			modifier = cas_non_interferance
		}
	}
}

# French Meddling
country_event = {
	id = castilian_civil_war.8
	title = "castilian_civil_war.8.name"
	desc = "castilian_civil_war.8.desc"
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_disaster = castilian_civil_war
	}
	
	option = { #Despicable!
		name = "castilian_civil_war.8.opta"
		random_owned_province = {
			limit = {
				is_capital = no
			}
			spawn_rebels = {
				type = pretender_rebels
				size = 2
				friend = ENR
			}
		}
	}
}

country_event = {
	id = castilian_civil_war.12
	title = "castilian_civil_war.12.name"
	desc = "castilian_civil_war.12.desc"
	picture = COURT_eventPicture
	
	trigger = {
		OR = {
			tag = CAS
			tag = ENR
		}
		exists = ENR
		ENR = { is_vassal = no }
		ENR = { is_at_war = no }
		CAS = { is_at_war = no }
		OR = {
			senior_union_with = CAS
			senior_union_with = ENR
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	immediate = {
		CAS = {
			if = {
				limit = {
					has_disaster = castilian_civil_war
				}
				end_disaster = castilian_civil_war
			}
		}
	}
	
	option = {
		name = "castilian_civil_war.12.opta"
		trigger = {
			tag = CAS
		}
		inherit = ENR
		add_historical_friend = POR
		POR = {
			add_historical_friend = ROOT
		}
		if = {
			limit = {
				NOT = { has_country_modifier = title_5 }
				NOT = { has_country_modifier = title_6 }
			}
			change_title_5 = yes
		}
	}
	option = {
		name = "castilian_civil_war.12.opta"
		trigger = {
			tag = ENR
		}
		inherit = CAS
		change_tag = CAS
		add_historical_friend = POR
		remove_historical_friend = FRA
		POR = {
			add_historical_friend = ROOT
		}
		if = {
			limit = {
				NOT = { has_country_modifier = title_5 }
				NOT = { has_country_modifier = title_6 }
			}
			change_title_5 = yes
		}
		remove_country_modifier = trastamara_support
		Effect_set_capital = { target=2312 }
		hidden_effect = {
			country_event = {
				id = miscexpenses.001
			}
		}
		hidden_effect = { change_primary_culture = castillian }
		every_owned_province = {
			limit = {
				area = toledo_area
				has_province_modifier = lordship_of_toledo
			}
			remove_province_modifier = lordship_of_toledo
		}
		kill_leader = {
			type = "Bertrand du Guesclin"
		}
	}
}

country_event = {
	id = castilian_civil_war.13
	title = "castilian_civil_war.13.name"
	desc = "castilian_civil_war.13.desc"
	picture = BATTLE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.13.opta"
		random_owned_province = {
			limit = { controlled_by = ROOT }
			add_local_autonomy = 50
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}

country_event = {
	id = castilian_civil_war.14
	title = "castilian_civil_war.13.name"
	desc = "castilian_civil_war.13.desc"
	picture = BATTLE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.13.opta"
		random_owned_province = {
			limit = { controlled_by = ROOT }
			add_local_autonomy = 50
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}

country_event = {
	id = castilian_civil_war.15
	title = "castilian_civil_war.15.name"
	desc = "castilian_civil_war.15.desc"
	picture = CIVIL_WAR_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = ENR
		alliance_with = FRA
		exists = CAS
		is_year = 1360
		CAS = { has_disaster = castilian_civil_war }
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "castilian_civil_war.15.opta"
		#declare_war_with_cb = {
		#	who = CAS
		#	casus_belli = cb_claim_throne
		#}
		add_casus_belli = {
			target = CAS
			type = cb_restore_personal_union
			months = 6000
		}
		reverse_add_casus_belli = {
			target = CAS
			type = cb_restore_personal_union
			months = 6000
		}
		define_general = {
			name = "Bertrand du Guesclin"
			shock = 3
			fire = 6
			manuever = 6
			siege = 2
		}
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		infantry = 1421
		cavalry = 1421
		cavalry = 1421
		cavalry = 1421
		cavalry = 1421
		cavalry = 1421
		add_treasury = 350
		209 = { change_controller = ENR }
		198 = { change_controller = ENR }
		225 = { change_controller = ENR }
		215 = { change_controller = ENR }
		2312 = { change_controller = ENR }
	}
}

country_event = {
	id = castilian_civil_war.18
	title = "castilian_civil_war.18.name"
	desc = "castilian_civil_war.18.desc"
	picture = BATTLE_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		is_year = 1365
		tag = CAS
		war_with = ENR
	}
	
	option = {
		name = "castilian_civil_war.18.opta"
		kill_ruler = yes
	}
}

country_event = {
	id = castilian_civil_war.21
	title = "castilian_civil_war.21.name"
	desc = "castilian_civil_war.21.desc"
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		is_year = 1360
		tag = CAS
		war_with = ARA
	}
	
	option = {
		name = "castilian_civil_war.21.opta"
		white_peace = ARA
	}
}

province_event = {
	id = castilian_civil_war.22
	title = "war_dynamism.PROV_NAME"
	desc = "war_dynamism.02.d"
	picture = COURT_eventPicture
	
	trigger = {
		NOT = { owned_by = ENR }
		controlled_by = REB
		exists = ENR
		NOT = { has_siege = yes }
		religion = catholic
		is_capital = no
		OR = {
			region = leon_region
			region = castille_region
			region = andalucia_region
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "war_dynamism.02.opta"
		add_local_autonomy = 25
		cede_province = ENR
	}
}

# Contender Captured
country_event = {
	id = castilian_civil_war.30
	title = "castilian_civil_war.30.name"
	desc = "castilian_civil_war.30.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	fire_only_once = yes
	
	immediate = {
		random_list = {
			75 = {
				CAS = {
					save_global_event_target_as = faction_leader_captured
				}
				ENR = {
					country_event = { id = castilian_civil_war.31 days = 5 }
					save_global_event_target_as = faction_leader_success
				}
			}
			25 = {
				ENR = {
					save_global_event_target_as = faction_leader_captured
				}
				CAS = {
					country_event = { id = castilian_civil_war.31 days = 5 }
					save_global_event_target_as = faction_leader_success
				}
			}
		}
	}
	
	option = {
		name = "castilian_civil_war.31.opta"
	}
}

country_event = {
	id = castilian_civil_war.31
	title = "castilian_civil_war.31.name"
	desc = "castilian_civil_war.31.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.31.opta"
		ai_chance = {
			factor = 0
		}
		random_list = {
			60 = {
				event_target:faction_leader_success = {
					country_event = { id = castilian_civil_war.32 days = 5 }
				}
				event_target:faction_leader_captured = {
					country_event = { id = castilian_civil_war.33 days = 5 }
				}
			}
			40 = {
				event_target:faction_leader_captured = {
					country_event = { id = castilian_civil_war.34 days = 5 }
				}
				event_target:faction_leader_success = {
					country_event = { id = castilian_civil_war.35 days = 5 }
				}
			}
		}
	}
	option = {
		name = "castilian_civil_war.31.optb"
		ai_chance = {
			factor = 100
		}
		random_list = {
			95 = {
				event_target:faction_leader_success = {
					country_event = { id = castilian_civil_war.32 days = 5 }
				}
				event_target:faction_leader_captured = {
					country_event = { id = castilian_civil_war.33 days = 5 }
				}
			}
			5 = {
				event_target:faction_leader_captured = {
					country_event = { id = castilian_civil_war.34 days = 5 }
				}
				event_target:faction_leader_success = {
					country_event = { id = castilian_civil_war.35 days = 5 }
				}
			}
		}
	}
	option = {
		name = "castilian_civil_war.31.optc"
		ai_chance = {
			factor = 0
		}
		event_target:faction_leader_captured = {
			country_event = { id = castilian_civil_war.36 days = 5 }
		}
	}
}

country_event = {
	id = castilian_civil_war.32
	title = "castilian_civil_war.32.name"
	desc = "castilian_civil_war.32.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.32.opta"
	}
}

country_event = {
	id = castilian_civil_war.33
	title = "castilian_civil_war.33.name"
	desc = "castilian_civil_war.33.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.33.opta"
		kill_ruler = yes
	}
}

country_event = {
	id = castilian_civil_war.34
	title = "castilian_civil_war.34.name"
	desc = "castilian_civil_war.34.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.34.opta"
	}
}

country_event = {
	id = castilian_civil_war.35
	title = "castilian_civil_war.35.name"
	desc = "castilian_civil_war.35.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.35.opta"
		kill_ruler = yes
	}
}

country_event = {
	id = castilian_civil_war.36
	title = "castilian_civil_war.36.name"
	desc = "castilian_civil_war.36.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_lesser_in_union = no
		ENR = { is_lesser_in_union = no }
	}
	
	option = {
		name = "castilian_civil_war.36.opta"
		ai_chance = {
			factor = 100
		}
		FROM = { country_event = { id = castilian_civil_war.37 days = 5 }	}
	}
	option = {
		name = "castilian_civil_war.36.optb"
		ai_chance = {
			factor = 0
		}
		FROM = { country_event = { id = castilian_civil_war.38 days = 5 }	}
	}
}

country_event = {
	id = castilian_civil_war.37
	title = "castilian_civil_war.37.name"
	desc = "castilian_civil_war.37.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.37.opta"
		create_union = FROM
	}
}

country_event = {
	id = castilian_civil_war.38
	title = "castilian_civil_war.38.name"
	desc = "castilian_civil_war.38.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.38.opta"
		FROM = { country_event = { id = castilian_civil_war.39 days = 25 } }
	}
}

country_event = {
	id = castilian_civil_war.39
	title = "castilian_civil_war.39.name"
	desc = "castilian_civil_war.39.desc"
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.39.opta"
		kill_ruler = yes
	}
}

# End of Castilian Civil War
country_event = {
	id = castilian_civil_war.100
	title = "castilian_civil_war.100.name"
	desc = "castilian_civil_war.100.desc"
	picture = CIVIL_WAR_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = had_cas_civil_war
		clear_global_event_target = faction_leader_success
		clear_global_event_target = faction_leader_captured
	}

	option = {
		name = "castilian_civil_war.100.opta" # Aragon Triumphs
		trigger = {
			OR = {
				AND = {
					NOT = { has_ruler = "Pedro I" }
					ENR = { has_ruler = "Enrique II" }
				}
				junior_union_with = ENR
				NOT = { num_of_cities = 6 }
			}
		}
		if = {
			limit = {
				has_country_flag = cas_civil_war_rebels_won
			}
			subtract_stability_1 = yes
		}
		if = {
			limit = {
				has_opinion_modifier = {
					modifier = cas_civil_war_interferance
					who = ARA
				}
			}
			remove_opinion = {
				who = ARA
				modifier = cas_civil_war_interferance
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = cas_civil_war_rebels_won }
			}
			add_stability_1 = yes
		}
		add_opinion = {
			who = ARA
			modifier = cas_civil_war_winner
		}
		ARA = {
			add_opinion = {
				who = CAS
				modifier = cas_civil_war_winner
			}
		}
		set_global_flag = enrique_wins
		ENR = { country_event = { id = castilian_civil_war.102 days = 0 } }
	}
	option = {
		name = "castilian_civil_war.100.optb" # Portugal Triumphs
		trigger = {
			OR = {
				AND = {
					has_ruler = "Pedro I"
					ENR = { NOT = { has_ruler = "Enrique II" } }
				}
				senior_union_with = ENR
				ENR = { NOT = { num_of_cities = 6 } }
			}
		}
		if = {
			limit = {
				has_country_flag = cas_civil_war_rebels_won
			}
			subtract_stability_1 = yes
		}
		if = {
			limit = {
				has_opinion_modifier = {
					modifier = cas_civil_war_interferance
					who = POR
				}
			}
			remove_opinion = {
				who = POR
				modifier = cas_civil_war_interferance
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = cas_civil_war_rebels_won }
			}
			add_stability_1 = yes
		}
		if = {
			limit = {
				exists = ENR
				ENR = { ai = yes }
			}
			inherit = ENR
		}
		if = {
			limit = {
				exists = ENR
				ENR = { ai = no }
			}
			ENR = { country_event = { id = castilian_civil_war.101 days = 0 } }
		}
		add_opinion = {
			who = POR
			modifier = cas_civil_war_winner
		}
		POR = {
			add_opinion = {
				who = CAS
				modifier = cas_civil_war_winner
			}
		}
		set_global_flag = pedro_wins
	}
	option = {
		name = "castilian_civil_war.100.optc"
		trigger = {
			NOT = { has_ruler = "Pedro I" }
			ENR = { NOT = { has_ruler = "Enrique II" } }
			ai = no
		}
		switch_tag = ENR
		country_event = { id = castilian_civil_war.102 days = 0 }
	}
	option = {
		name = "castilian_civil_war.100.opta"
		trigger = {
			NOT = { has_ruler = "Pedro I" }
			ENR = { NOT = { has_ruler = "Enrique II" } }
			ai = yes
		}
		ENR = { country_event = { id = castilian_civil_war.102 days = 0 } }
		set_global_flag = enrique_wins
	}
}

country_event = {
	id = castilian_civil_war.101
	title = "castilian_civil_war.101.name"
	desc = "castilian_civil_war.101.desc"
	picture = CIVIL_WAR_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.101.opta"
		switch_tag = CAS
		country_event = { id = castilian_civil_war.104 days = 0 }
	}
}

country_event = {
	id = castilian_civil_war.102
	title = "castilian_civil_war.102.name"
	desc = "castilian_civil_war.102.desc"
	picture = CIVIL_WAR_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.101.opta"
		inherit = CAS
		change_tag = CAS
		add_historical_friend = POR
		remove_historical_friend = FRA
		POR = {
			add_historical_friend = ROOT
		}
		if = {
			limit = {
				NOT = { has_country_modifier = title_5 }
				NOT = { has_country_modifier = title_6 }
			}
			change_title_5 = yes
		}
		remove_country_modifier = trastamara_support
		Effect_set_capital = { target=2312 }
		hidden_effect = {
			country_event = {
				id = miscexpenses.001
			}
		}
		hidden_effect = { change_primary_culture = castillian }
		every_owned_province = {
			limit = {
				area = toledo_area
				has_province_modifier = lordship_of_toledo
			}
			remove_province_modifier = lordship_of_toledo
		}
		if = {
			limit = {
				has_country_flag = cas_civil_war_rebels_won
			}
			subtract_stability_1 = yes
		}
		if = {
			limit = {
				has_opinion_modifier = {
					modifier = cas_civil_war_interferance
					who = ARA
				}
			}
			remove_opinion = {
				who = ARA
				modifier = cas_civil_war_interferance
			}
		}
		if = {
			limit = {
				NOT = { has_country_flag = cas_civil_war_rebels_won }
			}
			add_stability_1 = yes
		}
		add_opinion = {
			who = ARA
			modifier = cas_civil_war_winner
		}
		ARA = {
			add_opinion = {
				who = CAS
				modifier = cas_civil_war_winner
			}
		}
	}
}

country_event = {
	id = castilian_civil_war.103
	title = "castilian_civil_war.103.n"
	desc = "castilian_civil_war.103.t"
	picture = COURT_eventPicture
	
	trigger = {
		tag = ENR
		NOT = { exists = CAS }
	}
	
	mean_time_to_happen = {
		months = 6
	}
	
	option = {
		name = "castilian_civil_war.103.a"
		ai_chance = { factor = 100 }
		change_tag = CAS
		hidden_effect = { change_primary_culture = castillian }
	}
}

country_event = {
	id = castilian_civil_war.104
	title = "castilian_civil_war.104.name"
	desc = "castilian_civil_war.104.desc"
	picture = CIVIL_WAR_eventPicture
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = "castilian_civil_war.104.opta"
		inherit = ENR
		add_historical_friend = POR
		remove_historical_friend = FRA
		POR = {
			add_historical_friend = ROOT
		}
		if = {
			limit = {
				NOT = { has_country_modifier = title_5 }
				NOT = { has_country_modifier = title_6 }
			}
			change_title_5 = yes
		}
		remove_country_modifier = trastamara_support
		Effect_set_capital = { target=2312 }
		hidden_effect = {
			country_event = {
				id = miscexpenses.001
			}
		}
		every_owned_province = {
			limit = {
				area = toledo_area
				has_province_modifier = lordship_of_toledo
			}
			remove_province_modifier = lordship_of_toledo
		}
	}
}
