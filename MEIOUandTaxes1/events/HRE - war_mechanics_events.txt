###############################
### HRE War Mechanic Events ###
###############################

### Choices event for the emperor at the defense hre on action
country_event = {
	id = HRE_war_mechanics.001
	title = HRE_war_mechanics.001.t
	desc = HRE_war_mechanics.001.d
	picture = HRE_eventPicture

	is_triggered_only = yes
	
	trigger = {
		is_emperor = yes
		# NOT = { has_global_flag = Reichstag_formalized }
	}
	
	
	immediate = {
		hidden_effect = {
			### hre rework code
			#ERG = { change_variable = { which = hre_military_reform_desire value = 5 } }
			FROM = {
				save_event_target_as = hre_war_defender_leader

				set_variable = { which = hre_war_strength_comparison_gain which = hre_war_strength }
				set_variable = { which = hre_war_strength_comparison_remove which = hre_war_strength }
				
				every_country = {
					limit = {
						is_in_war = {
							attacker_leader = THIS
							defender_leader = PREV
						}
					}
					save_event_target_as = hre_war_attacker_leader
					set_variable = { which = hre_war_strength_test value = 1 }
					set_variable = { which = hre_war_strength_comparison_gain value = 0 }
					set_variable = { which = hre_war_strength_comparison_gain which = hre_war_strength }
					set_variable = { which = hre_war_strength_comparison_remove which = hre_war_strength }
					
					
					divide_variable = { which = hre_war_strength_comparison_gain which = event_target:hre_war_defender_leader }
				}
				
				divide_variable = { which = hre_war_strength_comparison_remove which = event_target:hre_war_attacker_leader }
				set_variable = { which = hre_war_strength_comparison_gain which = event_target:hre_war_attacker_leader }
	            
			}
			set_variable = { which = hre_war_strength_comparison_gain which = event_target:hre_war_defender_leader }
			set_variable = { which = hre_war_strength_comparison_remove which = event_target:hre_war_defender_leader }
		}
	}
	
	after = {
		emperor = {
			set_variable = { which = hre_war_strength_comparison_gain value = 0 }
			set_variable = { which = hre_war_strength_comparison_remove value = 0 }
		}
		event_target:hre_war_attacker_leader = {
			set_variable = { which = hre_war_strength_comparison_gain value = 0 }
			set_variable = { which = hre_war_strength_comparison_remove value = 0 }
		}
		event_target:hre_war_defender_leader = {
			set_variable = { which = hre_war_strength_comparison_gain value = 0 }
			set_variable = { which = hre_war_strength_comparison_remove value = 0 }
		}


		
	}
	
	option = {
		name = "HRE_war_mechanics.001.Ask_empire_for_help"
		custom_tooltip = "HRE_war_mechanics.001.Ask_empire_for_help.ctp"
		ai_chance = {
			factor = 100
		}
		### HRE rework code
		##### Hoftag to discuss the HRE defense
		#hidden_effect = {
		#		### AGENDA:
		#		emperor = { set_country_flag = hre_defense_help }
		#		
		#		### INVITE EVERYONE
		#		hoftag_invite_everyone = yes
		#		
		#		### RANDOM FREE CITY AS PLACE
		#		hoftag_preparations_fast = yes
		#		
		#		
		#	### Fasten up the progress toward reforms
		#	ERG = { change_variable = { which = hre_administrative_reform_desire	value = 2.5 } }
		#}
		
		hidden_effect = {
			#clr_country_flag = hre_defense_help
	
			every_country = {
				limit = {
					is_part_of_hre = yes
					is_emperor = no
				}
				country_event = { id = HRE_war_mechanics.002 days = 14 }
				
	
			}
			country_event = {
				id = HRE_war_mechanics.003
				days = 75
			}
		}
		
		defense_call_scaling_imperial_authority_remove = yes
	}
	
	### HRE_rework code
	### disfunctional atm
	#option = {
	#	name = "HRE_war_mechanics.001.Ask_great_princes_for_help"
	#	custom_tooltip = "HRE_war_mechanics.001.Ask_great_princes_for_help.ctp"
	#
	#	ai_chance = {
	#		factor = 30
	#	}
	#	
	#	hidden_effect = {
	#		every_country = {
	#			limit = {
	#				is_part_of_hre = yes
	#				NOT = { is_emperor = yes }
	#				NOT = { is_subject = yes }
	#				total_development = 30
	#				OR = {
	#					has_country_flag = imperial_prince
	#					has_country_flag = imperial_bishopric
	#					has_country_flag = imperial_count
	#					has_country_flag = imperial_elector
	#				}
	#			}
	#			
	#			country_event = { id = HRE_war_mechanics.002 days = 14 }
	#		}
	#		
	#		country_event = {
	#			id = HRE_war_mechanics.003
	#			days = 75
	#		}
	#	}
	#}
	
	option = {
		name = "HRE_war_mechanics.001.Do_it_alone"
		custom_tooltip = "HRE_war_mechanics.001.Do_it_alone.ctp"
		
		ai_chance = {
			factor = 5
		}
		
		defense_call_scaling_imperial_authority_add = yes
		
	}
}


### Reaction to demand of Heerfolge - princes
country_event = {
	id = HRE_war_mechanics.002
	title = HRE_war_mechanics.002.t
	desc = HRE_war_mechanics.002.d
	picture = HRE_eventPicture

	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			change_variable = { which = numb_of_princes_def value = 1 }
			
			export_to_variable = {
				which = their_manpower_contr
				value = max_manpower
				who = ROOT
			}
			
			export_to_variable = {
				which = ful_money_contr
				value = years_of_income
				who = ROOT
			}
			
			set_variable = { which = base_man_contr value = 0.15 }
			
			set_variable = { which = calc_inf_cost value = 3 }

			set_variable = { which = calc_calv_cost value = 4.5 }

			set_variable = { which = calc_art_cost value = 6 }

			#set_variable = { which = calc_help value = 1000 }
			
			if = {
				limit = {
					has_country_flag = mil_tech_0_war
				}
				
				multiply_variable = { which = calc_inf_cost 	value = 0.7 }
				multiply_variable = { which = calc_calv_cost 	value = 0.3 }
				
				change_variable = { which = their_money_contr 	which = calc_inf_cost }
				change_variable = { which = their_money_contr 	which = calc_calv_cost }
				
			}
			else_if = {
				limit = {
					has_country_flag = mil_tech_10_war
				}
				
				multiply_variable = { which = calc_inf_cost value = 0.7 }
				multiply_variable = { which = calc_calv_cost value = 0.2 }
				multiply_variable = { which = calc_art_cost value = 0.1 }
				
				change_variable = { which = their_money_contr which = calc_inf_cost }
				change_variable = { which = their_money_contr which = calc_calv_cost }
				change_variable = { which = their_money_contr which = calc_art_cost }
			}
			else_if = {
				limit = {
					has_country_flag = mil_tech_20_war
				}
				
				multiply_variable = { which = calc_inf_cost value = 0.7 }
				multiply_variable = { which = calc_calv_cost value = 0.15 }
				multiply_variable = { which = calc_art_cost value = 0.15 }
				
				change_variable = { which = their_money_contr which = calc_inf_cost }
				change_variable = { which = their_money_contr which = calc_calv_cost }
				change_variable = { which = their_money_contr which = calc_art_cost }
			}
			else_if = {
				limit = {
					has_country_flag = mil_tech_30_war
				}
				
				multiply_variable = { which = calc_inf_cost value = 0.65 }
				multiply_variable = { which = calc_calv_cost value = 0.15 }
				multiply_variable = { which = calc_art_cost value = 0.20 }
				
				change_variable = { which = their_money_contr which = calc_inf_cost }
				change_variable = { which = their_money_contr which = calc_calv_cost }
				change_variable = { which = their_money_contr which = calc_art_cost }
			}
			else_if = {
				limit = {
					has_country_flag = mil_tech_40_war
				}
				
				multiply_variable = { which = calc_inf_cost value = 0.65 }
				multiply_variable = { which = calc_calv_cost value = 0.1 }
				multiply_variable = { which = calc_art_cost value = 0.25 }
				
				change_variable = { which = their_money_contr which = calc_inf_cost }
				change_variable = { which = their_money_contr which = calc_calv_cost }
				change_variable = { which = their_money_contr which = calc_art_cost }
			}
			else_if = {
				limit = {
					has_country_flag = mil_tech_50_war
				}
				
				multiply_variable = { which = calc_inf_cost value = 0.60 }
				multiply_variable = { which = calc_calv_cost value = 0.15 }
				multiply_variable = { which = calc_art_cost value = 0.25 }
				
				change_variable = { which = their_money_contr which = calc_inf_cost }
				change_variable = { which = their_money_contr which = calc_calv_cost }
				change_variable = { which = their_money_contr which = calc_art_cost }
			}
		}
	}
	
	option = {
		name = "HRE_war_mechanics.002.manpower"
		trigger = {
			#manpower_percentage = 0.1
			#years_of_income = 0.05
		}
		
		ai_chance = {
			factor = 40
			modifier = {
				factor = 4
				government = monarchy
			}
			modifier = {
				factor = 2
				government = theocracy
			}
		}
		
		custom_tooltip = "HRE_war_mechanics.002.manpower.ctp"
		
		#### Give 150 + 0.5% of max_manpower and 1 year of troop cost
		hidden_effect = {
		
			multiply_variable = { which = their_manpower_contr value = 0.005 }
			
			set_variable = { which = supplied_manpower which = base_man_contr }
			
			change_variable = { which = supplied_manpower which = their_manpower_contr }
			
			emperor = { change_variable = { which = supplied_manpower which = PREV } }
			
			#set_variable = { which = calc_help which = supplied_manpower }
			
			#divide_variable = { which = calc_help value = 1000 }
			
			multiply_variable = { which = their_money_contr which = supplied_manpower }
			
			set_variable = { which = supplied_money which = their_money_contr }

			emperor = { change_variable = { which = supplied_money which = PREV } }
			
			while = {
				limit = {
					check_variable = { which = their_manpower_contr value = 0 }
				}
				add_manpower = -1
				
				subtract_variable = {
					which = their_manpower_contr
					value = 1
				}
			}
			
			while = {
				limit = {
					check_variable = { which = their_money_contr value = 0 }
				}
				add_treasury = -1
				
				subtract_variable = {
					which = their_money_contr
					value = 1
				}
			}

			
			subtract_variable = {
				which = numb_of_princes_def
				value = 1
			}
		}
			
		reverse_add_opinion = {
			who = emperor
			modifier = HRE_defense_help_manpower
		}
	}
	
	option = {
		name = "HRE_war_mechanics.002.money"
		trigger = {
			years_of_income = 0.1
		}

		ai_chance = {
			factor = 40
			modifier = {
				factor = 4
				NOT = {
					OR = {
						government = monarchy
						government = republic
					}
				}
			}
			modifier = {
				factor = 1.5
				government = theocracy
			}
			modifier = {
				factor = 1.1
				check_variable = { which = hre_war_strength_comparison_gain value = 1.2 }
			}
			modifier = {
				factor = 1.05
				check_variable = { which = hre_war_strength_comparison_gain value = 1.1 }
			}
			modifier = {
				factor = 1.05
				check_variable = { which = hre_war_strength_comparison_gain value = 1 }
			}
			modifier = {
				factor = 1.05
				check_variable = { which = hre_war_strength_comparison_gain value = 0.9 }
			}
		}
		
		custom_tooltip = "HRE_war_mechanics.002.money.ctp"
		
		#### Give5 years of income as help
		hidden_effect = {
		
			multiply_variable = { which = ful_money_contr value = 1 }
			
			set_variable = { which = supplied_money which = ful_money_contr }

			emperor = { change_variable = { which = supplied_money which = PREV } }
			
			while = {
				limit = {
					check_variable = { which = ful_money_contr value = 0 }
				}
				add_treasury = -1
				
				subtract_variable = {
					which = ful_money_contr
					value = 1
				}
			}

			
			subtract_variable = {
				which = numb_of_princes_def
				value = 1
			}
		}
			
		reverse_add_opinion = {
			who = emperor
			modifier = HRE_defense_help_money
		}
	}
	
	option = {
		name = "HRE_war_mechanics.002.refuse"
		
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				emperor = { NOT = { reverse_has_opinion = { who = ROOT value = 0 } } }
			}
			modifier = {
				factor = 4
				emperor = { NOT = { reverse_has_opinion = { who = ROOT value = -25 } } }
			}
			modifier = {
				factor = 10
				emperor = { NOT = { reverse_has_opinion = { who = ROOT value = -50 } } }
			}
			modifier = {
				factor = 20
				emperor = { is_rival = ROOT }
			}
			modifier = {
				factor = 10
				is_at_war = yes
			}
			modifier = {
				factor = 0.9
				emperor = { has_global_modifier_value = { which = diplomatic_reputation value = 2 } }
			}
			modifier = {
				factor = 0.8
				emperor = { has_global_modifier_value = { which = diplomatic_reputation value = 3 } }
			}
			modifier = {
				factor = 0.75
				emperor = { has_global_modifier_value = { which = diplomatic_reputation value = 4 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.8 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.7 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.6 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.5 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.4 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.3 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.25 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.2 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.15 } }
			}
			modifier = {
				factor = 1.05
				NOT = { check_variable = { which = hre_war_strength_comparison_gain value = 0.1 } }
			}
		}
		
		custom_tooltip = "HRE_war_mechanics.002.refuse.ctp"
		
		hidden_effect = {
			subtract_variable = {
				which = numb_of_princes_def
				value = 1
			}
		}
		
		every_country = {
			limit = {
				is_part_of_hre = yes
				NOT = { is_emperor = yes }
			}
			add_opinion = {
				who = ROOT
				modifier = HRE_defense_help_refuse
			}
		}
		
		emperor = {
			add_opinion = {
				who = ROOT
				modifier = HRE_defense_help_refuse_emperor
			}
		 }
		
	}
}

### Result of the Heerfolge - Emperor
country_event = {
	id = HRE_war_mechanics.003
	title = HRE_war_mechanics.003.t
	desc = HRE_war_mechanics.003.d
	picture = HRE_eventPicture

	is_triggered_only = yes

	trigger = {
		is_emperor = yes
		NOT = { check_variable = { which = numb_of_princes_def value = 1 } }
	}
	
	immediate = {
		hidden_effect = {
			mil_tech_at_war_start = yes
			set_variable = { which = supplied_force value = 0 }
			set_variable = { which = supplied_force which = supplied_manpower }
			
			set_variable = { which = supplied_infantry value = 0 }
			set_variable = { which = supplied_infantry which = supplied_force }

			set_variable = { which = supplied_cavarly value = 0 }
			set_variable = { which = supplied_cavarly which = supplied_force }

			set_variable = { which = supplied_artillery value = 0 }
			set_variable = { which = supplied_artillery which = supplied_force }
			
			if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_0_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.7 }
				multiply_variable = { which = supplied_cavarly value = 0.3 }
			}
			else_if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_10_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.7 }
				multiply_variable = { which = supplied_cavarly value = 0.2 }
				multiply_variable = { which = supplied_artillery value = 0.1 }
			}
			else_if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_20_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.7 }
				multiply_variable = { which = supplied_cavarly value = 0.15 }
				multiply_variable = { which = supplied_artillery value = 0.15 }
			}
			else_if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_30_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.65 }
				multiply_variable = { which = supplied_cavarly value = 0.15 }
				multiply_variable = { which = supplied_artillery value = 0.20 }
			}
			else_if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_40_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.65 }
				multiply_variable = { which = supplied_cavarly value = 0.1 }
				multiply_variable = { which = supplied_artillery value = 0.25 }
			}
			else_if = {
				limit = {
					#NOT = { has_global_flag = imperial_army_established }
					has_country_flag = mil_tech_50_war
				}
				
				multiply_variable = { which = supplied_infantry value = 0.60 }
				multiply_variable = { which = supplied_cavarly value = 0.15 }
				multiply_variable = { which = supplied_artillery value = 0.25 }
			}
			
			set_country_flag = raised_special_units
			set_country_flag = imperial_estates_war_support
			add_country_modifier = {
				name = estate_war_support
				duration = 140
				hidden = yes
			}
		}
	}

	option = {
		name = "HRE_war_mechanics.003.a"
		
		HRE_defense_force = yes
				
		imperial_estate_recruit_infantry = yes
		imperial_estate_recruit_cavalry = yes
		imperial_estate_recruit_artillery = yes
		set_country_flag = imperial_war
		add_country_modifier = {
			name = estate_war_support
			duration = 140
			hidden = yes
		}
	
	}
}


### End of the imperial war - Emperor
country_event = {
	id = HRE_war_mechanics.004
	title = HRE_war_mechanics.004.t
	desc = HRE_war_mechanics.004.d
	picture = HRE_eventPicture

	trigger = {
		is_emperor = yes
		OR = {
			NOT = { any_country = { offensive_war_with = emperor } }
			has_country_flag = imperial_romzug
		}
		has_country_flag = imperial_war
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = "HRE_war_mechanics.004.a"
		custom_tooltip = "HRE_war_mechanics.004.a.ctp"
		
		hidden_effect = {
			clear_war_variables_hre = yes
			HRE_defense_war_end = yes
		}
	}
}

#updates army size comparison for the HRE defense system
country_event = {
	id = HRE_war_mechanics.005
	title = "HRE_war_mechanics.005.t"
	desc = "HRE_war_mechanics.005.d"
	
	
	picture = HRE_eventPicture
	hidden = yes
	
	is_triggered_only = yes
	
	trigger = {
		is_emperor = yes
	}
	
	immediate = {
	
		hidden_effect = {
			ERG = {
				set_variable = { which = hre_war_strength	value = 0 }
			}
			every_country = {
				set_variable = { which = hre_war_strength	value = 0 }
				set_variable = { which = hre_war_strength_comparison_gain	value = 0 }
				set_variable = { which = hre_war_strength_comparison_remove	value = 0 }
			}
			
			###calc strength of def system
			HRE_support_estimate_defense = yes
		
			### calc strength of possible attackers
			every_country = {
				limit = {
					NOT = { is_part_of_hre = yes }
					any_neighbor_country = {
						is_part_of_hre = yes
					}
					is_subject = no
				}
				offensive_strength = yes
				#removed as this nation isn't set as hre_member_target ### clear_global_event_target = hre_member_target
			}
			
			### strength of emperor when attacked directly
			emperor = {
				HRE_member_def_strength = yes
				clear_global_event_target = hre_member_target
			}
			
			###calc strength of border countries
			every_country = {
				limit = {
					is_part_of_hre = yes
					any_neighbor_country = {
						NOT = { is_part_of_hre = yes }
					}
				}
				HRE_member_def_strength = yes
				clear_global_event_target = hre_member_target
			}
			
			#### Add defense system calc to all countries hre_war_strength
			ERG = {
				every_country = {
					limit = {
						OR = {
							AND = {
								is_part_of_hre = yes
								any_neighbor_country = {
									NOT = { is_part_of_hre = yes }
								}
							}
							is_emperor = yes
						}
					}
					change_variable = { which = hre_war_strength which = PREV }
				}
			}
		}
	}
	
	option = {
		name = "HRE_war_mechanics.005.o"
	

	}
	
}