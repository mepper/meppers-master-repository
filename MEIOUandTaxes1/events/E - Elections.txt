# modified by Ignatich

# Election!
country_event = {
	id = elections.700
	title = "EVTNAME700"
	desc = {
		trigger = { NOT = { has_ruler_flag = leader_has_been_pushed_out } }
		desc = "EVTDESC700"
	}
	
	desc = {
		trigger = { has_ruler_flag = leader_has_been_pushed_out }
		desc = "elections.700.d"
	}
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_colonial_nation = no
		NOT = { government = siena_republic }
		NOT = { has_country_modifier = "medici_system_information" }
		NOT = { has_country_flag = medici_signoria }
		NOT = { government = dutch_republic }
	}
	
	option = {
		name = "EVTOPTA700"	# Current ruler stays
		trigger = {
			has_regency = no
			NOT = { has_ruler_flag = leader_has_been_pushed_out }
		}
		ai_chance = {
			factor = 60
			modifier = {
				factor = 0
				NOT = { republican_tradition = 25 }
			}
			modifier = {
				factor = 0.5
				NOT = { republican_tradition = 50 }
			}
			modifier = {
				factor = 0.5
				NOT = { republican_tradition = 75 }
			}
			modifier = {
				factor = 2.0
				republican_tradition = 90
			}
		}
		custom_tooltip = remains_ruler
		change_adm = 1
		change_dip = 1
		change_mil = 1
		if = {
			limit = {
				NOT = { republican_tradition = 20 }
				government = republic
				NOT = { num_of_cities = 10 }
				culture_group = latin
				NOT = { adm_tech = 26 }
			}
			change_government = signoria_monarchy
		}
		if = {
			limit = {
				NOT = { republican_tradition = 20 }
				government = republic
				adm_tech = 40
				full_idea_group = administrative_ideas
			}
			change_government = bureaucratic_despotism
		}
		if = {
			limit = {
				NOT = { republican_tradition = 20 }
				government = republic
				NOT = { num_of_cities = 10 }
				is_tribal = no
			}
			change_government = despotic_monarchy
			change_title_3 = yes
		}
		if = {
			limit = {
				NOT = { republican_tradition = 20 }
				government = republic
				num_of_cities = 10
				NOT = { num_of_cities = 50 }
				is_tribal = no
			}
			change_government = despotic_monarchy
			change_title_5 = yes
		}
		if = {
			limit = {
				NOT = { republican_tradition = 20 }
				government = republic
				num_of_cities = 50
				is_tribal = no
			}
			change_government = despotic_monarchy
			change_title_6 = yes
		}
		add_scaled_republican_tradition = -10
		random_list = {
			10 = {
				add_adm_power = 50
			}
			10 = {
				add_dip_power = 50
			}
			10 = {
				add_mil_power = 50
			}
		}
	}
	option = {
		name = "EVTOPTB700"	# An administrator type
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				mil = 0
				adm = 3
				dip = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
	option = {
		name = "EVTOPTC700"	# A diplomat
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				dip = 3
				adm = 0
				mil = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
	option = {
		name = "EVTOPTD700"	# A military man
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				mil = 3
				adm = 0
				dip = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
}

# Election!
country_event = {
	id = elections.701
	title = "EVTNAME700"
	desc = "EVTDESC701"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_colonial_nation = no
		NOT = { government = siena_republic }
		NOT = { government = dutch_republic }
		NOT = { has_country_modifier = "medici_system_information" }
		NOT = { has_country_flag = medici_signoria }
	}
	
	option = {
		name = "EVTOPTB700"	# An administrator type
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				mil = 0
				adm = 3
				dip = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
	option = {
		name = "EVTOPTC700"	# A diplomat
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				dip = 3
				adm = 0
				mil = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
	option = {
		name = "EVTOPTD700"	# A military man
		ai_chance = { factor = 20 }
		hidden_effect = {
			define_ruler = {
				mil = 3
				adm = 0
				dip = 0
				fixed = yes
			}
			random_list = {
				33 = {
					change_mil = 1
				}
				33 = {
					change_mil = 2
				}
				33 = {
					change_mil = 3
				}
			}
			random_list = {
				33 = {
					change_adm = 1
				}
				33 = {
					change_adm = 2
				}
				33 = {
					change_adm = 3
				}
			}
			random_list = {
				33 = {
					change_dip = 1
				}
				33 = {
					change_dip = 2
				}
				33 = {
					change_dip = 3
				}
			}
		}
	}
}

# Election!
country_event = {
	id = elections.706
	title = "elections.EVTNAME706"
	desc = "elections.EVTDESC706"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		is_colonial_nation = yes
	}
	
	option = {
		name = "elections.EVTOPTB706"
		colonial_parent = {
			country_event = { id = elections.707 days = 0 }
		}
	}
}

country_event = {
	id = elections.707
	title = "elections.EVTNAME707"
	desc = "elections.EVTDESC707"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "elections.EVTOPTA707"	# Same Candidate
		ai_chance = { factor = 20 }
		trigger = {
			has_country_flag = feudal_colonisation
		}
	}
	option = {
		name = "elections.EVTOPTB707"	# Bureaucrat Candidate
		ai_chance = { factor = 20 }
		FROM = {
			define_ruler = {
				adm = 4
				mil = 2
				dip = 2
				fixed = yes
			}
			set_ruler_flag = appointed_by_overlord
		}
	}
	option = {
		name = "elections.EVTOPTC707"	# Diplomat Candidate
		ai_chance = { factor = 20 }
		FROM = {
			define_ruler = {
				adm = 2
				dip = 4
				mil = 2
				fixed = yes
			}
			set_ruler_flag = appointed_by_overlord
		}
	}
	option = {
		name = "elections.EVTOPTD707"	# Military Candidate
		ai_chance = { factor = 20 }
		FROM = {
			define_ruler = {
				adm = 2
				dip = 2
				mil = 4
				fixed = yes
			}
			set_ruler_flag = appointed_by_overlord
		}
	}
}

# Colonial Assembly
country_event = {
	id = elections.711
	title = "elections.EVTNAME711"
	desc = "elections.EVTDESC711"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "elections.EVTOPTB711"	# Suspicious indeed...
		tooltip = {
			FROM = {
				add_liberty_desire = -5
			}
		}
	}
}

# Winds of Liberty
country_event = {
	id = elections.712
	title = "elections.EVTNAME712"
	desc = "elections.EVTDESC712"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "elections.EVTOPTB712"	# Most worrisome...
		tooltip = {
			FROM = {
				add_liberty_desire = -25
			}
		}
	}
}

# Dutch Republic
country_event = {
	id = elections.720
	title = elections.720.t
	desc = elections.720.d
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government = dutch_republic
	}
	
	option = {
		name = elections.720.a
		define_ruler = {
		}
		change_statists_vs_orangists = -0.33
	}
	option = {
		name = elections.720.b
		define_ruler = {
		}
		change_statists_vs_orangists = 0.33
	}
}

country_event = {
	id = elections.721
	title = elections.720.t
	desc = elections.721.d
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government = dutch_republic
	}
	
	option = {
		name = elections.720.a
		define_ruler = {
		}
		change_statists_vs_orangists = -0.33
	}
	option = {
		name = elections.720.b
		define_ruler = {
		}
		change_statists_vs_orangists = 0.33
	}
}
