### Jurchen and Manchu events - Warial

### Written script for Jurchens
country_event = {
	id = jurchen_ascendancy.1
	
	title = "jurchen_ascendancy.1.t"
	desc = "jurchen_ascendancy.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { ### Adapt Mongol script
		name = "jurchen_ascendancy.1.a"
		add_country_modifier = {
			name = "manchu_alphabet"
			duration = -1
		}
		set_country_flag = manchu_alphabet
		
		ai_chance = {
			factor = 90
			
			modifier = {
				factor = 10
				tag = MJZ
			}
			modifier = {
				factor = 0
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MHX
			}
		}
	}
	option = { ### Restore old Jurchen script
		name = "jurchen_ascendancy.1.b"
		add_country_modifier = {
			name = "jurchen_script"
			duration = -1
		}
		set_country_flag = jurchen_script
		
		ai_chance = {
			factor = 10
			
			modifier = {
				factor = 10
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MJZ
			}
		}
	}
}

### Military and tribal reform - Eight Banners vs clans
country_event = {
	id = jurchen_ascendancy.2
	
	title = "jurchen_ascendancy.2.t"
	desc = "jurchen_ascendancy.2.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { ### Destroy tribal structures and create banner system
		name = "jurchen_ascendancy.2.a"
		add_country_modifier = {
			name = "eight_banners"
			duration = -1
		}
		set_country_flag = eight_banners
		
		ai_chance = {
			factor = 90
			
			modifier = {
				factor = 10
				tag = MJZ
			}
			modifier = {
				factor = 0
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MHX
			}
		}
	}
	option = { ### Create new organization based upon clans
		name = "jurchen_ascendancy.2.b"
		add_country_modifier = {
			name = "jurchen_clans"
			duration = -1
		}
		set_country_flag = jurchen_clans
		
		ai_chance = {
			factor = 10
			
			modifier = {
				factor = 10
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MJZ
			}
		}
	}
}

### Evenks and Daurs join Manchu banners
province_event = {
	id = jurchen_ascendancy.3
	
	title = "jurchen_ascendancy.3.t"
	desc = "jurchen_ascendancy.3.d"
	
	picture = SIEGE_eventPicture
	
	trigger = {
		owner = { has_country_flag = integrated_new_manchu }
		culture_group = tungusic
		NOT = { culture = manchu }
	}
	
	mean_time_to_happen = { months = 48 }
	
	option = { ### Make $REGION$ Manchu
		name = "jurchen_ascendancy.3.a"
		owner = {
			every_owned_province = {
				limit = {
					area = ROOT
					culture_group = tungusic
					NOT = { culture = manchu }
				}
				change_culture = manchu
			}
		}
	}
}

### Flavour event, creating Manchu ethnicity
country_event = {
	id = jurchen_ascendancy.4
	
	title = "jurchen_ascendancy.4.t"
	desc = "jurchen_ascendancy.4.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { ### Rejoice!
		name = "jurchen_ascendancy.4.a"
	}
}

### Change clan name to Aisin Gioro
country_event = {
	id = jurchen_ascendancy.5
	
	title = "jurchen_ascendancy.5.t"
	desc = "jurchen_ascendancy.5.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = { ### Rejoice!
		name = "jurchen_ascendancy.5.a"
		trigger = {
			has_heir = no
		}
		define_heir = {
			dynasty = "Aisin Gioro"
		}
		ai_chance = {
			factor = 50
			
			modifier = {
				factor = 10
				tag = MJZ
			}
			modifier = {
				factor = 0
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MHX
			}
		}
	}
	option = { ### Rejoice!
		name = "jurchen_ascendancy.5.b"
		trigger = {
			has_heir = yes
		}
		hidden_effect = {
			export_to_variable = {
				which = heir_adm_variable
				value = heir_adm
			}
			export_to_variable = {
				which = heir_dip_variable
				value = heir_dip
			}
			export_to_variable = {
				which = heir_mil_variable
				value = heir_mil
			}
		}
		define_heir = {
			dynasty = "Aisin Gioro"
			adm = 6
			dip = 5
			mil = 6
		}
		hidden_effect = {
			change_heir_adm = -6
			change_heir_dip = -6
			change_heir_mil = -6
			while = {
				limit = {
					check_variable = { which = heir_adm_variable value = 1 }
				}
				change_variable = { which = heir_adm_variable value = -1 }
				increase_heir_adm_effect = yes
			}
			while = {
				limit = {
					check_variable = { which = heir_dip_variable value = 1 }
				}
				change_variable = { which = heir_dip_variable value = -1 }
				increase_heir_dip_effect = yes
			}
			while = {
				limit = {
					check_variable = { which = heir_mil_variable value = 1 }
				}
				change_variable = { which = heir_mil_variable value = -1 }
				increase_heir_mil_effect = yes
			}
			set_variable = { which = heir_adm_variable value = 0 }
			set_variable = { which = heir_dip_variable value = 0 }
			set_variable = { which = heir_mil_variable value = 0 }
		}
		add_legitimacy = 5
		ai_chance = {
			factor = 50
			
			modifier = {
				factor = 10
				tag = MJZ
			}
			modifier = {
				factor = 0
				tag = MXI
			}
			modifier = {
				factor = 0
				tag = MHX
			}
		}
	}
	option = { ### Rejoice!
		name = "jurchen_ascendancy.5.c"
		add_prestige = 5
		ai_chance = {
			factor = 50
			
			modifier = {
				factor = 0
				tag = MJZ
			}
			modifier = {
				factor = 10
				tag = MXI
			}
			modifier = {
				factor = 10
				tag = MHX
			}
		}
	}
}
