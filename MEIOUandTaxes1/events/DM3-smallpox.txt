
#Smallpox hits natives
province_event = {
	id = smallpox.1
	title = "SPEVENTNAME1"
	desc = "SPEVENTDESC1"
	picture = COLONIZATION_eventPicture
	
	trigger = {
		native_size = 9
		is_colony = yes
		owner = {
			NOT = {
				capital_scope = {
					OR = {
						continent = north_america
						continent = south_america
					}
				}
			}
		}
		OR = {
			continent = north_america
			continent = south_america
		}
	}
	
	mean_time_to_happen = { months = 24 }
	
	option = {
		name = "SPEVENTOPT"
		
		#Natives 9k or greater
		if = {
			limit = {
				native_size = 90
			}
			change_native_size = -50
			change_native_ferocity = -1
			change_native_hostileness = -1
		}
		
		#Natives 2k to 9k
		if = {
			limit = {
				native_size = 20
				NOT = { native_size = 90 }
			}
			change_native_size = -10
			change_native_ferocity = -1
			change_native_hostileness = -1
		}
		
		#Natives less then 2k
		if = {
			limit = {
				NOT = { native_size = 20 }
			}
			change_native_size = -5
			change_native_ferocity = -1
			change_native_hostileness = -1
		}
	}
}