#Compiling the History of [former_dynasty_of_china.GetName]
country_event = {
	id = tianxia.100
	title = "tianxia.100.t"
	desc = "tianxia.100.d"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "tianxia.100a" #It's just a tradition, do it quickly
		custom_tooltip = compiling_history_quick
		add_country_modifier = {
			name = compiling_history_book
			duration = -1
		}
		hidden_effect = {
			country_event = { id = tianxia.101 days = 730 random = 365 }
		}
	}
	option = {
		name = "tianxia.100b" #We should be careful not not make any mistakes
		custom_tooltip = compiling_history_quality
		add_country_modifier = {
			name = compiling_history_book
			duration = -1
		}
		hidden_effect = {
			country_event = { id = tianxia.101 days = 7300 random = 3650 }
		}
	}
}
# The work is finished
country_event = {
	id = tianxia.101
	title = "tianxia.101.t"
	desc = "tianxia.101.d"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "tianxia.101a" #Well done
		remove_country_modifier = compiling_history_book
		if = {
			limit = {
				had_country_flag = {
					flag = compiling_dynastic_history
					days = 7300
				}
			}
			add_prestige = 35
			add_legitimacy = 35
			add_adm_power = 200
		}
		else_if = {
			limit = {
				had_country_flag = {
					flag = compiling_dynastic_history
					days = 3650
				}
			}
			add_prestige = 20
			add_legitimacy = 20
			add_adm_power = 150
		}
		else_if = {
			limit = {
				had_country_flag = {
					flag = compiling_dynastic_history
					days = 1825
				}
			}
			add_prestige = 10
			add_legitimacy = 10
			add_adm_power = 100
		}
		else_if = {
			limit = {
				had_country_flag = {
					flag = compiling_dynastic_history
					days = 730
				}
			}
			add_prestige = 5
			add_legitimacy = 5
			add_adm_power = 50
		}
	}
}
province_event = {
	id = tianxia.110
	title = "tianxia.110.t"
	desc = "tianxia.110.d"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "tianxia.110a" #Great
		#if = {
		#	limit = {
		#		province_id = 708
		#	}
		#	#Beijing Walls - Khardinal
		#}
		#if = {
			#	limit = {
		#		province_id = 2150
		#	}
		#	#Nanjing Walls - Khardinal
		#}
		if = {
			limit = {
				region = shaangan_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = shanxi_region
								region = hebei_region
								region = jiaoliao_region
								region = zhongyuan_region
							}
						}
					}
				}
				rename_capital = "Xijing"
					owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = shanxi_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							region = shaangan_region
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = hebei_region
								region = jiaoliao_region
								region = zhongyuan_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = hebei_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = shaangan_region
								region = shanxi_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							area = liaoning_area
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
						capital_scope = {
					rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = zhongyuan_region
								AND = {
									region = jiaoliao_region
									NOT = { area = liaoning_area }
								}
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = jiaoliao_region
				NOT = { area = liaoning_area }
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = shaangan_region
								region = shanxi_region
								area = north_henan_area
								area = heluo_area
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								area = liaoning_area
								region = hebei_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
								area = huaibei_area
								area = haozhou_area
								area = south_henan_area
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				area = liaoning_area
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = hebei_region
								region = shanxi_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
								area = huaibei_area
								area = haozhou_area
								area = south_henan_area
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = zhongyuan_region
				}
				if = {
				limit = {
					owner = {
						capital_scope = {
							region = shaangan_region
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
							rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = sichuan_region
								region = jiangnan_region
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = huguang_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = jiangnan_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = huguang_region
								region = sichuan_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}	
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = zhejiang_region
								region = fujian_region
								region = jiangxi_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = zhejiang_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiangxi_region
								region = huguang_region
								region = sichuan_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = guangdong_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = fujian_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiangxi_region
								region = huguang_region
								region = sichuan_region
								region = guangdong_region
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = jiangnan_region
								region = zhejiang_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = jiangxi_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = zhejiang_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = huguang_region
								region = sichuan_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = guangdong_region
								region = guangxi_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = huguang_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = zhejiang_region
								region = jiangxi_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							region = sichuan_region
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = guangdong_region
								region = yunnan_region
								region = guangxi_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = sichuan_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = zhejiang_region
								region = jiangxi_region
								region = jiangnan_region
								region = huguang_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = guangdong_region
								region = yunnan_region
								region = guangxi_region
							}
						}
					}
				}
				rename_capital = "Beijing"
				owner = {
					capital_scope = {
						rename_capital = "Nanjing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = guangdong_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							region = fujian_region
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = guangxi_region
								region = yunnan_region
							}
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = sichuan_region
								region = huguang_region
								region = jiangxi_region
								region = zhejiang_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = guangxi_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = guangdong_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							region = yunnan_region
						}
					}
				}
				rename_capital = "Dongjing"
				owner = {
					capital_scope = {
						rename_capital = "Xijing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = sichuan_region
								region = huguang_region
								region = jiangxi_region
								region = zhejiang_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
		}
		else_if = {
			limit = {
				region = yunnan_region
			}
			if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = fujian_region
								region = guangdong_region
								region = guangxi_region
							}
						}
					}
				}
				rename_capital = "Xijing"
				owner = {
					capital_scope = {
						rename_capital = "Dongjing"
					}
				}
			}
			else_if = {
				limit = {
					owner = {
						capital_scope = {
							OR = {
								region = jiaoliao_region
								region = shanxi_region
								region = hebei_region
								region = zhongyuan_region
								region = shaangan_region
								region = sichuan_region
								region = huguang_region
								region = jiangxi_region
								region = zhejiang_region
								region = jiangnan_region
							}
						}
					}
				}
				rename_capital = "Nanjing"
				owner = {
					capital_scope = {
						rename_capital = "Beijing"
					}
				}
			}
		}
	}
		
	after = {
		add_legitimacy = 5
	}
}
