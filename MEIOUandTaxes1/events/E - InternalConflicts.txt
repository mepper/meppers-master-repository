
# Internal Conflicts
country_event = {
	id = internal_strife.3001
	title = "EVTNAME3001"
	desc = "EVTDESC3001"
	picture = PLAGUE_eventPicture
	
	major = yes
	
	trigger = {
		NOT = { prestige = 0 }
		war_exhaustion = 12
		num_of_cities = 5
		NOT = { stability = 1 }
		NOT = { has_country_flag = religious_turmoil }
		NOT = { has_country_flag = peasant_war }
		NOT = { has_country_flag = internal_conflicts }
		NOT = { has_country_flag = FRA_french_revolution }
		NOT = { has_country_flag = revolution }
		NOT = { has_country_flag = liberalism }
		NOT = { has_country_flag = civil_war }
		NOT = { has_country_flag = total_war_series }
	}
	
	mean_time_to_happen = {
		years = 36
		
		
		modifier = {
			factor = 0.6
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
	}
	
	immediate = {
		set_country_flag = internal_conflicts
	}
	
	option = {
		name = "EVTOPTA3001" # Dire times are ahead of us
		subtract_stability_3 = yes
	}
}

# Religious struggles
province_event = {
	id = internal_strife.3002
	title = "EVTNAME3002"
	desc = "EVTDESC3002"
	picture = RELIGION_eventPicture
	
	trigger = {
		owner = {
			has_country_flag = internal_conflicts
			is_at_war = yes
			NOT = { religion = ROOT }
			NOT = { has_country_flag = religious_struggles }
		}
	}
	
	mean_time_to_happen = { months = 12 }
	
	option = {
		name = "EVTOPTA3002"			# One nation, one belief!
		owner = {
			add_dip_power = -50
			set_country_flag = religious_struggles
			add_country_modifier = {
				name = "religious_intolerance"
				duration = 3650
			}
		}
	}
	option = {
		name = "EVTOPTB3002"			# Tolerate all beliefs
		owner = {
			add_adm_power = -50
			set_country_flag = religious_struggles
			add_country_modifier = {
				name = "religious_tolerance"
				duration = 3650
			}
		}
	}
}

# National instability
country_event = {
	id = internal_strife.3003
	title = "EVTNAME3003"
	desc = "EVTDESC3003"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		has_country_flag = internal_conflicts
		had_country_flag = { flag = internal_conflicts days = 365 }
		is_at_war = yes
		NOT = { stability = 0 }
		NOT = { has_country_flag = national_instability }
	}
	
	mean_time_to_happen = { months = 32 }
	
	immediate = {
		set_country_flag = national_instability
	}
	
	option = {
		name = "EVTOPTA3003"			# The war is our main concern
		add_mil_power = 50
	}
	option = {
		name = "EVTOPTB3003"			# Our domestic problems are our main concern
		add_adm_power = 50
	}
}

# Unrest among the peasants
country_event = {
	id = internal_strife.3004
	title = "EVTNAME3004"
	desc = "EVTDESC3004"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		has_country_flag = internal_conflicts
		NOT = { has_country_flag = unrest }
	}
	
	mean_time_to_happen = { months = 12 }
	
	immediate = {
		set_country_flag = unrest
	}
	
	option = {
		name = "EVTOPTA3004"			# Restrict serfdom
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = nobles_organizing }
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		add_country_modifier = {
			name = "restrict_serfdom"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
	option = {
		name = "EVTOPTB3004"			# Keep the masses in chains
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		add_country_modifier = {
			name = "enforce_serfdom"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
}

# War exhaustion
country_event = {
	id = internal_strife.3005
	title = "EVTNAME3005"
	desc = "EVTDESC3005"
	picture = BATTLE_eventPicture
	
	trigger = {
		has_country_flag = internal_conflicts
		has_wartaxes = yes
		had_country_flag = { flag = internal_conflicts days = 730 }
		war_exhaustion = 5
		NOT = { has_country_flag = war_exhuastion }
	}
	
	mean_time_to_happen = { months = 28 }
	
	immediate = {
		set_country_flag = war_exhuastion
	}
	
	option = {
		name = "EVTOPTA3005"			# There is no immediate solution at hand
		add_country_modifier = {
			name = "decreased_morale"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
	
	option = {
		name = "EVTOPTB3005"			# Let their voices be heard
		add_country_modifier = {
			name = "disarmament"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
}

# Inefficient ruler
country_event = {
	id = internal_strife.3006
	title = "EVTNAME3006"
	desc = "EVTDESC3006"
	picture = BAD_WITH_MONARCH_eventPicture
	
	trigger = {
		has_country_flag = internal_conflicts
		had_country_flag = { flag = internal_conflicts days = 365 }
		NOT = { adm = 3 }
		NOT = { mil = 3 }
		government = monarchy
		NOT = { has_country_flag = inefficient_ruler }
	}
	
	mean_time_to_happen = { months = 24 }
	
	immediate = {
		set_country_flag = inefficient_ruler
	}
	
	option = {
		name = "EVTOPTA3006"			# Leave the nation's fate in his hands
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = pretender_organizing }
			}
			add_province_modifier = {
				name = "pretender_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		add_country_modifier = {
			name = "support_monarch"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
	option = {
		name = "EVTOPTB3006"			# Oppose him
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = nobles_organizing }
			}
			add_province_modifier = {
				name = "nobles_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		add_country_modifier = {
			name = "oppose_monarch"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
}

# Famine
country_event = {
	id = internal_strife.3007
	title = "EVTNAME3007"
	desc = "EVTDESC3007"
	picture = FACMNE_eventPicture
	
	trigger = {
		has_country_flag = internal_conflicts
		NOT = { stability = 0 }
		NOT = { has_country_flag = famine }
	}
	
	mean_time_to_happen = { months = 24 }
	
	immediate = {
		set_country_flag = famine
	}
	
	option = {
		name = "EVTOPTA3007"			# Try to alleviate the suffering
		add_years_of_income = -0.5
		add_country_modifier = {
			name = "alleviate_population"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
	option = {
		name = "EVTOPTB3007"			# We are helpless in the face of such a disaster...
		add_country_modifier = {
			name = "starvation"
			duration = -1
			desc = "END_OF_INTERNAL_CONFLICTS"
		}
	}
}


# Stability returns
country_event = {
	id = internal_strife.3008
	title = "EVTNAME3008"
	desc = "EVTDESC3008"
	picture = CIVIL_WAR_eventPicture
	
	major = yes
	
	trigger = {
		has_country_flag = internal_conflicts
		had_country_flag = { flag = internal_conflicts days = 1000 }
		NOT = { num_of_revolts = 1 }
		is_at_war = no
		stability = 1
	}
	
	mean_time_to_happen = {
		years = 7
		
		modifier = {
			factor = 0.9
			mil = 3
		}
		modifier = {
			factor = 0.8
			mil = 4
		}
		modifier = {
			factor = 1.1
			NOT = { mil = 3 }
		}
		modifier = {
			factor = 1.2
			NOT = { mil = 2 }
		}
		modifier = {
			factor = 0.9
			adm = 3
		}
		modifier = {
			factor = 0.8
			adm = 4
		}
		modifier = {
			factor = 1.1
			NOT = { adm = 3 }
		}
		modifier = {
			factor = 1.2
			NOT = { adm = 2 }
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -2 }
		}
	}
	
	option = {
		name = "EVTOPTA3008"			# Restore order
		clr_country_flag = internal_conflicts
		clr_country_flag = religious_struggles
		clr_country_flag = national_instability
		clr_country_flag = unrest
		clr_country_flag = war_exhuastion
		clr_country_flag = famine
		clr_country_flag = inefficient_ruler
		add_stability_2 = yes
		
		remove_country_modifier = "restrict_serfdom"
		remove_country_modifier = "enforce_serfdom"
		remove_country_modifier = "decreased_morale"
		remove_country_modifier = "disarmament"
		#remove_country_modifier = "domestic_improvements"
		remove_country_modifier = "support_monarch"
		remove_country_modifier = "oppose_monarch"
		remove_country_modifier = "alleviate_population"
		remove_country_modifier = "starvation"
	}
}
