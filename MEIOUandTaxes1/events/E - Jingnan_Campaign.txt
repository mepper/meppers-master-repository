### Event List ###
#1. Crown Prince Zhu Biao falls ill
#2. The Territorial Princes
#3. New Emperor attempts to control Princes
#4. Faster/Slower
#5. Zhu Di's uprising
#6. Fall of capital, Pretender becomes Emperor
#7. Rebellion crushed

#### Crown Prince Zhu Biao falls ill ####
# Zhu Biao's disease
country_event = {
	id = "jingnan.1"
	
	title = "jingnan.1.t"
	desc = "jingnan.1.d"
	
	picture = PLAGUE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_heir = yes
		heir_age = 23
		is_year = 1378
		NOT = { is_year = 1400 }
		NOT = { has_country_flag = biao_sick }
	}
	
	mean_time_to_happen = { months = 6 }
	
	option = {
		name = "EVTOPTA9456"
		add_years_of_income = -0.2
		random_list = {
			75 = { kill_heir = yes }
			25 = { add_legitimacy = 5 }
		}
		set_country_flag = biao_sick
		set_country_flag = jingnan
		ai_chance = { factor = 5 }
	}
	option = {
		name = "EVTOPTB9456"
		kill_heir = yes
		ai_chance = { factor = 95 }
		set_country_flag = biao_sick
		set_country_flag = biao_death
	}
}

# New Heir?
country_event = {
	id = "jingnan.2"
	
	title = "jingnan.2.t"
	desc = "jingnan.2.d"
	
	picture = KING_SICK_IN_BED_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		OR = {
			has_country_flag = biao_death
			has_heir = no
		}
		NOT = {
			has_country_flag = jingnan
			has_country_flag = no_jingnan
		}
		NOT = { is_year = 1400 }
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = "jingnan.2a"
		define_heir = {
			name = "Yunwen"
			monarch_name = "Yunwen"
			claim = 90
			dynasty = ROOT
			birth_date = 1377.12.5
			ADM = 4
			DIP = 1
			MIL = 4
		}
		set_country_flag = jingnan
		clr_country_flag = biao_sick
		clr_country_flag = biao_death
	}
	option = {
		name = "jingnan.2b"
		define_heir = {
			name = "Di"
			monarch_name = "Di"
			claim = 80
			dynasty = ROOT
			birth_date = 1360.5.2
			ADM = 2
			DIP = 2
			MIL = 3
		}
		set_country_flag = no_jingnan
		clr_country_flag = biao_sick
		clr_country_flag = biao_death
	}
}

#### The Territorial Princes ####
country_event = {
	id = "jingnan.3"
	
	title = "jingnan.3.t"
	desc = "jingnan.3.d"
	
	picture = LAND_MILITARY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		NOT = { has_country_flag = ming_territorial_princes }
		tag = MNG
		has_ruler = "Yuanzhang"
		owns = 708 # Dadu
		owns = 2247 # Datong
		owns = 2468 # Taiyuan
		check_variable = { which = "Demesne_in_the_Mandate_of_Heaven" value = 70 }
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = "jingnan.3a"
		set_country_flag = ming_territorial_princes
		set_ruler_flag = emperor_agrees
	}
}

#### New Emperor attempts to control Princes ####
## New Emperor is here
country_event = {
	id = "jingnan.4"
	
	title = "jingnan.4.t"
	desc = "jingnan.4.d"
	
	picture = COURT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_country_flag = ming_territorial_princes
		NOT = { has_ruler = "Yuanzhang" }
		NOT = { has_ruler_modifier = princes_in_control }
		NOT = { has_ruler_flag = emperor_agrees }
		NOT = { has_country_flag = prince_purge }
		is_at_war = no
	}
	
	mean_time_to_happen = { months = 3 }
	
	option = {
		name = "jingnan.4a"
		set_country_flag = clearing_princes
		if = {
			limit = { NOT = { has_country_flag = jingnan } }
			set_country_flag = jingnan
		}
		set_ruler_flag = emperor_agrees
		set_country_flag = prince_purge
		add_faction_influence = { faction = bureaucrats influence = 5 }
		ai_chance = { factor = 95 }
	}
	option = {
		name = "jingnan.4b"
		add_ruler_modifier = { name = princes_in_control duration = -1 }
		ai_chance = { factor = 5 }
	}
}

#### Speed of Purge ####
## Choose it, your majesty

country_event = {
	id = "jingnan.5"
	
	title = "jingnan.5.t"
	desc = "jingnan.5.d"
	
	picture = DEBATE_REPUBLICAN_eventPicture
	
	
	trigger = {
		tag = MNG
		has_country_flag = ming_territorial_princes
		has_country_flag = clearing_princes
		OR = {
			has_country_flag = jingnan
			has_country_flag = jingnan_won
			has_country_flag = jingnan_lost
		}
		NOT = { has_ruler_modifier = princes_in_control }
		NOT = { has_country_flag = cleared }
		is_at_war = no
	}
	
	mean_time_to_happen = { months = 3 }
	
	option = {
		name = "jingnan.5a"
		if = {
			limit = {
				NOT = { has_country_flag = jingnan_won }
				NOT = { has_country_flag = jingnan_lost }
			}
			set_country_flag = cleared
			clr_country_flag = clearing_princes
			add_country_modifier = { name = korea_under_reform duration = 1825 }
			add_adm_power = -50
			add_dip_power = -50
			add_mil_power = -50
			subtract_stability_2 = yes
			add_faction_influence = { faction = bureaucrats influence = 10 }
		}
		if = {
			limit = {
				OR = {
					has_country_flag = jingnan_won
					has_country_flag = jingnan_lost
				}
			}
			set_country_flag = cleared
			clr_country_flag = clearing_princes
			add_country_modifier = { name = korea_under_reform duration = 365 }
			add_adm_power = -30
			add_dip_power = -30
			add_mil_power = -30
		}
		ai_chance = {
			factor = 0
			modifier = {
				has_country_flag = jingnan_won
				factor = 25
			}
			modifier = {
				has_country_flag = jingnan_lost
				factor = 25
			}
		}
	}
	option = {
		name = "jingnan.5b"
		clr_country_flag = clearing_princes
		add_country_modifier = { name = korea_under_reform duration = 1095 }
		add_faction_influence = { faction = bureaucrats influence = 5 }
		ai_chance = {
			factor = 25
			modifier = {
				has_country_flag = jingnan_won
				factor = 0
			}
			modifier = {
				has_country_flag = jingnan_lost
				factor = 0
			}
		}
	}
}

#### Uprising! ####
## the war is here!

country_event = {
	id = "jingnan.6"
	
	title = "jingnan.6.t"
	desc = "jingnan.6.d"
	
	picture = COUNTRY_COLLAPSE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = MNG
		has_country_flag = ming_territorial_princes
		has_country_modifier = korea_under_reform
		has_country_flag = jingnan
		NOT = { has_country_flag = uprising }
		NOT = { has_ruler_modifier = princes_in_control }
		NOT = { has_country_flag = cleared }
	}
	
	mean_time_to_happen = { years = 2 }
	
	option = {
		name = "jingnan.6a"
		if = {
			limit = {
				NOT = { capital_scope = { region = hebei_region } }
				NOT = { exists = CYN }
				NOT = { tag = CYN }
			}
			every_owned_province = {
				limit = { region = hebei_region }
				add_core = CYN
			}
			set_country_flag = uprising
			add_legitimacy = -50
			subtract_stability_2 = yes
			release = CYN
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			CYN = { set_country_flag = red_turban_rebel add_treasury = 250 }
			if = {
				limit = {
					tag = MNG
					NOT = { has_country_flag = no_jingnan }
					NOT = { is_year = 1410 }
				}
				CYN = {
					define_ruler = {
						name = "Di"
						dynasty = ROOT
						adm = 2
						dip = 2
						mil = 3
						age = 40
						claim = 25
					}
					define_ruler_to_general = {
						fire = 4
						shock = 4
						manuever = 4
						siege = 2
					}
				}
				set_country_flag = no_jingnan
			}
			if = {
				limit = {
					NOT = { tag = MNG }
					has_country_flag = no_jingnan
					is_year = 1410
				}
				CYN = {
					define_ruler = {
						dynasty = ROOT
					}
					define_ruler_to_general = {
						fire = 3
						shock = 3
						manuever = 3
						siege = 1
					}
				}
			}
			CYN = {
				declare_war_with_cb = {
					who = ROOT
					casus_belli = cb_claim_throne
				}
			}
		}
		if = {
			limit = {
				NOT = { capital_scope = { region = sichuan_region } }
				NOT = { exists = SHU }
				NOT = { tag = SHU }
			}
			every_owned_province = {
				limit = { region = sichuan_region }
				add_core = SHU
			}
			set_country_flag = uprising
			add_legitimacy = -50
			subtract_stability_2 = yes
			release = SHU
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			random_owned_province = {
				limit = {
					NOT = { region = east_manchuria_region }
					NOT = { region = west_manchuria_region }
					NOT = { area = liaoning_area }
					NOT = { has_province_modifier = pretender_organizing }
				}
				add_province_modifier = {
					name = "pretender_organizing"
					duration = 5475
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 15 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_15
			}
			SHU = { set_country_flag = red_turban_rebel add_treasury = 250 }
			if = {
				limit = {
					tag = MNG
					NOT = { has_country_flag = no_jingnan }
					NOT = { is_year = 1410 }
				}
				SHU = {
					define_ruler = {
						name = "Yongle"
						dynasty = ROOT
						adm = 2
						dip = 2
						mil = 3
						age = 40
						claim = 25
					}
					define_ruler_to_general = {
						fire = 4
						shock = 4
						manuever = 4
						siege = 2
					}
					set_country_flag = no_jingnan
				}
			}
			if = {
				limit = {
					NOT = { tag = MNG }
					has_country_flag = no_jingnan
					is_year = 1410
				}
				SHU = {
					define_ruler = {
						dynasty = ROOT
					}
					define_ruler_to_general = {
						fire = 3
						shock = 3
						manuever = 3
						siege = 1
					}
				}
			}
			SHU = {
				declare_war_with_cb = {
					who = ROOT
					casus_belli = cb_claim_throne
				}
			}
		}
	}
}

### Fall of Capital ###
# Yan

province_event = {
	id = "jingnan.7"
	
	title = "jingnan.7.t"
	desc = "jingnan.7.d"
	
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owner = {
			has_country_flag = jingnan
			tag = MNG
		}
		OR = {
			AND = {
				is_capital = yes
				controlled_by = CYN
			}
			owner = {
				is_at_war = no
				OR = {
					junior_union_with = CYN
					junior_union_with = SHU
				}
			}
		}
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = "jingnan.7a"
		remove_country_modifier = korea_under_reform
		clr_country_flag = jingnan
		if = {
			limit = {
				owner = { tag = MNG }
			}
			CYN = {
				inherit = MNG
				change_tag = MNG
				add_prestige = 25
				MNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CEN }
			}
			CYN = {
				inherit = CEN
				change_tag = CEN
				add_prestige = 25
				CEN = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DAA }
			}
			CYN = {
				inherit = DAA
				change_tag = DAA
				add_prestige = 25
				DAA = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DSH }
			}
			CYN = {
				inherit = DSH
				change_tag = DSH
				add_prestige = 25
				DSH = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DXI }
			}
			CYN = {
				inherit = DXI
				change_tag = DXI
				add_prestige = 25
				DXI = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = JIN }
			}
			CYN = {
				inherit = JIN
				change_tag = JIN
				add_prestige = 25
				JIN = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = LNG }
			}
			CYN = {
				inherit = LNG
				change_tag = LNG
				add_prestige = 25
				LNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CLI }
			}
			CYN = {
				inherit = CLI
				change_tag = CLI
				add_prestige = 25
				CLI = { set_country_flag = jingnan_won }
			}
		}
		#		if = {
		#			limit = {
		#				owner = { tag = CMN }
		#			}
		#			CYN = {
		#				inherit = CMN
		#				change_tag = CMN
		#				add_prestige = 25
		#				CMN = { set_country_flag = jingnan_won }
		#			}
		#		}
		if = {
			limit = {
				owner = { tag = CNG }
			}
			CYN = {
				inherit = CNG
				change_tag = CNG
				add_prestige = 25
				CNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = QII }
			}
			CYN = {
				inherit = QII
				change_tag = QII
				add_prestige = 25
				QII = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = QNG }
			}
			CYN = {
				inherit = QNG
				change_tag = QNG
				add_prestige = 25
				QNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = SHU }
			}
			CYN = {
				inherit = SHU
				change_tag = SHU
				add_prestige = 25
				SHU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = SNG }
			}
			CYN = {
				inherit = SNG
				change_tag = SNG
				add_prestige = 25
				SNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = WUU }
			}
			CYN = {
				inherit = WUU
				change_tag = WUU
				add_prestige = 25
				WUU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = XNG }
			}
			CYN = {
				inherit = XNG
				change_tag = XNG
				add_prestige = 25
				XNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CYU }
			}
			CYN = {
				inherit = CYU
				change_tag = CYU
				add_prestige = 25
				CYU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = YUE }
			}
			CYN = {
				inherit = YUE
				change_tag = YUE
				add_prestige = 25
				YUE = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = ZOU }
			}
			CYN = {
				inherit = ZOU
				change_tag = ZOU
				add_prestige = 25
				ZOU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = XIA }
			}
			CYN = {
				inherit = XIA
				change_tag = XIA
				add_prestige = 25
				XIA = { set_country_flag = jingnan_won }
			}
		}
	}
}

# Shu

province_event = {
	id = "jingnan.8"
	
	title = "jingnan.8.t"
	desc = "jingnan.8.d"
	
	
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owner = {
			has_country_flag = jingnan
			tag = MNG
		}
		is_capital = yes
		controlled_by = SHU
	}
	
	mean_time_to_happen = { months = 1 }
	
	
	option = {
		name = "jingnan.8a"
		remove_country_modifier = korea_under_reform
		clr_country_flag = jingnan
		set_country_flag = jingnan_won
		if = {
			limit = {
				owner = { tag = MNG }
			}
			SHU = {
				inherit = MNG
				change_tag = MNG
				add_prestige = 25
				MNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CEN }
			}
			SHU = {
				inherit = CEN
				change_tag = CEN
				add_prestige = 25
				CEN = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DAA }
			}
			SHU = {
				inherit = DAA
				change_tag = DAA
				add_prestige = 25
				DAA = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DSH }
			}
			SHU = {
				inherit = DSH
				change_tag = DSH
				add_prestige = 25
				DSH = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = DXI }
			}
			SHU = {
				inherit = DXI
				change_tag = DXI
				add_prestige = 25
				DXI = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = JIN }
			}
			SHU = {
				inherit = JIN
				change_tag = JIN
				add_prestige = 25
				JIN = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = LNG }
			}
			SHU = {
				inherit = LNG
				change_tag = LNG
				add_prestige = 25
				LNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CLI }
			}
			SHU = {
				inherit = CLI
				change_tag = CLI
				add_prestige = 25
				CLI = { set_country_flag = jingnan_won }
			}
		}
		#		if = {
		#			limit = {
		#				owner = { tag = CMN }
		#			}
		#			SHU = {
		#				inherit = CMN
		#				change_tag = CMN
		#				add_prestige = 25
		#				CMN = { set_country_flag = jingnan_won }
		#			}
		#		}
		if = {
			limit = {
				owner = { tag = CNG }
			}
			SHU = {
				inherit = CNG
				change_tag = CNG
				add_prestige = 25
				CNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = QII }
			}
			SHU = {
				inherit = QII
				change_tag = QII
				add_prestige = 25
				QII = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = QNG }
			}
			SHU = {
				inherit = QNG
				change_tag = QNG
				add_prestige = 25
				QNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = SHU }
			}
			SHU = {
				inherit = CYN
				change_tag = CYN
				add_prestige = 25
				CYN = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = SNG }
			}
			SHU = {
				inherit = SNG
				change_tag = SNG
				add_prestige = 25
				SNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = WUU }
			}
			SHU = {
				inherit = WUU
				change_tag = WUU
				add_prestige = 25
				WUU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = XNG }
			}
			SHU = {
				inherit = XNG
				change_tag = XNG
				add_prestige = 25
				XNG = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = CYU }
			}
			SHU = {
				inherit = CYU
				change_tag = CYU
				add_prestige = 25
				CYU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = YUE }
			}
			SHU = {
				inherit = YUE
				change_tag = YUE
				add_prestige = 25
				YUE = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = ZOU }
			}
			SHU = {
				inherit = ZOU
				change_tag = ZOU
				add_prestige = 25
				ZOU = { set_country_flag = jingnan_won }
			}
		}
		if = {
			limit = {
				owner = { tag = XIA }
			}
			SHU = {
				inherit = XIA
				change_tag = XIA
				add_prestige = 25
				XIA = { set_country_flag = jingnan_won }
			}
		}
	}
}


### The Rebellion is Crushed ###

country_event = {
	id = "jingnan.9"
	
	title = "jingnan.9.t"
	desc = "jingnan.9.d"
	
	picture = COURT_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		NOT = { has_country_flag = jingnan_lost }
		has_country_flag = uprising
		has_country_flag = jingnan
		has_country_flag = ming_territorial_princes
		is_at_war = no
		OR = {
			CYN = { exists = no }
			SHU = { exists = no }
		}
		owns = 708 # Dadu
		owns = 1337 # Chengdu
	}
	
	mean_time_to_happen = { months = 3 }
	
	option = {
		name = "jingnan.9a"
		set_country_flag = jingnan_lost
		remove_country_modifier = korea_under_reform
		add_legitimacy = 50
		add_treasury = 500
		add_stability_1 = yes
	}
}