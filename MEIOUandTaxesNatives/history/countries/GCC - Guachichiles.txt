government = native_council
mercantilism = 0.1
primary_culture = chichimecha
religion = aztec_reformed
technology_group = north_american
capital = 861

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1   = {
	monarch = {
		name = "Native Council"
		adm = 3
		dip = 3
		mil = 3
		regent = yes
	}
}
