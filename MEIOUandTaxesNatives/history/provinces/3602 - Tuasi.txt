# No previous file for Tuasi

owner = CRE
controller = CRE
add_core = CRE
is_city = yes
culture = creek
religion = totemism
capital = "Tuasi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1814.8.9  = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
	trade_goods = cotton
	is_city = yes
} # Treaty of Fort Jackson ending the Creek War
