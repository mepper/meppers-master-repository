# No previous file for Xaayda gwaay

owner = HDH
controller = HDH
add_core = HDH
is_city = yes
culture = hayda
religion = totemism
capital = "Xaayda gwaay"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 3
native_hostileness = 4
