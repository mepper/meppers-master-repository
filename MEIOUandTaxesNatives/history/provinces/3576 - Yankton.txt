# No previous file for Yankton

culture = lakota
religion = totemism
capital = "Yankton"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 30
native_ferocity = 3
native_hostileness = 4

1541.1.1 = {  } # Francisco V�squez de Coronado
1710.1.1  = {	owner = SIO
		controller = SIO
		add_core = SIO
		is_city = yes
		trade_goods = fur
		culture = dakota } #Horses cause waves of migration on the Great Plains
1743.1.1 = {  }# Pierre Gaultier de Varennes, no real settlements until the 1800's
1819.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	trade_goods = fur
	religion = protestant
	culture = american
} #Fort Atkinson
