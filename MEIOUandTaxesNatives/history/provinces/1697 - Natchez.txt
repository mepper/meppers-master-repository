# No previous file for Natchez

culture = choctaw
religion = totemism
capital = "Natchez"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1540.1.1   = {  } # Hernando de Soto
1716.1.1  = {	owner = FRA
		controller = FRA
		citysize = 250
		religion = catholic
	    	culture = francien
	    } # French settlement, Fort Rosalie
1741.1.1  = { add_core = FRA citysize = 650 trade_goods = cotton }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris, France gave up its claim to New France
1763.10.9 = {	owner = CHO
		controller = CHO
		add_core = CHO
		is_city = yes
		culture = choctaw
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1786.1.3= { owner = USA
		controller = USA
		is_city = yes
		culture = american
		religion = protestant } #Treaty of Hopewell (with the Choctaw), come under US authority
