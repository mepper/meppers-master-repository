# No previous file for Odaawaa

owner = OTW
controller = OTW
add_core = OTW
is_city = yes
culture = odawa
religion = totemism
capital = "Odaawaa"
trade_goods = fur
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 1 
native_hostileness = 6
