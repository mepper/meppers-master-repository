# No previous file for Cayuga

owner = IRO
controller = IRO
add_core = IRO
is_city = yes
culture = iroquois
religion = totemism
capital = "Cayuga"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 40
native_ferocity = 2
native_hostileness = 5

1615.1.1  = {  } # �tienne Br�l� 
1629.1.1  = {  }
1707.5.12 = {  }
1784.10.22  = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
	is_city = yes
} #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1809.10.22 = { add_core = USA }
