# No previous file for Atabachi

culture = choctaw
religion = totemism
capital = "Atabachi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 1 
native_hostileness = 6

1519.1.1   = {  } # Alonzo Alvarez de Pineda
1717.1.1   = { owner = FRA
		controller = FRA
		citysize = 500
		culture = francien
		religion = catholic } #Fort Toulouse
1732.1.1  = { add_core = FRA citysize = 1000 trade_goods = cotton }
1750.1.1   = { citysize = 6900 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris, France gave up its claim to New France
1763.10.9 = {	owner = CRE
		controller = CRE
		add_core = CRE
		is_city = yes
		culture = creek
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1814.8.9  = {	owner = USA
		controller = USA
		add_core = USA
		is_city = yes
		culture = american
		religion = protestant } #Treaty of Fort Jackson ending the Creek War
