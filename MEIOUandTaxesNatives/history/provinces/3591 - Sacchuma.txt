# No previous file for Sacchuma

owner = CHO
controller = CHO
add_core = CHO
is_city = yes
culture = choctaw
religion = totemism
capital = "Sacchuma"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 1 
native_hostileness = 6

1540.1.1   = {  } # Hernando de Soto
1786.1.3   = {
	owner = USA
	controller = USA
	culture = american
	trade_goods = cotton
	is_city = yes
	religion = protestant
} #Treaty of Hopewell (with the Choctaw), come under US authority
