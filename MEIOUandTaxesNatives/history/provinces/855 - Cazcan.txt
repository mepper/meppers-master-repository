# 855 - Cazcán

owner = CXC
controller = CXC
add_core = CXC
is_city = yes
culture = chichimecha
religion = aztec_reformed
capital = "Nochistlan"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1.0
native_size = 15
native_ferocity = 4
native_hostileness = 7


1540.1.1   = {
	
} # Francisco Vázquez de Coronado y Luján
1563.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Durango"
	citysize = 200
	culture = castillian
	religion = catholic
} # Francisco de Ibarra
1588.1.1   = {
	add_core = SPA
	citysize = 1000
}
1650.1.1   = {
	citysize = 2000
}
1700.1.1   = {
	citysize = 5000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 10000
}
1800.1.1   = {
	citysize = 20000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cordóba

