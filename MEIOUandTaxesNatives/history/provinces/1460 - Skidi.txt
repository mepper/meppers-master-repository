# No previous file for Skidi

culture = pawnee
religion = totemism
capital = "Skidi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 4
native_hostileness = 6

1760.1.1  = {	owner = PAW
		controller = PAW
		add_core = PAW
		trade_goods = fur
		is_city = yes } #Great Plain tribes spread over vast territories
