name="M&T Horsemen of the Crescent DLC Support"
path="mod/MEIOUandTaxes_crescent_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesHoC.jpg"
supported_version="1.24.*.*"
