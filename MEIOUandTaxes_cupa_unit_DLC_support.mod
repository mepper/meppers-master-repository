name="M&T Conquistadors Unit DLC Support"
path="mod/MEIOUandTaxes_cupa_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesICU.jpg"
supported_version="1.24.*.*"
