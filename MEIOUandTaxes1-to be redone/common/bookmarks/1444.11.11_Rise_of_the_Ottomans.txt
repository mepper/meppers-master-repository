bookmark =
{
	name = "RISE_OF_THE_OTTOMANS"
	desc = "RISE_OF_THE_OTTOMANS_DESC"
	date = 1444.11.11

	country = TUR
	country = CAS
	country = FRA
	country = ENG
	country = HAB
	country = SWE
	country = POR
	country = MOS
	country = VEN
	country = POL

	easy_country = CAS
	easy_country = FRA
	easy_country = TUR

	effect = {
		1 = { set_global_flag = f_game_start }
	}
}
