#For the Grand Canal

#Draught plans event
country_event = {
	id = grand_canal.1
	
	title = "grand_canal.1.t"
	desc = "grand_canal.1.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	immediate = {
		1089 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
	}
	option = {
		name = "grand_canal.1.a" #Beijing
		set_global_flag = grand_canal_beijing
		
		country_event = {
			id = grand_canal.2
			days = 90
			random = 30
		}
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 0
				NOT = { capital = 708 }
			}
		}
	}
	option = {
		name = "grand_canal.1.b" #Kaifeng
		set_global_flag = grand_canal_kaifeng
		
		country_event = {
			id = grand_canal.2
			days = 90
			random = 30
		}
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 0
				NOT = { capital = 701 }
			}
		}
	}
	option = {
		name = "grand_canal.1.d" #Luoyang
		set_global_flag = grand_canal_luoyang
		
		country_event = {
			id = grand_canal.2
			days = 90
			random = 30
		}
		ai_chance = { 
			factor = 1 
			modifier = {
				factor = 0
				NOT = { capital = 702 }
			}
		}
	}
}
country_event = {
	id = grand_canal.2
	
	title = "grand_canal.2.t"
	desc = "grand_canal.2.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.2.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		1089 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1046 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.3
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.2.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		1089 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1046 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.3
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.2.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		1089 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1046 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.3
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.3
	
	title = "grand_canal.3.t"
	desc = "grand_canal.3.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.3.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		1046 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1068 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.4
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.3.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		1046 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1068 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.4
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.3.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		1046 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		1068 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.4
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.4
	
	title = "grand_canal.4.t"
	desc = "grand_canal.4.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.4.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		1068 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2147 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.5
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.4.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		1068 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2147 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.5
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.4.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		1068 = {
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2147 = {
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.5
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.5
	
	title = "grand_canal.5.t"
	desc = "grand_canal.5.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.5.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		2147 = { #Changzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2125 = { #Zhenjiang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.6
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.5.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		2147 = { #Changzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2125 = { #Zhenjiang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.6
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.5.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		2147 = { #Changzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2125 = { #Zhenjiang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.6
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.6
	
	title = "grand_canal.6.t"
	desc = "grand_canal.6.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.6.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		2125 = { #Zhenjiang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2607 = { #Yangzhou
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.7
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.6.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		2125 = { #Zhenjiang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2607 = { #Yangzhou
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.7
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.6.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		2125 = { #Zhenjiang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2607 = { #Yangzhou
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.7
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.7
	
	title = "grand_canal.7.t"
	desc = "grand_canal.7.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.7.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		2607 = { #Yangzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		696 = { #Huai'an
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.8
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.7.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		2607 = { #Yangzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		696 = { #Huai'an
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.8
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.7.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		2607 = { #Yangzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		696 = { #Huai'an
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.8
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.8
	
	title = "grand_canal.8.t"
	desc = "grand_canal.8.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.8.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		696 = { #Huai'an
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2148 = { #Xuzhou
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.9
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.8.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		696 = { #Huai'an
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		689 = { #Fengyang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.9
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.8.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		696 = { #Huai'an
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		689 = { #Fengyang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.9
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.9
	
	title = "grand_canal.9.t"
	desc = "grand_canal.9.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.9.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		2148 = { #Xuzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		698 = { #Yanzhou
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.10
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.9.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		689 = { #Fengyang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		701 = { #Kaifeng
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.10
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.9.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		689 = { #Fengyang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		701 = { #Kaifeng
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.10
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.10
	
	title = "grand_canal.10.t"
	desc = "grand_canal.10.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.10.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		698 = { #Yanzhou
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		2491 = { #Dongchang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.11
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.10.b" #Kaifeng
		trigger = { 
			has_global_flag = grand_canal_kaifeng
		}
		701 = { #Kaifeng
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal_terminus"
				duration = -1
			}
		}
	}
	option = {
		name = "grand_canal.10.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		701 = { #Kaifeng
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		702 = { #Henan/Luoyang
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.11
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.11
	
	title = "grand_canal.11.t"
	desc = "grand_canal.11.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.11.a" #Beijing
		trigger = { 
			has_global_flag = grand_canal_beijing
		}
		2491 = { #Dongchang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		705 = { #Hejian
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.12
			days = 90
			random = 30
		}
	}
	option = {
		name = "grand_canal.11.d" #Luoyang
		trigger = { 
			has_global_flag = grand_canal_luoyang
		}
		702 = { #Henan/Luoyang
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal_terminus"
				duration = -1
			}
		}
	}
}
country_event = {
	id = grand_canal.12
	
	title = "grand_canal.12.t"
	desc = "grand_canal.12.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.12.a" #Beijing
		705 = { #Hejian
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal"
				duration = -1
			}
		}
		708 = { #Beijing
			add_permanent_province_modifier = {
				name = "grand_canal_construction"
				duration = -1
			}
		}
		add_years_of_income = -0.25
		country_event = {
			id = grand_canal.13
			days = 90
			random = 30
		}
	}
}
country_event = {
	id = grand_canal.13
	
	title = "grand_canal.13.t"
	desc = "grand_canal.13.desc"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	option = {
		name = "grand_canal.13.a" #Beijing
		708 = { #Beijing
			remove_province_modifier = grand_canal_construction
			add_permanent_province_modifier = {
				name = "grand_canal_terminus"
				duration = -1
			}
		}
	}
}
