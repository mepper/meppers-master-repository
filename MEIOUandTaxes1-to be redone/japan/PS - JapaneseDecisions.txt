country_decisions = {

	shogunate_adm_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_adm_1 }
		}
		allow = {
			adm_tech = 12
			adm_power = 250
			num_of_owned_provinces_with = {
				value = 10
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_adm_power = -250
			add_country_modifier = {
				name = shogunate_reformed_adm_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}
	
	shogunate_dip_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_dip_1 }
		}
		allow = {
			dip_tech = 12
			dip_power = 250
			num_of_owned_provinces_with = {
				value = 10
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_dip_power = -250
			add_country_modifier = {
				name = shogunate_reformed_dip_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	shogunate_mil_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_mil_1 }
		}
		allow = {
			mil_tech = 12
			mil_power = 250
			num_of_owned_provinces_with = {
				value = 10
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_mil_power = -250
			add_country_modifier = {
				name = shogunate_reformed_mil_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	shogunate_adm_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_adm_2 }
		}
		allow = {
			adm_tech = 15
			adm_power = 250
			num_of_owned_provinces_with = {
				value = 15
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_adm_power = -250
			add_country_modifier = {
				name = shogunate_reformed_adm_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}
	
	shogunate_dip_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_dip_2 }
		}
		allow = {
			dip_tech = 15
			dip_power = 250
			num_of_owned_provinces_with = {
				value = 25
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_dip_power = -250
			add_country_modifier = {
				name = shogunate_reformed_dip_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	shogunate_mil_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			#NOT = { government_rank = 6 }
			government_rank = 5
			NOT = { has_country_modifier = shogunate_reformed_mil_2 }
		}
		allow = {
			mil_tech = 15
			mil_power = 250
			num_of_owned_provinces_with = {
				value = 25
				AND = { superregion = japan_superregion NOT = { region = korea_region } }
				is_core = ROOT
			}
		}
		effect = {
			add_mil_power = -250
			add_country_modifier = {
				name = shogunate_reformed_mil_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_adm_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_adm_1 }
		}
		allow = {
			adm_power = 250
			adm_tech = 12
		}
		effect = {
			add_adm_power = -250
			add_country_modifier = {
				name = daimyo_reformed_adm_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_dip_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_dip_1 }
		}
		allow = {
			dip_power = 250
			dip_tech = 12
		}
		effect = {
			add_dip_power = -250
			add_country_modifier = {
				name = daimyo_reformed_dip_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_mil_reform_1 = {
		potential = {
			government = early_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_mil_1 }
		}
		allow = {
			mil_power = 250
			mil_tech = 12
		}
		effect = {
			add_mil_power = -250
			add_country_modifier = {
				name = daimyo_reformed_mil_1
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_adm_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_adm_2 }
		}
		allow = {
			adm_tech = 15
			adm_power = 250
		}
		effect = {
			add_adm_power = -250
			add_country_modifier = {
				name = daimyo_reformed_adm_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_dip_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_dip_2 }
		}
		allow = {
			dip_tech = 15
			dip_power = 250
		}
		effect = {
			add_dip_power = -250
			add_country_modifier = {
				name = daimyo_reformed_dip_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}

	daimyo_mil_reform_2 = {
		potential = {
			government = late_medieval_japanese_gov
			NOT = { government_rank = 5 }
			NOT = { has_country_modifier = daimyo_reformed_mil_2 }
		}
		allow = {
			mil_power = 250
			mil_tech = 15
		}
		effect = {
			add_mil_power = -250
			add_country_modifier = {
				name = daimyo_reformed_mil_2
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
		}
	}
	
	reform_shogunate = {
		major = yes
		potential = {
			government = early_modern_japanese_gov
			government_rank = 5
		}
		allow = {
			technology_group = western
		}
		effect = {
			japan_superregion = { limit = { owned_by = ROOT } remove_core = JAP add_core = JAP }
			japan_superregion = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = JAP }
			set_global_flag = westernized_japan
			change_government = despotic_monarchy
			if = {
				limit = {
					exists = JAP
					NOT = { tag = JAP }
				}
				inherit = JAP
			}
			change_tag = JAP
			every_country = {
				limit = {
					vassal_of = ROOT
					NOT = {
						tag = JAP
					}
					OR = {
						government = early_medieval_japanese_gov
						government = late_medieval_japanese_gov
						government = early_modern_japanese_gov
					}
					NOT = { government_rank = 5 }
					#ai = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	
} #End of country decisions
