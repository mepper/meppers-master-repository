early_medieval_japanese_gov = {		#Medieval Japanese governments
	monarchy = yes
	
	color = { 200 150 80 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	maintain_dynasty = yes
	
	max_states = 2
	
	rank = {
		6 = {
			global_tax_modifier = -0.3
			stability_cost_modifier = 0.3
			trade_efficiency = -0.1
			land_forcelimit_modifier = -0.25
			naval_forcelimit_modifier = -0.25			
			global_autonomy = 0.1
		} #Emperor
		5 = {
			global_tax_modifier = -0.3
			stability_cost_modifier = 0.3
			trade_efficiency = -0.1
			land_forcelimit_modifier = -0.25
			naval_forcelimit_modifier = -0.25			
			global_autonomy = 0.1
		} #Shogun
		4 = {
			army_tradition = 0.25
			stability_cost_modifier = 0.2
			trade_efficiency = -0.2
			naval_forcelimit_modifier = -0.5

			global_autonomy = 0.1
		} #Shogun contender
		3 = {
			army_tradition = 0.25
			stability_cost_modifier = 0.2
			trade_efficiency = -0.2
			naval_forcelimit_modifier = -0.5
			liberty_desire = -50

			global_autonomy = 0.1
		} #Daimyo
	}
	
}

late_medieval_japanese_gov = {	#Late Medieval Japanese governments
	monarchy = yes
	
	color = { 220 135 40 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	maintain_dynasty = yes
	
	max_states = 2
	
	rank = {
		6 = {
			global_tax_modifier = -0.1
			stability_cost_modifier = 0.1
			trade_efficiency = -0.05
						
			global_autonomy = 0.05
		} #Emperor
		5 = {
			global_tax_modifier = -0.1
			stability_cost_modifier = 0.1
			trade_efficiency = -0.05
						
			global_autonomy = 0.05
		} #Shogun
		4 = {
			army_tradition = 0.5
			global_tax_modifier = 0.1
			land_maintenance_modifier = -0.1
			land_forcelimit_modifier = 0.25
			trade_efficiency = -0.1
			naval_forcelimit_modifier = -0.25

			global_autonomy = 0.05
		} #Shogun contender
		3 = {
			army_tradition = 0.5
			global_tax_modifier = 0.1
			land_maintenance_modifier = -0.1
			land_forcelimit_modifier = 0.25
			trade_efficiency = -0.1
			naval_forcelimit_modifier = -0.25

			global_autonomy = 0.05
		} #Daimyo
	}
	
}

early_modern_japanese_gov = {		#Modern Japanese governments
	monarchy = yes
	
	color = { 240 120 5 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes nation_designer_cost = 0
	allow_vassal_war = no
	allow_vassal_alliance = no
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}	
	maintain_dynasty = yes
	
	max_states = 2
	
	rank = {
		6 = {
			global_tax_modifier = 0.1
			land_maintenance_modifier = -0.1
			merchants = 1

			global_autonomy = 0
		} #Emperor
		5 = {
			global_tax_modifier = 0.1
			land_maintenance_modifier = -0.1
			merchants = 1

			global_autonomy = 0
		} #Shogun
		4 = {
			global_tax_modifier = 0.1
			stability_cost_modifier = -0.10
			discipline = 0.05
			land_maintenance_modifier = -0.2

			global_autonomy = 0
		} #Shogun contender
		3 = {
			global_tax_modifier = 0.1
			stability_cost_modifier = -0.10
			discipline = 0.05
			land_maintenance_modifier = -0.2

			global_autonomy = 0
		} #Daimyo
	}
	
}