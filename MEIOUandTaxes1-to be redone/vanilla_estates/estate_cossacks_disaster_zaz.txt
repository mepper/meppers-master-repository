#estate_cossacks_disaster_zaz = {
#	potential = {
#		NOT = { culture_group = east_slavic }
#		owns = 300
#		NOT = { exists = ZAZ }
#		has_dlc = "The Cossacks"
#		has_estate = estate_cossacks
#		OR = {
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 60
#			}
#			is_year = 1640
#		}
#		OR = {
#			NOT = { has_country_flag = cossack_estate_triggered }
#			had_country_flag = {
#				flag = cossack_estate_triggered
#				days = 365
#			}
#		}
#	}
#
#
#	can_start = {
#		has_any_disaster = no
#		OR = {
#			AND = {
#				NOT = { is_year = 1600 }
#				estate_influence = {
#					estate = estate_cossacks
#					influence = 80
#				}
#			}
#			AND = {
#				is_year = 1600
#				NOT = { is_year = 1648 }
#				estate_influence = {
#					estate = estate_cossacks
#					influence = 60
#				}
#			}
#			AND = {
#				is_year = 1648
#				estate_influence = {
#					estate = estate_cossacks
#					influence = 30
#				}
#			}
#		}
#	}
#	
#	can_stop = {
#		always = no
#	}
#	
#	progress = {
#		modifier = {
#			factor = 5
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 80
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_cossacks
#						influence = 85
#					}			
#				}			
#			}
#		}
#		modifier = {
#			factor = 7
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 85
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_cossacks
#						influence = 90
#					}			
#				}			
#			}
#		}
#		modifier = {
#			factor = 8
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 90
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_cossacks
#						influence = 95
#					}			
#				}			
#			}
#		}	
#		modifier = {
#			factor = 9
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 95
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_cossacks
#						influence = 100
#					}			
#				}			
#			}
#		}	
#		modifier = {
#			factor = 10
#			estate_influence = {
#				estate = estate_cossacks
#				influence = 100
#			}
#		}	
#		modifier = {
#			factor = 5
#			NOT = { accepted_culture = ukrainian }
#		}	
#		modifier = {
#			factor = 5
#			NOT = { accepted_culture = ruthenian }
#		}
#	}
#	
#	can_end = {
#		always = no
#	}
#	
#	modifier = {
#		prestige_decay = 0.05
#	}
#
#	on_start = estate_disasters.11
#	on_end = estate_disasters.14
#	
#	on_monthly = {
#	}
#}
