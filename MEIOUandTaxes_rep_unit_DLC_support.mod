name="M&T Indian Subcontinent and Indian Ships DLCs Support"
path="mod/MEIOUandTaxes_rep_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesRP.jpg"
supported_version="1.24.*.*"
